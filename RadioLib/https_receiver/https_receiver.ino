/*
  https_receiver.ino file using RadioLib library
  The RadioLib library has been customized to be able to use the HTTPClient native for ESP32 available by Espressif

  Containing all the code to receive datas picked up by the sensor and send them to the server via HTTPS requests
  Send data via LoRa to the end nodes (reconfiguration, ok messages)
  This microcontroller always listen for LoRa packet


  Can send : ok and ok_with_configuration
  Can receive : datas and ok_for_reconfiguration
  Can send HTTPS POST and PUT requests and receive the response from the server

  LoRa codes :
    - DATAS => 1 (To identify a message with datas inside)
    - OK => 2 (The main node and the server has well received the data + No reconfiguration)
    - OK_AND_CONF => 3 (The main node and the server has well received the data + With reconfiguration)
    - RECONF_OK => 4 (Reconfiguration on the end node is good => Tell the server that he can update its database)

  Payload codes :
    - 1 => The transaction with the server went well
    - -1 => The transaction with the server went wrong (cannot reach the server)

  Informations are sent by LoRa by using one element:
    -body => All informations for address identification (sender and recipient address), id of the message (sending datas or ok_reconf), the payload (if the transaction of the server is good)
             and the message sent by the server in JSON

  Infomations are received by LoRa using one element:
    - body => All informations for address identification (sender and recipient address), for the processing (messageID) using the JSON format, for the cycle identification 
              and the id of the msg for the aggregation

   SD codes :
    - -1 => Code got when we cannot read the certificate present on the SD card
    - certificate => Value of the certificate when we have well read the certificate present on the SD card

  @version : 0.7
*/

// Librairies included
#include <SPI.h>
#include <RadioLib.h> // Custom RadioLib
#include <WiFi.h>
#include <Arduino_JSON.h>
#include <HTTPClient.h>
#include <WiFiClientSecure.h>
#include "SD_MMC.h"


// Definition of the pins to use
#define NSS 5
#define RST -1 // If used => 14 ; If not used => -1
#define DIO0 4


// Define the messages ID for the acknowledgment messages
#define DATAS 1
#define OK 2
#define OK_AND_CONF 3
#define RECONFIG_OK 4


// WiFi identification
// #define SSID "AND"
// #define PASSWORD "@nd1sH3re" // Si pas H alors h

#define SSID "SFR-e068"
#define PASSWORD "DXGZZD3KIM1J"


// Server identification part (URL, PORT, complete addr)
#define URL "192.168.0.18"
#define PORT 3001
#define SERVER_ADDR "https://" + String(URL) + ":" + String(PORT)
#define ROUTE_MICROCONTROLLER "/api/microcontroller"
#define ROUTE_DATAS "/api/datas"
#define APIKEY "5D0BENX-E6BMWJ2-Q4A93T3-B16KW47"


// Adress
const byte local_address = 0xF3; // Device


// Creation of the LoRa module to receive datas
SX1278 radio = new Module(NSS, DIO0, RST); // spi chip select => NSS ; interrupt pin => DIO0 ; reset pin => RST


// Flags to know if we have received a message or not
volatile bool received_flag = false; // Flag to indicate that a packet was received
volatile bool enable_interrupt = true; // Disable interrupt when it's not needed


// Restart LoRa and WiFi library => To avoid freeze problems (precisely automatic idle problems)
// => This appears more time in Arduino_LoRa => Do it here to be sure
long last_update_time = 0; // Last time we restart the LoRa module
int interval = 300000; // Interval to restart the LoRa module (5 minutes)
bool is_in_conversation = false; // To know if we are in conversation or not


// Definition of paths for the microSD card
#define PATH_TO_FILE "/certificate.txt" // Path to the certificate
#define SDCARD "/sdcard" // Path to go on the sd card


// Declaration of the main certificate if we cannot read the SD card
const char* main_certificate = \
                               "-----BEGIN CERTIFICATE-----\n" \
                               "MIICljCCAX4CCQCQC92mluy4OTANBgkqhkiG9w0BAQUFADANMQswCQYDVQQGEwJm\n" \
                               "cjAeFw0yMTA0MTYxMjE1MDBaFw00ODA4MzExMjE1MDBaMA0xCzAJBgNVBAYTAmZy\n" \
                               "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3NsAtNfCZzBCGKxvhXMn\n" \
                               "gvveL0C2kc+Q+htYUaAdufWudVk4qZ8wTn2tPHOCcg8RcN8z6TLs/CBNRGgAs3rN\n" \
                               "1won+c2rCzWb1yXIhwwHyjzx1pR/NzYZC/GumaiSPwOJbUIP8V4lhobOcN7EOa4x\n" \
                               "07OMeu+OLPwD3AnYuo8pHqfQZJ6djLNWDlnabjP2UTaiqCKcQK9XDV1cou3xe2lJ\n" \
                               "PZPMHXpCXOXphdrdaWyQ62ZOTuDBH3QYPHXMPZ7voJDdwBcfV62sl2XcW6FagJhl\n" \
                               "5koejdp2aSdWpbHRHi0iNns76oEn/81r32aYt57ZOKAjwa4mUQYjn2KcF4EHGJ83\n" \
                               "VwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQAjDVIu8n9ZoL0sh9pTNLFN753kIyIZ\n" \
                               "NavbaJGEBRHcvZhkhVLfcM2zQM74S4ruGT/cii/yrZm39sFcbTJwTOVPh2mPQl82\n" \
                               "SCwdS6XquAm4NaeYRpyeZ21NJz3qBmqBq+PD3LQXffhlsqX5Oh7d78jTqvGz986f\n" \
                               "euEjNY9fFnHQlL+hf1bWFJWlBlSFnuCcdUurt9ssl5oh4UOoTTMzsGFQe8BGJClK\n" \
                               "Gazyqjkrymaw2yDcO0rd5+Ask8hqJXV8P99VwLiIP3FKuNkaY1E81V6sCMfxxsRZ\n" \
                               "t9B/2EH7MN0ACHOMfJCI0ZVgOK/7Co6hec0rBA9+la+9p0F/gk308ceE\n" \
                               "-----END CERTIFICATE-----\n";


/**
   Set up of the variables and the modules
*/
void setup() {
  Serial.begin(115200); // Serial initialization
  while (!Serial);


  // Start the LoRa module
  int state = radio.begin(); // freq : 433 ; bw : 125 ; sf : 9 ; cr : 7 ; syncWord : SX127X_SYNC_WORD ; txPower : 10 ; preambleLength : 8 ; gain : 0


  // Check if all is allright during the initialization of the module
  if (state == ERR_NONE) {
    
    // Set the coding rate to 5
    if (radio.setCodingRate(5) == ERR_INVALID_CODING_RATE) {
      Serial.println(F("Selected coding rate is invalid for this module!"));
      while (true);
    }

    // Set spreading factor to 10
    if (radio.setSpreadingFactor(10) == ERR_INVALID_SPREADING_FACTOR) {
      Serial.println(F("Selected spreading factor is invalid for this module!"));
      while (true);
    }

    // Set the TX power to 17
    if (radio.setOutputPower(17) == ERR_INVALID_OUTPUT_POWER) {
      Serial.println(F("Selected output power is invalid for this module!"));
      while (true);
    }
    
    Serial.println("LoRa init succeeded.");

  } else { // Print the error code
    Serial.print("failed, code ");
    Serial.println(state);
    while (true);
  }


  // Connection to the WiFi with the credentials put above
  WiFi.begin(SSID, PASSWORD);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }


  // Connection OK and get local ip address
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());


  // Set the action to do when we receive a packet and start to receive a packet
  radio.setDio0Action(setFlag); // Set the function that will be called when new packet is received
  Serial.print(F("[SX1278] Starting to listen ... "));   // Start listening for LoRa packets
  state = radio.startReceive(); // Start to receive messages


  // Check if no error when we launch the receiving messages
  if (state == ERR_NONE) {
    Serial.println(F("success!"));

  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }


  Serial.println("The initialization is OK");
}


/**
   Main program
*/
void loop() {

  // Check if the flag is set
  if (received_flag) {
    enable_interrupt = false; // Disable the interrupt service routine while processing the datas
    received_flag = false; // Reset flag


    String str; // To store our received message
    int state = radio.readData(str); // Read datas


    // Check if we have no error during the reception of the messages
    if (state == ERR_NONE) {

      // Check if the message is my message
      if (str.equals("")) {
        setVariablesForReception();
        return;
      }


      // Transform the message received in JSON format
      JSONVar packet = JSON.parse(str);
      Serial.println(str); // Print the packet for debug


      // Test if the decoding is good or not => If not print an error_message and quit
      if (JSON.typeof(packet) == "undefined") { // Not good decoding
        Serial.println("Parse of the message failed");
        setVariablesForReception();
        return;
      }


      // If the LoRa packet doesn't contain the receiver address and the sender of the message
      if (!packet.hasOwnProperty("to") || (!packet.hasOwnProperty("AM") && !packet.hasOwnProperty("A") && !packet.hasOwnProperty("from"))) { // addrMicrocontroller for DATAS and addr for RECONFIGURATION
        Serial.println("Packet not contains the receiver or the sender address");
        setVariablesForReception();
        return;
      }


      // Test if the message is not for me
      if (!String(local_address, HEX).equals(String((const char*)packet["to"]))) {
        Serial.println("This message is not for me.");
        setVariablesForReception();
        return;
      }


      // Need to declare the variables from the server processing outside of the switch
      String response_server; // Response of the server
      JSONVar response_server_JSON; // Response of the server formatted in JSON
      String certificate = readCertificate(); // Read the certificate from the SD card


      // Check the value of the message ID and run the appropriate structure
      switch ((int)packet["mID"]) {

        // For a classical acknowledgment message => Do nothing
        case DATAS:
          Serial.println("SEND DATA TO THE SERVER");


          // If we cannot read the certificate => Use the main certificate ; use the reading one otherwise
          if (certificate.equals("-1")) {
            response_server = httpsRequest(SERVER_ADDR, ROUTE_DATAS, str, "POST", main_certificate); // Make the request on the server => POST via the hard-stored certificate
          } else {
            response_server = httpsRequest(SERVER_ADDR, ROUTE_DATAS, str, "POST", certificate); // Make the request on the server => POST via the certificate got from SD card
          }

          response_server_JSON = JSON.parse(response_server); // Transform the response to JSON format


          // Test the parsing
          if (JSON.typeof(response_server_JSON) == "undefined") {
            Serial.println("Problem during the parsing of the payload sent by the server"); // Problem

          } else {

            // No reconfiguration => Send an OK message to the sender
            if ((bool)response_server_JSON["HCC"] == false) {
              Serial.println("OK message");

              String message;

              // OK message with a relay
              if (packet.hasOwnProperty("from")) {
                 message = toJSON(OK, String((const char*)packet["from"]),  response_server_JSON, response_server, String((const char*)packet["cy"])); // addrMicrocontroller for DATAS
              
              // OK messgae without a relay
              } else {
                 message = toJSON(OK, String((const char*)packet["AM"]),  response_server_JSON, response_server, String((const char*)packet["cy"])); // addrMicrocontroller for DATAS
              }
             
              Serial.println(message);
              sendMessage(message); // Send the message

            } else { // A reconfiguration is present => Send a reconfiguration message
              Serial.println("Send a reconfiguration message");

              String message;

              // OK_AND_CONF message with a relay
              if (packet.hasOwnProperty("from")) {
                 message = toJSON(OK_AND_CONF, String((const char*)packet["from"]),  response_server_JSON, response_server,  String((const char*)packet["cy"])); // addrMicrocontroller for DATAS
              
              // OK_AND_CONF message without a relay
              } else {
                 message = toJSON(OK_AND_CONF, String((const char*)packet["AM"]),  response_server_JSON, response_server,  String((const char*)packet["cy"])); // addrMicrocontroller for DATAS
              }

              
              Serial.println(message);
              sendMessage(message); // Send the message
            }
          }

          break;


        // For an acknowledgment message with a reconfiguration of time to sleep
        case RECONFIG_OK:
          Serial.println("Send that reconfiguration is ok to the server");

          if (certificate.equals("-1")) {
            response_server = httpsRequest(SERVER_ADDR, ROUTE_MICROCONTROLLER, str, "PUT", main_certificate); // Make the request on the server => PUT via the hard-stored certificate
          } else {
            response_server = httpsRequest(SERVER_ADDR, ROUTE_MICROCONTROLLER, str, "PUT", certificate); // Make the request on the server => PUT via the certificate got from SD card
          }

          response_server_JSON = JSON.parse(response_server); // Transform the response to JSON format

          // Test the parsing
          if (JSON.typeof(response_server_JSON) == "undefined") {
            Serial.println("Problem during the parsing of the payload sent by the server"); // Problem

          } else {

            // RECONFIG_OK message with a relay
            if (packet.hasOwnProperty("from")) {
               sendMessage(toJSON(OK, String((const char*)packet["from"]), response_server_JSON, response_server,  String((const char*)packet["cy"]))); // addr for RECONF_OK
            
            // RECONFIG_OK message without a relay
            } else {
              sendMessage(toJSON(OK, String((const char*)packet["A"]), response_server_JSON, response_server,  String((const char*)packet["cy"]))); // addr for RECONF_OK
            }
          }

          break;
      }

    } else if (state == ERR_CRC_MISMATCH) { // Pakcet received but malformed
      Serial.println(F("[SX1278] CRC error!"));

    } else { // Another error occured
      Serial.print(F("[SX1278] Failed, code "));
      Serial.println(state);
    }


    // Reset the variables for reception of new messages
    setVariablesForReception();
  }



  // Restart the LoRa module to avoid automatic idle mode
  if (millis() - last_update_time > interval && is_in_conversation != true) {
    Serial.println();


    // LoRa connection reset
    Serial.println("Restart the LoRa module");
    radio = new Module(NSS, DIO0, RST); // Reset of the module
    int state = radio.begin(); // freq : 433 ; bw : 125 ; sf : 9 ; cr : 7 ; syncWord : SX127X_SYNC_WORD ; txPower : 10 ; preambleLength : 8 ; gain : 0 => Restart the module

    if (state == ERR_NONE) { // Check if all is allright during the re-initialization
     
      // Set the coding rate to 5
      if (radio.setCodingRate(5) == ERR_INVALID_CODING_RATE) {
        Serial.println(F("Selected coding rate is invalid for this module!"));
        while (true);
      }
  
      // Set spreading factor to 10
      if (radio.setSpreadingFactor(10) == ERR_INVALID_SPREADING_FACTOR) {
        Serial.println(F("Selected spreading factor is invalid for this module!"));
        while (true);
      }
  
      // Set the TX power to 17
      if (radio.setOutputPower(17) == ERR_INVALID_OUTPUT_POWER) {
        Serial.println(F("Selected output power is invalid for this module!"));
        while (true);
      }
    
      radio.setDio0Action(setFlag); // Set the function that will be called when new packet is received
      state = radio.startReceive(); // Start to receive messages

      if (state == ERR_NONE) { // Check if all is allright when we launch the startReceive method
        Serial.println("LoRa module restart is ok");
        Serial.println();
      }
    }


    // WiFi connection reset
    Serial.println("Restart the WiFi");
    WiFi.disconnect(); // Disconnect the WiFi
    WiFi.begin(SSID, PASSWORD); // Re-connection to the WiFi
    Serial.println("Re-connection to the WiFi");
    while (WiFi.status() != WL_CONNECTED) { // Verify the status
      delay(500);
      Serial.print(".");
    }
    Serial.println("The WiFi has well restarted");
    Serial.println();

    last_update_time = millis(); // Reupdate the time
  }
}


/**
   Function used to retrieve the certificate from the SD card
   @return the certificate if we can read it ; -1 otherwise
*/
String readCertificate() {
  // SD card initialization
  if (!SD_MMC.begin(SDCARD, true)) { // Uses the pins number : 15, 14 and 2
    Serial.println("Cannot connect to the SD card");
    return "-1";
  }


  // Open the file to read
  File certificate = SD_MMC.open(PATH_TO_FILE, FILE_READ);


  // Check if we have well opened the file
  if (!certificate) {
    Serial.println("Opening file to read failed");
    return "-1";
  }


  String certificate_to_return = ""; // Value of the certificate read from the SD card


  // Read the certificate char by char
  while (certificate.available()) {
    char value_readed = certificate.read(); // Read char by char because ascii code is equals to value when we use char
    certificate_to_return += String(value_readed); // Add char to the certificate
  }


  certificate.close(); // Close the file to read
  SD_MMC.end(); // Close the library


  return certificate_to_return;
}


/**
  Function used to send an HTTPS request to the server
  @param server => Address of the server (Example : https://localhost:12345)
  @param route => Route to have access to the API (/api/datas for example)
  @param json => JSON to send to the server
  @param type_of_request => Request type to do (GET, POST, PUT, …)
  @param certificate => Certificate of the server to communicate with
  @return json send by the server if all is good ; -1 otherwise
*/
String httpsRequest(String server, String route, String json, String type_of_request, String certificate) {
  // Verify the WiFi connection to be able to well transmit the datas
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.disconnect(); // Disconnect the WiFi
    WiFi.begin(SSID, PASSWORD); // Reconnect the WiFi
  }

  WiFiClientSecure *client = new WiFiClientSecure; // Client for the secure transaction

  // If the creation of the client is good
  if (client) {
    client -> setCACert(certificate.c_str()); // Set the certificate

    { // Add a scoping block for HTTPClient https to make sure it is destroyed before WiFiClientSecure *client is


      HTTPClient https; // HTTPS client
      https.begin(server + route); // Begin the HTTPS request with the address of the server given
      https.addHeader("Content-Type", "application/json"); // Allow to send json
      https.addHeader("apikey", APIKEY);


      int https_response_code; // Response code we will have when we will make the request


      // According to the type of request passed in parameter
      if (type_of_request.equals("GET")) { // GET method
        https_response_code = https.GET();

      } else if (type_of_request.equals("POST")) { // POST method
        https_response_code = https.POST(json);

      } else if (type_of_request.equals("PUT")) { // PUT method
        https_response_code = https.PUT(json);

      } else { // Others
        https_response_code = -1;
      }

      if (https_response_code > 0 && (https_response_code == 201 || https_response_code == 200)) { // no error =>
        Serial.print("HTTPS response code : ");
        Serial.println(https_response_code);

        return https.getString();

      } else { // Print error code
        Serial.print("Error code : ");
        Serial.println(https_response_code);
        Serial.println(https.getString());

        return "-1";
      }


      // Free the resources
      https.end();


    } // End extra scoping block


    delete client; // Destroy the client

  } else {
    Serial.println("Unable to create client");
    return "-1";
  }
}


/**
   Function to send a message to a recipient
   @param message => Message to send to the receiver
*/
void sendMessage(String message) {
  int transmissionState = radio.transmit(message); // Transmit the message


  // Check if problems appeared during the transmission of the message
  if (transmissionState != ERR_NONE) {
    Serial.print("Problems appeared during the transmission of the message. Code : ");
    Serial.println(transmissionState);
  }
}


/**
   Function used to formate the message to send into the JSON format
   It can formate messages for sending that all is OK or to say that the reconfiguration is good
   @param message_id => ID of the message for the future processing
   @param destination => Recipient of the message
   @param my_json => JSON send by the server that we modify to add recipient and sender addresses, the payload (transaction good or not ?), the id of the message and the cycle identifier
   @param state_request_server => String that contains the response of the server (-1 if not reachable ; body otherwise)
   @param cycle_identifier => Identifier of the cycle of the end node
   @return => datas to send to the end node (for ok message or for reconfiguration) into the string format
*/
String toJSON(int message_id, String destination, JSONVar my_json, String state_request_server, String cycle_identifier) {
  // If problem with server => Return payload not ok
  if (state_request_server.equals("-1")) {
    return " { \"from\": \"" + String(local_address, HEX) + "\", \"to\": \"" + destination + "\", \"TSS\": \"" + state_request_server + "\", \"cy\": \"" + cycle_identifier + "\" } ";
  }


  // Write informations in the body of the message
  my_json["from"] = String(local_address, HEX); // Add my device address
  my_json["to"] = destination; // Add the recipient of the message
  my_json["mID"] = message_id; // Add the message id for the future processing
  my_json["TSS"] = 1; // State of the transaction with the server
  my_json["cy"] = cycle_identifier; // Add the cycle identifier of the end node


  return JSON.stringify(my_json); // Transform the JSON to string
}


/**
   This function is called when a complete packet is received by the module
   IMPORTANT : this function MUST be 'void' type and MUST NOT have any arguments!
   IMPORTANT : THIS FUNCTION NEEDS TO BE PRESENT FOR THE MESSAGE RECEIVING
   @param NO ARGUMENTS
*/
void setFlag(void) {
  // Check if the interrupt is enabled
  if (!enable_interrupt) {
    return;
  }

  received_flag = true; // We got a packet, set the flag
}


/**
   Function to set the variables to receive new messages
*/
void setVariablesForReception() {
  radio.startReceive();// Put the module in listen mode
  enable_interrupt = true; // Enable interrupt service routine to receive more packets
}
