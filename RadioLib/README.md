# Main code microcontroller V0.7 with RadioLib

## Card and modules
We are using ESP32 TTGO T8 V1.7.1 for all the nodes (end and main ones). An end node will be a node that will capture all the datas (ph, humidity, temperature, conductivity, nitrogen, phosphorus and potassium) to send to the main node which will be in charge to communicate with the server via HTTPS.

Here is the configuration of our nodes :
> - Main node <br/>
> This is the most simplest one. It is composed of an ESP32 TTGO T8 V1.7.1 with a LoRa SX1278 433MHz module plugged in VSPI (pins 5 => NSS, 18 => SCK, 19 => MISO, 23 => MOSI and 4 => DIO0) and the SD card module integrated to the board to be able to read the server's certificate (more better to read it on the SD than in the memory because we can change it in the SD without problems) to send datas via the HTTPS protocol.
> ![alt text](../README_images/configuration_main_node.png "Configuration of a main node")
> - End node <br/>
> The end node is a little more complex. It is composed of an ESP32 TTGO T8 V1.7.1 with a LoRa SX1278 433MHz module plugged in VSPI (pins 5 => NSS, 18 => SCK, 19 => MISO, 23 => MOSI and 4 => DIO0), a SD card module integrated to the board to be able to store, upload or see the datas the microcontroller has taken from the JXBS-3001-TR sensor which is a soil multi-parameter sensor. It uses the ModBus protocol to transmit datas. To use this protocol, we have plugged a RS485 board which is connected to the ESP32 as follow : pin 25 for the RX (read) and pin 26 for the TX (transmit).
> ![alt text](../README_images/configuration_end_node.png "Configuration of an end node")
> - Relay node <br/>
> It is composed of an ESP32 TTGO T8 V1.7.1 with a LoRa RA-02 SX1278 433 MHz plugged in VSPI (pins 5 => NSS, 18 => SCK, 19 => MISO, 23 => MOSI and 4 => DIO0). It will be in charge to receive and forward the data from the end node to the main one and vice-versa
> ![alt text](../README_images/configuration_main_node.png "Configuration of a relay node")

<br/>

## Files
### sender.ino
sender.ino will contain the code which allows the microcontroller to capture datas like ph, humidity, temperature, conductivity, nitrogen, phosphorus and potassium and send these datas via LoRa to the main node. We will also store these datas in the SD card if it is available. The end nodes have the ability to reconfigure themselves in term of wake-up times. **WE STORE ALSO THE MESSAGE ID TO SEND TO THE SERVER IN A FILE NAMED idMsg.txt ON THE SD CARD. DON'T DELETE OR WRITE THE FILE BECAUSE THAT CAN CAUSE DAMAGE TO THE DATABASE WHEN YOU WILL MAKE AN UPLOAD OF THE DATAS ON THE SERVER.**

### receiver.ino
receiver.ino will contain the code which allows the microcontroller to exchange datas with the server, via HTTP, and then send the response of the server to the end nodes.

### https_receiver.ino
https_receiver.ino will contain the code which allows the microcontroller to exchange datas with the server via HTTPS, and then send the response of the server to the end nodes. To be able to make requests in HTTPS, we need to have the certificate's value of the server to put in the microcontroller's code (just if we cannot read the one from the SD card) and into the SD card to plug on the ESP32 (in the certificate.txt file). The certificate read from the SD card will prevail on the hard-stored certificate present in the ESP32 memory.

### relay.ino
relay.ino will contain the code which allows the microcontroller to receive and forward the packet to the right microcontroller.

<br/>

## Librairies used
### [SPI](https://github.com/PaulStoffregen/SPI)
```arduino
#include <SPI.h>
```
SPI.h will help us to manage the SPI protocols and to give access to the different SPI interfaces like HSPI or VSPI.


### [RadioLib](https://github.com/jgromes/RadioLib)
```arduino
#include <RadioLib.h>
```
RadioLib.h will help us to exchange datas between the end and the main nodes. It is very interesting to use LoRa to send datas because it is low power consuming and the messages can go through multiple kilometers in good conditions (good antenna parameters, open environment vs urban/forest environment, etc …).

***IMPORTANT : The RadioLib library we use is based on the RadioLib from jgromes but we have modified some files to optimized the processing we will do on the main node. The main files we have modified are the following :***
> - ***src/protocols/HTTP/HTTP.cpp***
> - ***src/protocols/HTTP/HTTP.h***

***In fact, we have modified the name of the class. So we have changed 'HTTPClient' by 'HTTPClient2' to not override the class given by the native HTTPClient library from Espressif for the ESP32.***

***IF YOU DON'T WANT TO HACK THE LIBRARY BY YOURSELF, THE RADIOLIB CUSTOM LIBRARY CAN BE DONWLOADED AT THIS ADDRESS :*** [https://github.com/Starburste/Ecopolis/blob/main/Custom%20Librairies/RadioLib.zip](https://github.com/Starburste/Ecopolis/blob/main/Custom%20Librairies/RadioLib.zip)


### [WiFi, WiFiClientSecure and HTTPClient](https://github.com/espressif/arduino-esp32)
```arduino
#include <WiFi.h>
#include <HTTPClient.h>
#include <WiFiClientSecure.h>
```
These librairies are used by the main node to exchange and send datas to the server. It can be used to send datas or update the state of the end nodes. HTTPClient.h is a native library given by Espressif for the ESP32 which can be used with the Arduino IDE. To add datas or update the state of the microcontroller, we need an API key which is given by the server when we download the code of the main microcontroller. The WiFiClientSecure library is only used in HTTPS code. This library helps us to put the certificate's value in the request to be able to make a secure transaction (an HTTPS request) between the end node ESP32 and the server.

***IMPORTANT : You need to modify the RadioLib library from jgromes to be able to include the native HTTPClient library for ESP32 from Espressif. If you don't modify the RadioLib library, you will not be able to include the HTTPClient library from Espressif***


### [Arduino_JSON](https://github.com/arduino-libraries/Arduino_JSON)
```arduino
#include <Arduino_JSON.h>
```
Arduino_JSON.h allows us to transform a string into the JSON format. It will be very usefull to know the recipient or the sender of the message for example. In contrary of Arduino_LoRa, the message we sent is not structured like "packet header" and body, we just have the body. So, all the datas are sent by JSON in the body of the message.


### [SD_MMC and FS](https://github.com/espressif/arduino-esp32)
```arduino
#include <SD_MMC.h>
#include <FS.h>
```
These librairies are used to write datas on the SD card. FS will help us to append the datas in the file and SD_MMC will be used to open a connection with the SD card. SD_MMC is also used to read datas from the SD card to retrieve the server's certificate to be able to make HTTPS requests.


### [ModbusMaster](https://github.com/craftmetrics/esp32-modbusmaster)
```arduino
#include <ModbusMaster.h>
```
This library is used to be able to retrieve the datas from the JXBS-3001-TR sensor which uses the ModBus protocol. This sensor is connected to a RS485 card which is connected to the ESP32 as you can see on the assembly pictures.

<br/>

## Life cycle of the end nodes
![alt text](../README_images/life_cycle.png "Life cycle of the end node")

<br/>

## Reconfiguration
The reconfiguration of an end node is for the sleeping time (really, this represents the waking hours of a microcontroller). Indeed, these times can be modified by the user to retrieve less or more datas. We store them into an array which will be sent when the end nodes will need to be reconfigured. The end nodes will update their array of sleep to agree with the one on the server.

In fact (with the v0.3), if we can communicate with the server, we will use the sleep time he sends to us. With that, we can have a more precise sleep time than just get a static value we have stored on the microcontrollers. If we cannot communicate with the server, here, we will use the sleep times stored on the microcontrolller (which are the same on the server). When it is the first time that we launch an ESP32, the server request will contain a value to say to the server that it is the first time the microcontroller is up so we need to reconfigure it.

The sleep times can be like this :
> - In case of a manual reconfiguration : { "HCC": true, "ST": [ 500, 600, 800 ], "NST": 400 } <br/>
> - In case of an automatic reconfiguration (generated by the server) : { "HCC": true, "V": 909, "NBV": 96, "NST": 498 } <br/>
> With : HCC for hasConfigChanged (boolean) ; V for value (integer in seconds) ; NBV for number of values (integer) ; NST for next sleep time (integer in seconds) ; ST for sleep time (array of integers in seconds)

<br/>

## Possible codes
### Treatment codes 
The treatment codes are integers exchanged between the end and the main nodes. These codes are common to all the microcontrollers and are the following : 
> - 1
>>> The message contains all the datas to store in the database of the server (end to main node)
> - 2 
>>> No actions available so end node can sleep (main to end node)
> - 3
>>> Datas are well sent to the server and a reconfiguration is available (main to end node)
> - 4
>>> Reconfiguration of the end node is good, we can store it on the server (end to main node)


### Error codes
We have also error codes available which are :
> - 1 
>>> Transaction with the server is good
> - -1
>>> Problem during the transaction with the server


### SD codes (for end nodes to put in CSV)
We have two codes available when we want to write datas in the SD card : 
> - true
>>> The datas are well stored into the SD card
> - false
>>> Problem with the SD card (connection problem, problem when we want to write in the file, …)

### SD codes (for end nodes to get the actual message ID)
We have two codes available when we want to write datas in the SD card : 
> - true
>>> The datas are well stored into the SD card
> - false
>>> Problem with the SD card (connection problem, problem when we want to write in the file, …)

### SD codes (for HTTPS main node)
We have two codes available when we want to read datas from the SD card :
> - -1
>>> The certificate's value cannot be read from the SD card => Use of the hard-stored value in the memory of the microcontroller
> - certificate's value
>>> The certificate's value has been read and returned from the SD card

### Cycle identifier
An end node make a cycle when it sends datas to the main one. So, we have put in place counters of cycle that can identify in which cycle is the end node and we treat only the messages that are received for the current cycle. The others are not taken into account.

<br/>

## Packets
Here, the packets don't use the "packet header" like in Arduino_LoRa library. That means that all the informations are stored in the body of the message, so in the JSON. We will be obliged to transform the body into a JSON to verfify all the datas. We have four types of messages (you need to keep in mind that in the first wake up of the microcontroller a variable will be set to say to the server that it is the first the microcontroller woke up => This variable just appears in the datas message) : 
> - Datas message
>>> This is the message that contains the datas captured by the end nodes. This message will contain all the datas like the recipient and the sender addresses, the message ID, the cycle identifier of the end node and all the values captured by the sensor. To be sent, all this datas will be formatted into the JSON format.

> - Reconfiguration OK message
>>> This message means that the reconfiguration of the end node is good. The message will be formatted in the JSON format and will composed like this : recipient and sender addresses, message ID and the cycle identifier.

> - OK message
>>> This message means that all is OK (server has stored the datas and we don't have reconfiguration) and the end node can sleep now. This message will be sent by the main node after its transaction with the server. This message will be in JSON and will be composed like this : recipient and sender addresses, message ID, state transaction with the server, the cycle identifier of the end node and a variable that says that we don't have reconfiguration.

> - OK + Reconfiguration message
>>> This message means that the server has stored the datas and that a reconfiguration is available for the end node. This message will be sent in JSON format and will be composed like this : recipient and server addresses, message ID, state transaction with the server, cycle identifier of the end node, a variable that says that we have a reconfiguration and the new array of sleep time.

<br/>

## LoRa configuration
The LoRa configuration is the following:
> - Bandwidth 125 KHz <br/>
> - Spreading Factor 10 <br/>
> - Coding Rate 5 <br/>

This configuration can achieve ~200m by the tests we have done. The objective is to increase the range without sacrifice the time in the air (sending/receiving).


## Execution time
The execution time will be calculated on the end nodes. The execution time is calculated from the boot of the board to the reception of the 'OK message'. The execution time can be very variable according to the transaction with the server, the time to send the messages, if we have a relay in the system, etc …. For example, if we get the acknowledgment message on the first try, the execution time will be very fast. However, if we don't receive the acknowledgment message in time, we re-send a message every 3-5 seconds (to avoid as much as possible the spam of the main node).

It can vary a lot, also, with different librairies. In the first version of the code, we have used [ArduinoHttpClient library](https://github.com/arduino-libraries/ArduinoHttpClient) but this library is not very optimized for the ESP32. The time is three to four times more than the native one given by Espressif. That's why, we decided to custom the [RadioLib library](https://github.com/jgromes/RadioLib/tree/master/examples) to be able to use the native library from Espressif which is named [HTTPClient](https://github.com/espressif/arduino-esp32).


| Case  | Execution time (Restart to the sleep of the board) => High range |
| :---------------: |:---------------:|
| **Send data with reconfiguration with a relay** | Cycle of ~11 seconds |
| **Send data without reconfiguration with a relay** | Cycle of ~5 seconds |
| **Send data with reconfiguration without a relay** | Cycle of ~7 seconds |
| **Send data without reconfiguration without a relay** | Cycle of ~3-4 seconds  |
| **Maximum time the end node can be active before sleeping** | Between 30-35 seconds (trying to send 5 messages) |


<br/>

## Power consumption
During the visualization of the power consumption on an oscilloscope, we can say that the waveform of the power consumption is always the same (See the picture below).

![alt text](../README_images/global_waveform_cycle_RadioLib.png "Global waveform of the power consumption of an end node")

In fact, for now, we can send up to 5 messages without response of the server. So, if we get the response at the first try, we will just have one peak of consumption and one waiting time for the message (we are not obliged to wait all the time, when the packet that says that all is OK is received, we can continue our treatment). If we have the SD card, we will always have the peak associated to it. If we don't have a SD card, we will not have this peak. ***We can simplify this diagram but if we do that, we will miss informations because we need to know the rectangles' width to calculate their area (the time to send a message => length of the purple rectangles) multiplied by the height (we can get easily).*** We can see that the form produced during a transmission is very different than Arduino_LoRa's one. Indeed, in Arduino_LoRa library, we have peaks that are isoceles triangles whereas here, we have rectangles. We can explain that with the formatted datas we sent. In Arduino_LoRa, we have sent the more important datas (sender and recipient addresses, message ID, cycle identifier, …) in bytes and not formatted into JSON format, so the datas to be sent have less weight and the transmission is also more quicker. So, we can also deduce that the power consumption will be higher with this library.

With that, we can deduce a complete formula (it doesn't change from Arduino_LoRa even with the different waveform) :
<br/>
<br/>

![alt text](../README_images/consumption_equation.png "Equation of the power consumption of an end node for one cycle")

<br/>

But we can express a more simpler one : 
<br/>
<br/>

![alt text](../README_images/consumption_simpler_equation.png "Equation of the power consumption of an end node for one cycle")

<br/>

So the total consumption of the ESP32 during one cycle will be the area of all our waveform. But, thanks to the equation, we can estimate the power consumption for the future cycles. We also need to take into account the power consumption of the board when it wakes up from the deep sleep.

To estimate the battery life, we need to know globally the power consumption in multiple cases. These cases are given below :
> - Deep sleep mode
> - Main cycle without reconfiguration and server problems
> - Main cycle with reconfiguration and without server problems
> - Main cycle with server connection problems

<br/>

***THESE RESULTS ARE GIVEN BY THE OSCILLOSCOPE WITH AN OVERESTIMATION OF ABOUT 8-10 mA***

<br/>

| / | AVERAGE CONSUMPTION |
| :---------------: |:---------------:|
| **Deep sleep mode (all the system without the sensor)** | 60 uA |
| **Deep sleep mode (all the system)** | 10.5-11.5 mA |
| **ESP32 in process of acquisition of the data** | 62.3 mA |
| **ESP32 in waiting** | 85 mA |
| **ESP32 by sending data with LoRa** | 100-110 mA |

<br/>

These results are generally reliable because they are calculated directly from the waveform given by the oscilloscope or by the result given by the voltmeter (in the case of the deep sleep mode). These results are the average of the consumption, per second, during the cycle of the microcontroller. So the consumption may vary according to the time passed to try to send messages, the peak of power consumption to add datas to the SD card, etc …. After multiple tests, we can assume that the board will consume between 60 and 70 mA as average consumption during a cycle (with and without reconfiguration). ***So here, the time will be a key for the global consumption of the board (more the board is powered more will be the power consumption). BUT, FOR RADIOLIB, THE OPTIMIZATION OF THE JSON WOULD BE A VERY IMPORTANT POINT FOR THE POWER CONSUMPTION TOO.*** 
> - Let's take an example : If a board has a power consumption of 70 mA for one cycle but it is powered 1 second and the other board has a power consumption of 60 mA but on two seconds for one cycle. The capacity of the battery is also a 3000 mAh. The equation to calculate the number of cycles will be : 
![alt text](../README_images/cycles_equation.png "Equation to calculate the possible number of cycles on a battery")
>> - 70 mA => bat_capacity / power_consumption * hTOs / time => 3000 / 70 * 3600 / 1 = 154285 cycles <br/>
>> - 60 mA => bat_capacity / power_consumption * hTOs / time => 3000 / 60 * 3600 / 2 = 90000 cycles <br/>
> The example shows you that it will be better to send quickly the datas and consume a little more power than send them slowly but consumes less power.

THESE RESULTS DON'T INCLUDE THE BOOT OR THE WAKE UP OF THE BOARD.

<br/>

| / | ESTIMATED LIFE TIME (Always do the / item on ***12000 mAh battery***) |
| :---------------: |:---------------:|
| **Deep sleep mode (all the system without the sensor)** | 200 000 hours => 8333 days => 22.8 years |
| **Deep sleep mode (all the system)** | 1090.9 hours => 45.4 days |
| **ESP32 in process of acquisition of the data** | 193 hours => 8 days |
| **ESP32 in waiting** | 141 hours => 5.8 days |
| **ESP32 by sending data with LoRa** | 11.4 hours |

<br/>

These results are calculated on ***3000 mAh battery***. So it is not really reliable of the final version because the batteries will have more mAh. For example, if we take the line **Deep sleep mode**, the result of this line just respresents the time that the microcontroller can be in this mode before shutting down due to battery life.


### Deep Sleep wake up
![alt text](../README_images/deep_sleep_waveform_wakeUp.png "Waveform of a wakeup for an ESP32")

A wake up from a deep sleep mode can take around 1 second. We can see that we have one rectangle and one power consumption peak. Generally, it takes less than 0.5 second to be on and to be ready to do tasks. We have multiple parts :
> - A rectangle which is the base consumption during the set up of the variables and the modules
> - A peak which corresponds to the booting of all the elements on the ESP32

With the oscilloscope, we can deduce that the part after the peak corresponds to the initialization of the LoRa RA-02 SX1278 and of the BME280 modules. The first part with the peak corresponds to the normal initialization of the ESP32. We can deduce that by deleting the initialization of a module at each step.

| TIME TO WAKE UP | AVERAGE POWER CONSUMPTION DURING A WAKE UP |
| :-------------: | :----------------------------------------: |
| 0.3 second | ≈ 43.5 mA |
| 0.4 second | ≈ 42.625 mA |

<br/>

| AVERAGE POWER CONSUMPTION DURING A WAKE UP | NUMBER OF WAKE UP ***ON 3000 mAh BATTERY*** |
| :----------------------------------------: | :---------------: |
| ≈ 43.5 mA | 827586 possible wakes up |
| ≈ 42.625 mA | 633431 possible wakes up |

<br/>

## My observations
This code is a less optimized than the Arduino_LoRa's one for multiple reasons. First of all, we can't use directly the native HTTPClient library for ESP32 given by Espressif because the name of this class is already defined into the RadioLib library. That's why, we recommand to use our custom library that we proposed because it fixes the problem. This optimization gives one second of improvement on the previous code with ArduinoHttpClient. Secondly, we can't structure the packet like Arduino_LoRa library. Indeed, in Arduino_LoRa, we can write bytes in the "packet header" and we just put in the body of the message the informations we need for the server. But, in RadioLib you can't use that. So all the informations go to the body in JSON format, so the packet is heavier to send by LoRa but also to trasmit by HTTP requests. We put in this JSON informations like sender and recipient addresses, cycle identifier of the end node, message ID and the payload (message from main node to end one). So, at each time we receive a packet, we need to deserialize it to JSON format, to test the informations and send datas according to the response of the server (also send in JSON format). These kinds of actions can cost time execution and so, power consumption of the battery of the end node (the main node will be always on and not on batteries so we don't care of this node). Basically, RadioLib is not designed for duplex communications like we try to put in place, so sometimes, we can receive our own messages we have sent. But as they contained strange datas, the deserialization failed (but the board doesn't crash). Another thing that is strange is that in Arduino_LoRa we can detect if a packet is arrive via LoRa.parsePacket() but here, we need to define an action on our interruption pin and launch a method that will modify variables to say that a packet is arrive to be able to get finally the packet.