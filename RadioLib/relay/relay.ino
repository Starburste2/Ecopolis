/*
  relay.ino file using RadioLib library

  Containing all the code to receive messages from LoRa and re-send it
  Can send and receive all types of messages from all types of nodes

  LoRa codes :
    - DATAS => 1 (To identify a message with datas inside)
    - OK => 2 (The main node and the server has well received the data + No reconfiguration)
    - OK_AND_CONF => 3 (The main node and the server has well received the data + With reconfiguration)
    - RECONF_OK => 4 (Reconfiguration on the end node is good => Tell the server that he can update its database)

  @version : 0.7
*/

// Librairies included
#include <SPI.h>
#include <RadioLib.h> // Custom RadioLib
#include <Wire.h>
#include <Arduino_JSON.h>


// Definition of the pins to use
#define NSS 5
#define RST -1 // If used => 14 ; If not used => -1 
#define DIO0 4


// Define the messages ID for the acknowledgment messages
#define DATAS 1
#define OK 2
#define OK_AND_CONF 3
#define RECONF_OK 4


// Define the variables to put the microcontroller in deep sleep mode
#define uS_TO_S_FACTOR 1000000ULL // To convert microseconds in seconds and use formatter unsigned long long to have the correct value when we put the timer
RTC_DATA_ATTR int next_sleep_time = 0; // Next time to sleep
RTC_DATA_ATTR int time_to_wait_before_sleep = 0; // Time to wait before sleep
RTC_DATA_ATTR int nb_child = 0; // Number of child
RTC_DATA_ATTR int nb_messages_of_child = 0; // Number of messages got from child


// Addresses for sending
const byte local_address = 0xFD; // Device's address
const byte destination = 0xF3; // Destination of the message => Main node


// Flags to know if we have received a message or not
volatile bool received_flag = false; // flag to indicate that a packet was received
volatile bool enable_interrupt = true; // disable interrupt when it's not needed


// Creation of the LoRa module to receive and send datas
SX1278 radio = new Module(NSS, DIO0, RST); // spi chip select => NSS ; interrupt pin => DIO0 ; reset pin => RST


/**
   Set up of the variables and the modules
*/
void setup() {
  Serial.begin(115200); // Serial initialization
  while (!Serial);


  // Start the LoRa module
  int state = radio.begin(); // freq : 433 ; bw : 125 ; sf : 9 ; cr : 7 ; syncWord : SX127X_SYNC_WORD ; txPower : 10 ; preambleLength : 8 ; gain : 0

  // Check if all is allright during the initialization of the module
  if (state == ERR_NONE) {

    // Set the coding rate to 5
    if (radio.setCodingRate(5) == ERR_INVALID_CODING_RATE) {
      Serial.println(F("Selected coding rate is invalid for this module!"));
      while (true);
    }

    // Set spreading factor to 10
    if (radio.setSpreadingFactor(10) == ERR_INVALID_SPREADING_FACTOR) {
      Serial.println(F("Selected spreading factor is invalid for this module!"));
      while (true);
    }

    // Set the TX power to 17
    if (radio.setOutputPower(17) == ERR_INVALID_OUTPUT_POWER) {
      Serial.println(F("Selected output power is invalid for this module!"));
      while (true);
    }


    Serial.println("LoRa init succeeded.");

  } else { // Print the error code
    Serial.print("failed, code ");
    Serial.println(state);
    while (true);
  }


  radio.setDio0Action(setFlag); // To update the flags when we receive a packet
  setVariablesForReception(); // Set the variables to receive messages
  state = radio.startReceive(); // Start to receive messages


  // Check if no error when we launch the receiving messages
  if (state == ERR_NONE) {
    Serial.println(F("success!"));

  } else {
    Serial.print(F("failed, code "));
    Serial.println(state);
    while (true);
  }


  Serial.println("The initialization is OK => We can receive messages");
}


/**
   Main program
*/
void loop() {

  // Check if the flag is set
  if (received_flag) {
    enable_interrupt = false; // Disable the interrupt service routine while processing the datas
    received_flag = false; // Reset flag


    String str; // To store our received message
    int state = radio.readData(str); // Read datas


    // Check if we have no error during the reception of the messages
    if (state == ERR_NONE) {

      // Check if the message is my message
      if (str.equals("")) {
        setVariablesForReception();
        return;
      }


      // Transform the message received in JSON format
      JSONVar packet = JSON.parse(str);
      Serial.println(str); // Print the packet for debug


      // Test if the decoding is good or not => If not print an error_message and quit
      if (JSON.typeof(packet) == "undefined") { // Not good decoding
        Serial.println("Parse of the message failed");
        setVariablesForReception();
        return;
      }


      // If the LoRa packet doesn't contain the receiver address and the sender of the message
      if (!packet.hasOwnProperty("to") || (!packet.hasOwnProperty("AM") && !packet.hasOwnProperty("A") && !packet.hasOwnProperty("from"))) { // addrMicrocontroller for DATAS and addr for RECONFIGURATION
        Serial.println("Packet not contains the receiver or the sender address");
        setVariablesForReception();
        return;
      }


      // Test if the message is not for me
      if (!String(local_address, HEX).equals(String((const char*)packet["to"]))) {
        Serial.println("This message is not for me.");
        setVariablesForReception();
        return;
      }


      // If the sender is the main node
      if (String(destination, HEX).equals(String((const char*)packet["from"]))) {
        Serial.println("Message received from the main node");


        // Message formatted to send to the end node
        String message_to_send = toJSONForEndNode(packet, packet["ENA"]);


        // Print debug
        Serial.print("Sending : ");
        Serial.println(message_to_send);

        // Send the message to the end node
        sendMessage(message_to_send);


        // If the sender is an end node
      } else {
        Serial.println("Message received from an end node");

        // Message formatted to send to the main node
        String message_to_send = toJSONForMainNode(packet);


        // Print debug
        Serial.print("Sending : ");
        Serial.println(message_to_send);

        // Send the message to the main node
        sendMessage(message_to_send);
      }


    } else if (state == ERR_CRC_MISMATCH) { // Pakcet received but malformed
      Serial.println(F("[SX1278] CRC error!"));


    } else { // Another error occured
      Serial.print(F("[SX1278] Failed, code "));
      Serial.println(state);
    }


    // Reset the variables for reception of new messages
    setVariablesForReception();
  }
}


/**
   Function used to be able to update the message got from an end node to send to the main one
   We update the sender and receiver address to make the main node capable to read the message and to send the response to this node
   @param my_json => JSON sent by the end node to update for the sending
   @returns the stringify JSON to send to the main node
*/
String toJSONForMainNode(JSONVar my_json) {

  // Write informations in the body of the message
  my_json["from"] = String(local_address, HEX); // Add my device address
  my_json["to"] = String(destination, HEX); // Add the recipient of the message


  return JSON.stringify(my_json); // Transform the JSON to string
}


/**
   Function used to be able to update the message got from the main node to send to an end one
   We update the sender and receiver address to make the end node capable to read the message
   @param my_json => JSON sent by the main node to update for the sending
   @param destination => Destination address of the message in integer (to transform into byte for sending)
   @returns the stringify JSON to send to the end node
*/
String toJSONForEndNode(JSONVar my_json, int destination) {
  byte end_node_address = destination; // Get the end node address

  // Write informations in the body of the message
  my_json["from"] = String(local_address, HEX); // Add my device address
  my_json["to"] = String(end_node_address, HEX); // Get the end node address


  return JSON.stringify(my_json); // Transform the JSON to string
}


/**
   Function to send a message to a recipient
   @param message => Message to send to the receiver
*/
void sendMessage(String message) {
  int transmissionState = radio.transmit(message); // Transmit the message


  // Check if problems appeared during the transmission of the message
  if (transmissionState != ERR_NONE) {
    Serial.print("Problems appeared during the transmission of the message. Code : ");
    Serial.println(transmissionState);
  }
}


/**
   This function is called when a complete packet is received by the module
   IMPORTANT : this function MUST be 'void' type and MUST NOT have any arguments!
   IMPORTANT : THIS FUNCTION NEEDS TO BE PRESENT FOR THE MESSAGE RECEIVING
   @param NO ARGUMENTS
*/
void setFlag(void) {
  // check if the interrupt is enabled
  if (!enable_interrupt) {
    return;
  }

  // we got a packet, set the flag
  received_flag = true;
}


/**
   Function to set the variables to be able to receive new messages
*/
void setVariablesForReception() {
  radio.startReceive();// Put the module in listen mode
  enable_interrupt = true; // Enable interrupt service routine to receive more packets
}
