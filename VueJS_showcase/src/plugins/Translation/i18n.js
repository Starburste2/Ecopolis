import Vue from 'vue'
import VueI18n from 'vue-i18n'

// Import the FR translations
import ActivitiesTranslationsFR from './fr/Activities.json'
import AppTranslationsFR from './fr/App.json'
import ErrorsTranslationFR from './fr/Errors.json'
import HomeTranslationFR from './fr/Home.json'
import ImagesTranslationFR from './fr/ImagesText.json'
import PageNotFoundTranslationFR from './fr/PageNotFound.json'
import PostsTranslationFR from './fr/Posts.json'
import WarningTranslationFR from './fr/Warning.json'

// Import the EN translations
import HomeTranslationEN from './en/Home.json'
import AppTranslationEN from './en/App.json'
import ImagesTranslationEN from './en/ImagesText.json'
import ErrorsTranslationEN from './en/Errors.json'
import WarningTranslationEN from './en/Warning.json'
import PostsTranslationEN from './en/Posts.json'
import ActivitiesTranslationsEN from './en/Activities.json'
import PageNotFoundTranslationsEN from './en/PageNotFound.json'

Vue.use(VueI18n)

// Translation of the messages
const messages = {
  'fr': {
    home: HomeTranslationFR,
    appVue: AppTranslationsFR,
    imgTrad: ImagesTranslationFR,
    errors: ErrorsTranslationFR,
    warning: WarningTranslationFR,
    post: PostsTranslationFR,
    activities: ActivitiesTranslationsFR,
    pageNotFound: PageNotFoundTranslationFR
  },

  'en': {
    home: HomeTranslationEN,
    appVue: AppTranslationEN,
    imgTrad: ImagesTranslationEN,
    errors: ErrorsTranslationEN,
    warning: WarningTranslationEN,
    post: PostsTranslationEN,
    activities: ActivitiesTranslationsEN,
    pageNotFound: PageNotFoundTranslationsEN
  }
}

// Module to use to translate with fr as locale language - en is used in foreign language
const i18n = new VueI18n({
  locale: 'fr', // set locale
  fallbackLocale: 'en', // set fallback locale
  messages, // set locale messages
  silentTranslationWarn: true
})

// Export the module to use
export default i18n
