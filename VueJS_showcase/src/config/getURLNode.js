/**
* Function to be able to get the informations about the server
* We can get easily the URL and port => Just modify this file ot modify the URL and port
* @returns url or port
*/
export default function getURLNode () {
  return {
    /**
     * @returns the URL of the NodeJS server => Use HTTPS
     */
    getURL () {
      return 'https://10.11.11.43:'
    },

    /**
     * @returns the port of the NodeJS server => Use the HTTPS port
     */
    getPort () {
      return '3001'
    }
  }
}
