import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import PageNotFound from '@/components/PageNotFound'
import Posts from '@/components/Posts'
import LastActivities from '@/components/LastActivities'

Vue.use(Router)

export default new Router({
  routes: [
    // Route to access to the home
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    // Route to access to the posts made by the admin
    {
      path: '/posts',
      name: 'Posts',
      component: Posts
    },
    // Route to access to the last activities of the microcontrollers
    {
      path: '/last-activities',
      name: 'LastActivities',
      component: LastActivities
    },
    // 404 not found
    {
      path: '/404-not-found',
      name: 'PageNotFound',
      component: PageNotFound
    },
    // Others
    {
      path: '*',
      redirect: '/404-not-found'
    }
  ]
})
