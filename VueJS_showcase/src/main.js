// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import i18n from '@/plugins/Translation/i18n'
import FlagIcon from 'vue-flag-icon'

Vue.use(FlagIcon) // Use the flag icons
Vue.use(BootstrapVue) // Use BootstrapVue
Vue.config.productionTip = false

// Render the view with translation and router
new Vue({
  i18n,
  render: h => h(App),
  router
}).$mount('#app')
