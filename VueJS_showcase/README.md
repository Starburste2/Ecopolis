# Écopolis showcase application

This application is the showcase for the Écopolis project. In fact, this application is more for the project's promoting than management of nodes, get all datas, etc …. On this application, a user will be able to see the global purpose of the project, see the posts made by the administrator of the managing application and see the last fifty data sent by the microcontrollers on a graph and the position of the microcontrollers on the old industrial wasteland.


## Build Setup
``` bash
# install dependencies
npm install

# serve with hot reload at 10.11.11.43:8081
npm run start

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Packages used
### axios@0.21.1
This package is used to be able to make HTTP requests on the server to get some informations like the posts written by the administrators, the microcontrollers present in the system and the last fifty datas recorded by them. On this application, we just use the GET method.

To install and [see the documentation](https://www.npmjs.com/package/axios) :
```bash
npm install axios --save
```

### bootstrap@4.6.0 and bootstrap-vue@2.21.2
These packages are used to be able to beautify the application. ***WARNING : YOU NEED TO USE BOOTSTRAP IN THE 4.6.0 VERSION. IF YOU USE ANOTHER VERSION, YOU CAN ENCOUNTER PROBLEMS WITH THE RENDERING OF THE VIEW (BAD COLORS FOR A SPECIFIC BOOTSTRAP CLASS, ETC …).***

To install and [see the bootstrap@4.6.0 documentation](https://www.npmjs.com/package/bootstrap/v/4.6.0) or [see the bootstrap-vue documentation](https://www.npmjs.com/package/bootstrap-vue) :
```bash
npm install bootstrap@4.6.0 bootstrap-vue --save
```

### easy-scroll@0.7.0
This package is used to be able to scroll in the view of the application (mainly in the Home component). We use this package to be able to have an animation, firstly, but also to have a compatibility with all the browsers secondly.

To install and [see the documentation](https://www.npmjs.com/package/easy-scroll) :
```bash
npm install easy-scroll --save
```

### fusioncharts@3.17.0 and vue-fusioncharts@3.1.0
These packages are used to be able to display graphics on the application. The interactions with the graphic are nearly complete. We can select or deselect some curves to show or not show, we can have informations about the point of the curve by passing the mouse on it, etc …. This package will help us to display the last fifty records of a microcontroller to show as last activities.

To install and [see the fusioncharts documentation](https://www.npmjs.com/package/fusioncharts) or [see the vue-fusioncharts documentation](https://www.npmjs.com/package/vue-fusioncharts) :
```bash
npm install fusioncharts vue-fusioncharts --save
```

### leaflet@1.7.1 and vue2-leaflet@2.7.1
These packages are used to display a map and place the microcontrollers on the map to know where they are.

To install and [see the leaflet documentation](https://www.npmjs.com/package/leaflet) or [see the vue2-leaflet documentation](https://www.npmjs.com/package/vue2-leaflet) :
```bash
npm install leaflet vue2-leaflet --save
```

### vue-flag-icon@1.0.6
This package is used to be able to display the correct flag associated to the language. This package apply a CSS class to the element we want and the background image will be the flag of the country provided in the CSS class.

To install and [see the documentation](https://www.npmjs.com/package/vue-flag-icon) :
```bash
npm install vue-flag-icon --save
```

### vue-i18n@8.24.5
This package is used to be able to translate the text we have put in the HTML in the correct language. All the translations are already done, so this package displays what we need to show to the user according to the selected language of the user.

To install and [see the documentation](https://www.npmjs.com/package/vue-i18n) :
```bash
npm install vue-i18n --save
```


## Configuration of the project
This project is configured to be launched on the IP and port given in the /config/index.js file. If you want to use another IP or port, you need to edit this file by modifiying the host and port lines. You can also change the IP and the port of the server to make request on in /src/config/getURLNode.js file by modifying the getURL() and getPort() functions. This allows you to be able to make HTTP requests on your own network.

If you want to use HTTPS protocol instead of HTTP, you can do it by modifiying the /build/webpack.dev.conf.js file (only during the development of the application). You can have a better view of what you can do [here](https://webpack.js.org/configuration/dev-server/#devserverhttps).


## Carousel configuration
On the home page, we have a carousel of images to show to the user. You can configure this carousel by adding, modifying or deleting some images.

### Add images
To add an image, it is simple. First copy and paste your image in /static/carousel_images folder. When you have done that, open the /src/assets/carousel.json file. You can add an image with the code below :
```json
,
  {
    "id": id,
    "text": text_in_translation_file,
    "imgSrc": path_to_the_image,
    "altText": text_that_replaces_the_image
  }
```

When you have paste the code from above, you need to change some elements :
> - id => Last id in the file + 1
> - text_in_translation_file => Name to put in the ImagesText.json translation file (imgID) => If the ID image = 6, we will have : img6
> - path_to_the_image => Path to access to the image. Basically it is /static/carousel_images/img_name
> - text_that_replaces_the_image => Name to put in the ImagesText.json translation file (altTextImgID) => If of the ID image = 6, we will have : altTextImg6

Now, you need to add the translation text into the ImagesText.json files. Open /src/plugins/Translation folder. Here you will find two folders : en and fr. The two folders contain the files for the translations. Open /en/ImagesText.json and /fr/ImagesText.json files. Add these lines to the file :
```json
,
"imgID": "Text to display",
"altTextImgID": "Alternative text to display (if the image cannot be loaded)"
```

The imgID corresponds to the text_in_translation_file you have put before. And altTextImgID corresponds to the text_that_replaces_the_image you have put before.

### Modification of an image
To modify an image, it is pretty simple. You can change the image. In this case, open the /src/assets/carousel.json file and change the "imgSrc" input of the image you want to change with the new one.

You can also want to change some texts. To do that, you will need the ID of the image. Open /src/assets/carousel.json file and get the "text" and "altText" inputs. Then open /src/plugins/Translation/en/ImagesText.json and /src/plugins/Translation/fr/ImagesText.json files. Find the "text" and "altText" values inputs and change the texts.

### Deleting an image
To delete an image, first open /src/assets/carousel.json file and select the image you want to delete. Take the name of the image in the "imgSrc" input, take the "text" and "altText" inputs too. Delete the image from "imgSrc" (basically, if you have followed all the steps, the images will be in /static/carousel_images/ folder). When you have deleted the image, open the /src/plugins/Translation/en/ImagesText.json and /src/plugins/Translation/fr/ImagesText.json files, find "text" and "altText" and delete these lines.


## Translation configuration
All the translations are stored in the Translation plugin. You can access to it via /src/plugins/Translation. In the Translation folder, you will find two other folders : en and fr. These folders contains the translations in the target language. In these folders, you will find multiple JSON files. You will find JSON files for :
> - Components translation (Activities.json, App.json, Home.json, PageNotFound.json and Posts.json) <br/>
> These files contain the translation of the text that are present in the components <br/><br/>
> - Carousel translation (ImagesText.json) <br/>
> This file contains the translation of the text that will go with the images <br/><br/>
> - Errors (Errors.json) <br/>
> This file contains the translation for the possible errors <br/><br/>
> - Warning (Warning.json) <br/>
> This file contains the translation for the possible warnings

So, to change a translation, open the file you want to modify the translation and change the translation text. It is just that to change some translations.


## Routes available
Four routes are available for this application. **In this application, we don't have authentication or management of roles. This simplifies a lot the application.**

All the HTTP requests we will do are authorized with a valid API key given for this application.

Here are the available routes of this application :
> - / <br/>
> This is the route to access to the home of the application. In this route, we will display some informations about the Écopolis project (what we will do, the people concerned, …). <br/><br/>
> - /posts <br/>
> This is the route to be able to see all the posts made by the administrators. In this route, we can see all the posts (the newest first). <br/><br/>
> - /last-activities <br/>
> This is the route to be able to see the latest activities of the microcontrollers. On this route, we can see the place where the microcontrollers are, the last fifty datas sent (PH, humidity level, temperature, conductivity, nitrogen, phosphorus and potassium) by the microcontrollers and the waking hours of the microcontrollers. <br/><br/>
> - /404-not-found <br/>
> This route is the route when the user want to access to an unknown route. When he tries to access to an unknown route, he will be always redirected on this route. </br><br/>


## Guide
### Menu
When you open the menu, you will see first a link to return to the home. This link is also the title of the application/project. Then, you have a little presentation of what we will do in this project. After, we have the links to access to the possible routes of the application (home, posts and last activities). Finally, you can select the language of the application you want and you can close the menu by clicking on the button "Fermer le menu" or "Close the menu" according to the selected language. The language selected is the disable green button, that is to say that you cannot click on the green button when you have the same language selected. You can close the menu by clicking on the red button or by clicking in the right dark part.

![alt text](./README_images/menu.png "Menu of the application")

### Home
When you are on the home of the application, first, you have a welcome message. Behind, you have a carousel to display multiple images about the project with a little description below the images. The images change themselves with the time but you can also force the changing of the image by clicking on the arrow on the right or on the left of the shown picture.

![alt text](./README_images/homePart1.png "Part 1 of the home of the application") <br/><br/>

Behind the carousel, we will present the three domains of research of the project. The first part will present the agronomy (definition and application), the second will present the phytomanagement (what is it ?) and the third part will present the concerned people that will work on this project. By clicking on the button "Voir les détails" or "See the details" according to the selected language, you will slide to the part you want to see.

![alt text](./README_images/homePart2.png "Part 2 of the home of the application") <br/><br/>

Now, you will see some explanations about agronomy with a little definition and application. You can access to this part by clicking on the button "Voir les détails" or "See the details" according to the selected language of the user or you can scroll down to access to this part.

![alt text](./README_images/homePart3.png "Part 3 of the home of the application") <br/><br/>

The next part of the home concerns the phytomanagement. We will present the phytomanagement, what is it and the objectives. You have also a picture on the left that well explains how the phytomanagement works. You have some text in blue separated by lines that you can click on to display some tooltips that explain more the words.

![alt text](./README_images/homePart4.png "Part 4 of the home of the application") <br/><br/>
![alt text](./README_images/homePart4Tooltips.png "Part 4 of the home of the application with an opened tooltip") <br/><br/>

The final part concerns what type of professions will work on this project. This time, the blue text cannot be clicked to display more information.

![alt text](./README_images/homePart5.png "Part 5 of the home of the application") <br/>

### Posts
When you are on the posts route, you can see all the posts made by the administrators of the application. You cannot add, modify or delete a post. The post will be formatted as the administrator has formatted it when he created or modified it. The text of the posts are not translated by default. A post can contain links to another website, formatted text, lists, ….

![alt text](./README_images/posts.png "Example of a post posted by the administrator") <br/>

### Last activities
When you are on the last activities route, you can see all the end nodes microcontrollers that are present in the system. You can see the position of the microcontrollers and you can also see the last fifty datas recorded by the microcontrollers.

![alt text](./README_images/lastActivities.png "List of all the end nodes microcontrollers to see the last fifty datas sent by them")

To be able to see the positions of the microcontrollers on a map, click on the button "Positions des microcontrôleurs" or "Positions of the microcontrollers" behind the title. By hoving a marker (on a PC) or by clicking on it (on a smartphone or a tablet), you can display the address of the microcontroller. You can close the modal by clicking on the arrow in the top right corner or by clicking in the dark part around the modal.

![alt text](./README_images/positionsMicrocontrollers.png "Positions of the microcontrollers on a map")

To be able to see the last fifty datas sent by the microcontrollers on a graphic, click on the button "Voir les dernières données" or "See the last datas". We will display a graphic that will contains the last fifty datas recorded by a microcontroller. You can select the curves to display by clicking on the text next to a color circle. When you select or deselect the information to display on the curve, the graphic is updated to be able to read it better with the new scale. You can close the modal by clicking on the arrow in the top right corner or by clicking in the dark part around the modal.

![alt text](./README_images/last50Datas.png "Last fifty datas sent by the microcontroller")

### Translation
You can translate the application by selecting the language you want in the menu. The selected language is the disable green button. The language you can select is the enable button with a grey line. The selected language is by default the French but you can also select the english one as a new language.  ***NOTE THAT THE DEFAULT LANGUAGE IS THE FRENCH SO IF YOU RELOAD THE PAGE, THE SELECTED LANGUAGE WILL BE RESET TO FRENCH EVEN IF YOU HAVE SELECTED THE ENGLISH LANGUAGE BEFORE RELOADING THE PAGE.*** When you change the language, the buttons to change the language are updated. The one which is the language selected is now disable and green, and the other has now a grey line and can be selected. All the application is translated when you change the language of the application except the text of the posts that remains in the language the administrator has written it. You can see an example of translation below :

![alt text](./README_images/menu.png "Menu in French of the application")
![alt text](./README_images/englishMenu.png "Menu in English of the application")

### 404 not found
When you are on this route, you normally tried to access to an unknown route. You can also access to this route by modifying the URLs. By opening the menu, you can access to all the available routes of the application. ***IMPORTANT : NOTE THAT THIS PAGE IS ALSO TRANSLATED IN ENGLISH.***

![alt text](./README_images/404NotFound.png "404 not found")