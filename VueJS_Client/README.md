# Ecopolis

This project is the view for the user that will use the global project. With this application, the users can login in it and if their roles are sufficient, they can manage microcontrollers (add, modify or delete), they can also see the datas displayed in graphic form but also with the real datas with a pagination of all of them. They can also download microcontroller's code and datas or they can upload datas in the database via files.


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload on IP address and port specified
npm run start

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Packages used
### *axios@0.21.1*
This package is used to make HTTP requests on the API. 

To install this package and [see the documentation](https://www.npmjs.com/package/axios) :
```bash
npm install axios --save
```

### *bootstrap@4.6.0 and bootstrapvue@2.21.2*
These packages are used to display the views with a sympathetic appearence. ***WARNING : YOU NEED TO USE THE 4.6.0 VERSION OF BOOTSTRAP. IF YOU DON'T USE THIS VERSION, YOU CAN HAVE PROBLEMS WITH THE APPPEARENCE OF THE APPLICATION (SOME CLASS WILL NOT BE APPLIED TO SOME HTML ELEMENTS).*** 

To install these packages and see [the documentation of bootstrap@4.6.0](https://www.npmjs.com/package/bootstrap/v/4.6.0) or [the documentation of bootstrapvue@2.21.2](https://www.npmjs.com/package/bootstrap-vue) :
```bash
npm install vue bootstrap bootstrap-vue --save
```

### *fusioncharts3.16.0 and vue-fusioncharts@3.1.0*
This package is used to display the datas from a microcontroller in a graphic. We can choose the datas we want by clicking on the name and when we pass the mouse on a line, we can get the datas associated.

To install this package and see [the documentation of fusioncharts](https://www.npmjs.com/package/fusioncharts) or [the documentation of vue-fusioncharts](https://www.npmjs.com/package/vue-fusioncharts) :
```bash
npm install fusioncharts vue-fusioncharts --save
```

### *jw-vue-pagination@1.0.3*
This package is used to display all the real datas in a list. But like we have several dozens of records, we use a pagination to be able to show all of them in a good way.

To install this package and [see the documentation](https://jasonwatmore.com/post/2019/08/21/vue-js-simple-pagination-example) :
```bash
npm install jw-vue-pagination --save
```

### *vue-cookie-law@1.13.3*
This package is used to prevent the user that cookies will be used for the proper operation of the VueJS application. These cookies will be only used to store the authentication token of the user. With the authentication token, the user can be reconnected directly to the application provided the authentication token is valid.

To install this package and [see the documentation](https://www.npmjs.com/package/vue-cookie-law) :
```bash
npm install vue-cookie-law --save
```

### *vue-cookie@1.1.4*
This package is used to be able to manage easily the cookies in the VueJS application. With that, we will store the authentication token of the user.

To install this package and [see the documentation](https://www.npmjs.com/package/vue-cookie) :
```bash
npm install vue-cookie --save
```

### *password-validator@5.1.1*
This package is used to validate the password the user enters.  To create an account, the user needs to respect our security policy. We check that the password is formed like this : it contains between 7 and 30 characters, it contains 1 digit minimum and it contains at least one uppercase and one lowercase character and no space into. We are doing that to verify the password before sending the email/password pair to the server to minimize the error possibility.

To install this package and [see the documentation](https://www.npmjs.com/package/password-validator) :
```bash
npm install password-validator --save
```

### *vue-password-strength-meter@1.7.2*
This package is used to verify the password complexity. We will use the score given by this package to be able to know if the password respects our security policy and if it is strong enough.

To install this package and [see the documentation](https://www.npmjs.com/package/vue-password-strength-meter):
```bash
npm install vue-password-strength-meter --save
```

### *vue-recaptcha@1.3.0*
This package is used to be able to include a captcha in our login and account creation forms to avoid the possibly spam. This package will generate a recaptcha V2 invisible and we will send the result to the server (a token). This token will be verified on the server.

To install and [see the documentation](https://www.npmjs.com/package/vue-recaptcha) :
```bash
npm install vue-recaptcha --save
```

### *leaflet@1.7.1 and vue2-leaflet@2.7.0*
These packages are used to display a map on the webiste to position a microcontroller or to see where are the microcontrollers.

To install and [see the documentation of leaflet](https://www.npmjs.com/package/leaflet):
```bash
npm install leaflet --save
```

To install and [see the documentation of vue2-leaflet](https://www.npmjs.com/package/vue2-leaflet):
```bash
npm install vue2-leaflet
```

### *easy-scroll@0.7.0*
This package is used to animate the window when the user clicks on the button to scroll directly to the part he wants. As the classic version doesn't work on all the browsers, we use this package to do that.

To install or [see the documentation](https://www.npmjs.com/package/easy-scroll)
```bash
npm install easy-scroll --save
```

### *quill@1.3.7 and vue2-editor@2.10.2*
These packages are use by the administrators to be able to write their posts. The administrators can format their posts as they want : add a list of object, list of tasks to do (task done are checked), add links to website of for an email, ….

To install or see [the documentation of quill](https://www.npmjs.com/package/quill) or [the documentation of vue2-editor](https://www.npmjs.com/package/vue2-editor) :
```bash
npm install quill vue2-editor --save
```


## Configuration of the project
The project is configured to be launch on the IP address and port given in the /config/index.js file. If you want to use another IP address or port, be free to do so. You can change the URL and port of the Node server by modifying the file src/config/getURLNode.js to use the HTTP requests on your own network.

If you want to use HTTPS, you can use it by declaring it in the /build/webpack.dev.conf.js file (during the development of the program). You can have a better view of you can do [here](https://webpack.js.org/configuration/dev-server/#devserverhttps).


## Recaptcha
The recaptcha will help us to know if the person who wants to connect is a bot or not. First of all, to be able to generate a valid captcha, you need to generate your keys here : [https://www.google.com/recaptcha/admin](https://www.google.com/recaptcha/admin). You need to enter the title of your website you want generate keys for. Then, you need to select "Recaptcha V2" with "Badge Recaptcha invisible" in French, "Invisible Recaptcha badge" in English. Next, you need to add the domain of your website (same URL as the VueJS application => If the website runs in 1.1.1.1, the domain will be 1.1.1.1). And finally, you need to accept the conditions of use for Recaptcha. You can see below an example : <br/>

![alt text](./README_images/configuration_recaptcha.png "Configuration of a Recaptcha for your application") <br/><br>

Last thing to do is to run this command, in a terminal, in the Serveur folder of the Ecopolis project : 
```bash
node generateKeysForRecaptcha.js site_key secret_key
```

site_key => Site key given by Google <br/>
secret_key => Secret key given by Google <br/>
You can see these keys in the parameters by clicking on the little arrow beside "Clés ReCaptcha" in French or "ReCaptcha keys" in English.

![alt text](./README_images/recaptcha_keys.png "See the Recaptcha keys of your application") <br/><br>


## Cookies
As this application will use some cookies, we are mandatory to warn the user that the application will use some cookies. These cookies are stored just for the proper functionning of the application.

The site will use some cookies to store important datas of the user's connection. We will store only the authentication token of the user to let him reconnect when he will come back into the application if the IP contained in the cookie is the same than the machine of the user. The authentication token will contain some important datas like the email of the user, the IP of the machine, the role of the user, …. The authentication token has an expired time of 7 days.

## Routes available
Multiple routes are available but you can't access to the available routes when you're not connected to the application (you will just see the home). When you try to access to a route that does not exist, you will be redirected to the 404 error page. All the requests on the server the application will make need a valid session ID. Without that, you will get an error. A session ID has an expired time of 1 hour. Some important routes also required a CSRF token to avoid the cross site request forgery, like add a microcontroller, modify or delete one or upload datas on the server and export the datas + download code too. A CSRF token has the same expired time as the session ID which is 1 hour. Without valid CSRF tokens, you will be rejected when you will try to access some important routes like add a microcontroller for example. CSRF tokens are only used on routes that can modify datas or retrieve lots of datas (source code or datas in themselves) from the database. Some routes will use an API key to be able to identify the application that makes the request (login and createAccount routes that are public). The account creation and login routes need also a valid captcha token to be accessible.

When a user is connected to the application, he will have some rights that will say that he can do this or that on the application. So, a user that is estimated as a client will not have access to the route like /addMicrocontroller or /microcontroller/:id. These routes are only accessible by the administrators. Some of the informations have also disappeared like the button to have the modal to upload datas, to have link to add microcontroller, to have buttons to download the microcontroller's code, to add, to modify or delete a microcontroller for example.

> - / <br/>
> This is the route to access to the home of our application. <br/><br/>
> - /microcontrollers <br/>
> This is the route to access to the page that will list all the microcontrollers. On this page, you can download the microcontroller's code ***(if you have the rights)*** and datas (download all the datas without applying any options), you can upload datas ***(if you have the rights)*** on the server and you can delete the microcontroller ***(if you have the rights)***. You can also see where are the microcontrollers. <br/><br/>
> - /addMicrocontroller <br/>
> This route is used to be able to add a microcontroller into the database. You can give the type of the microcontroller (end or main node), you can specify the address between all the available addresses and the sleep times you want to put on the microcontroller (sleep times are the waking hours of our microcontrollers). You can also place the microcontroller you want to add on the map. ***Note that only end node can be placed and only end node can have sleep times (waking hours)***<br/><br/>
> - /microcontroller/:id <br/>
This route is used to modify the sleep times of a microcontroller (waking hours). We can modify the sleep times values and the position, if we need, of the microcontroller selected. The microcontroller will be updated at its next datas sending. ***Note that we can only update end nodes***<br/><br/>
> - /showDatas/:addr <br/>
> This route is used to visualize the datas sent by the microcontroller. We can visualize the datas via a graphic but we can also see the real datas with the pagination we have put in place. We can also select the date interval for the datas. The user can also download the datas as the microcontroller route but here he can select the options he wants to use to filter the datas (if no options are selected, the user will download all the datas that are present in the database). <br/><br/>
> - /my-account <br/>
> This route is used by the user to update his credentials to connect to the application. <br/><br/>
> - /verify-account/:activationToken <br/>
> This route is used by the user to activate his account to be able to access to the application. The activationToken is given in an URL to the user. <br/><br/>
> - /modify-forgotten-passwd/:token <br/>
> This route is used by the user to modify a forgotten password. This token is given by email in an URL to the user. <br/><br/>
> - /dashboard <br/>
> This route is used by the administrator to update the user's credential and his role + see the different logs that are saved by the server <br/><br/>
> - /posts <br/>
> This route is used by the administrator to manage the posts <br/><br/>
> - /groups <br/>
> This route is used by the administrator to manage groups and microcontrollers that are present in the groups <br/><br/>
> - /404-not-found <br/>
> This route is used to say to the user that the route he wants to access doesn't exist. <br/><br/>
> - /not-the-rights <br/>
> This route is used to say to the user that he doesn't have sufficient rights to access to the route he wants. <br/>

When you are connected, you can access to all of the routes given above ***(depends on your rights you have)***. But if you are not connected, you can just access to a specific page that containing the home page plus a sidebar to login or create an account.


## Guide
In this guide, you will have first the pages you can access by being not connected and after you will have the pages that need to be connected to access them.

### Cookies authorization
When you arrive on the application for the first time, you will need to accept the cookies. These cookies are only used to make the application more comfortable and to have the better experience as possible on the application. With that, the user can quit the application and then come back, and if was connected (with an authentication token), it will be reconnnected to the application automatically. 

![alt text](./README_images/cookies_authorization.png "Cookies authorization") <br/><br>

### Authentication
First of all, before to accesss to all the available routes, we need to be authentified. So you need to identify yourself by enter your email and your password in the input (See the picture below). When you are identified, we will display the component associated to the url you've entered. When you are not connected, you can see the home page, a menu to connect you or create an account and you can reset your password if you have forgotten it by clicking on "Mot de passe oublié ?".
> - For example, if you have entered the basic url + /microcontrollers, we will display the component associated to the route /microcontrollers when you are connected to the application.

![alt text](./README_images/login.png "Authentication on the application") <br/><br>

### Creation of an account
If you don't have any account, you can create one by enter your email and your password (that you need to confirm in another input). After that, we will check that the email format is valid and if the password respects the security policy. We have two systems to check if a password is good :
> - Score => It is given to know the security level of the password (0 => too guessable ; 1 => very guessable ; 2 => somewhat guessable ; 3 => safely unguessable, 4 => very unguessable)
> - Validator => It will validate the password by our measures of security (do we need to have uppercase and lowercase letters, how many digits do we need, how many characters are required, can we put space into the password). All these questions are used by the validator to know if the password respects our security policy.

After that, the server will make some checks to verify if the email entered is already use or not for example. If the email is already used, you will get an error. 

When you will enter your password for the first time (entry field just below "Entrez votre mot de passe"), you will see a tooltip that explains you what security constraints for the password we want in order to respect our security policy.

When you have enter your email, password and you have confirmed your password, you will need also to validate your "identity" (if you are a bot or not) by clicking on the case beside "Je ne suis pas un robot" to be able to connect you. When you have created your account, you will receive an email to tell you to activate your account to be able to access to the application (when the account is not activated, you cannot access to it).

![alt text](./README_images/account_creation.png "Account creation on the application") <br/><br>

### Activation of the account
When you have created your account, you will receive an email to tell you to activate your account to be able to access to the application. If you try to access to the application when you have not activated your account, you will not be able to connect to the application (see the picture below). 

![alt text](./README_images/account_not_activated.png "Account not activated")

So, in your email box, check if you have receive an email named "Activation de votre compte" with a view that looks like the picture below :

![alt text](./README_images/email_account_activation.png "Email concerning the account activation")

To activate your account, click on the green button named "Activer mon compte". You will be redirected to our website and we tell to you if a problem appears. If no problem appears, you will be able to see a picture like below :

![alt text](./README_images/account_activation_good.png "Account activation is good")

After that, you will be able to connect to the application.

### Password forgotten
By clicking in the menu on "Mot de passe oublié ?", you will be able to reset your password if you have forgotten it. When you click on "Mot de passe oublié ?", you will open a tab. In this tab you will have an input in which you need to enter the email address of your account. When you have validate the form, and if you have an account, the server will send you an email to reset your password. The picture below shows the tab :

![alt text](./README_images/forgotten_password_email_to_enter.png "Email to enter to reinitialize the forgotten password")

When you have validated the reinitialization, you will receive an email named "Modification de votre mot de passe oublié". By opening the mail, you will see a green button in which it is written "Modifier mon mot de passe" as you can see on the picture below.

![alt text](./README_images/password_modif_email.png "Email to be able to modify the password")

By clicking on the green button, we will verify the token in the URL and if it is good, you will be able to see the form to modify your forgotten password as you can see on the picture below.

![alt text](./README_images/form_modif_forgotten_passwd.png "Form to be able to modify your forgotten password")

Here, you need to enter your email and your new password (that you need to confirm). If all the informations are correct, you will be able to modify your forgotten password.

![alt text](./README_images/modif_forgotten_passwd_ok.png "Modification of the forgotten password is OK")

When you see the picture above, you have well modified your password. Then, the application will redirect you on the home page of the application. You, now, will be able to connect to the application with your new password.

### Home
When the user want to access to the route associated to the home component, we will display this component. It can be accessible if you're connected but also if you're not connected to the application. The user will have the choice to see the posts posted by the administrators or the last fifty datas recorded by the microcontrollers.

When the user wants to see the posts, he can see all the posts that are made by the administrators and can interact with the posts : follow a link, send emails (if an email address is provided), ….
![alt text](./README_images/home_posts.png "See the posts made by the administrators on the home") <br/><br>

When the user want to see the last fifty datas recorded by the microcontrollers, first, we will show all the end nodes microcontrollers. And finally, he can click on the button "Voir les dernières activités" in order to see the last datas recorded by the microcontroller selected on a graph. He can also see the positions of the microcontrollers by clicking on the button "Voir la position des microcontrôleurs".

![alt text](./README_iages/../README_images/home_last_activities.png "See the end nodes microcontrollers on the home") <br/><br/>
![alt text](./README_images/home_microcontrollers_position.png "See the positions of all the end nodes microcontrollers") <br/><br/>
![alt text](./README_images/home_microcontroller_datas.png "See the last fifty datas of a microcontroller on a graph") <br/><br/>


### 404 not found
When the user tries to access to a page that doesn't exist, you will be redirected to the 404 not found page. In the 404 not found page you will have a little animation of the 404 number to display and a message that says that the page you want to access doesn't exist.

![alt text](./README_images/404-not-found.png "404 not found page") <br/><br>


### Authentified with administrator access
This is the biggest rights you can have on the application. With these rights, you can do everything on the application. So, you will not be restricted in the possible actions you will make.

#### Menu
This part will contain the menu to be able to navigate into our application. We have the title of the application and a little description. We have also several links to access to the home, to the microcontrollers, to the dashboard or to add a post. The user has a button to logout (when a user logs out, all the informations stored about his session are deleted in database but also the cookies are deleted), a button to be able to update his credentials and also a button to close the menu. The menu can be closed by clicking in the dark part too. <br/>

![alt text](./README_images/menu.png "Menu of the application when you have an admin access") <br/><br>

#### Microcontrollers
This part shows to the user all the microcontrollers that are stored in the database. We have multiple possibilities here : we can add a new microcontroller, upload datas, download the code or exports the datas of the microcontroller, see the datas stored by one microcontroller, modify or delete one microcontroller. We can also see the position of the microcontrollers. You can also export all the datas recorded by all the microcontrollers and manage the groups

![alt text](./README_images/microcontrollers.png "Microcontrollers stored in the database when we have admin access") <br/><br>

##### Position of the microcontrollers
By clicking on the button "Afficher la position des microcontrôleurs", we will open a modal that will contain the map with all the position of the microcontrollers. When you will fly over a marker with your mouse, you will be able to see the address of the microcontroller that is in this position. To close the window, you have a cross at the top right.

![alt text](./README_images/microcontrollers_position.png "Position of the microcontrollers on a map") <br/><br/>

##### Upload of the datas
When you click on the button "Upload des données", you will launch a modal that is used to upload the datas on the server.
We have just to put the CSV file that will contain the microcontroller's recording. We can have multiple messages when we want to upload datas : 
> - **OK message** : All the datas are well been registered into the database. <br/><br/>
> - **Already present** : The datas you want to insert into the database are already present in it. <br/><br/>
> - **CSV formation problem** : It is a problem that appears when the server cannot insert correctly the datas into the database. This problem appears most of the time when we have modify the CSV file we want to upload. <br/><br/>
> - **Network error** : It appears when we cannot reach the server. <br/><br/>

![alt text](./README_images/upload_datas.png "Upload of datas in the database") <br/><br>

##### Download of the code
When you click on the button "Télécharger le code de microcontroller_addr", you will directly download the code of the microcontroller without exiting the actual page. The code is formated with all the datas stored in the database. So before upload the code on the ESP32, you need to verify that all the datas are put correctly in the file via [this link](https://github.com/Starburste/Ecopolis/tree/main/Serveur/serveur#download-1). <br/><br/>

##### Exports of the datas
When you click on the button "Exporter toutes les données de microcontroller_addr", you will directly download the CSV file that will contain all the datas from a microcontroller (without processing them). You will not exit the page when you will click on the button. The format is the following : 
> microcontroller_addr ; ph ; humiduty ; temperature ; conductivity ; nitrogen ; phosphorus ; potassium ; added_date 

<br/>

##### Exports of all the datas recorded by all the microcontrollers
When you click on the button "Exporter toutes les données", you will directly download the CSV file that will contain all the datas from all the microcontrollers (without processing them). You will not exit the page when you will click on the button. The format is the following : 
> microcontroller_addr ; ph ; humiduty ; temperature ; conductivity ; nitrogen ; phosphorus ; potassium ; added_date 

<br/>

##### Download the server's certificate
When you click on the button "Télécharger le certificat", you will directly download the certificate of the server to put on the SD card of the main node. By doing this, the main node will be able to directly read the certificate of the server to be able to make HTTPS requests. Indeed, if the certificate has changed, by downloading the new certificate and put it on the SD card, the microcontroller will be updated and it will be able to make HTTPS requests on the server with the new certificate.

<br/>

##### Manage the groups
When you click on the button "Voir les groups", we will open a new component formated to be able to manage the groups. <br/><br/>

##### Add a microcontroller
When you click on the button "Ajouter un microcontrôleur", we will open a new component formated to be able to add a microcontroller. Here, we will get all the available addresses to add a microcontroller.<br/><br/>

##### Modify a microcontroller
When you click on the button "Modifier microcontroller_addr", we will open a new component with the variables of the microcontroller you have chosen. <br/><br/>

##### See the datas
When you click on the button "Voir les données", we will open a new component with all the datas recording by the microcontroller you have chosen. You can only see datas from an end node. <br/><br/>

##### Delete a microcontroller
When you click on the button "Supprimer microcontroller_addr", we will open a prompt window for the user. Here the user needs to enter the address of the microcontroller he wants to delete. If he enters a wrong address, you will see an alert and the microcontroller will not be deleted. If the address is good, you will have a message that is saying that the microcontroller has been deleted and the microcontroller will disappear from the list of the microcontrollers.

![alt text](./README_images/delete_microcontroller.png "Delete a microcontroller in the database") <br/><br>

#### Add a microcontroller
When you want to add a microcontroller, you have two choices and one input to do. The first choice concerns the type of the microcontroller, if the microcontroller is an end or a main node (***WARNING : ONE MAIN NODE ONLY FOR THE WHOLE APPLICATION***). After that, you can choose the address of the microcontroller between all the available addresses. Secondly, you need to place the microcontroller on the map by clicking on it when you think that is the best position for the microcontroller. If you don't place a marker on the map, you will get an error when you will submit the form. Finally, you can enter the sleep times values of the microcontroller (that are hours of the day like 08:00 or 13:00). The sleep times for the microcontroller are only mandatory when the user has selected the end node type for a microcontroller. When all the datas don't follow the good line, you can't enter the form. When you have added a microcontroller to the database, you will have a message that saying that the microcontroller is inserted in the database and you will be redirected to the list of microcontrollers. You can also choose the group of the microcontroller.

The picture below shows you the form when you want to add a new end node :
![alt text](./README_images/add_microcontroller_end_node.png "Add a new end node in the database") <br/><br>

A main node needs only the address to be functional. The picture below shows you the form when you want to add a new main node :
![alt text](./README_images/add_microcontroller_main_node.png "Add a new main node in the database") <br/><br>

#### Modify a microcontroller
When you want to modify a microcontroller, we will load the values of the sleep times and about the position from the database. The user can change the sleep times values (that are hours of the day like 06:00 or 20:00) and when the user posts the form, a message will be displayed on the screen to know if all is good or not. He can also change the position of the microcontroller by positioning a new marker on the map (click on the map to position the marker). You can also change the group of a microcontroller (it's not advisable because you will need to reupload the microcontroller's code on the ESP32)

![alt text](./README_images/modify_microcontroller.png "Modify a microcontroller in the database") <br/><br>

#### See the datas
When you want to see the datas recording by a microcontroller, you have multiple choices to see that : a graphic or the real datas paginated in a list. You can also download the datas of the microcontroller as we can do on the microcontrollers page but here, the user can select some options to filter the datas he wants in the CSV file.

##### Exports of the datas (With options)
When you click on the button "Exporter les données de microcontroller_addr", you will see a modal appears that allows you to select or not the options to filter the datas you want to retrieve in the CSV. Here, you will be able to select the options you want to use to filter the datas. The options are the following :
> - Without duplicates <br/>
> By activating this filter, you will be able to retrieve datas without duplicates (a duplicate is a data present several times in an interval of 5 minutes) <br/><br/>
> - Beginning date <br/>
> Here, with the datepicker, you can select the beginning date of the first data in the CSV file you will download. By doing this, if you want to study an interval of time, you can by selecting the beginning date and the ending date. So, the selected datas will be the datas with an added date that is more than the date you have provided. <br/><br/>
> - Ending date <br/>
> Here, with the datepicker, you can select the ending date of the last data in the CSV file you will download. by doing this, if you want to study an interval of time, you can by selecting the beginning date and the ending date. So, the selected datas will be the datas with an ending date that is less than the date you have provided.

![alt text](./README_images/export_datas_options.png "Export of the datas with options to select")

If you select multiple options, multiple filters will be applied. If you select just one filter, this is the filter you have selected that will be applied. Finally, if you don't select any options, you will download all the datas that are present in the database without applying any filters.

##### Graphic
You can see the datas in graphic mode. With that, we can see the curves of the different datas. With the library we have used, we can select the curve(s) to display on the graphic. For example, here, we can deselect the curve we don't want and this modify directly the Y axe (as we can see on the pictures below). We can also have the real values of the datas when we pass the mouse on the graphic.

![alt text](./README_images/see_datas_graphic.png "See the datas of a microcontroller") <br/><br>
![alt text](./README_images/see_datas_graphic_deselect.png "See the datas of a microcontroller with one deselection") <br/><br>

You can also select the dates of the datas you want to display. For example, you can choose to only retrieve the datas between the 27th of April and the 28th of April. We will update the graphic and the pagination list with the datas we will get. If you have deselect some elements, when you will get the datas, these elements will be reselected. So you will need to deselect them again if you want to. ***It is important to note that you cannot select datas before the first date of sending and after the last date of sending of the microcontroller.***

![alt text](./README_images/see_graphic_between_dates.png "See the datas between dates of a microcontroller") <br/><br>
![alt text](./README_images/see_graphic_between_dates_deselect.png "See the datas between dates of a microcontroller with one deselection") <br/><br>

##### List with pagination
You can also see the datas in list mode with a pagination. Indeed, you can select the part of the list you want to see and you can go to the end or to the beginning directly by clicking on a button. On the picture below, we have selected the part 1 of the pagination and you can go to part 2 by clicking on the button "Suivant" (you can also go the previous part by clicking on "Précédent" => Here we are on part 1 so we cannot go to previous). The button "Début" is used to go to the beginning of the list and the button "Fin" is used to go to the end of the list.

![alt text](./README_images/see_datas_list.png "See the datas of a microcontroller") <br/><br>

When the user will choose to see datas between two dates, this pagination will be automatically updated with the datas the user will get from the request.

#### Update your credentials
If you want to update your credentials, it is possible. You can update the email address to use with the address you want, you need to put your old password, and you need to put your new one and validate it by putting it in another input. Finally, validate the captcha and validate the modifications. If all the modifications you have done are correct, you will have a message to this effect. But if the server found problems, he will warn you that you have problems to correct before validating the modifications.

This functionality is the same with the administrator access than the client one.

When you arrive on the page, you will see a page like the picture below :
![alt text](./README_images/update_no_form.png "Page displayed when you arrive on the page to update your credentials") <br/><br>

By clicking on the "Modifier mes informations" button, you will see that the form appears to have a picture like below:
![alt text](./README_images/update_form.png "Form to update your credentials") <br/><br>

You can put your new credentials and if want to stop, you can click on the "Annuler la modification" button and if you want to validate you can click on the green button. If you click on the yellow button to stop the modification, if you stay on this page, you will keep the informations you have entered but if you reload or quit the page, you will lost the informations you have entered in the form.

#### Administrator dashboard
With the adminstrator dashboard, the user that is admin can access to the logs of the server and modify the user's credentials. When you arrive on the page, you can display the informations on what you want by clicking on the circle with the arrow. We will display an alert with the addresses of the microcontrollers that have not sent data since 1 day (the administrators will also receive a mail from the server).

![alt text](./README_images/dashboard.png "View when you arrive on the dashboard")

##### Modify the user credential, Delete the user or Add a user
When you have clicked on the circle with the arrow on the right of "Gestion des utilisateurs", you will see a table that contains all the users stored in the database (as you can see on the picture below).

![alt text](./README/../README_images/dashboard_users.png "Dashboard with the displayed users")

When you can see the table, then you can select the user you want to modify or delete. By doing this, just click on the user you want to modify or delete.

![alt text](REAMD/../README_images/dashboard_user_select.png "Dashboard with a selected user to modify or delete")

With that, you can delete the account of the user by clicking on the button "Supprimer le compte". By clicking on this button, you will need to enter the mail of the user you want to delete and then confirm that you really want to delete the user.

![alt text](./README_images/dashboard_delete_account.png "Delete the account of the user")

If you just want to modify the user, you can modify the user by multiple ways :
> - Modify the role of the user <br/>
> By clicking on the slider "Modifier le role", you will be able to modify the role of the user. Select the role you want to put to the user and then click on "Modifier le compte". The user will be modify with the value you have entered. ![alt text](README_images/dashboard_user_modify_role.png "Modification of the role of the user") <br/><br/>
> - Modify the password of the user <br/>
> By clicking on the slider "Modifier le mot de passe", you will be able to modify the password of the user. You need to enter a password that respect the security policy and then confirm the new password of the user. By clicking on the button "Modifier le compte", you will save the modification of the password of the user with the new password you have entered. ![alt text](README_images/dashboard_user_modify_password.png "Modification of the password of the user") <br/><br/>
> - Modify the account state of the user <br/>
> By clicking on the slider "Modifier l'état du compte", you will be able to modify the state of the account of the user. You will be able to switch on or switch off the account of the user. With that, you can disable the access of the users you don't want them to access to the application. The advantage, here, is that the account is still stored in the database and only the administrator can switch it on. ![alt text](./README_images/dashboard_user_modify_state.png "Modification of the state of the account of the user") <br/><br/>
> - Modify multiple elements together <br/>
> By selected multiple elements to update (role, password or account state), you will be able to update all these informations in one step. By selecting the elements you want to modify with the different switches, you will be able to modify the user's values and then save the modification in the database. **NOTICE THAT THE VALUES THAT ARE NOT SELECTED WILL NOT BE MODIFIED.** ![alt text](./README_images/dashboard_modify_multiple_elements.png "Modification of multiple elements in the user account") <br/><br/>


You can deselect the selection you have made by clicking on the user you want to modify. All the modifications you have entered will not been taken into account. But if you just change the position of the sliders to modify the password or the role, the modification will stay in memory (the modifications are not taken into account on the server so you need to validate them).

You can also add a user by clicking on the button "Ajouter un utilisateur". When you have clicked on the button, you will see a modal and you will need to enter the email address, the password of the new user and re-confirm the password. When you have done that, you need to select the role of the user (client or admin) and the account state (account activated or not). When all these steps are done, you can click on the button "Ajouter" to be able to add the user. On the server, we will check the different inputs and we will check if the email is already used or not.

![alt text](./README_images/dashboard_add_user.png "Adding a user from the dashboard")


##### Access to the logs
When you have clicked on the circle with the arrow on the right of "Journal de requêtes", you will see a form to fill with the datas you want. You need to fill the URL, the HTTP code, the method (GET, POST, PUT or DELETE) inputs and select the first date and last date. All these informations are mandatory to be able to make a research. When all the informations are provided, you can click on the button "Rechercher" to search the requests that can match with the values you have entered. The server will select them and then it will return the values to the VueJS application. When you want to reinitialize quickly the inputs (for the URL, method and code), you have a little cross at the right of them to clean it. For the datepicker (first date and last date), you just need to click on it, and then click on the button "Réinitialiser".

![alt text](./README_images/dashboard_requests.png "Dashboard with the displayed logs")


#### Posts
When you go on the posts route, you will see all the posts made by the administrator. We will display the posts as they were formatted during their writings, you will see the title, the text, the person who has published the post and the published date of the post. When a post is modified, you will see a little message like "Modifié le ...".

![alt text](./README_images/posts.png "All the posts present in database")

You can add a post by clicking on the button "Ajouter un post". Here, you will need to enter the title of the post and the text of the post. The text of the post can be formatted as the administrator wants : we can add links to website, to add emails, change the text color or the background color, put text in bold, in italic, underline, we can put numbered list, todo list, images, etc …. When you have written your post, click on the button "Ajouter" to add a new post.

![alt text](./README_images/add_post.png "Add a new post to the database")

You can modify a post by clicking on the button "Modifier le post". By clicking on this button, you will see that the title and the text are already charged into the inputs and you just have to modify the lines you want. When you have finished to modify the post, you can click on the button "Modifier" to modify the post definitively.

![alt text](./REAMDE_images/../README_images/modify_post.png "Modification of a post")

When you want to delete a post, you can do that by clicking on the button "Supprimer le post". When you click on this button, you will see a message asking you to enter the title of the post to delete it. Enter the title of the post and click on "OK".

![alt text](./REAMDE_images/../README_images/delete_post.png "Delete a post")

#### Manage of the groups
When you arrive on the page, you will see all the possible groups. You will be able to add a new group or to configure the waking hours of the microcontrollers that have no groups.

![alt text](./REAMDE_images/../README_images/groups_management.png "Management of the groups")

By clicking on the button "Configurer le réveil des noeuds sans groupe", you will be able to configure the waking hours of the microcontrollers that have no groups. You will see a modal and you will be able to select the beginning hours, the wake-up interval and the wake-up interval between two microcontrollers.

![alt text](./REAMDE_images/../README_images/config_uc_without_goups.png "Configure the microcontrollers without groups")

When you want to add a group, click on "Ajouter un groupe". You will see a modal and you will need to enter the name of the group, the address of the relay to create, the beginning hours, the wake-up interval and the wake-up interval between two microcontrollers.

![alt text](./REAMDE_images/../README_images/add_group.png "Add of a group")

When you click on the button "Ajouter des microcontrôleurs", you will be able to select the microcontrollers you want to add in the group by using dropdown lists to select a range of addresses or by select one by one the microcontrollers.

![alt text](./REAMDE_images/../README_images/add_microcontrollers_to_group.png "Add microcontrollers to groups")

You can see the microcontrollers that are present in a group by clicking on the button "Liste des microcontrôleurs". You will be able to see the relay microcontroller and the end nodes that are present in the group.

![alt text](./REAMDE_images/../README_images/list_microcontrollers.png "List the microcontrollers of a group")

You can click on the button "Modifier" to be able to modify the informations of a group. To be able to apply the modifications, click on the button "Appliquer les heures de réveil à tous les microcontrôleurs du groupe".

![alt text](./REAMDE_images/../README_images/modify_group.png "Modify a group")

When you click on the button "Supprimer", you will need to enter the name of the group to be able to delete the group.

![alt text](./REAMDE_images/../README_images/delete_group.png "Delete a group")

### Authentified with client access
With these rights, you will be restricted on the application. **You will not be able to add, modify and delete microcontrollers. You will not be able to upload datas on the server or download microcontroller's file too.** You just will be able to see the datas via graphics or pagination and download the microcontrollers' datas. If you try to access to routes that need higher rights, you will see the picture below : 
![alt text](./README_images/not_the_rights.png "You have not the rights to access to this route") <br/><br>

#### Menu
This part will contain the menu to be able to navigate into our application. It contains less informations as the one we can have when we have the administrator rights. We have the title of the application and a little description. We have also several links to access to the home or to see the microcontrollers. The user has a button to logout (when a user logs out, all the informations stored about his session are deleted in database but also the cookies are deleted), a button to update his credentials and also a button to close the menu. The menu can be closed by clicking in the dark part too. <br/>

![alt text](./README_images/menu_not_admin.png "Menu of the application when you have a client access") <br/><br>

#### Microcontrollers
This part shows to the user all the microcontrollers that are stored in the database. We have multiple possibilities here : see the datas stored by one microcontroller, download all the datas of one microcontroller or see the position of all the microcontrollers. We have just the soft possibilities that is to say the ones that don't modify the database.

![alt text](./README_images/microcontrollers_not_admin.png "Microcontrollers stored in the database when we have client access") <br/><br>

##### Exports of the datas
When you click on the button "Exporter les données de microcontroller_addr", you will directly download the CSV file that will contain all the datas from a microcontroller. You will not exit the page when you will click on the button. The format is the following : 
> microcontroller_addr ; ph ; humidity ; temperature ; conductivity ; phosphorus ; potassium ; added_date

<br/>

##### Position of the microcontrollers
By clicking on the button "Afficher la position des microcontrôleurs", we will open a modal that will contain the map with all the position of the microcontrollers. When you will fly over a marker with your mouse, you will be able to see the address of the microcontroller that is in this position. To close the window, you have a cross at the top right.

![alt text](./README_images/microcontrollers_position_not_admin.png "Position of the microcontrollers on a map") <br/><br/>

#### See the datas
When you want to see the datas recording by a microcontroller, you have multiple choices to see that : a graphic or the real datas paginated in a list. You can also download the datas of the microcontroller as we can do on the microcontrollers page but here, the user can select some options to filter the datas he wants in the CSV file.


##### Exports of the datas (With options)
When you click on the button "Exporter les données de microcontroller_addr", you will see a modal appears that allows you to select or not the options to filter the datas you want to retrieve in the CSV. Here, you will be able to select the options you want to use to filter the datas. The options are the following :
> - Without duplicates <br/>
> By activating this filter, you will be able to retrieve datas without duplicates (a duplicate is a data present several times in an interval of 5 minutes) <br/><br/>
> - Beginning date <br/>
> Here, with the datepicker, you can select the beginning date of the first data in the CSV file you will download. By doing this, if you want to study an interval of time, you can by selecting the beginning date and the ending date. So, the selected datas will be the datas with an added date that is more than the date you have provided. <br/><br/>
> - Ending date <br/>
> Here, with the datepicker, you can select the ending date of the last data in the CSV file you will download. by doing this, if you want to study an interval of time, you can by selecting the beginning date and the ending date. So, the selected datas will be the datas with an ending date that is less than the date you have provided.

![alt text](./README_images/export_datas_options.png "Export of the datas with options to select")

If you select multiple options, multiple filters will be applied. If you select just one filter, this is the filter you have selected that will be applied. Finally, if you don't select any options, you will download all the datas that are present in the database without applying any filters.

##### Graphic
You can see the datas in graphic mode. With that, we can see the curves of the different datas. With the library we have used, we can select the curve(s) to display on the graphic. For example, here, we can deselect the curve we don't want and this modify directly the Y axe (as we can see on the pictures below). We can also have the real values of the datas when we pass the mouse on the graphic.

![alt text](./README_images/see_datas_graphic.png "See the datas of a microcontroller") <br/><br>
![alt text](./README_images/see_datas_graphic_deselect.png "See the datas of a microcontroller with one deselection") <br/><br>

You can also select the dates of the datas you want to display. For example, you can choose to only retrieve the datas between the 27th of April and the 28th of April. We will update the graphic and the pagination list with the datas we will get. If you have deselect some elements, when you will get the datas, these elements will be reselected. So you will need to deselect them again if you want to. ***It is important to note that you cannot select datas before the first date of sending and after the last date of sending of the microcontroller.***

![alt text](./README_images/see_graphic_between_dates.png "See the datas between dates of a microcontroller") <br/><br>
![alt text](./README_images/see_graphic_between_dates_deselect.png "See the datas between dates of a microcontroller with one deselection") <br/><br>

##### List with pagination
You can also see the datas in list mode with a pagination. Indeed, you can select the part of the list you want to see and you can go to the end or the beginning directly by clicking on a button. On the picture below, we have selected the part 5 of the pagination and you can go from part 4 or 6 by clicking on the button "Précédent" or "Suivant". The button "Début" is used to go to the beginning of the list and the button "Fin" is used to go to the end of the list.

![alt text](./README_images/see_datas_list.png "See the datas of a microcontroller")

When the user will choose to see datas between two dates, this pagination will be automatically updated with the datas the user will get from the request.

#### Update your credentials
If you want to update your credentials, it is possible. You can update the email address to use with the address you want, you need to put your old password, and you need to put your new one and validate it by putting it in another input. Finally, validate the captcha and validate the modifications. If all the modifications you have done are correct, you will have a message to this effect. But if the server found problems, he will warn you that you have problems to correct before validating the modifications.

This functionality is the same with the administrator access than the client one.

When you arrive on the page, you will see a page like the picture below :
![alt text](./README_images/update_no_form_not_admin.png "Page displayed when you arrive on the page to update your credentials") <br/><br>

By clicking on the "Modifier mes informations" button, you will see that the form appears to have a picture like below:
![alt text](./README_images/update_form_not_admin.png "Form to update your credentials") <br/><br>

You can put your new credentials and if want to stop, you can click on the "Annuler la modification" button and if you want to validate you can click on the green button. If you click on the yellow button to stop the modification, if you stay on this page, you will keep the informations you have entered but if you reload or quit the page, you will lost the informations you have entered in the form.