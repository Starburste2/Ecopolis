/**
* Plugin for the authentication of a user
*/
export default {
  install: (Vue, { strategy }) => {
    Vue.prototype.$auth = new Vue({
      /**
      * @returns the user connected or null if no one is connected
      */
      data () {
        return {
          user: null
        }
      },

      methods: {
        /**
        * Function to set the state of the user (if he is connected or not)
        * @param user => State of the user (a JSON if a user is connected ; null otherwise)
        */
        setUser (user) {
          this.user = user
        },

        /**
        * Function to know if a user is connected => Used when a user tries to access to a view (reload the page)
        * Check also if the user has still an account when he is connected and he reloads the page
        * => Security => If a user has no account and he can access to all the datas => :(
        * @returns the user if all the informations are correct ; null otherwise
        */
        init () {
          return strategy.getConnectedUser()
            .then(user => {
              this.setUser(user) // Set the user with the possible one we have in the cookies

              // Test if the user has still his account
              if (user) {
                return strategy.loginWithToken(user)
                  .then(testUser => {
                    // We have tried to authentified the user with no success => Delete the elements in the cookie
                    // No success => No user with this token or token expired  OR  IP in token ≠ IP of the machine that send the token  OR  -1 for error
                    if (!testUser || testUser.message === 'IP incorrect' || testUser === -1) {
                      this.logout() // No user with email and password => Logout the last user connected
                      return null

                    // We have authentified the user => Set the user and return it
                    } else {
                      this.setUser(testUser) // If we have one => Get the possible updated values
                      return testUser
                    }
                  })
                  .catch(error => console.log(error))

              // If no error => Force logout => Possibility of corrupted cookie
              } else {
                this.logout()
              }
            })
            .catch(error => {
              this.user = null // Set to null when error
              console.log(error)
            })
        },

        /**
        * Function to identify the user
        * @param {*} email => Email entered by the user
        * @param {*} password => Password entered by the user
        * @return the result of the authentication (1 if we can transact with the server ; -1 otherwise)
        */
        login (email, password) {
          return strategy.login(email.toLowerCase(), password)
            .then(user => {
              if (user === -1) return -1 // No user => Return -1

              // If no user found => Set user to null and return
              if (user == null) {
                this.setUser(null)
                return
              }

              // If the user contains informations
              if (user !== null) {
                // Account is not activated => Set the user to null and return error code -2
                if (user.message === 'Account not activated') {
                  this.setUser(null)
                  return -2
                }
              }

              this.setUser(user) // Set the user
              return 1
            })
            .catch(error => {
              this.user = null // Set to null when error
              console.log(error)
            })
        },

        /**
        * Function to logout the user
        * @returns the state of the user (JSON if he is connected ; null otherwise)
        */
        logout () {
          return strategy.logout()
            .then(() => this.setUser(null))
            .catch(error => {
              this.user = null // Set to null when error
              console.log(error)
            })
        },

        /**
        * Function to get if a user is already connected or not
        * @returns the state of the user (JSON if he is connected ; null otherwise)
        */
        getConnectedUser () {
          return strategy.getConnectedUser()
            .then(user => this.setUser(user))
            .catch(error => {
              this.user = null // Set to null when error
              console.log(error)
            })
        },

        /**
        * Function used to create an account
        * @param {*} email => Email entered by the user
        * @param {*} password => Password entered by the user
        * @param {*} captchaToken => Token got from the captcha
        * @returns the response of the promise of createAccount()
        */
        createAccount (email, password, captchaToken) {
          return strategy.createAccount(email.toLowerCase(), password, captchaToken)
            .then(response => {
              return response
            })
            .catch(error => console.log(error))
        },

        /**
        * Function used to modify a user
        * @param {*} email => New email provided by the used
        * @param {*} oldPassword => Old password of the user
        * @param {*} password => New password provided by the user
        * @param {*} captchaToken => Value of the captcha clicked by the user
        * @returns the response from strategy
        */
        modifyAccount (email, oldPassword, password, captchaToken) {
          return strategy.modifyAccount(email.toLowerCase(), oldPassword, password, captchaToken, this.user)
            .then(response => {
              // If the datas returned by the strategy contains the informations we want
              if (response.email !== undefined && response.role !== undefined && response.session_ID !== undefined && response.authenticationToken !== undefined && response.csrfToken !== undefined) {
                this.setUser(response) // Store the new informations about the user
              }

              return response // Return the response
            })
            .catch(error => console.log(error))
        },

        /**
         * Function used to modify a user when the connected user is an administrator
         * @param {*} email => Email of the user to modify
         * @param {*} newPassword => New password entered for the user
         * @param {*} newRole => New role entered for the user
         * @param {*} newAccountState => New account state entered for the user
         * @param {*} modificationOfPassword => Boolean to know if we want to modify the password of the user
         * @param {*} modificationOfRole => Boolean to know if we want ot modify the role of the user
         * @param {*} modificationOfAccountState => Boolean to know if we want ot modify the account state of the user
         * @returns 1 if the user has been modified ; the error message otherwise
         */
        updateInfosUserByAdmin (email, newPassword, newRole, newAccountState, modificationOfPassword, modificationOfRole, modificationOfAccountState) {
          return strategy.updateInfosUserByAdmin(email.toLowerCase(), newPassword, newRole, newAccountState, modificationOfPassword, modificationOfRole, modificationOfAccountState, this.user)
            .then(response => {
              if (response.message === 'OK') return 1 // Return 1 => The user has well been modified
              else return response.message // Return the message if we have a problem before
            })
            .catch(error => console.log(error))
        },

        /**
         * Function used to delete a user when the connected user is an administrator
         * @param {*} idUser => ID of the user to delete
         * @returns 1 if the user has been deleted ; the error message otherwise
         */
        deleteUserByAdmin (idUser) {
          return strategy.deleteUserByAdmin(idUser, this.user)
            .then(response => {
              if (response.message === 'OK') return 1 // Return 1 => The user has been deleted
              else return response.message // Return the message if we have a problem before
            })
            .catch(error => console.log(error))
        },

        /**
         * Function used to activate the account of the user
         * @param {*} activationToken => Token to activate the account
         * @returns 1 if the account has been activated ; the error message otherwise
         */
        activateUserAccount (activationToken) {
          return strategy.activateUserAccount(activationToken)
            .then(response => {
              if (response.message === 'OK') return 1 // Return 1 => The user account has been acticvated
              else return response.message // Return the message if we have a problem before
            })
            .catch(error => console.log(error))
        },

        /**
         * Function used to be able to check if we have a user with the email entered
         * @param {*} email => Email of the account of the user
         * @returns 1 if the account exists + email sent to it ; the error otherwise
         */
        sendEmailForReinitPassword (email) {
          return strategy.sendEmailForReinitPassword(email.toLowerCase())
            .then(response => {
              if (response.message === 'OK') return 1 // Return 1 => The modification of the passwd has been taken into account
              else return response.message // Return the message if we have problem before
            })
            .catch(error => console.log(error))
        },

        /**
         * Function used to be able to know if the user can access to the form to update the passsword
         * @param {*} token => Token given in the URL of the mail by the server
         * @returns the response (it contains data) if the token is associated to a user ; the error otherwise
         */
        checkAccessToModifyForgottenPassRoute (token) {
          return strategy.checkAccessToModifyForgottenPassRoute(token)
            .then(response => {
              if (response.message === 'OK') return response // Return the complete response if it's good
              else return response.message // Return the message if we have a problem before
            })
            .catch(error => console.log(error))
        },

        /**
         * Function used to be able to update the forgotten password of the user
         * @param {*} email => Email of the account of the user
         * @param {*} password => New password entered by the user
         * @param {*} token => Token given by the server
         * @returns 1 if the password has been updated ; the error otherwise
         */
        modifyForgottenPassword (email, password, token) {
          return strategy.modifyForgottenPassword(email.toLowerCase(), password, token)
            .then(response => {
              if (response.message === 'OK') return 1 // Return 1 => The modification of the password is good
              else return response.message
            })
            .catch(error => console.log(error))
        },

        /**
         * Function used to be able to add a user when we are administrator
         * @param {*} email => Email of the new user
         * @param {*} password => Password of the new user
         * @param {*} role => Role of the new user (client or admin)
         * @param {*} isAccountActivated => State of the account of the new user (activated or not)
         * @returns the response if the user has been added (infos of the user) ; the error otherwise
         */
        createUserByAdmin (email, password, role, isAccountActivated) {
          return strategy.createUserByAdmin(email.toLowerCase(), password, role, isAccountActivated, this.user)
            .then(response => {
              // Return the response of the server to the view
              if (response._id !== undefined && response.email !== undefined && response.isActivated !== undefined && response.role !== undefined) return response
              else return response.message
            })
            .catch(error => console.log(error))
        }
      }
    })
  }
}
