import axios from 'axios'
import getInfosServer from '../../config/getURLNode'
import getAPIKey from '../../config/getAPIKey'
import VueCookie from 'vue-cookie'

/**
 * @param users => Users which can be authenticated
 * @param lsKey => Key to know if the user is already connected
 */
export default function getAuthentication (lsKey = 'user') {
  return {
    /**
    * Function to know if a user is already connected
    * @returns the user if he is connected ; null otherwise
    */
    getConnectedUser () {
      var user

      // If we have a cookie already stored
      if (VueCookie.get(lsKey)) {
        user = JSON.stringify({ authenticationToken: VueCookie.get(lsKey) }) // Set the user with the authentication token before the re-authentication on the server

      // Else we set the user to null
      } else {
        user = null
      }

      return Promise.resolve(user ? JSON.parse(user) : null)
    },

    /**
    * Function to test if we have a user with the email and password entered
    * @param {*} email => Email entered by the user
    * @param {*} password => Password entered by the user
    * @returns the user ; null otherwise -- OR -- Error send by the server ; -1 if network error
    */
    login (email, password) {
      return axios
        .post(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user',
          {
            email: email,
            password: password
          },
          {
            headers: {
              apikey: getAPIKey().getKey()
            }
          }
        )
        .then(response => {
          // If we have a response that containing datas
          if (response.data != null) {
            if (response.data.message === 'Account not activated') return -2 // Account not activated => Return error

            // Creation of the user to store in the variable of the application
            var user = {
              session_ID: response.data.sessionID,
              authenticationToken: response.data.authenticationToken,
              csrfToken: response.data.csrf_token,
              email: response.data.email,
              role: response.data.role
            }

            VueCookie.set(lsKey, user.authenticationToken, { expires: 7 }) // Store the authentication token in the cookie with an expiration date of 7 days

            return user // Return the user
          }

          return null // Return null because no user
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return -1
        })
    },

    /**
    * Function used to identified a user with his authentication token
    * @param {*} user => Use we have in cookies
    * @returns a user if he is authentified ; null otherwise
    */
    loginWithToken (user) {
      return axios
        .post(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/login',
          {
            token: user.authenticationToken
          },
          {
            headers: {
              apikey: getAPIKey().getKey()
            }
          }
        )
        .then(response => {
          if (response.data != null) {
            // Variable to store in the variable of the application
            var user = {
              session_ID: response.data.sessionID,
              authenticationToken: response.data.authenticationToken,
              csrfToken: response.data.csrf_token,
              email: response.data.email,
              role: response.data.role
            }

            VueCookie.set(lsKey, user.authenticationToken, { expires: 7 }) // Store the authentication token in the cookie with an expiration date of 7 days

            return user // Return the user
          }

          return null
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return -1
        })
    },

    /**
    * Function to logout the user
    * @returns a promise for deleting
    */
    logout () {
      VueCookie.delete(lsKey) // Remove the cookie
      return Promise.resolve() // Return
    },

    /**
    * Function used to register a new account in the database
    * @param {*} email => Email entered by the user
    * @param {*} password => Password entered by the user
    * @param {*} captchaToken => Token got from the captcha
    * @returns the response or the error (wrong request) from the server if it is available ; an another error otherwise
    */
    createAccount (email, password, captchaToken) {
      return axios
        .post(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/add',
          {
            email: email,
            password: password,
            'g-recaptcha-response': captchaToken
          },
          {
            headers: {
              apikey: getAPIKey().getKey()
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
     * Function used to be able to make an HTTP request to modify a user
     * The user is modified and we get all the new important values generated by the server :
     *   - Authentication token
     *   - Session ID
     *   - CSRF token
     * @param {*} email => New email provided by the user
     * @param {*} oldPassword => Old password of the user
     * @param {*} password => New password provided by the user
     * @param {*} captchaToken => Value of the captcha clicked by the user
     * @param {*} user => Actual informations about the user
     * @returns the response got from the server (formated with the good elements if we have the important informations)
     */
    modifyAccount (email, oldPassword, password, captchaToken, user) {
      return axios
        .put(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/modify',
          {
            email: email,
            oldPassword: oldPassword,
            password: password,
            'g-recaptcha-response': captchaToken,
            token: user.authenticationToken,
            last_email: user.email.toLowerCase()
          },
          {
            headers: {
              Authorization: user.session_ID,
              CSRFToken: user.csrfToken
            }
          }
        )
        .then(response => {
          if (response.data.email !== undefined && response.data.role !== undefined && response.data.sessionID !== undefined && response.data.authenticationToken !== undefined && response.data.csrf_token !== undefined) {
            // Variable to store in the variable of the application
            var user = {
              session_ID: response.data.sessionID,
              authenticationToken: response.data.authenticationToken,
              csrfToken: response.data.csrf_token,
              email: response.data.email,
              role: response.data.role
            }

            VueCookie.set('user', user.authenticationToken, { expires: 7 }) // Store the new informations in the cookie

            return user // Return the user
          }

          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
     * Function used to be able to make the HTTP request to modify a user when the connected user is an administrator
     * @param {*} email => Email of the user to modify
     * @param {*} newPassword => New password of the user
     * @param {*} newRole => New role of the user
     * @param {*} newAccountState => New account state of the user
     * @param {*} modificationOfPassword => Boolean to know if we want to modify the password of the user
     * @param {*} modificationOfRole => Boolean to know if we want to modify the role of the user
     * @param {*} modificationOfAccountState => Boolean to know if we want to modify the account state of the user
     * @param {*} user => Actual informations about the admin
     * @returns the response of the server  OR  the error if a problem appears
     */
    updateInfosUserByAdmin (email, newPassword, newRole, newAccountState, modificationOfPassword, modificationOfRole, modificationOfAccountState, user) {
      return axios
        .put(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/modifyByAdmin',
          {
            email: email,
            password: newPassword,
            modifyPassword: modificationOfPassword,
            role: newRole,
            modifyRole: modificationOfRole,
            accountState: newAccountState,
            modifyState: modificationOfAccountState
          },
          {
            headers: {
              Authorization: user.session_ID,
              CSRFToken: user.csrfToken
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
     * Function used to be able to make the HTTP request to delete a user when the connected user is an administrator
     * @param {*} idUser => ID of the user to delete
     * @param {*} user => Connected user
     * @returns the response of the server  OR  the error if a problem appears
     */
    deleteUserByAdmin (idUser, user) {
      return axios
        .delete(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/deleteByAdmin/' + idUser,
          {
            headers: {
              Authorization: user.session_ID,
              CSRFToken: user.csrfToken
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
    * Function used to be able to make the HTTP request to activate the account of a user
    * @param {*} activationToken => Activation token to activate the account
    * @returns the response of the server  OR  the error if a problem appears
    */
    activateUserAccount (activationToken) {
      return axios
        .post(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/activate-account',
          {
            activationToken: activationToken
          },
          {
            headers: {
              apikey: getAPIKey().getKey()
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
     * Function used to be able to make the HTTP request to send the email to reinit the password of the user
     * @param {*} email => Email of the account entered by the user
     * @returns the response of the server  OR  the error if a problem appears
     */
    sendEmailForReinitPassword (email) {
      return axios
        .post(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/email-reinit-passwd',
          {
            email: email
          },
          {
            headers: {
              apikey: getAPIKey().getKey()
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
     * Function used to be able to make the HTTP request to be able to know if the user can access to the form to modify the forgotten password
     * @param {*} token => Token given in the URL of the email
     * @returns the response of the server  OR  the error if a problem appears
     */
    checkAccessToModifyForgottenPassRoute (token) {
      return axios
        .get(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/access-modify-passwd/' + token,
          {
            headers: {
              apikey: getAPIKey().getKey()
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
     * Function used to be able to make the HTTP request to update the forgotten password to the new one
     * @param {*} email => Email of the account entered by the user
     * @param {*} password => New password of the user
     * @param {*} token => Token given by the server to update the forgotten password of the user
     * @returns the response of the server  OR  the error if a problem appears
     */
    modifyForgottenPassword (email, password, token) {
      return axios
        .put(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/modify-forgotten-passwd',
          {
            email: email,
            password: password,
            token: token
          },
          {
            headers: {
              apikey: getAPIKey().getKey()
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    },

    /**
     * Function used to be able to make the HTTP request to add a new user in the database
     * @param {*} email => Email of the new user
     * @param {*} password => Password of the new user
     * @param {*} role => Role of the new user (client or admin)
     * @param {*} isAccountActivated => State of the account of the new user (activated or not)
     * @param {*} user => Actual informations about the admin
     * @returns the response of the server  OR  the error if a problem appears
     */
    createUserByAdmin (email, password, role, isAccountActivated, user) {
      return axios
        .post(getInfosServer().getURL() + getInfosServer().getPort() + '/api/user/addByAdmin',
          {
            email: email,
            password: password,
            role: role ? 'admin' : 'client',
            isAccountActivated: isAccountActivated
          },
          {
            headers: {
              Authorization: user.session_ID,
              CSRFToken: user.csrfToken
            }
          }
        )
        .then(response => {
          return response.data
        })
        .catch(error => {
          if (error.response != null) return error.response.data

          return error
        })
    }
  }
}
