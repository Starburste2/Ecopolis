/**
* Function to do the animation of the not the rights error
* Animation based on : https://codepen.io/Ahmed_B_Hameed/pen/LkqNmp
* Same animation as 404 error
* @returns the function to launch the animation for the not the rights error
*/
export default function launchNTR () {
  return {
    /**
     * Function to launch the animation for the not the rights error
     */
    launch () {
      var loop1 = 30 // Time for loop1
      var loop2 = 30 // Time for loop2
      var loop3 = 30 // Time for loop3
      var time = 20 // Time for each animation
      var i = 0 // Number of iterations
      var selector1 = document.querySelector('.firstDigit') // First digit to display
      var selector2 = document.querySelector('.secondDigit') // Second digit to display
      var selector3 = document.querySelector('.thirdDigit') // Third digit to display

      /**
      * Animation to display the N value
      */
      loop3 = setInterval(function () {
        'use strict'
        if (i > 40) {
          clearInterval(loop3) // Stop the timer
          selector3.textContent = 'N' // Put the N as value
        } else {
          selector3.textContent = String.fromCharCode(Math.floor(Math.random() * 26) + 65) // Put a random lowercase letter
          i++ // Increment the number of iterations
        }
      }, time)

      /**
      * Animation to display the O value
      */
      loop2 = setInterval(function () {
        'use strict'
        if (i > 80) {
          clearInterval(loop2) // Stop the timer
          selector2.textContent = '0' // Put the O as value
        } else {
          selector2.textContent = String.fromCharCode(Math.floor(Math.random() * 26) + 65) // Put a random lowercase letter
          i++ // Increment the number of iterations
        }
      }, time)

      /**
      * Animation to display the N value
      */
      loop1 = setInterval(function () {
        'use strict'
        if (i > 100) {
          clearInterval(loop1) // Stop the timer
          selector1.textContent = 'N' // Put the N as value
        } else {
          selector1.textContent = String.fromCharCode(Math.floor(Math.random() * 26) + 65) // Put a random lowercase letter
          i++ // Increment the number of iterations
        }
      }, time)
    }
  }
}
