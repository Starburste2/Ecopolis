import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Microcontrollers from '@/components/Microcontrollers'
import ModifyMicrocontroller from '@/components/ModifyMicrocontroller'
import AddMicrocontroller from '@/components/AddMicrocontroller'
import ShowDatas from '@/components/ShowDatas'
import PageNotFound from '@/components/PageNotFound'
import NotTheRights from '@/components/NotTheRights'
import MyAccount from '@/components/MyAccount'
import Dashboard from '@/components/Dashboard'
import VerifyAccount from '@/components/VerifyAccount'
import ModifyForgottenPasswd from '@/components/ModifyForgottenPasswd'
import Posts from '@/components/Posts'
import Groups from '@/components/Groups'

Vue.use(Router)

export default new Router({
  routes: [
    // Main page
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    // Show microcontrollers
    {
      path: '/microcontrollers',
      name: 'Microcontrollers',
      component: Microcontrollers
    },
    // Modify one microcontroller
    {
      path: '/microcontroller/:id',
      name: 'ModifyMicrocontroller',
      component: ModifyMicrocontroller
    },
    // Add one microcontroller
    {
      path: '/addMicrocontroller',
      name: 'AddMicrocontroller',
      component: AddMicrocontroller
    },
    // Show datas of one microcontroller
    {
      path: '/showDatas/:addr',
      name: 'ShowDatas',
      component: ShowDatas
    },
    // Modify the user's account
    {
      path: '/my-account',
      name: 'MyAccount',
      component: MyAccount
    },
    // Access to dashboard
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    // Access to the activate account
    {
      path: '/verify-account/:token',
      name: 'VerifyAccount',
      component: VerifyAccount
    },
    // Access to the modify forgotten passwd
    {
      path: '/modify-forgotten-passwd/:token',
      name: 'ModifyForgottenPasswd',
      component: ModifyForgottenPasswd
    },
    // Access to the posts
    {
      path: '/posts',
      name: 'Posts',
      component: Posts
    },
    // Access to the groups
    {
      path: '/groups',
      name: 'Groups',
      component: Groups
    },
    // 404 not found
    {
      path: '/404-not-found',
      name: 'PageNotFound',
      component: PageNotFound
    },
    // Not the rights
    {
      path: '/not-the-rights',
      name: 'NotTheRights',
      component: NotTheRights
    },
    // Others
    {
      path: '*',
      redirect: '/404-not-found'
    }
  ]
})
