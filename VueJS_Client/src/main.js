// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import AuthWrapper from './components/AuthWrapper'
import AuthPlugin from './plugins/Authentication/pluginAuthentication'
import getAuthentication from './plugins/Authentication/authentication'

Vue.use(BootstrapVue) // Use BootstrapVue
Vue.config.productionTip = false

// Use the authentication plugin
Vue.use(AuthPlugin, { strategy: getAuthentication() })

// Render the view with the authentication
new Vue({
  render: h => h(AuthWrapper),
  router
}).$mount('#app')
