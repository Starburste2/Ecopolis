/**
* @returns the CSRF Token key for encryption
*/
exports.getCSRFKey = function () {
  return '$2b$10$OHGl7r/wZonSIIqTwNk3aur9NVV9tICsfBV71AYUh/yrpIbfFq3CK'
}