/**
 * Function to be able to get the Recaptcha site key provided by Google
 * @returns the site key for the Recaptcha package
 */
exports.getSiteKey = function() {
    return '6Lfa6t0aAAAAALaEuuqcQSwxOf0H8NFPwR89vW0L'
}


/**
 * Function to be able to get the Recaptcha secret key provided bu Google
 * @returns the secret key for the Recaptcha package
 */
exports.getSecretKey = function() {
    return '6Lfa6t0aAAAAAP4y8qX0b1IABXDZMJphpNizYQQm'
}