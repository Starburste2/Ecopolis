/**
* @returns the salt to encrypt the password
*/
exports.getSalt = function () {
  return '$2b$10$.EQnwKPZYcOKzaRRVcd9U.'
}