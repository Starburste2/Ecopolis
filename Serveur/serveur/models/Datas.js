const mongoose = require('mongoose'); // Import the mongoose module for MongoDB database


// Schema for the datas
const data = mongoose.Schema({
    addrMicrocontroller: { 
      type: String, 
      required: true 
    },

    ph: { 
      type: Number,
      required: true
    },

    humidity: { 
      type: Number, 
      required: true 
    },
  
    temperature: { 
      type: Number,
      required: true
    },

    conductivity: {
        type: Number,
        required: true
    },

    nitrogen: { 
      type: Number, 
      required: true 
    },

    phosphorus: { 
      type: Number, 
      required: true 
    },

    potassium: { 
      type: Number, 
      required: true 
    },

    dateAdded: {
      type: Date,
      required: true
    },

    dateModification: {
      type: Date,
      required: true
    },

    idMsg: {
      type: Number,
      required: true
    }
});


// Export the model
module.exports = mongoose.model('Datas', data);