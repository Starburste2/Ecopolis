const mongoose = require('mongoose'); // Import the mongoose module for MongoDB database


// Schema for the datas
const post = mongoose.Schema({
    title: { 
      type: String, 
      required: true 
    },
  
    text: { 
      type: String,
      required: true
    },

    addedBy: {
      type: String,
      required: true
    },

    dateAdded: {
      type: Date,
      required: true
    },

    dateModification: {
      type: Date,
      required: true
    }
});


// Export the model
module.exports = mongoose.model('Post', post);