const mongoose = require('mongoose'); // Import the mongoose module for MongoDB database


// Schema for the user
const user = mongoose.Schema({
    email: { 
      type: String, 
      required: true 
    },
  
    password: { 
      type: String,
      required: true
    },

    sessionID: {
      type: String,
      required: false
    },

    dateSession: {
      type: Date,
      required: false
    },

    authenticationToken: {
      type: String,
      required: false
    },
    
    dateAuthentication: {
      type: Date,
      required: false
    },

    csrfToken: {
      type: String,
      required: false
    },
    
    dateCsrf: {
      type: Date,
      required: false
    },

    role: {
      type: String,
      required: true
    },

    isActivated: {
      type: Boolean,
      required: true
    },

    activationToken: {
      type: String,
      required: false
    },

    tokenRouteForgottenPasswd: {
      type: String,
      required: false
    },

    dateTokenRouteForgottenPasswd: {
      type: Date,
      required: false
    },

    tokenForgottenPasswd: {
      type: String,
      required: false
    },

    dateTokenForgottenPasswd: {
      type: Date,
      required: false
    }
});


// Export the model
module.exports = mongoose.model('User', user);