const mongoose = require('mongoose'); // Import the mongoose module for MongoDB database


// Schema for the datas
const poolAddress = mongoose.Schema({
    addr: { 
      type: String, 
      required: true 
    },
  
    isAvailable: { 
      type: Boolean,
      required: true
    },
});


// Export the model
module.exports = mongoose.model('PoolAddress', poolAddress);