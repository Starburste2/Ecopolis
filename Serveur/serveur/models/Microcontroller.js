const mongoose = require('mongoose'); // Import the mongoose module for MongoDB database


// Schema for a microcontroller
const microcontroller = mongoose.Schema({
    addr: { 
      type: String, 
      required: true 
    },
  
    sleepTime: { 
      type: [String],
      required: true
    },
    
    hasConfigChanged: { 
      type: Boolean, 
      required: true 
    },

    manualReconfiguration: {
      type: Boolean,
      required: false
    },

    isEndNode: { 
      type: Boolean, 
      required: true 
    },

    creationDate: {
      type: Date,
      required: true
    },

    modificationDate: {
      type: Date,
      required: true
    },

    latitude: {
      type: Number,
      required: false
    },
 
    longitude: {
      type: Number,
      required: false
    },

    groups: {
      type: String,
      required: false
    },

    destinationGroup: {
      type: String,
      required: false
    },

    isRelay: {
      type: Boolean,
      required: false
    },

    lastSending: {
      type: Date,
      required: false
    }
});


// Export the model
module.exports = mongoose.model('Microcontroller', microcontroller);