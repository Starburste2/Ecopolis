const mongoose = require('mongoose'); // Import the mongoose module for MongoDB database


// Schema for the datas
const APIKey = mongoose.Schema({
    UUID: {
        type: String,
        required: true
    },

    key: {
        type: String,
        required: true
    },

    applicationName: {
        type: String,
        required: true
    }
});


// Export the model
module.exports = mongoose.model('APIKey', APIKey);