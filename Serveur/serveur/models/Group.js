const mongoose = require('mongoose'); // Import the mongoose module for MongoDB database


// Schema for a microcontroller
const group = mongoose.Schema({
    name: { 
      type: String, 
      required: true 
    },
  
    referingNode: { 
      type: String,
      required: true
    },

    wakingHours: {
      type: [String],
      required: true
    },

    beginningHour: {
      type: String,
      required: true
    },

    delayBetweenHours: {
      type: String,
      required: true
    },

    delayBetween2uCInS: {
      type: Number,
      required: true
    }
});


// Export the model
module.exports = mongoose.model('Group', group);