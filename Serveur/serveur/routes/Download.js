const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to check the session ID
const CSRFMiddleware = require('../middlewares/checkCSRFTokens'); // Import the middleware to check the CSRF tokens
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the user can access the route
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the user has his account activated or not

const DownloadController = require("../controllers/Download/Download"); // Import the download controller

router.get("/microcontrollerFile/:addr/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, DownloadController.downloadMicrocontrollerFile); // Route to download microcontroller files
router.get("/microcontrollerDatas/:addr/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFMiddleware.requireValidCSRFTokens, DownloadController.downloadMicrocontrollerDatas); // Route to download microcontroller datas
router.get("/allMicrocontrollersDatas", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFMiddleware.requireValidCSRFTokens, DownloadController.downloadAllMicrocontrollersData); // Route to download all the microcontrollers datas
router.get("/serverCertificate/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, DownloadController.downloadServerCertificate); // Route to download server's certificate to put it on the SD of the main node

module.exports = router; // Export the router