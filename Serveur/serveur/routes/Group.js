const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to check the session ID
const CSRFTokenMiddleware = require('../middlewares/checkCSRFTokens'); // Import the middleware to check the CSRF tokens
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the user can access the routes
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the account of the user is activated or not
const checkObjectIdParam = require('../middlewares/checkObjectIDParam'); // Import the middleware to check the addr passed in parameter

const GroupController = require("../controllers/Group/Group"); // Import the groups controller

router.get("/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, GroupController.getAllGroups); // Route to get the groups
router.get("/withoutAllInfo", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, GroupController.getGroupsWithoutAllInformations); // Route to get the groups without all the informations of the DB
router.get("/getForReferences", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, GroupController.getMicrocontrollersForReferences); // Route to get the microcontrollers which can be refering
router.get("/getMicrocontrollersGroup", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, GroupController.getMicrocontrollersOfAGroup); // Route to get the microcontrollers of a group
router.get("/getMicrocontrollersWithoutGroups", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, GroupController.getMicrocontrollersWithNoGroup); // Route to get all the microcontrollers that don't have a group
router.get("/applyConfigToAllUc", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, GroupController.updateMicrocontrollersConfig); // Route to update the config of all the microcontrollers in the group
router.get("/applyConfigToAllUcOfMainNodeGroup", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, GroupController.updateMainNodeGroupMicrocontrollersConfig); // Route to update the config of all the microcontrollers of the main node's group
router.post("/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, CSRFTokenMiddleware.requireValidCSRFTokens, GroupController.addGroup); // Route to add a group
router.post("/addMicrocontrollersToGroup", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, CSRFTokenMiddleware.requireValidCSRFTokens, GroupController.addListOfMicrocontrollers); // Route to add a list of microcontrollers in the group
router.put("/modify/:id", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, CSRFTokenMiddleware.requireValidCSRFTokens, checkObjectIdParam.requireValidMongooseObjectID, GroupController.modifyGroup); // Route to modify a group
router.delete("/delete/:id", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, CSRFTokenMiddleware.requireValidCSRFTokens, checkObjectIdParam.requireValidMongooseObjectID, GroupController.deleteGroup); // Route to delete a group

module.exports = router; // Export the router