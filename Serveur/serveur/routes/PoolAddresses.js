const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to check the session ID
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the user can access the routes
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the account of the user is activated or not

const PoolAddressesController = require("../controllers/PoolAddress/PoolAddresses"); // Import the pool address controller

router.get("/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, PoolAddressesController.getAvailableAddresses); // Route to get the available addresses

module.exports = router; // Export the router