const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to check the session ID
const CSRFTokenMiddleware = require('../middlewares/checkCSRFTokens'); // Import the middleware to check the CSRF tokens
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the user can access the route
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Impor the middleware to check if the user has an account that is activated

const UploadController = require("../controllers/Upload/Upload"); // Import the upload controller

router.post("/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, UploadController.uploadDatas); // Route to upload datas file

module.exports = router; // Export the router