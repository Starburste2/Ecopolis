const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const APIKeyMiddleware = require('../middlewares/checkAPIKey'); // Import the middleware to check if we have an API key passed in parameter
const RecaptchaMiddleware = require('../middlewares/checkRecaptcha'); // Import Recaptcha middleware to check the Recaptcha token
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to verify the session IDs
const CSRFTokenMiddleware = require('../middlewares/checkCSRFTokens'); // Import the middleware to verify the CSRF tokens
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the request is made by an admin
const checkObjectIdParam = require('../middlewares/checkObjectIDParam'); // Import the middleware to check the id passed in parameter => Not return the type of database with the catch error
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the account of the user is activated or not

const UserController = require("../controllers/User/User"); // Import the user controller

router.post("/", APIKeyMiddleware.requireValidAPIForVueJSMainApp, UserController.login); // Route to identify a user from the VueJS main app
router.post("/login", APIKeyMiddleware.requireValidAPIForVueJSMainApp, UserController.loginWithToken); // Route to identify a user via a token via the VueJS main app
router.post("/logout", APIKeyMiddleware.requireValidAPIForVueJSMainApp, UserController.logout); // Route to logout a user vie the VueJs main app
router.post("/add", APIKeyMiddleware.requireValidAPIForVueJSMainApp, RecaptchaMiddleware.requireValidRecaptcha, UserController.addUser); // Route to add a user via the VueJS main app
router.put("/modify", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, RecaptchaMiddleware.requireValidRecaptcha, UserController.modifyUser); // Route to modify user's connection datas
router.get('/getAllUsers', SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, UserController.getAllUsersForDashboard); // Route to get all the users to display them in the dashboard
router.put("/modifyByAdmin", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, UserController.modifyUserByAdmin); // Route to modify the user values by the administrator
router.delete("/deleteByAdmin/:id", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, checkObjectIdParam.requireValidMongooseObjectID, UserController.deleteUserByAdmin); // Route to delete a user when we are administrator
router.post('/activate-account', APIKeyMiddleware.requireValidAPIForVueJSMainApp, UserController.activateAccount); // Route to activate the account of a user vie the VueJS main app
router.post('/email-reinit-passwd', APIKeyMiddleware.requireValidAPIForVueJSMainApp, UserController.emailForReinitPassword); // Route to reinit the password of a user via the VueJS main app
router.get('/access-modify-passwd/:token', APIKeyMiddleware.requireValidAPIForVueJSMainApp, UserController.canModifyForgottenPassword); // Route to know if we can access to the route to modify the user's forgotten passowrd vie the VueJS main app
router.put('/modify-forgotten-passwd', APIKeyMiddleware.requireValidAPIForVueJSMainApp, UserController.modifyForgottenPassword); // Route to modify the forgotten password of the user vie the VueJS main app
router.post('/addByAdmin', SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, UserController.addUserByAdmin); // Route to be able to add a user when we are administrator

module.exports = router; // Export the router