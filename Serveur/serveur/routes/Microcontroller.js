const express = require('express'); // Import the express module
const router = express.Router(); // Creation of an Express router
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to check the session ID
const CSRFTokenMiddleware = require('../middlewares/checkCSRFTokens'); // Import the middleware to check the CSRF tokens
const checkObjectIdParam = require('../middlewares/checkObjectIDParam'); // Import the middleware to check the addr passed in parameter
const APIKeyMiddleware = require('../middlewares/checkAPIKey'); // Import the middleware to check the API Key passed in parameter
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the user can access the routes
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the account of the user is activated or not
const ipfilter = require('express-ipfilter').IpFilter; // Filter routes by IP
const requestIP = require('request-ip'); // Package to be able to get the actual IP of the client

const MicrocontrollerController = require("../controllers/Microcontroller/Microcontroller"); // Import the Microcontroller Controller


/**
 * Function to get the actual IP of the client
 * @param {*} req 
 * @returns the actual IP of the client
 */
 const clientIP = req => {
    return requestIP.getClientIp(req);
}


const IP_ranges = require('../config/IPRanges').getIPRanges(); // Get the range of available IPs


router.post("/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, MicrocontrollerController.createMicrocontroller); // Route to create a microcontroller
router.get("/:id", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, checkObjectIdParam.requireValidMongooseObjectID, MicrocontrollerController.getOneMicrocontroller); // Route to get one microcontroller
router.put('/:id', SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, checkObjectIdParam.requireValidMongooseObjectID, checkObjectIdParam.requireValidMongooseObjectID, MicrocontrollerController.modifyAMicrocontroller); // Route to modify one microcontroller
router.put('/', ipfilter(IP_ranges,{ mode: 'allow', detectIp: clientIP }), APIKeyMiddleware.requireValidAPIForMicrocontrollers, MicrocontrollerController.microcontrollerIsUpdated); // Route to to update the hasConfigChanged when a microcontroller has its new config and filter by IPs
router.delete('/:id/:addr', SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, checkObjectIdParam.requireValidMongooseObjectID, MicrocontrollerController.deleteMicrocontroller); // Route to delete a microcontroller
router.get('/', SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, MicrocontrollerController.getAllMicrocontrollers); // Route to find all the microcontrollers
router.get('/lessInfos/forShowcase', APIKeyMiddleware.requireValidAPIForVueJSShowcaseApp, MicrocontrollerController.getMicrocontrollersForShowcase); // Route to get all the microcontrollers for the showcase app
router.get('/lessInfos/forMain', APIKeyMiddleware.requireValidAPIForVueJSMainApp, MicrocontrollerController.getMicrocontrollersForShowcase); // Route to get all the microcontrollers for the VueJS main app
router.get('/datas/InLastDay', SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, MicrocontrollerController.hasMicrocontrollerSentDatasInTheLastDay); // Route to know if the microcontroller has sent data in the last day (in the last 24 hours)

module.exports = router; // Export the router