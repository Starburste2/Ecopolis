const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to check the session ID
const checkObjectIdParam = require('../middlewares/checkObjectIDParam'); // Import the middleware to check the id passed in parameter => Not return the type of database with the catch error
const APIKeyMiddleware = require('../middlewares/checkAPIKey'); // Import the middleware to check the API Key
const checkActivatedAccount = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the account of the user is activated
const ipfilter = require('express-ipfilter').IpFilter; // Filter routes by IP
const requestIP = require('request-ip'); // Package to be able to get the actual IP of the client

const DatasController = require("../controllers/Datas/Datas"); // Import the Datas Controller


/**
 * Function to get the actual IP of the client
 * @param {*} req 
 * @returns the actual IP of the client
 */
const clientIP = req => {
    return requestIP.getClientIp(req);
}


const IP_ranges = require('../config/IPRanges').getIPRanges(); // Get the range of available IPs


router.post("/", ipfilter(IP_ranges,{ mode: 'allow', detectIp: clientIP }), APIKeyMiddleware.requireValidAPIForMicrocontrollers, DatasController.addData); // Route to add a data to the database and filter by IPs
router.get('/byAddr/:addr', SessionIDMiddleware.requireValidSessionID, checkActivatedAccount.requireAccountActivated, DatasController.getDatasFromAddr); // Route to find datas by addr
router.get('/datasFromInterval/:addr', SessionIDMiddleware.requireValidSessionID, checkActivatedAccount.requireAccountActivated, DatasController.getDatasFromInterval); // Route to find the datas in an interval by addr
router.get('/lastForShowCase', APIKeyMiddleware.requireValidAPIForVueJSShowcaseApp, DatasController.getLastDatas); // Route to get the last 24 hours datas to return to the showcase app
router.get('/lastForMain', APIKeyMiddleware.requireValidAPIForVueJSMainApp, DatasController.getLastDatas); // Route to get the last 24 hours datas to return to the VueJS main app

module.exports = router; // Export the router