const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const APIKeyMiddleware = require('../middlewares/checkAPIKey'); // Import the middleware to check the API Key
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to check the session ID
const CSRFTokenMiddleware = require('../middlewares/checkCSRFTokens'); // Import the middleware to check the CSRF tokens
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the user can access the route
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the user has an account that is activated
const checkIdObject = require('../middlewares/checkObjectIDParam'); // Import the middleware to check the if the ID is correct or not

const PostController = require('../controllers/Post/Post'); // Import the post controller

router.get("/", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, UserRoleMiddleware.requireAdminRole, PostController.getAllPosts); // Route to get all the posts if we are admin
router.get("/fromShowcase", APIKeyMiddleware.requireValidAPIForVueJSShowcaseApp, PostController.getAllPostsFromShowcase); // Route to get all the posts from the VueJS showcase app
router.get("/fromMain", APIKeyMiddleware.requireValidAPIForVueJSMainApp, PostController.getAllPostsFromShowcase); // Route to get all the posts from the VueJS main app
router.post("/addPost", SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, PostController.addPost); // Route to add a post
router.delete("/deletePost/:id", checkIdObject.requireValidMongooseObjectID, SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, PostController.deletePost); // Route to delete a post
router.put("/modifyPost/:id", checkIdObject.requireValidMongooseObjectID, SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, PostController.modifyPost); // Route to modify a post

module.exports = router; // Export the router