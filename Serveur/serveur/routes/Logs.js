const express = require('express');// Import the express module
const router = express.Router(); // Creation of an Express router
const SessionIDMiddleware = require('../middlewares/checkSessionID'); // Import the middleware to verify the session IDs
const CSRFTokenMiddleware = require('../middlewares/checkCSRFTokens'); // Import the middleware to verify the CSRF tokens
const UserRoleMiddleware = require('../middlewares/checkUserRole'); // Import the middleware to check if the request is made by an admin
const checkAccountActivated = require('../middlewares/checkAccountActivated'); // Import the middleware to check if the account of the user is activated or not

const LogsController = require("../controllers/Logs/Logs"); // Import the logs controller

router.get('/getLogs', SessionIDMiddleware.requireValidSessionID, checkAccountActivated.requireAccountActivated, CSRFTokenMiddleware.requireValidCSRFTokens, UserRoleMiddleware.requireAdminRole, LogsController.getLogs); // Route to get all the debug logs

module.exports = router; // Export the router