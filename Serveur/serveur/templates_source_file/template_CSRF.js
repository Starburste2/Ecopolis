/**
* @returns the CSRF Token key for encryption
*/
exports.getCSRFKey = function () {
  return CSRF_TOKEN
}