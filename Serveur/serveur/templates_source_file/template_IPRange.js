/**
 * @returns the available IP range for IP filter
 */
 exports.getIPRanges = function () {
    return [[MINIP, MAXIP]];
}