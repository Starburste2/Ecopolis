/**
 * Function to be able to get the Recaptcha site key provided by Google
 * @returns the site key for the Recaptcha package
 */
exports.getSiteKey = function() {
    return 'SITE_KEY'
}


/**
 * Function to be able to get the Recaptcha secret key provided bu Google
 * @returns the secret key for the Recaptcha package
 */
exports.getSecretKey = function() {
    return 'SECRET_KEY'
}