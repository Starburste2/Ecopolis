const Microcontroller = require("../../models/Microcontroller"); // Import the Microcontroller model to verify if the address exists
const Datas = require("../../models/Datas"); // Import the Datas model to exports the datas
const fs = require('fs'); // Import the filesystem module
const DatasHelper = require("../Datas/DatasHelper"); // Import the helper to be able to retrieve informations about sleep times of microcontrollers
const MicrocontrollerValidation = require('../Microcontroller/MicrocontrollerValidation'); // Import the validation file of the microcontroller => To chek if the addr of the microcontroller is good or not
const APIKey = require('../../models/APIKey'); // Import the APIKey model
const moment = require('moment'); // Import moment to be able to format date to the good time to export them in CSV


/**
 * Function used to export the server's certificate to put it on the SD card of the main node
 * @param {*} req 
 * @param {*} res 
 */
exports.downloadServerCertificate = function (req, res) {
    // Path and name for the file to download
    var filename = "certificate.txt";
    var path = "./files_downloaded/" + filename;

    
    // Read the server's certificate file
    var server_certificate = fs.readFileSync('./cert.pem');


    // Write the datas in the temporary file
    fs.writeFileSync(path, server_certificate.toString());


    // Return the file to download and delete it from /files_downloaded
    res.status(200).download(path, filename, function(err){
        if(err) console.log("Error during the downloading of the file.");

        fs.unlink(path, function(error, result) {
            if(error) console.log(error);
        });
    });
}


/**
 * Function used to download the datas from a microcontroller in CSV format
 * We can pass options in queries to be able to make more processing if the user wants (No mandatory)
 * We can get a CSV file with : 
 *   - all the datas (no filter)
 *   - all the datas without duplicates (filter withoutDuplicates to true)
 * @param {*} req 
 * @param {*} res 
 */
exports.downloadMicrocontrollerDatas = function (req, res) {
    // Check the microcontroller address
    resultCheckAddr = MicrocontrollerValidation.checkAddr(req.params.addr, null, false);
    if(!resultCheckAddr[0]){
        res.status(400).send({error: resultCheckAddr[1]});
        return;
    }


    // Retrieve the datas
    Datas.find({ addrMicrocontroller: req.params.addr }, { _id: 0, dateModification: 0 }).sort({ dateAdded: 1 })
    .then(datas => {
        
        // Return an error because no datas are present into the database
        if(datas.length == 0) {
            return res.status(400).json({ message: 'No datas' })
        }

        // Path and name for the file to download
        var filename = "datas_" + String(req.params.addr).toUpperCase() + ".csv";
        var path = "./files_downloaded/" + filename;
        

        // Transform the queries into an object if exists ; undefined if not exists
        try {
            if (req.query.options != undefined) {
                var options = JSON.parse(req.query.options); // Set the options with the JSON object in query
                options.firstDate = moment(options.firstDate); // First date is the date passed in parameter at midnight
                options.lastDate = moment(options.lastDate); // Last date is the date passed in parameter

                // If the last date is a valid date ; if not the date is invalid so no need to hours/minutes
                if (options.lastDate.isValid()) {
                    options.lastDate.add(23, 'hours'); // Add 23 hours
                    options.lastDate.add(59, 'minutes'); // And 59 minutes to get all the datas from the last date day
                }

            } else {
                var options = undefined; // No options in query => Set options to undefined
            }
        } catch(error) {
            return res.status(400).json({ message: 'Unable to parse the JSON' }); // Return an error
        }
        

        // If we have no queries or no options selected in the request => Download all the datas
        if(options == undefined || (!options.withoutDuplicates && !options.firstDate.isValid() && !options.lastDate.isValid())) {
            for(const data of datas) {
                // Get and formate informations about the date by moment to have all the good informations (mainly the hours)
                const moment_date = moment(data.dateAdded, 'YYYY-MM-DD H:mm:ss'); // Get the moment date to have the real date with the good hours
                var day = moment_date.date(); // Get the date
                var month = (moment_date.month() + 1) < 10 ? '0' + (moment_date.month() + 1) : (moment_date.month() + 1); // Get the month + 1 to have the good one + Do this operation to obtain good month format
                var year = moment_date.year(); // Get the year
                var hour = moment_date.hour(); // Get the hours
                var minutes = moment_date.minute() < 10 ? '0' + moment_date.minute() : moment_date.minute(); // Get the minutes + Do this operation to obtain good minutes format
    
                // Formate the string to append into the file
                var string_to_append = data.addrMicrocontroller + ";" + data.ph + ";" + data.humidity + ";" + data.temperature + ";" + data.conductivity + ";" + data.nitrogen + ";" + data.phosphorus + ";" + data.potassium + ";" + (day + "-" + month + "-" + year + " " + hour + ":" + minutes) + "\n";
    
                // Append the datas into the file
                fs.appendFileSync(path, string_to_append);
            }
        

        // Else, we have options => Do processing according to the options selected
        } else {
            var datas_filtered = []; // Datas that are filtered by the options and to return into the CSV file

            // ! If we just want the datas without duplicates
            if(options.withoutDuplicates == true) {
                var time_for_duplicates = 300000; // Minimum of miliseconds (5 minutes) to decline duplicates
                var blacklist = []; // Blacklist of index to not use to complete the file to download
                var datas_for_option_processing = []; // Datas to use for this loop processing
                var datas_filtered_processing = []; // Datas we have filtered during this processing

                
                if(datas_filtered.length != 0) datas_for_option_processing = datas_filtered; // Use the array with datas that are already processed
                else datas_for_option_processing = datas; // Use the array got from DB because no processing done
            

                // For to go through all the datas
                for(var i = datas_for_option_processing.length - 1 ; i >= 0 ; i--) {

                    // If i - 1 is more than 0 and we don't have this index in the blacklist array 
                    if(i - 1 >= 0 && blacklist.indexOf(i) == -1) {

                        // Make another for to blacklist the index that are before me and with less than 5 minutes
                        for(var j = i - 1 ; j >= 0 ; j--) {
                            if(datas_for_option_processing[i].dateAdded.getTime() - time_for_duplicates < datas_for_option_processing[j].dateAdded.getTime()) blacklist.push(j); // Less than 5 minutes => Blacklist the index
                            else break; // Else break because as we bring together the index 0 => The date are more far so when we cannot blacklist => Quit because we will not be able to blacklist another index (Optimize processing)
                        }
                    }
                }

                // For to put the datas into the file
                for (const [index, data] of datas_for_option_processing.entries()) {

                    // If the index is not blacklisted => Put in file
                    if(blacklist.indexOf(index) == -1) datas_filtered_processing.push(data); // Push the data in the array
                }


                datas_filtered = datas_filtered_processing; // Copy the array into the main one
            }


            // ! If the first date option is selected
            if(options.firstDate.isValid()) {
                var datas_for_first_date_process = []; // Array to store the datas for this option processing
                var datas_filtered_first_date_process = []; // Datas filtered during this option processing


                if(datas_filtered.length != 0) datas_for_first_date_process = datas_filtered; // Use the array with datas that are already processed
                else datas_for_first_date_process = datas; // Use the array got from DB because no processing done


                // Filter the datas
                for(const data of datas_for_first_date_process) {
                    var moment_date = moment(data.dateAdded); // Transform the 

                    // If the added date is after the beginning date selected by the user => Store the data
                    if(moment_date.isAfter(options.firstDate)) datas_filtered_first_date_process.push(data); // Push the data into the array
                }


                datas_filtered = datas_filtered_first_date_process; // Copy the array into the main one
            }


            // ! If the last date option is selected
            if(options.lastDate.isValid()) {
                var datas_for_last_date_process = []; // Array to store the datas for this option processing
                var datas_filtered_last_date_process = []; // Datas filtered during this option processing


                if(datas_filtered.length != 0) datas_for_last_date_process = datas_filtered; // Use the array with datas that are already processed
                else datas_for_last_date_process = datas; // Use the array got from DB because no processing done


                // Filter the datas
                for(const data of datas_for_last_date_process) {
                    var moment_date = moment(data.dateAdded); // Transform the 

                    // If the added date is after the beginning date selected by the user => Store the data
                    if(moment_date.isBefore(options.lastDate)) datas_filtered_last_date_process.push(data); // Push the data into the array
                }


                datas_filtered = datas_filtered_last_date_process; // Copy the array into the main one
            }


            // Append all the datas into the file to download
            for(const data of datas_filtered) {
                
                // Get and formate informations about the date by moment to have all the good informations (mainly the hours)
                const moment_date = moment(data.dateAdded, 'YYYY-MM-DD H:mm:ss'); // Get the moment date to have the real date with the good hours
                var day = moment_date.date(); // Get the date
                var month = (moment_date.month() + 1) < 10 ? '0' + (moment_date.month() + 1) : (moment_date.month() + 1); // Get the month + 1 to have the good one + Do this operation to obtain good month format
                var year = moment_date.year(); // Get the year
                var hour = moment_date.hour(); // Get the hours
                var minutes = moment_date.minute() < 10 ? '0' + moment_date.minute() : moment_date.minute(); // Get the minutes + Do this operation to obtain good minutes format

                
                // Append the line of datas into the file
                fs.appendFileSync(path, data.addrMicrocontroller + ";" + data.ph + ";" + data.humidity + ";" + data.temperature + ";" + data.conductivity + ";" + data.nitrogen + ";" + data.phosphorus + ";" + data.potassium + ";" + (day + "-" + month + "-" + year + " " + hour + ":" + minutes) + "\n");
            }
        }


        // Return the file to download and delete it from /temp
        res.status(200).download(path, filename, function(err){
            if(err) console.log("Error during the downloading of the file.");

            fs.unlink(path, function(error, result) {
                if(error) console.log(error);
            });
        });
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function to download the code of the microcontroller according to its address and its type
 * @param {*} req 
 * @param {*} res 
 */
exports.downloadMicrocontrollerFile = function (req, res) {
    // Check the microcontroller address
    resultCheckAddr = MicrocontrollerValidation.checkAddr(req.params.addr, null, false);
    if(!resultCheckAddr[0]){
        res.status(400).send({error: resultCheckAddr[1]});
        return;
    }

    // Find the microcontroller associated to the addr passed in parameter
    Microcontroller.findOne({ addr: req.params.addr })
    .then(microcontroller => {
        
        // Check if the microcontroller exists
        if(microcontroller == null){
            res.status(400).json({message: "Le microcontrôlleur n'existe pas."}); // The microcontroller doesn't exist => Quit
            return;
        } 
    

        // If it is an end node
        if(microcontroller.isEndNode) {

            // If the microcontroller has no group => Use the main node as destination
            if(microcontroller.destinationGroup == "") {
                
                // Find the main node
                Microcontroller.findOne({ isEndNode: false, isRelay: false })
                .then(main_node => {

                    // Check if we have a main node in the system
                    if(main_node == null){
                        res.status(400).json({message: "Pas de noeud principal"}); // No main node
                        return;
                    }

                    codeForEndNode(req, res, microcontroller, main_node.addr); // Download the sender code associate to the microcontroller values
                })
                .catch(error => res.status(400).json(error))


            // If the microcontroller has a group => Use the relay as destination
            } else {
                codeForEndNode(req, res, microcontroller, microcontroller.destinationGroup); // Download the sender code associate to the microcontroller values
            }


        // If it is not an end node
        } else {

            // If it is a relay
            if (microcontroller.isRelay) {
                codeForRelay(req, res, microcontroller); // Download the relay code associated to the microcontroller


            // If it is a main node
            } else {
                // codeForMainNodeHTTP(req, res, microcontroller); // Downlaod the HTTP receiver code associate to the microcontroller
                codeForMainNodeHTTPS(req, res, microcontroller); // Downlaod the HTTPS receiver code associate to the microcontroller
            }
        } 
        
    })
    .catch( (error) => res.status(400).json({ error }))
}


/**
 * Function used to download codes for the end nodes => template_sender.ino
 * The file to download will be formatted with the datas concerning the microcontroller that are present in the database
 * @param {*} req 
 * @param {*} res 
 * @param {*} microcontroller => Contains all the informations about the microcontroller
 * @param {*} destination => Destination of the messages sent by the microcontroller
 */
function codeForEndNode(req, res, microcontroller, destination){

    // Path and name for the file to download
    var filename = String(microcontroller.addr).toUpperCase() + ".ino";
    var path = "./templates_microcontrollers/sender/temp/" + filename;


    // Read the template file
    var text = fs.readFileSync('./templates_microcontrollers/sender/template_sender.ino','utf8');

    // Replace the address for the sender
    text = text.split("ADDRESS_TO_MODIFY").join("0x" + String(microcontroller.addr).toUpperCase());


    // Replace the address of the destination
    text = text.split("DESTINATION_TO_MODIFY").join("0x" + destination.toUpperCase());


    // Get the first sleep time and the array of sleep times
    var all_sleep_times = DatasHelper.getAllSleepTimeValues(microcontroller);


    // Replace the first sleep time
    text = text.split("ADD_FIRST_DEEP_SLEEP").join(String(all_sleep_times[0]));


    // Replace the sleeptime length of the array for the sender
    text = text.split("ADD_SLEEPTIME_LENGTH").join(String(all_sleep_times[1].length));    


    // Creation of the string for the sleep time values to add in the file
    var sleep_time = "";
    all_sleep_times[1].forEach(function(element, index){
        sleep_time += "sleep_time[" + index + "] = " + element + ";\n    ";
    });

    // Add the sleep time values in the template
    text = text.split("SLEEP_TIME_VALUES").join(sleep_time);

    
    // Write the datas in the temporary file
    fs.writeFileSync(path, String(text));


    // Return the file to download and delete it from /temp
    res.status(200).download(path, filename, function(err){
        if(err) console.log("Error during the downloading of the file.");

        fs.unlink(path, function(error, result) {
            if(error) console.log(error);
        });
    });
}


/**
 * Function used to download HTTP codes for the main nodes => template_receiver.ino
 * The file to download will be formatted with the datas concerning the microcontroller that are present in the database
 * @param {*} req 
 * @param {*} res 
 * @param {*} microcontroller => Contains all the informations about the microcontroller
 */
function codeForMainNodeHTTP(req, res, microcontroller){

    // Find the API Key to put it in the code
    APIKey.findOne({ applicationName : "Microcontrollers" })
    .then(APIKey_found => {
        // Path and name for the file to download
        var filename = String(microcontroller.addr).toUpperCase() + ".ino";
        var path = "./templates_microcontrollers/receiver/temp/" + filename;


        // Read the template file
        var text = fs.readFileSync('./templates_microcontrollers/receiver/template_receiver.ino','utf8');

        // Replace the address for the receiver
        text = text.split("ADDRESS_TO_MODIFY").join("0x" + String(microcontroller.addr).toUpperCase());

        
        // Write the datas in the temporary file
        fs.writeFileSync(path, String(text));


        // Replace the APIKey of the receiver
        text = text.split("APIKEY_TO_MODIFY").join("\"" + APIKey_found.key + "\"");


        // Write the datas in the temporary file
        fs.writeFileSync(path, String(text));


        // Return the file to download and delete it from /temp
        res.status(200).download(path, filename, function(err){
            if(err) console.log("Error during the downloading of the file.");

            fs.unlink(path, function(error, result) {
                if(error) console.log(error);
            });
        });
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to download HTTPS codes for the main nodes => template_receiver_https.ino
 * The file to download will be formatted with the datas concerning the microcontroller that are present in the database
 *   and with the value of the certificate we will format to fit with the Arduino format
 * @param {*} req 
 * @param {*} res 
 * @param {*} microcontroller => Contains all the informations about the microcontroller
 */
function codeForMainNodeHTTPS (req, res, microcontroller) {

    // Find the API Key to put it in the code
    APIKey.findOne({ applicationName : "Microcontrollers" })
    .then(APIKey_found => {
        // Path and name for the file to download
        var filename = String(microcontroller.addr).toUpperCase() + ".ino";
        var path = "./templates_microcontrollers/receiver/temp/" + filename;


        // Read the template file
        var text = fs.readFileSync('./templates_microcontrollers/https_receiver/template_receiver_https.ino','utf8');

        // Replace the address for the receiver
        text = text.split("ADDRESS_TO_MODIFY").join("0x" + String(microcontroller.addr).toUpperCase());

        
        // Write the datas in the temporary file
        fs.writeFileSync(path, String(text));


        // Replace the APIKey of the receiver
        text = text.split("APIKEY_TO_MODIFY").join("\"" + APIKey_found.key + "\"");


        // Read and replace the certificate
        var certificate = fs.readFileSync('./cert.pem', 'utf8');
        var certificate_part_for_file = ['\\']; // Array for formatting certificate for good work on ESP32
        
        // Push each line of the certificate with the good format if the line is different to ''
        for(const [index, line] of certificate.split('\n').entries()){
            if(line.trim() != '') {
                if(index != (certificate.split('\n').length - 2)) certificate_part_for_file.push(' "' + line + '\\n" \\'); // Push the formatted line to write in the file
                else certificate_part_for_file.push('"' + line + '\\n"'); // Last line, with datas (not ''), of the certificate
            }
        }

        // Replace the certificate
        text = text.split("CERTIFICATE_TO_MODIFY").join(certificate_part_for_file.join('\n'));


        // Write the datas in the temporary file
        fs.writeFileSync(path, String(text));


        // Return the file to download and delete it from /temp
        res.status(200).download(path, filename, function(err){
            if(err) console.log("Error during the downloading of the file.");

            fs.unlink(path, function(error, result) {
                if(error) console.log(error);
            });
        });
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to download the codes for the relays => template_relay.ino
 * The file to download will be formatted with the values stored in the database concerning the microcontroller
 * @param {*} req 
 * @param {*} res 
 * @param {*} microcontroller => Contains all the informations about the microcontroller
 */
function codeForRelay (req, res, microcontroller) {

    // Path and name for the file to download
    var filename = String(microcontroller.addr).toUpperCase() + ".ino";
    var path = "./templates_microcontrollers/relay/temp/" + filename;


    // Read the template file
    var text = fs.readFileSync('./templates_microcontrollers/relay/template_relay.ino','utf8');


    // Get the main node
    Microcontroller.findOne({ isEndNode: false, isRelay: false })
    .then(main_node => {

        // Replace the address of the relay
        text = text.split("LOCAL_ADDRESS").join("0x" + String(microcontroller.addr).toUpperCase());


        // Replace the destination address of the relay
        text = text.split("DESTINATION").join("0x" + String(main_node.addr).toUpperCase());

        
        // Write the datas in the temporary file
        fs.writeFileSync(path, String(text));


        // Return the file to download and delete it from /temp
        res.status(200).download(path, filename, function(err){
            if(err) console.log("Error during the downloading of the file.");

            fs.unlink(path, function(error, result) {
                if(error) console.log(error);
            });
        });
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to export all the datas of all teh microcontrollers from the DB
 * @param {*} req 
 * @param {*} res 
 */
exports.downloadAllMicrocontrollersData = function (req, res) {

    // Find all the datas to export from the oldest to the newest
    Datas.find().sort({ addrMicrocontroller: 1, dateAdded: 1 })
    .then(datas => {
        
        // Return an error because no datas are present into the database
        if(datas.length == 0) {
            return res.status(400).json({ message: 'No datas' })
        }


        // Path and name for the file to download
        var filename = "all_datas.csv";
        var path = "./files_downloaded/" + filename;


        // Browse all the datas got from the DB to put them into the CSV file to return
        for(const data of datas) {
            
            // Convert the date
            const moment_date = moment(data.dateAdded, 'YYYY-MM-DD H:mm:ss'); // Get the moment date to have the real date with the good hours
            var day = moment_date.date(); // Get the date
            var month = (moment_date.month() + 1) < 10 ? '0' + (moment_date.month() + 1) : (moment_date.month() + 1); // Get the month + 1 to have the good one + Do this operation to obtain good month format
            var year = moment_date.year(); // Get the year
            var hour = moment_date.hour(); // Get the hours
            var minutes = moment_date.minute() < 10 ? '0' + moment_date.minute() : moment_date.minute(); // Get the minutes + Do this operation to obtain good minutes format
            

            // Append the line of datas into the file
            fs.appendFileSync(path, data.addrMicrocontroller + ";" + data.ph + ";" + data.humidity + ";" + data.temperature + ";" + data.conductivity + ";" + data.nitrogen + ";" + data.phosphorus + ";" + data.potassium + ";" + (day + "-" + month + "-" + year + " " + hour + ":" + minutes) + "\n");
        }


        // Return the file to download and delete it from the path : ./files_downloaded/all_datas.csv
        res.status(200).download(path, filename, function(err){
            if(err) console.log("Error during the downloading of the file.");

            fs.unlink(path, function(error, result) {
                if(error) console.log(error);
            });
        });
    })
    .catch(error => res.status(400).json(error))
}