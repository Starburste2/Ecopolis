const Group = require('../../models/Group'); // Import the Group model
const GroupValidation = require('./GroupValidation'); // Import the Validation file for the groups
const Microcontroller = require('../../models/Microcontroller'); // Import the Microcontroller model
const PoolAddress = require('../../models/PoolAddress'); // Import the pool address model


/**
 * Function to get all the groups
 * @param {*} req 
 * @param {*} res 
 */
exports.getAllGroups = function (req, res) {
    Group.find({}, { __v: 0 })
    .then(groups => res.status(200).json(groups))
    .catch(error => res.status(400).json(error))
}


/**
 * Function to get all the possible references microcontrollers
 * @param {*} req 
 * @param {*} res 
 */
exports.getMicrocontrollersForReferences = function (req, res) {

    // Get the available addresses in the pool
    PoolAddress.find({ isAvailable: true }, { addr: 1, _id: 0 })
    .then(pooladdress => {
        res.status(200).json(pooladdress) // Return the available address in the pool
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to get all the groups without all the informations contained in the DB
 * Just get the usefull informations
 * @param {*} req 
 * @param {*} res 
 */
exports.getGroupsWithoutAllInformations = function (req, res) {
    Group.find({}, { name: 1, referingNode: 1, _id: 0 })
    .then(groups => res.status(200).json(groups))
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to create a new group
 * When we create a new group, we create also a new microcontroller
 * @param {*} req 
 * @param {*} res 
 * @returns if we have a problem during the validation of the data sent
 */
exports.addGroup = function (req, res) {
    // Get the values from the body
    var group_name = req.body.name;
    var refering_node = req.body.referingNode;
    var beginning_hour = req.body.beginningHour;
    var delay_between_2_microcontrollers = req.body.delayBetween2uCInS;
    var delay_between_hours = req.body.delayBetweenHours;


    // Check quickly the values
    if(group_name == undefined || group_name == "") return res.status(400).json({ message: "Name incorrect" });
    if(refering_node == undefined || refering_node == "") return res.status(400).json({ message: "referingNode incorrect" });
    if(!GroupValidation.checkHours(beginning_hour)) return res.status(400).json({ message: 'beginningHours incorrect' });
    if(delay_between_2_microcontrollers == undefined || !(/^\d+$/.test(delay_between_2_microcontrollers))) return res.status(400).json({ message: 'delayBetween2Microcontrollers incorrect' });
    if(delay_between_hours == undefined || !(/^\d+$/.test(delay_between_hours))) return res.status(40).json({ message: 'delayBetweenHours incorrect' });

    
    // Get values from DB for more checks
    PoolAddress.findOne({ addr: refering_node })
    .then(pool_address => {
        
        // Check the addr
        resultCheckAddr = GroupValidation.checkAddr(refering_node, pool_address, true);
        if(!resultCheckAddr) return res.status(400).send({ message: 'Addr incorrect' });

        // Find a group by name to know if the name is already present in DB or not
        Group.findOne({ name: group_name })
        .then(possible_group => {
            if(possible_group) return res.status(400).json({ message: 'Name already used' }); // Name is already present in DB


            // Find the main node for the destination group of the relay
            Microcontroller.findOne({ isEndNode: false, isRelay: false })
            .then(main_node => {
                if(!main_node) return res.status(400).json({ message: 'No main node' }); // Return the no main node error


                var my_relay; // The relay to use
                var today = new Date(); // Get the today date


                // Create the new relay
                my_relay = new Microcontroller({
                    addr: refering_node,
                    sleepTime: [],
                    hasConfigChanged: false,
                    isEndNode: false,
                    creationDate: today,
                    modificationDate: today,
                    groups: group_name,
                    destinationGroup: main_node.addr,
                    isRelay: true,
                    manualReconfiguration: true
                });


                // Save the new relay
                my_relay.save()
                .then(() => {
                    // Update the pool to say that the addr is not available now
                    PoolAddress.updateOne({ addr: refering_node }, {$set: { isAvailable: false }})
                    .then(() => {
                        var hours = beginning_hour.split(':'); // Split the hours
                        var waking_hours = []; // Array of waking hours with the first hours passed in parameters

                        // Generate the waking hours
                        var date1 = new Date();
                        date1.setUTCHours(hours[0], hours[1], 0, 0); // Set the hours, minutes, seconds, milliseconds

                        var date2 = new Date();
                        date2.setDate(date2.getDate() + 1); // Add one day to date2
                        date2.setUTCHours(hours[0], hours[1], 0, 0); // Set the hours, minutes, seconds, milliseconds

                        
                        // Add values to waking hours array
                        while(date1.getTime() < date2.getTime()) {
                            waking_hours.push((String(date1.getUTCHours()).length == 1 ? '0' + date1.getUTCHours() : date1.getUTCHours()) + ':' + (String(date1.getUTCMinutes()).length == 1 ? '0' + date1.getUTCMinutes() : date1.getUTCMinutes())); // Push in the array
                            date1.setMinutes(date1.getMinutes() + parseInt(delay_between_hours)); // Set the minutes
                        }


                        // Group to save
                        var group_to_save = new Group({
                            name: group_name,
                            referingNode: refering_node,
                            wakingHours: waking_hours,
                            beginningHour: beginning_hour,
                            delayBetweenHours: delay_between_hours,
                            delayBetween2uCInS: delay_between_2_microcontrollers
                        });


                        // Save the group
                        group_to_save.save().then(() => {

                            // And update the microcontroller which is a reference now
                            Microcontroller.updateOne({ addr: refering_node }, { $set: { groups: group_to_save.name } })
                            .then(() => {
                                res.status(201).json({ group: group_to_save })
                            })
                            .catch(error => console.log(error))
                        })
                        .catch(error => res.status(400).json(error))
                    })
                    .catch(error => res.status(400).json({ error }));
                })
                .catch(error => res.status(400).json(error))
            })
            .catch(error => res.status(400).json(error))
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to modify the values of a group
 * @param {*} req 
 * @param {*} res 
 * @returns if we have a problem
 */
exports.modifyGroup = function (req, res) {
    // Get the values from the body
    var group_name = req.body.nameGroup;
    var beginning_hour = req.body.beginningHour;
    var delay_between_2_microcontrollers = req.body.delayBetween2uCInS;
    var delay_between_hours = req.body.delayBetweenHours;


    // Check quickly the values
    if(group_name == undefined || group_name == "") return res.status(400).json({ message: "Name incorrect" });
    if(!GroupValidation.checkHours(beginning_hour)) return res.status(400).json({ message: 'beginningHours incorrect' });
    if(delay_between_2_microcontrollers == undefined || !(/^\d+$/.test(delay_between_2_microcontrollers))) return res.status(400).json({ message: 'delayBetween2Microcontrollers incorrect' });
    if(delay_between_hours == undefined || !(/^\d+$/.test(delay_between_hours))) return res.status(40).json({ message: 'delayBetweenHours incorrect' });


    // Find the group by name to compare with the one found by id
    Group.findOne({ name: group_name })
    .then(possible_group => {
        
        // Find the group by id to compare with the one found by name
        Group.findOne({ _id: req.params.id })
        .then(group => {
            if(group == null) return res.status(400).json({ message: 'Group incorrect' }); // If no group
            
            // If we have a finded group by name
            if(possible_group){
                if(group._id.toString() != possible_group._id.toString()) return res.status(400).json({ message: 'Name already used' }); // Test if the name already exists
            }

            
            // Update the microcontrollers with the new name of the group
            Microcontroller.find({ groups: group.name })
            .then(microcontrollers => {
                
                // Update the micrcocontrollers
                for(var microcontroller of microcontrollers){
                    microcontroller.groups = group_name;
                    microcontroller.save();
                }


                // Initialize the values to generate the waking hours
                var hours = beginning_hour.split(':'); // Split the hours
                var waking_hours = []; // Array of waking hours with the first hours passed in parameters

                // Generate the waking hours
                var date1 = new Date();
                date1.setUTCHours(hours[0], hours[1], 0, 0); // Set the hours, minutes, seconds, milliseconds

                var date2 = new Date();
                date2.setDate(date2.getDate() + 1); // Add one day to date2
                date2.setUTCHours(hours[0], hours[1], 0, 0); // Set the hours, minutes, seconds, milliseconds

                
                // Add values to waking hours array
                while(date1.getTime() < date2.getTime()) {
                    waking_hours.push((String(date1.getUTCHours()).length == 1 ? '0' + date1.getUTCHours() : date1.getUTCHours()) + ':' + (String(date1.getUTCMinutes()).length == 1 ? '0' + date1.getUTCMinutes() : date1.getUTCMinutes())); // Push in the array
                    date1.setMinutes(date1.getMinutes() + parseInt(delay_between_hours)); // Set the minutes
                }


                // Update the group
                Group.updateOne({ _id: req.params.id }, { $set: 
                    {
                        name: group_name,
                        beginningHour: beginning_hour,
                        delayBetweenHours: delay_between_hours,
                        delayBetween2uCInS: delay_between_2_microcontrollers,
                        wakingHours: waking_hours
                    }
                })
                .then(() => {
                    res.status(200).json({ message: 'OK', wakingHours: waking_hours }); // Send the OK message
                })
                .catch(error => res.status(400).json(error))
            })
            .catch(error => res.status(400).json(error))
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to delete a group
 * Deleting a group also includes the deletion of the microcontroller which is the refering node of the group
 * @param {*} req 
 * @param {*} res 
 * @returns if we have a problem
 */
exports.deleteGroup = function (req, res) {

    // Find the group associated to the id passed in parameter
    Group.findOne({ _id : req.params.id })
    .then(group => {
        if(group == null) return res.status(400).json({ message: 'No group' })
        
        // Delete the group
        Group.deleteOne({ _id: group._id })
        .then(() => {

            // Delete the relay node
            Microcontroller.deleteOne({ addr: group.referingNode })
            .then(() => {

                // Update the pool of addresses
                PoolAddress.updateOne({ addr: group.referingNode }, { $set: { isAvailable: true } })
                .then(() => {
                    
                    // Find all the microcontrollers that were present in the group
                    Microcontroller.find({ groups: group.name })
                    .then(microcontrollers => {
                        
                        // Update the micrcontrollers because they don't have a gruop now
                        for (var microcontroller of microcontrollers) {
                            microcontroller.groups = "";
                            microcontroller.destinationGroup = "";
                            microcontroller.save();
                        }
                        
                        res.status(200).json({ message: 'OK' }); // Send the OK message
                    })
                    .catch(error => res.status(400).json(error))
                })
                .catch(error => res.status(400).json(error))
            })
            .catch(error => res.status(400).json(error))
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to get the microcontrollers of a group
 * @param {*} req 
 * @param {*} res 
 */
exports.getMicrocontrollersOfAGroup = function (req, res) {
    // Get the values
    var group_name = req.query.groupName;


    // Find if we have a group
    Group.findOne({ name: group_name })
    .then(group => {
        if(group == null) return res.status(400).json({ message: 'Group not exists' }); // The group doesn't exist => Error


        // Get all the microcontrollers of the existing group
        Microcontroller.find({ groups: group.name }, { addr: 1, _id:0 })
        .then(microcontrollers => {
            res.status(200).json(microcontrollers); // Return the microcontrollers of the group
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to get all the microcontrollers that don't have a group
 * @param {*} req 
 * @param {*} res 
 */
exports.getMicrocontrollersWithNoGroup = function (req, res) {
    Microcontroller.find({ groups: "", destinationGroup: "", isEndNode: true }, { addr: 1, _id: 0 })
    .then(microcontrollers => {
        res.status(200).json(microcontrollers)
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to add a list of microcontrollers into a group
 * @param {*} req 
 * @param {*} res 
 * @returns if we have a problem during the validation of the data
 */
exports.addListOfMicrocontrollers = function (req, res) {
    // Get the values in the body
    var refering_node_of_group = req.body.referingNode;
    var list_of_microcontrollers = req.body.microcontrollersList;


    // Validate the format of the refering node
    if(!GroupValidation.checkAddr(refering_node_of_group, null, false)) return res.status(400).json({ message: 'Addr incorrect' });
    
    // Validate the format and the addr contained in the list
    if(!GroupValidation.checkListOfMicrocontrollers(list_of_microcontrollers)) return res.status(400).json({ message: 'List incorrect' });


    // Find if a group exists with this microcontroller as a refering node
    Group.findOne({ referingNode: refering_node_of_group })
    .then(group => {
        if(group == null) return res.status(400).json({ message: 'No group' }); // No group associated to the refering node passed in parameter

        // Check if all the microcontrollers are defined or not by comparing the length of the array in body and the one get from DB
        Microcontroller.find({ addr: { $in: list_of_microcontrollers } })
        .then(microcontrollers => {
            if(microcontrollers == null || microcontrollers.length != list_of_microcontrollers.length) return res.status(400).json({ message: 'Array not good' });

            
            // Browse all the microcontrollers and update them
            for(const microcontroller of microcontrollers) {
                microcontroller.groups = group.name;
                microcontroller.destinationGroup = group.referingNode;
                microcontroller.save();
            }

            res.status(200).json({ message: 'OK' }); // Return the OK message + 200 status
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to re-configure all the microcontrollers of a group with the waking hours of the group
 * @param {*} req 
 * @param {*} res 
 * @returns if we have a problem during the validation of the data
 */
exports.updateMicrocontrollersConfig = function (req, res) {
    // Get the values
    var refering_node = req.query.referingNode;


    // Check the refering node passed in parameter
    if(refering_node == undefined || refering_node == '') return res.status(400).json({ message: 'referingNode incorrect' });


    // Find the group to have all teh informations
    Group.findOne({ referingNode: refering_node })
    .then(group => {
        if(group == null) return res.status(400).json({ message: 'No group' }); // No group error


        // Find all the microcontrollers of the group
        Microcontroller.find({ destinationGroup: refering_node })
        .then(microcontrollers => {
            
            // Update all the microcontroller with the waking hours of the group
            for(var [index, microcontroller] of microcontrollers.entries()) {
                var waking_hours = group.wakingHours; // Get the waking hours of the group
                var my_array = []; // Array of generated waking hours to store in the microcontrollers


                // Transform the waking hours to a date to generate all the possible hours
                for (var i = 0; i < waking_hours.length; i++) {
                    var today = new Date(); // Get the actual date
                    var hours = waking_hours[i].split(':'); // Split the array by :

                    today.setUTCHours(hours[0]); // Set the hours
                    today.setUTCMinutes(hours[1]); // Set the minutes
                    today.setUTCSeconds(0); // Set the seconds to 0 for a good calculation
                    today.setUTCSeconds(today.getSeconds() + group.delayBetween2uCInS * index); // Set the seconds

                    my_array.push(String( (String(today.getUTCHours()).length == 1 ? "0" + today.getUTCHours() : today.getUTCHours()) + ':' + (String(today.getUTCMinutes()).length == 1 ? "0" + today.getUTCMinutes() : today.getUTCMinutes()) + ':' + (String(today.getUTCSeconds()).length == 1 ? "0" + today.getUTCSeconds() : today.getUTCSeconds()))); // Push in the array to store on the microcontrollers
                }


                // Set the new values of the microcontroller and save it
                microcontroller.sleepTime = my_array;
                microcontroller.hasConfigChanged = true;
                microcontroller.manualReconfiguration = false;
                microcontroller.save();
            }


            res.status(200).json({ message: 'OK' }); // Return the OK message
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error =>  res.status(400).json(error))
}


/**
 * Function used to be able to reconfigure the microcontrollers that are in the main node's group
 * @param {*} req 
 * @param {*} res 
 * @returns if we have a problem during the validation of the data
 */
exports.updateMainNodeGroupMicrocontrollersConfig = function (req, res) {
    // Get the values
    var beginning_hour = req.query.referingBeginningTime;
    var delay_between_hours = req.query.selectTime;
    var delay_between_2_microcontrollers = req.query.delayUc;


    // Check the values passed in parameters
    if(!GroupValidation.checkHours(beginning_hour)) return res.status(400).json({ message: 'beginningHours incorrect' });
    if(delay_between_2_microcontrollers == undefined || !(/^\d+$/.test(delay_between_2_microcontrollers))) return res.status(400).json({ message: 'delayBetween2Microcontrollers incorrect' });
    if(delay_between_hours == undefined || !(/^\d+$/.test(delay_between_hours))) return res.status(40).json({ message: 'delayBetweenHours incorrect' });


    // Initialize the values to generate the waking hours
    var hours = beginning_hour.split(':'); // Split the hours
    var global_waking_hours = []; // Array of waking hours with the first hours passed in parameters

    // Generate the waking hours
    var date1 = new Date();
    date1.setUTCHours(hours[0], hours[1], 0, 0); // Set the hours, minutes, seconds, milliseconds

    var date2 = new Date();
    date2.setDate(date2.getDate() + 1); // Add one day to date2
    date2.setUTCHours(hours[0], hours[1], 0, 0); // Set the hours, minutes, seconds, milliseconds

    
    // Add values to waking hours array
    while(date1.getTime() < date2.getTime()) {
        global_waking_hours.push((String(date1.getUTCHours()).length == 1 ? '0' + date1.getUTCHours() : date1.getUTCHours()) + ':' + (String(date1.getUTCMinutes()).length == 1 ? '0' + date1.getUTCMinutes() : date1.getUTCMinutes())); // Push in the array
        date1.setMinutes(date1.getMinutes() + parseInt(delay_between_hours)); // Set the minutes
    }

    
    // Find all the microcontrollers of the group
    Microcontroller.find({ destinationGroup: '', isEndNode: true })
    .then(microcontrollers => {
        
        // Update all the microcontroller with the waking hours of the group
        for(var [index, microcontroller] of microcontrollers.entries()) {
            var waking_hours = global_waking_hours; // Get the waking hours of the group
            var my_array = []; // Array of generated waking hours to store in the microcontrollers


            // Transform the waking hours to a date to generate all the possible hours
            for (var i = 0; i < waking_hours.length; i++) {
                var today = new Date(); // Get the actual date
                var hours = waking_hours[i].split(':'); // Split the array by :

                today.setUTCHours(hours[0]); // Set the hours
                today.setUTCMinutes(hours[1]); // Set the minutes
                today.setUTCSeconds(0); // Set the seconds to 0 for a good calculation
                today.setUTCSeconds(today.getSeconds() + delay_between_2_microcontrollers * index); // Set the seconds

                my_array.push(String( (String(today.getUTCHours()).length == 1 ? "0" + today.getUTCHours() : today.getUTCHours()) + ':' + (String(today.getUTCMinutes()).length == 1 ? "0" + today.getUTCMinutes() : today.getUTCMinutes()) + ':' + (String(today.getUTCSeconds()).length == 1 ? "0" + today.getUTCSeconds() : today.getUTCSeconds()))); // Push in the array to store on the microcontrollers
            }


            // Set the new values of the microcontroller and save it
            microcontroller.sleepTime = my_array;
            microcontroller.hasConfigChanged = true;
            microcontroller.manualReconfiguration = false;
            microcontroller.save();
        }


        res.status(200).json({ message: 'OK' }); // Return the OK message
    })
    .catch(error => res.status(400).json(error))
}