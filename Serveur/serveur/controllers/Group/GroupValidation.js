var check = require('check-types');


/**
 * Function to check if the address of the microcontroller passed in parameter
 * is good or not 
 * @param {*} address => Address of the microcontroller
 * @param pool_address => A record of the pooladdresses collection of our database
 * @param use_pool_address => Instance of pooladdresses available
 * @param is_master => To know if we need to compare with the master node
 * @returns a boolean => false or true
 */
function checkAddr(address, pool_address, use_pool_address = true) {
    if(use_pool_address) if(pool_address == null) return false; // Check if the address is known or not
    if(address == undefined) return false; // Check undefined
    if(check.not.string(address)) return false; // Check string
    if(address.length > 2) return false; // Check the length
    if(use_pool_address) if(!pool_address.isAvailable) return false; // Check if the addr is available in the pool of addresses => Problem because the microcontroller needs to exist
    
    return true;
}

/**
 * Function to check if the boolean parameter is good or not
 * @param {*} my_boolean => Boolean to know if the group is a master
 * @returns a boolean => false or true
 */
 function checkBoolean(my_boolean) {
    if(my_boolean == undefined) return false;
    if(check.not.boolean(my_boolean)) return false;

    return true;
}


/**
 * Function used to be able to check the list of the microcontrollers, passed in parameter, to add in a group
 * @param {*} microcontrollers_list => List of microcontrollers
 * @returns a boolean => false or true
 */
function checkListOfMicrocontrollers(microcontrollers_list) {
    if(microcontrollers_list == undefined) return false; // Check undefined
    if(!Array.isArray(microcontrollers_list)) return false; // Check if an array
    if(microcontrollers_list.length == 0) return false; // More than one element in the list
    if(microcontrollers_list.filter(Array.isArray).length != 0) return false; // Check if a 1D array

    // Check all the addr one by one
    for(const microcontroller_addr of microcontrollers_list) {
        if(!checkAddr(microcontroller_addr, null, false, false)) return false; // Problem on one of the addresses
    }

    return true;
}


/**
 * Function used to be able to check the hours passed in parameters
 * @param {*} hours => Beginning hours
 * @returns a boolean => false or true
 */
function checkHours (hours) {
    if(hours == undefined || hours == '') return false; // Check undefined
    
    var new_hours = hours.split(':'); // Split for future tests
    if(new_hours.length != 2) return false; // Check length of array
    if(!(/^\d+$/.test(new_hours[0])) || !(/^\d+$/.test(new_hours[1]))) return false; // Check if values are positive integers
    if(parseInt(new_hours[0]) < 0 || parseInt(new_hours[0]) >= 24) return false; // Check the hours
    if(parseInt(new_hours[1]) < 0 || parseInt(new_hours[1]) >= 60) return false; // Check the minutes

    return true;
}


// Exports the function
exports.checkAddr = checkAddr;
exports.checkBoolean = checkBoolean;
exports.checkListOfMicrocontrollers = checkListOfMicrocontrollers;
exports.checkHours = checkHours