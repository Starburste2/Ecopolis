const PoolAddress = require("../../models/PoolAddress"); // Import the PoolAddress model
const User = require("../../models/User"); // Import the User model

/**
 * Function to get all the available addresses in the database
 * @param {*} req 
 * @param {*} res 
 */
exports.getAvailableAddresses = function (req, res) {
    // Return all the available addresses to add a new microcontroller
    PoolAddress.find({ isAvailable: true }, { __v: 0, isAvailable: 0, _id: 0 })
    .then(addresses => {
        res.status(200).json(addresses)
    })
    .catch(error => res.status(400).json({ error }))
}