/**
 * Function used to validate the title of the post
 * @param {*} title => Title of the post
 * @returns true if the title is validated ; false otherwise 
 */
function validateTitle(title) {
    if (title == undefined) return false;
    if (title == '') return false;

    return true;
}


/**
 * Function used to validate the text of the post
 * @param {*} text => Text of the post
 * @returns true if the text is validated ; false otherwise
 */
function validateText(text) {
    if (text == undefined) return false;
    if (text == '') return false;

    return true;
}


// Export the function
exports.validateTitle = validateTitle
exports.validateText = validateText