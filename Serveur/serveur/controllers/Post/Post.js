const Post = require("../../models/Post"); // Import the Post model
const PostValidation = require("./PostValidation"); // Import the validation file


/**
 * Function to get all the posts from the database
 * @param {*} req 
 * @param {*} res 
 */
exports.getAllPosts = function (req, res) {
    Post.find({}, { __v: 0 }).sort({ dateAdded: -1 })
        .then(posts => res.status(200).json(posts))
        .catch(error => res.status(400).josn(error))
}


/**
 * Function to get all the posts from the database
 * @param {*} req 
 * @param {*} res 
 */
exports.getAllPostsFromShowcase = function (req, res) {
    Post.find({}, { __v: 0, _id: 0 }).sort({ dateAdded: -1 })
        .then(posts => res.status(200).json(posts))
        .catch(error => res.status(400).josn(error))
}


/**
 * Function used to create a post with the datas given
 * @param {*} req 
 * @param {*} res 
 * @returns when we have found a problem in the datas given
 */
exports.addPost = function (req, res) {
    // Get all teh variables to create the post
    var title = req.body.title;
    var text = req.body.text;
    var added_by = req.body.addedBy;
    var today = new Date();
    

    // Validate the title
    if (!PostValidation.validateTitle(title)) return res.status(400).json({ message: 'Title not correct' });

    // Validate the text
    if (!PostValidation.validateText(text)) return res.status(400).json({ message: 'Text not correct' });


    // Creation of the post
    var post = new Post({
        title: title,
        text: text,
        addedBy: added_by,
        dateAdded: today,
        dateModification: today
    });

    // Save the post in the DB
    post.save()
        .then(() => {
            res.status(201).json({ message: 'OK', post: post }); // Return the creation code
        })
        .catch(error => res.status(400).json(error))
}


/**
 * Function used to delete an exisiting post from the DB
 * @param {*} req 
 * @param {*} res 
 */
exports.deletePost = function (req, res) {
    Post.deleteOne({ _id: req.params.id, title: req.body.titlePost })
        .then(() => {
            res.status(200).json({ message: 'OK' }); // Return the deleting code
        })
        .catch(error => res.status(400).json(error))
}


/**
 * Function used to modify an existing post
 * @param {*} req 
 * @param {*} res 
 * @returns when we have found an error in the given datas
 */
exports.modifyPost = function (req, res) {
    // Get all the variables to modify the post
    var new_title = req.body.newTitle;
    var new_text = req.body.newText;
    var today = new Date();


    // Validate the new title
    if (!PostValidation.validateTitle(new_title)) return res.status(400).json({ message: 'Title not correct' });

    // Validate the new text
    if (!PostValidation.validateText(new_text)) return res.status(400).json({ message: 'Text not correct' });


    // Find the post
    Post.findOne({ _id: req.params.id })
        .then(post => {
            if (post == null) return res.status(400).json({ message: 'Post not exist' }); // Test if we have found a post

            // Update the post
            Post.updateOne({ _id: req.params.id }, { $set: { title: new_title, text: new_text, dateModification: today } })
                .then(() => {
                    res.status(200).json({ message: 'OK', modificationDate: today }); // All is good => Retunr the modification code
                })
                .catch(error => res.status(400).json(error))
        })
        .catch(error => res.status(400).json(error))
}