const fs = require('fs'); // Import the file system module
const moment = require('moment'); // Import the moment package to check the dates


/**
 * Function used to be able to retrieve the logs according to multiple queries :
 *   - method => Method or part of the method given by the user
 *   - code => Code or part of the code given by the user
 *   - url => URL or part of the URL given by the user
 *   - firstDate => Search from this date
 *   - lastDate => To this date
 * @param {*} req 
 * @param {*} res 
 */
exports.getLogs = function (req, res) {
    // Get all the queries informations
    var method = req.query.method;
    var code = req.query.code;
    var url = req.query.url;
    var firstDate = req.query.firstDate;
    var lastDate = req.query.lastDate;
    

    // Test if the queries are defined or not
    if(method == undefined) return res.status(400).json({ message: 'Method undefined' }); // Method undefined error
    if(code == undefined || isNaN(code)) return res.status(400).json({ message: 'Code incorrect' }); // Code incorrect error
    if(url == undefined) return res.status(400).json({ message: 'URL undefined' }); // URL undefined error
    if(firstDate == undefined || !moment(firstDate, 'YYYY-MM-DD').isValid()) return res.status(400).json({ message: 'First date incorrect' }); // First date incorrect error
    if(lastDate == undefined || !moment(lastDate, 'YYYY-MM-DD').isValid()) return res.status(400).json({ message: 'Last date incorrect' }); // Last date incorrect error


    // Variables to read the file and push the values into it
    var debug_logs = fs.readFileSync("./logs/debug.log"); // Read the log file
    debug_logs = debug_logs.toString().split("\n"); // Split the lines
    debug_logs.pop(); // Delete the last element that is a blank line
    var requests_to_return = []; // Requests to return


    // For all elements in the debug file => Reverse to have the newer elements in first
    for (var request of debug_logs.reverse()) {
    
        request = request.split(' ; ') // Split the request
        request[1] = request[1].split(' ') // Split the protocol, method, url
        request[2] = JSON.parse(request[2]) // Parse the JSON that contains all of the datas
        var request_date = new Date(request[0]) // Get the date of the request date


        // Push the datas according to the queries
        if (
            String(request[1][1]).includes(method.toUpperCase()) && // Check if contains the method
            String(request[1][2]).includes(url.toLowerCase()) && // Check if contains the URL
            String(request[2].meta.res.statusCode).includes(code) && // Check if contains the code
            new Date(firstDate).getTime() <= request_date.getTime() && // Check if the request_date is more than the first one we want
            request_date <= new Date(lastDate).setHours(23, 59, 0, 0) && // Check if the request_date is less than the last one we want
            request[1][1] != "OPTIONS" // Don't take into account the OPTIONS method
        ) {

            requests_to_return.push({
                time: ((String(request_date.getDate()).length==1) ? ("0" + request_date.getDate()) : request_date.getDate()) + '-' + ((String(request_date.getMonth() + 1).length==1) ? "0" + (request_date.getMonth() + 1) : (request_date.getMonth() + 1)) + '-' + request_date.getFullYear() + ' ' + ((String(request_date.getHours()).length==1) ? ("0" + request_date.getHours()) : request_date.getHours()) + ':' + ((String(request_date.getMinutes()).length==1) ? ("0" + request_date.getMinutes()) : request_date.getMinutes()) + ':' + ((String(request_date.getSeconds()).length==1) ? ("0" + request_date.getSeconds()) : request_date.getSeconds()),
                protocol: request[1][0].toUpperCase(),
                method: request[1][1],
                url: request[1][2],
                statusCode: request[2].meta.res.statusCode
            })
        
        } else if (request_date < new Date(firstDate)) break; // Quit the for because the request date is less than the first one given by the user => Optimization
    }

    res.status(200).json(requests_to_return);
}