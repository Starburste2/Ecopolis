const PasswordValidator = require('password-validator'); // Import the password validator module
const check = require('check-types'); // Import the check-types package to check values
const validator = require('validator'); // Import validator package to verify variables

/**
 * Function used to validate an email address in the typos format
 * @param {*} email_address 
 * @returns true if all is good ; false + a message otherwise
 */
function validateEmailAddress(email_address) {
    if(!email_address) return [false, "Email address is not defined"]; // Not defined
    if (
        !(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email_address)) &&
        !(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(email_address))
    ) // Check type format
        return [false, "Email address not good"];
    
    return [true];
}


/**
 * Function to check if the pasword exists => Normally it has already been hashed
 * @param {*} password 
 * @returns true if all is good ; false + a message otherwise
 */
function validatePassword(password) {
    if(!password) return [false, "Password is not defined"]; // Not defined

    const validator = new PasswordValidator(); // Create a new instance of password validator
    validator
        .is().min(7) // Minimum length 7
        .is().max(30) // Maximum length 30
        .has().uppercase() // Must have uppercase letters
        .has().lowercase() // Must have lowercase letters
        .has().digits(1) // Must have at least 1 digits
        .has().not().spaces() // Should not have spaces

    if(!validator.validate(password)) return [false, "Password incorrect"]; // Incorrect password

    return [true]
}


/**
 * Function used to validate the role of the user
 * @param {} role => Role provided by the user
 * @returns true if the role is correct ; false otherwise
 */
function validateRole(role) {
    if (role == 'client' || role == 'admin') return true;
    else return false;
}


/**
 * Function used to validate the modification value provided by the user
 * @param {*} value => Modification possibility provided by the user
 * @returns true if the value is correct ; false otherwise
 */
function validateModificationValue(value) {
    return check.boolean(value);
}


/**
 * Function used to validate the UUID passed in parameter
 * It needs to be an UUID v4
 * @param {*} uuid => Token given by the user
 * @returns true if all is good ; false + a message otherwise
 */
function validateUUIDv4(uuid) {
    if(!validator.isUUID(uuid, 4)) return [false, 'Not an UUID']; // Return not an UUID error

    return [true]
}


exports.validateEmailAddress = validateEmailAddress
exports.validatePassword = validatePassword
exports.validateRole = validateRole
exports.validateModificationValue = validateModificationValue
exports.validateUUIDv4 = validateUUIDv4