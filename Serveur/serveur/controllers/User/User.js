const User = require("../../models/User") // Import the model
const UserValidation = require('./UserValidation') // Import UserValidation
const UUID = require('uuid'); // Import uuid package
const jsonwebtoken = require('jsonwebtoken'); // Import jsonwebtoken package
const CSRF_tokens = require('csrf'); // Import the CSRF package to generate tokens
const requestIP = require('request-ip'); // Import the request-ip package to retrieve IPs of the clients
const keys = require('../../keys/getCSRFKey'); // Import the functions to get the keys for encryption
const fs = require('fs'); // Import the file system module
const bcrypt = require('bcrypt'); // Import the bcrypt package to encrypt passwords
const salt = require('../../keys/getSalt'); // Import the function to get the salt to encrypt passwords
const nodemailer = require('nodemailer'); // Import the nodemailer package to verify the identity of the user


/**
 * Function to know if we have a user in the database with this login and password
 * We update the database with a unique session ID to return to user
 *  => Session ID will be used for big operations like add, update, delete, … => We can control who makes requests
 * @param {*} req 
 * @param {*} res 
 * @returns the user if he is null
 */
exports.login = function (req, res) {
    // Check if we have the email and the password entered in the request
    if(req.body.email == undefined || req.body.password == undefined) return res.status(400).json({ message: "Email or password not defined" });

    var password_hashed = bcrypt.hashSync(req.body.password, salt.getSalt()); // Hash the password
    var email_provided = req.body.email.toLowerCase(); // Get the email in lowercase

    User.findOne({ email: email_provided, password: password_hashed })
    .then(user => {
        // Check if the user is null => No user in the database with email and password
        if(user == null) return res.status(200).json(user); // Return the user if he is null
        if(!bcrypt.compareSync(req.body.password, user.password)) return res.status(403).json(null); // Check if the hash and the password correspond + Return null to display error authentication message on VueJS
        if(!user.isActivated) return res.status(403).json({ message: 'Account not activated' }); // Cehck if the account of the user is activated or not

        var IP = requestIP.getClientIp(req); // Get IP of the client to put it into the JWT
        var sessionID = UUID.v4(); // Create the ID of the session
        var privateKey = fs.readFileSync('./keys/JWTPrivateKey.key');
        var authenticationTokenComplete = jsonwebtoken.sign({ email: user.email, role: user.role, ip: IP }, privateKey, { algorithm: 'RS256'}); // Encrypt the mail address and IP of the client
       
        var token = new CSRF_tokens(); // Create the CSRF token object
        var csrf_token = token.create(keys.getCSRFKey()); // Generate the CSRF token
        
        var today = new Date(); // Get the date of today

        // Update the session ID and the authentication token
        User.updateOne({ email: email_provided, password: password_hashed }, {$set: {
            sessionID: sessionID,
            dateSession: today,
            authenticationToken: authenticationTokenComplete,
            dateAuthentication: today,
            csrfToken: csrf_token,
            dateCsrf: today
        }})
        .then(() => {
            // Return email, sessionID, authenticationToken and CSRF token
            res.status(200).json({ email: user.email, role: user.role, sessionID: sessionID, authenticationToken: authenticationTokenComplete, csrf_token: csrf_token });
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function to be able to retrieve a user in the database according to the token user
 * @param {*} req 
 * @param {*} res 
 */
exports.loginWithToken = function (req, res) {
    // Check if the token is well passed into the body
    if(req.body.token == undefined || req.body.token == "") return res.status(403).json({ message: 'No token provided' });


    var IP = requestIP.getClientIp(req); // Get IP of the client
    var token_sent = req.body.token; // Get the token sent by the client

    
    // Try to decrypt the token
    try {
        var token_decrypt = jsonwebtoken.verify(token_sent, fs.readFileSync('./keys/JWTPublicKey.key')); // Decryption of the token

        // If token_IP not equals to IP_client
        if(token_decrypt.ip != IP) {
            return res.status(403).json({ message: 'IP incorrect' }); // Return IP error
        }
    } catch(error) {
        return res.status(400).json(null); // Return null 
    }
    
    
    // IP token and sent corresponds => Find the user associate to the token
    User.findOne({ authenticationToken: token_sent })
    .then(user => {
        // Check if the user is null => No user in the database with email and password
        if(user == null) return res.status(200).json(user); // Return the user if he is null
        if(user.email != token_decrypt.email) return res.status(403).json(null); // Check if the email in the token is the same than in the database
        if(user.role != token_decrypt.role) return res.status(403).json(null); // Check if the role in the token is the same than in the database
        if(!user.isActivated) return res.status(403).json(null); // Check if the account is activated or not
        
        var sessionID = UUID.v4(); // Create the ID of the session
        var privateKey = fs.readFileSync('./keys/JWTPrivateKey.key');
        var authenticationTokenComplete = jsonwebtoken.sign({ email: user.email, role: user.role, ip: IP }, privateKey, { algorithm: 'RS256'}); // Encrypt the mail address and IP of the client
        
        var token = new CSRF_tokens(); // Create the CSRF token object
        var csrf_token = token.create(keys.getCSRFKey()); // Generate the CSRF token

        var today = new Date(); // Get the date of today

        // Update the session ID and the authentication token
        User.updateOne({ authenticationToken: token_sent }, {$set: {
            sessionID: sessionID,
            dateSession: today,
            authenticationToken: authenticationTokenComplete,
            dateAuthentication: today,
            csrfToken: csrf_token,
            dateCsrf: today
        }})
        .then(() => {
            // Return email, sessionID, authenticationToken and CSRF token
            res.status(200).json({ email: user.email, role: user.role, sessionID: sessionID, authenticationToken: authenticationTokenComplete, csrf_token: csrf_token }); // Return the session ID to the identified customer
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to logout a user and to put the session ID to ''
 * @param {*} req 
 * @param {*} res 
 * @returns the response to quit the function
 */
exports.logout = function (req, res) {
    // Check if the authentication token is defined
    if (req.headers.authorization == undefined) return res.status(400).json({ message: 'No token provided' });
    

    // Find the user after passing the middleware to check session ID
    User.findOne({ authenticationToken: req.headers.authorization })
    .then(user => {
        if(user == null) return res.status(400).json({ message: "Cannot find a valid user" })
        else {
            User.updateOne({ authenticationToken: req.headers.authorization }, {$set: {
                sessionID: "",
                dateSession: null,
                authenticationToken: "",
                dateAuthentication: null,
                csrfToken: "",
                dateCsrf: null
            }})
            .then(() =>{
                res.status(200).json({ message: 'You\'re well deconnected'})
            })
            .catch(error => res.status(400).json(error))
        }
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function to add a user into the database
 * @param {*} req 
 * @param {*} res 
 */
exports.addUser = function (req, res) {    
    // Check if the email is valid
    var resultCheckEmail = UserValidation.validateEmailAddress(req.body.email);
    if(!resultCheckEmail[0]) {
        res.status(400).json({ message: resultCheckEmail[1] });
        return;
    }

    var email_provided = req.body.email.toLowerCase(); // Get the email in lower case


    // Check if a user already exists with the email entered
    User.findOne({ email: email_provided })
    .then(user => {
        if(user) {
            res.status(400).json({ message: "Email address already used" });
            return;
        }

        // Check if the password is valid
        var checkPassword = UserValidation.validatePassword(req.body.password);
        if(!checkPassword[0]) {
            res.status(400).json({ message: checkPassword[1] });
            return;
        }

        var password_hashed = bcrypt.hashSync(req.body.password, salt.getSalt()); // Hash the password
        var activation_token = UUID.v4(); // Create an activation token to be able to validate the account after

        // Create a new user
        const new_user = new User({
            email: email_provided,
            password: password_hashed,
            role: 'client',
            isActivated: false,
            activationToken: activation_token
        });


        // Save the new user + Send a mail to verify the identity
        new_user.save()
        .then(() => {
            var credentials = require('../../config/email_credentials.json'); // Load the JSON of credentials to use to authentication
            
            // Create the transporter to be able to send mail
            const transporter = nodemailer.createTransport({
                port: 465,
                host: credentials.smtp_address,
                auth: {
                    user: credentials.email,
                    pass: credentials.password
                },
                secure: true
            });


            // Mail template part
            var template_mail = fs.readFileSync('./controllers/User/template/templateValidationMail.html', 'utf8'); // Read the mail template
            template_mail = template_mail.split('ACTIVATION_TOKEN').join(activation_token); // Replace token by the good one in the URL


            // Create the mail
            const mail = {
                from: credentials.email,
                to: new_user.email,
                subject: 'Activation de votre compte',
                html: template_mail
            };


            // Send the email
            transporter.sendMail(mail, function (err, info) {
                if (err) console.log(err);
            });


            res.status(200).json({ message: "ok" }); // Send the response
        })
        .catch(error => res.status(400).json(error)) 
    })
    .catch(error => res.status(400).json(error))      
}


/**
 * Function used to modify the connection values of a user (identification by multiple old values 
 *   like authentication token, sessionID, CSRF token, email or old password)
 * When we have changed the values, we return a new informations package that 
 *   will contain new authentication values like CSRF, session ID, authentication token, email and role
 * @param {*} req 
 * @param {*} res 
 * @returns the response to quit the function and stop the processing
 */
exports.modifyUser = function (req, res) {
    // Check if the token is well passed into the body
    if(req.body.token == undefined || req.body.token == "") return res.status(403).json({ message: 'Token error' });


    var IP = requestIP.getClientIp(req); // Get IP of the client
    var token_sent = req.body.token; // Get the token sent by the client

    
    // Try to decrypt the token
    try {
        var token_decrypt = jsonwebtoken.verify(token_sent, fs.readFileSync('./keys/JWTPublicKey.key')); // Decryption of the token

        // If token_IP not equals to IP_client
        if(token_decrypt.ip != IP) return res.status(403).json({ message: 'Token error' }); // Return a token error

        // If the email in the token is different from last email of the user
        if(token_decrypt.email != req.body.last_email) returnres.status(403).json({ message: 'Token error' }); // Return a token error

    } catch(error) {
        return res.status(400).json({ message: 'Token error' }); // Return a token error
    }
    
    // Check if the email is valid
    var resultCheckEmail = UserValidation.validateEmailAddress(req.body.email);
    if(!resultCheckEmail[0]) {
        res.status(400).json({ message: 'email invalid' });
        return;
    }

    var email_provided = req.body.email.toLowerCase(); // Get the email in lower case

    // Find if the new email the user wants to use is not already used
    User.findOne({ email: email_provided })
    .then(user => {
        // Email already used + Verify if the two addresses are different or not (Same address => It is the person who wants to modify its connection values)
        if(user && user.email != req.body.last_email) return res.status(400).json({ message: "Email address already used" });


        // Find the user to modify
        User.findOne({ email: req.body.last_email, authenticationToken: req.body.token, csrfToken: req.headers.csrftoken, sessionID: req.headers.authorization })
        .then(user_to_modify => {
            if(!user_to_modify) return res.status(400).json({ message: "No corresponding user" }); // Return an error if we have found no user


            // Check if the old password entered by the user is the same in the database
            if(!bcrypt.compareSync(req.body.oldPassword, user_to_modify.password)) return res.status(400).json({ message: "Not your old password" });

            // Check if the new password is valid
            var checkPassword = UserValidation.validatePassword(req.body.password);
            if(!checkPassword[0]) {
                res.status(400).json({ message: checkPassword[1] });
                return;
            }


            // Generate the new connection tokens (CSRF, sessionID, authentication token) and informations
            var sessionID = UUID.v4(); // Create the new ID of the session
            var privateKey = fs.readFileSync('./keys/JWTPrivateKey.key');
            var authenticationTokenComplete = jsonwebtoken.sign({ email: email_provided, role: user_to_modify.role, ip: IP }, privateKey, { algorithm: 'RS256'}); // Encrypt the mail address and IP of the client
            var token = new CSRF_tokens(); // Create the CSRF token object
            var csrf_token = token.create(keys.getCSRFKey()); // Generate the CSRF token
            var today = new Date(); // Get the date of today

            // Update the values and add new ones to the user
            user_to_modify.email = email_provided// Put the new email of the user
            user_to_modify.password = bcrypt.hashSync(req.body.password, salt.getSalt()) // Put the new password of the user
            user_to_modify.sessionID = sessionID; // Update the session ID
            user_to_modify.dateSession = today; // Update the date of creation of the session ID
            user_to_modify.authenticationToken = authenticationTokenComplete; // Update the authentication token
            user_to_modify.dateSession = today; // Update the date of creation of the authentication token
            user_to_modify.csrfToken = csrf_token; // Update the CSRF token
            user_to_modify.dateCsrf = today; // Update the date of creation of the CSRF token


            // Update the user with the new values
            User.updateOne({ email: req.body.last_email }, { $set: user_to_modify })
            .then(() => {
                // Return all the informations to identify a customer
                res.status(200).json(
                    {
                        email: user_to_modify.email,
                        role: user_to_modify.role,
                        sessionID: sessionID,
                        authenticationToken: authenticationTokenComplete,
                        csrf_token: csrf_token
                    }
                );
            })
            .catch(error => res.status(400).json(error))
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function to get all the users
 * Just get _id, email and role
 * @param {*} req 
 * @param {*} res 
 */
exports.getAllUsersForDashboard = function (req, res) {
    User.find({}, { _id: 1, email: 1, role: 1, isActivated: 1 })
    .then(users => {
        return res.status(200).json(users); // Return all the users
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to modify a user when the user is administrator
 * @param {*} req 
 * @param {*} res 
 */
exports.modifyUserByAdmin = function (req, res) {
    // Get all the infotmations sent by the administrator
    var email = req.body.email;
    var password = req.body.password;
    var modify_password = req.body.modifyPassword;
    var role = req.body.role;
    var modify_role =req.body.modifyRole;
    var account_state = req.body.accountState;
    var modify_account_state = req.body.modifyState;


    // Check the email address
    var resultCheckEmail = UserValidation.validateEmailAddress(email); // Result of the validation of the email address
    if(!resultCheckEmail[0]) return res.status(400).json({ message: 'Not an email' }); // Return not an email error

    var email_provided = req.body.email.toLowerCase(); // Get the email in lower case
    
    // Find the user by email
    User.findOne({ email: email_provided })
    .then(user => {
        if(user == null) return res.status(400).json({ message: 'No user' }); // Return the no user error


        // Validate the modifications values
        if(!UserValidation.validateModificationValue(modify_password)) return res.status(400).json({ message: 'Password modification incorrect' });
        if(!UserValidation.validateModificationValue(modify_role)) return res.status(400).json({ message: 'Role modification incorrect' });
        if(!UserValidation.validateModificationValue(modify_account_state)) return res.status(400).json({ message: 'Account state modification incorrect' });


        // Validate the password
        if(modify_password) {
            resultCheckPassword = UserValidation.validatePassword(password);
            if(!resultCheckPassword[0]) return res.status(400).json({ message: 'Password incorrect' });
        }


        // Validate the role
        if(modify_role) {
            if(!UserValidation.validateRole(role)) return res.status(400).json({ message: 'Role incorrect'});
        }


        // Validate the account state
        if(modify_account_state) {
            if(!UserValidation.validateModificationValue(account_state)) return res.status(400).json({ message: 'Account state incorrect'});
        }


        var request = {}; // Declare a variable for the dynamic request

        // Format the request with the value we want
        if(modify_password) request.password = bcrypt.hashSync(req.body.password, salt.getSalt()); // Set the password
        if(modify_role) request.role = role; // Set the role of the user
        if(modify_account_state) request.isActivated = account_state; // Set the account state of the user


        // Update the user
        User.updateOne({ email: email_provided }, { $set: request })
        .then(() => {
            return res.status(200).json({ message: 'OK' }); // Return the OK response => The user has been modified
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}

/**
 * Function used to delete a user when the user is administrator
 * @param {*} req 
 * @param {*} res 
 */
exports.deleteUserByAdmin = function (req, res) {
    // Find the user to delete
    User.findOne({ _id: req.params.id })
    .then(user => {
        if (user == null) return res.status(400).json({ message: 'No user' }); // Return the no user error => No user find in DB
    

        // Delete the user by its ID
        User.deleteOne({ _id: req.params.id })
        .then(() => {
            return res.status(200).json({ message: 'OK' }); // Return the OK response => The user has been deleted
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to activate the account of the user
 * @param {*} req 
 * @param {*} res 
 * @returns the error if we have one
 */
exports.activateAccount = function (req, res) {
    var activationToken = req.body.activationToken; // Token used to activate the account

    if (activationToken == undefined) return res.status(400).json({ message: 'Activation token undefined' }); // Return undefined error

    var resultCheckUUID = UserValidation.validateUUIDv4(activationToken); // Validate the token
    if(!resultCheckUUID[0]) return res.status(400).json({ message: resultCheckUUID[1] }); // Return the UUID error


    // Find the user associated to the token
    User.findOne({ activationToken: activationToken })
    .then(user => {
        if (user == null) return res.status(400).json({ message: 'No user' }); // No user corresponds to the token


        // Activate the user account
        User.updateOne({ email: user.email }, {$set: { isActivated: true, activationToken: '' }})
        .then(() => {
            res.status(200).json({ message: 'OK' }); // Return OK => the user can connect to the application
        })
        .catch(error => res.status(400).json(error));
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to begin the process to reinitialize the forgotten password of the user
 * Set token and date in DB + send the URL with the token to be able to access to the route to reinit the forgotten passwd
 * @param {*} req 
 * @param {*} res 
 * @returns the error if we have one
 */
exports.emailForReinitPassword = function (req, res) {
    var email = req.body.email; // Get the email from the body

    // Cehck the email
    var resultCheckEmail = UserValidation.validateEmailAddress(email); // Validate the email addr
    if(!resultCheckEmail[0]) return res.status(400).json({ message: 'Not an email' }); // Return the not an email error

    var email_provided = req.body.email.toLowerCase(); // Get the email provided in lower case

    // Find the user
    User.findOne({ email: email_provided })
    .then(user => {
        if (user == null) return res.status(400).json({ message: 'No user' }); // No user corresponds to the email given
        if(!user.isActivated) return res.status(400).json({ message: 'Account not activated' }); // The account is not activated

        var token_to_access_reinit_passwd_route = UUID.v4(); // Create a token to be able to access to the route to modify the forgotten passwd
        var today = new Date(); // Get the date of today to reinit the token when he will expire


        // Updat the user with the token and the date to be able to access to the route to update the password
        User.updateOne({ email: email_provided }, { $set: { tokenRouteForgottenPasswd: token_to_access_reinit_passwd_route, dateTokenRouteForgottenPasswd: today } })
        .then(() => {
            var credentials = require('../../config/email_credentials.json'); // Load the JSON of credentials to use to authentication
            
            // Create the transporter to be able to send mail
            const transporter = nodemailer.createTransport({
                port: 465,
                host: credentials.smtp_address,
                auth: {
                    user: credentials.email,
                    pass: credentials.password
                },
                secure: true
            });


            // Mail template part
            var template_mail = fs.readFileSync('./controllers/User/template/templateModifForgottenPasswdMail.html', 'utf8'); // Read the mail template
            template_mail = template_mail.split('MODIFICATION_TOKEN').join(token_to_access_reinit_passwd_route); // Replace token by the good one in the URL


            // Create the mail
            const mail = {
                from: credentials.email,
                to: email,
                subject: 'Modification de votre mot de passe oublié',
                html: template_mail
            };


            // Send the email
            transporter.sendMail(mail, function (err, info) {
                if (err) console.log(err);
            });


            res.status(200).json({ message: "OK" }); // Send the response
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to know if the VueJS application can display the form or not
 * @param {*} req 
 * @param {*} res 
 * @returns the error if we have one
 */
exports.canModifyForgottenPassword = function (req, res) {
    var token = req.params.token; // Get the token passed in parameter

    // Check the token
    if(token == undefined) return res.status(400).json({ message: 'Token undefined' }); // Return the undefined token error
    var resultCheckToken = UserValidation.validateUUIDv4(token);
    if(!resultCheckToken[0]) return res.status(400).json({ message: resultCheckToken[1] }); // Return the not an uuid error


    // Find the user by token
    User.findOne({ tokenRouteForgottenPasswd: token })
    .then(user => {
        if (user == null) return res.status(400).json({ message: 'No user' }); // Return the no user error

        var token_modify_forgotten_passwd = UUID.v4(); // Generate the token to be able to modify the passwd
        var today = new Date(); // Get the date of today


        // Reset the token and date to access to the route to modify the passwd of the user
        // And set the token and date to modify the user passwd
        User.updateOne({ email: user.email }, { $set: { tokenRouteForgottenPasswd: "", dateTokenRouteForgottenPasswd: null, tokenForgottenPasswd: token_modify_forgotten_passwd, dateTokenForgottenPasswd: today } })
        .then(() => {
            res.status(200).json({ message: 'OK', token: token_modify_forgotten_passwd }); // Return the token to use in the view to modify the passwd of the user
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to update the forgotten password of the user
 * @param {*} req 
 * @param {*} res 
 * @returns the error if we have one
 */
exports.modifyForgottenPassword = function (req, res) {
    var token = req.body.token; // Get the token to identify the user
    var email = req.body.email; // Get the email of the user
    var new_password = req.body.password; // Get the new password of the user


    // Check the token
    if(token == undefined) return res.status(400).json({ message: 'Token undefined' }); // Return the token undefined error
    
    var resultCheckToken = UserValidation.validateUUIDv4(token);
    if(!resultCheckToken[0]) return res.status(400).json({ message: resultCheckToken[1] }); // Return the not an uuid error


    // Check the email
    var resultCheckEmail = UserValidation.validateEmailAddress(email);
    if(!resultCheckEmail[0]) return res.status(400).json({ message: 'Not an email' }); // Return the not an email error


    // Check the password
    var resultCheckPassword = UserValidation.validatePassword(new_password);
    if(!resultCheckPassword[0]) return res.status(400).json({ message: 'Password invalid' }); // Return the invalid password error

    var email_provided = req.body.email.toLowerCase(); // Get the email provided in lower case

    // Find the user by token and email
    User.findOne({ email: email_provided, tokenForgottenPasswd: token })
    .then(user => {
        if (user == null) return res.status(400).json({ message: 'No user' }); // Return the no user error

        var new_encrypted_password = bcrypt.hashSync(new_password, salt.getSalt()); // Encrypt the new password of the user

        // Update the user
        User.updateOne({ email: email_provided }, { $set: { password: new_encrypted_password, tokenForgottenPasswd: "", dateTokenForgottenPasswd: null } })
        .then(() => {
            res.status(200).json({ message: 'OK' }); // Return the OK message
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to be able to add a new user into the database when the user who has made the request is admin of the system
 * @param {*} req 
 * @param {*} res 
 * @returns the error if we have one
 */
exports.addUserByAdmin = function (req, res) {
    // Get all the values contained in the body
    var email = req.body.email;
    var password = req.body.password;
    var role = req.body.role;
    var isAccountActivated = req.body.isAccountActivated;

    
    // Check the email address
    resultCheckEmail = UserValidation.validateEmailAddress(email);
    if(!resultCheckEmail[0]) return res.status(400).json({ message: 'Email incorrect' });

    // Check the password
    resultCheckPassword = UserValidation.validatePassword(password);
    if(!resultCheckPassword[0]) return res.status(400).json({ message: 'Password incorrect' });

    // Check the role
    resultCheckRole = UserValidation.validateRole(role);
    if(!resultCheckRole) return res.status(400).json({ message: 'Role incorrect' });

    // Check if the isAccoutnActivated
    resultCheckIsActivatedAccount = UserValidation.validateModificationValue(isAccountActivated);
    if(!resultCheckIsActivatedAccount) return res.status(400).json({ message: 'isAccountActivated incorrect' });

    
    // Check if we have already someone with the same email address
    User.findOne({ email: email.toLowerCase() })
    .then(user => {
        if(user) return res.status(400).json({ message: 'A user already exists' }); // Return an error if we have already a user defined with the email address


        // Creation of the new user
        const new_user = new User({
            email: email.toLowerCase(),
            password: bcrypt.hashSync(password, salt.getSalt()),
            role: role,
            isActivated: isAccountActivated
        });


        // Save the new user in database
        new_user.save()
        .then(() => {
            // Return a part of the new_user to display on the view
            return res.status(201).json({
                _id: new_user._id,
                email: new_user.email,
                password: new_user.password,
                isActivated: new_user.isActivated,
                role: new_user.role
            });
        })
        .catch(error => res.status(400).json(error))
    })
}