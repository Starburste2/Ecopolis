const Microcontroller = require("../../models/Microcontroller"); // Import the model
const verificationMicrocontroller = require("./MicrocontrollerValidation"); // Import the validation methods
const PoolAddress = require("../../models/PoolAddress"); // Import the model
const Datas = require("../../models/Datas"); // Import the model
const Group = require("../../models/Group"); // Import the Group model


/**
 * Function used to create one microcontroller
 * Can add main or end node
 * Can have multiple end nodes
 * But only one end node
 * @param {*} req 
 * @param {*} res 
 * @returns if we have an error when check the addr and the sleepTime
 */
exports.createMicrocontroller = function (req, res) {
    var address = req.body.addr;

    // Check the addr of the microcontroller
    PoolAddress.findOne({ addr: address })
    .then(pool_address => {
        
        // Check the addr
        resultCheckAddr = verificationMicrocontroller.checkAddr(address, pool_address);
        if(!resultCheckAddr[0]){
            res.status(400).send({ error: 'Addr incorrect' })
            return;
        };


        // Check the isEndNode
        resultIsEndNode = verificationMicrocontroller.checkIsEndNode(req.body.isEndNode);
        if(!resultIsEndNode[0]){
            res.status(400).send({ error: 'End node incorrect' })
            return;
        };


        // Check the sleepTimes after checking the isEndNode because we need this value
        resultCheckSleepTime = verificationMicrocontroller.checkSleepTime(req.body.sleepTime, req.body.isEndNode);
        if(!resultCheckSleepTime[0]){
            res.status(400).send({ error: 'Sleep times incorrect' })
            return;
        };


        // Check the position of the microcontroller only if it is an end node => We know where is placed the main one
        if(req.body.isEndNode) {
            resultCheckPosition = verificationMicrocontroller.checkPosition(req.body.markerPosition);
            if(!resultCheckPosition[0]){
                res.status(400).send({ error: 'Position incorrect' })
                return;
            }
        }


        // Check if the group is defined in the body
        if(req.body.group == undefined) return res.status(400).json({ error: 'Group undefined' });


        // If it is an End node => We can add it directly to the database
        if(req.body.isEndNode){
            actual_date = new Date();

            
            // Get the groups if we have one
            Group.findOne({ name: req.body.group })
            .then(group => {
                var microcontroller; // Create a microcontroller 
                
                // No group
                if(group == null) {
                    microcontroller = new Microcontroller({
                        addr: req.body.addr,
                        sleepTime: req.body.sleepTime,
                        hasConfigChanged: true, // True to get the configuration of the server even if it is the good conf
                        isEndNode: req.body.isEndNode,
                        creationDate: actual_date,
                        modificationDate: actual_date,
                        latitude: req.body.markerPosition[0],
                        longitude: req.body.markerPosition[1],
                        groups: "",
                        destinationGroup: "",
                        isRelay: false,
                        manualReconfiguration: true
                    });
                
                // A group
                } else {
                    microcontroller = new Microcontroller({
                        addr: req.body.addr,
                        sleepTime: req.body.sleepTime,
                        hasConfigChanged: true, // True to get the configuration of the server even if it is the good conf
                        isEndNode: req.body.isEndNode,
                        creationDate: actual_date,
                        modificationDate: actual_date,
                        latitude: req.body.markerPosition[0],
                        longitude: req.body.markerPosition[1],
                        groups: group.name,
                        destinationGroup: group.referingNode,
                        isRelay: false,
                        manualReconfiguration: true
                    });
                }

                // Save the new microcontroller
                microcontroller.save()
                .then(
                    () => {
                        
                        // Update the pool to say that the addr is not available now
                        PoolAddress.updateOne({ addr: address }, {$set: { isAvailable: false }})
                        .then(
                            () => {
                                res.status(201).json({
                                    message: 'End node saved successfully!'
                                });
                            }
                        )
                        .catch(error => res.status(400).json({ error }));
                    }
                )
                .catch(error => res.status(400).json({ error }));
            })
            .catch(error => res.status(400).json(error))
    
        } else {
            
            // Get the possible main nodes => Not an end or a relay node
            Microcontroller.find({ isEndNode: false, isRelay: false })
            .then(
                microcontroller_array => {

                    // If we don't have a main node
                    if(microcontroller_array.length == 0){
                        actual_date = new Date();


                        // Create a microcontroller with the parameters
                        const microcontroller = new Microcontroller({
                            addr: req.body.addr,
                            sleepTime: req.body.sleepTime,
                            hasConfigChanged: false, // No need to set a config changed because this microcontroller doesn't have a configuration
                            isEndNode: req.body.isEndNode,
                            creationDate: actual_date,
                            modificationDate: actual_date
                        });


                        // Save the new microcontroller
                        microcontroller.save()
                        .then(
                            () => {
                                
                                // Update the pool to say that the addr is not available now
                                PoolAddress.updateOne({ addr: address }, {$set: { isAvailable: false }})
                                .then(
                                    () => {
                                        res.status(201).json({
                                            message: 'Main node saved successfully!'
                                        });
                                    }
                                )
                                .catch(error => res.status(400).json({ error }));
                            }
                        )
                        .catch(error => res.status(400).json({ error }));
                        
                        
                    } else { // If we have a main node => Return error
                        res.status(400).json({ message: "Only one main node" });
                    }
                }
            )
            .catch(error => res.status(400).json({ error }))
        }
    })
    .catch(error => res.status(400).json({ error }))
};


/**
 * Function used to get one microcontroller by id
 * @param {*} req 
 * @param {*} res 
 */
exports.getOneMicrocontroller = function (req, res) {
    Microcontroller.findOne({ _id: req.params.id }, { latitude: 1, longitude: 1, modificationDate: 1, creationDate: 1, isEndNode: 1, addr: 1, sleepTime: 1, groups: 1, destinationGroup: 1, _id: 0 })
      .then(microcontroller => res.status(200).json(microcontroller))
        .catch(error => res.status(404).json({ error }));
};


/**
 * Function used to modify a microcontroller
 * @param {*} req 
 * @param {*} res 
 * @returns if we have an error when we check the parameters
 */
exports.modifyAMicrocontroller = function (req, res) {
    // Check the sleepTime
    resultCheckSleepTime = verificationMicrocontroller.checkSleepTime(req.body.sleepTime, true);
    if(!resultCheckSleepTime[0]){
        res.status(400).send({ message: 'Sleep time incorrect' })
        return;
    };

    
    // Check if the position of the microcontroller
    resultCheckPosition = verificationMicrocontroller.checkPosition(req.body.markerPosition);
    if(!resultCheckPosition[0]){
        res.status(400).send({ message: 'Position incorrect' })
        return;
    }


    // Check if the group is defined
    if(req.body.group == undefined) return res.status(400).json({ message: 'Group undefined' });


    // Find the group associated to the selected one of the user
    Group.findOne({ name: req.body.group })
    .then(group => {
        var microcontroller; // Create a microcontroller 
        
        // No group
        if(group == null) {
            microcontroller = new Microcontroller({
                _id: req.params.id,
                sleepTime: req.body.sleepTime,
                hasConfigChanged: true,
                latitude: req.body.markerPosition[0],
                longitude: req.body.markerPosition[1],
                modificationDate: new Date(),
                groups: "",
                destinationGroup: "",
                isRelay: false,
                manualReconfiguration: true
            });
        } else {
            microcontroller = new Microcontroller({
                _id: req.params.id,
                sleepTime: req.body.sleepTime,
                hasConfigChanged: true,
                latitude: req.body.markerPosition[0],
                longitude: req.body.markerPosition[1],
                modificationDate: new Date(),
                groups: group.name,
                destinationGroup: group.referingNode,
                isRelay: false,
                manualReconfiguration: true
            });
        }


        // Update the microcontrollers
        Microcontroller.updateOne({_id: microcontroller.id}, {
            $set: {
                sleepTime: microcontroller.sleepTime, 
                hasConfigChanged: microcontroller.hasConfigChanged,
                modificationDate: microcontroller.modificationDate,
                latitude: microcontroller.latitude,
                longitude: microcontroller.longitude,
                groups: microcontroller.groups,
                destinationGroup: microcontroller.destinationGroup,
                isRelay: microcontroller.isRelay,
                manualReconfiguration: microcontroller.manualReconfiguration
            }})
        .then(() => res.status(200).json({ message: 'Microcontroller modifié !'}))
        .catch(error => res.status(400).json({ error }));
    })
    .catch(error => res.status(400).json(error))
};


/**
 * Function used to delete one microcontroller by id and update the pool by the addr
 * @param {*} req 
 * @param {*} res 
 */
exports.deleteMicrocontroller = function (req, res) {
    var addr = req.params.addr; // Get the addr passed in parameter

    // Check the addr of the microcontroller
    resultCheckAddr = verificationMicrocontroller.checkAddr(addr, null, false);
    if(!resultCheckAddr[0]){
        res.status(400).send({ error: 'Addr invalid' })
        return;
    };

    // Delete a microcontroller by id and by addr
    Microcontroller.deleteOne({ _id: req.params.id, addr : addr })
    .then(
        (params) => {
            
            // Test if we have well deleted a value or not
            if(params.deletedCount != 0){
                
                // Update the pool to be able to re-use the addr
                PoolAddress.updateOne({ addr: addr }, {$set: { isAvailable: true }})
                .then(
                    () => {

                        // Remove all the datas associated to the microcontroller address
                        Datas.deleteMany({ addrMicrocontroller: addr })
                        .then(
                            () => {
                                res.status(200).json({ message: 'Microcontroller supprimé, pool modifié et données supprimées avec succès !'})
                            }
                        )
                        .catch(error => res.status(400).json({ error }))
                    }
                )
            
            } else {
                res.status(400).json({ message: "Microcontroller doesn't exist" });
            }
            
        }
    )
    .catch(error => res.status(400).json({ error }));
};


/**
 * Function used to get all the microcontroller
 * @param {*} req 
 * @param {*} res 
 */
exports.getAllMicrocontrollers = function (req, res) {
    Microcontroller.find({}, { __v: 0 })
        .then(microcontrollers => res.status(200).json(microcontrollers))
        .catch(error => res.status(400).json({ error }));
};


/**
 * Function to get all the microcontrollers for the VueJS showcase app
 * @param {*} req 
 * @param {*} res 
 */
exports.getMicrocontrollersForShowcase = function (req, res) {
    Microcontroller.find({ isEndNode: true }, { addr: 1, sleepTime: 1, longitude: 1, latitude: 1, _id: 0 })
        .then(microcontrollers => res.status(200).json(microcontrollers))
        .catch(error => res.status(400).json({ error }));
}


/**
 * Function used to change the hasConfigChanged to false
 * So the micrcontroller has received its new configuration
 * @param {*} req 
 * @param {*} res 
 * @returns if we have an error in the addr passed in parameter
 */
exports.microcontrollerIsUpdated = function (req, res) {
    addr = req.body.A;
    
    
    // Check the addr of the microcontroller
    resultCheckAddr = verificationMicrocontroller.checkAddr(addr, null, false);
    if(!resultCheckAddr[0]){
        res.status(400).send({error: resultCheckAddr[1]})
        return;
    };
    

    // First : Get the microcontroller which has the address given
    Microcontroller.findOne({addr: addr})
    .then(microcontroller => {
            
        // Second : Update the hasConfigChanged to false because the microcontroller has received its new config
        Microcontroller.updateOne({_id: microcontroller.id}, { $set: { hasConfigChanged: false } })
        .then(() => {
            
            // If the microcontroller has a group => Return the addr of the microcontroller which is reconfigured
            if (microcontroller.groups != "") {
                res.status(200).json({ ENA: Number.parseInt(microcontroller.addr, 16) }); // from Receiver => to Relay => to ENA (End Node Address)
                
            // No group => Use the address got by the receiver code
            } else {
                res.status(200).json({}); // from Receiver => to Sender
            }
        })
        .catch(error => res.status(400).json({ error }))
    })
    .catch(error => res.status(400).json({ error }))
}


/**
 * Function used to be able to know which microcontrollers have not sent their records since 1 day
 * @param {*} req 
 * @param {*} res 
 */
exports.hasMicrocontrollerSentDatasInTheLastDay = function (req, res) {
    var sending_before = new Date(); // Get the date of today
    sending_before.setDate(sending_before.getDate() - 1); // Retrieve one day to the today date


    // Find the microcontroller that have not sent their records since 1 day
    Microcontroller.find({ lastSending: { $lt: sending_before }, isEndNode: true })
    .then(microcontrollers => {

        // If we have microcontrollers => formatte the address and send them
        if(microcontrollers.length != 0) {
            var array_microcontrollers = []; // Array of microcontrollers addr
    
            // Push each addr in the array
            for (const microcontroller of microcontrollers){
                array_microcontrollers.push(microcontroller.addr)
            }


            // Return the OK message + the microcontrollers
            return res.status(200).json({ microcontrollers: array_microcontrollers.join(', ') }); 
        

        // Send an empty array
        } else {
            return res.status(200).json({ microcontrollers: [] }); // Return an empty arrayt + OK message
        }
    })
    .catch(error => res.status(400).json(error))
}