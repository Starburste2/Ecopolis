var check = require('check-types');


/**
 * Function to check if the address of the microcontroller passed in parameter
 * is good or not 
 * @param {*} address => Address of the microcontroller
 * @param pool_address => A record of the pooladdresses collection of our database
 * @param use_pool_address => Instance of pooladdresses available
 * @returns an array => [false, error_message] or [true]
 */
function checkAddr(address, pool_address, use_pool_address = true) {
    if(use_pool_address) if(pool_address == null) return [false, "Address not known"]; // Check if the address is known or not
    if(address == undefined) return [false, "Address is not defined"]; // Check undefined
    if(check.not.string(address)) return [false, "Adress is not a string"]; // Check string
    if(address.length > 2) return [false, "Address is too long"]; // Check the length
    if(use_pool_address) if(!pool_address.isAvailable) return [false, "Address not available"]; // Check if the addr is available in the pool of addresses
    
    return [true];
}


/**
 * Function to check if the sleep time of the microcontroller passed in parameter
 * is good or not
 * Little specificity : Main node => No sleep times (if we have one or more => Problem)
 * @param {*} sleepTime => Array of sleepTime of the microcontroller
 * @param {*} isEndNode => Boolean to know if the microcontroller will be an end node or not
 * @returns an array => [false, error_message] or [true]
 */
function checkSleepTime(sleepTime, isEndNode) {
    if(sleepTime === undefined) return [false, "Sleep time is not defined"] // Check undefined

    if(!isEndNode && sleepTime.length == 0) return [true] // Return true if the microcontroller is a main node and if the tab of sleep times contains nothing 
    if(!isEndNode && sleepTime.length != 0) return [false, "A main node doesn't need sleep times"] // Check if we pass a main node with sleep times

    if(!Array.isArray(sleepTime)) return [false, "Sleep time is not an array"]; // Check if an array
    if(sleepTime.filter(Array.isArray).length != 0) return [false, "Sleep time array is a 1D array"]; // Check if a 1D array
    if(sleepTime.length > 24) return [false, "Sleep time cannot contain more than 24 values"]; // Check numbers of sleepTime
    if(sleepTime.length == 0) return [false, "Sleep time array need to contain at least one value"]; // Check the min possible values

    // Check the values entered in sleepTime
    for(const sleepTimeValue of sleepTime) {
        var sleepTimeValueSplit = sleepTimeValue.toString().split(":");
        
        // Test length > 2
        if(sleepTimeValueSplit.length != 3) return [false, "Sleep time format is : hour:minutes:seconds"]; // Check format
        
        // Check all the values
        for(const [index, values] of sleepTimeValueSplit.entries()) {
            if(isNaN(values) || !Number.isInteger(parseFloat(values)) || Number(parseFloat(values)) < 0) return [false, "Sleep needs to contain integers (and greater than 0) for hours and minutes"]; // Check if integers

            // Check the hours
            if (index === 0) {
                if (values > 23) {
                    return [false, "Sleep time hours are between 0 and 23"]; // Check the hours
                }
    
              // Check the minutes
            } else {
                if (values > 59) {
                    return [false, "Sleep time hours are between 0 and 59"]; // Check the minutes
                }
            }
        }
    }

    return [true];
}


/**
 * Function to check if the boolean parameter to say if a microcontroller is an end node or not
 * is good or not
 * @param {*} isEndNode => Boolean to know if the microcontroller is an end or a main node
 * @returns an array => [false, error_message] or [true]
 */
function checkIsEndNode(isEndNode) {
    if(isEndNode == undefined) return [false, "isEndNode is not defined"];
    if(check.not.boolean(isEndNode)) return [false, "isEndNode is not a boolean"];

    return [true];
}


/**
 * Function used to verify if the latitude and longitude passed in parameters are correct or not
 * @param {*} markerPosition => Position of the microcontroller placed on the map
 * @returns an array => [false, error_message] or [true]
 */
function checkPosition(markerPosition) {
    if(markerPosition === undefined) return [false, "The position of the marker is not defined"]; // Check undefined
    if(!Array.isArray(markerPosition)) return [false, "The position of the marker is not an array"]; // Check if an array
    if(markerPosition.length !== 2) return [false, "The position of the marker is an array of length 2"]; // Check array length


    // Check if the variables are defined
    if(markerPosition[0] === undefined) return [false, "Latitude is not defined"];
    if(markerPosition[1] === undefined) return [false, "Longitude is not defined"];


    // Define the patter of the regexp to test the latitude and longitude value
    let pattern = new RegExp('^-?([1-8]?[1-9]|[1-9]0)\\.{1}\\d{1,6}');
    
    // Test the latitude and longitude with ther regexp
    if(!pattern.test(markerPosition[0])) return [false, "Latitude is not well formed"];
    if(!pattern.test(markerPosition[1])) return [false, "Longitude is not well formed"];
  

    return [true]
}


// Exports the functions
exports.checkAddr = checkAddr
exports.checkSleepTime = checkSleepTime
exports.checkIsEndNode = checkIsEndNode
exports.checkPosition = checkPosition