const Datas = require("../../models/Datas"); // Import the Datas model to retrieve the datas present in DB
const Microcontroller = require("../../models/Microcontroller"); // Import the Microcontroller model to be able to know the creation date when no datas in DB
const fs = require('fs'); // Import the filesystem module
const MicrocontrollerValidation = require('../Microcontroller/MicrocontrollerValidation'); // Import the MicrocontrollerValidation


/**
 * This function is used to upload a file with the key datas (We need to have a first data in the database)
 * This function will do an aggregation with the datas that are already present in database (up and down aggregation)
 * => sleep time will be in adquacy with the previous or the next one
 * @param {*} req 
 * @param {*} res 
 * Error code to return :
 *   1 => Insertion is OK
 *   2 => Datas are already here
 *  -1 => Microcontroller doesn't exist
 *  -2 => No datas present in database => Launch the microcontroller
 *  -3 => The CSV is poorly formed => Return problem
 */
exports.uploadDatas = function (req, res) {
    try {

        // If no file uploaded
        if(!req.files.datas) {
            res.status(400).json({
                message: 'No file with the key datas uploaded'
            });


        } else { // We have an uploaded file

            // Use the name of the key to upload the file => Here : datas
            let datas = req.files.datas;

            // Move the datas.csv file into the files_uploaded directory to be able to read it easily
            datas.mv('./files_uploaded/datas.csv', function (err) {
                // Error => Return the problem
                if(err) {
                    res.status(400).json({ err });
                    return;
                }


                // Read the CSV file
                var text_file = fs.readFileSync('./files_uploaded/datas.csv', 'utf8');

                // Convert into an array
                var splitted_array_sd_datas = String(text_file).split("\n"); // Array with just split the line break
                var array_sd_datas = []; // Define the array which contains the good datas
                
                // Loop on all the values in splitted_array_sd_datas => index of the iteration, data
                for(const [index, data] of splitted_array_sd_datas.entries()) { 
                    if(index + 1 != splitted_array_sd_datas.length) // To delete the last line in the array which contains nothing
                        array_sd_datas.push(data.split(";")); // Add the datas to the array
                }


                // Test the length of the array
                if(array_sd_datas.length == 0) {
                    deleteFileUploaded('./files_uploaded/datas.csv'); // Delete the uploaded file
                    return res.status(400).json({ message: 'File malformed', code: -3 }); // Return the error
                }


                // Check the addr to know if it is good or not => To know if we can make processing or not
                resultCheckAddr = MicrocontrollerValidation.checkAddr(array_sd_datas[0][0], null, false);
                if(!resultCheckAddr[0]){
                    res.status(400).send({ error: 'Addr invalid' })
                    return;
                };


                // Test if the array has a good format => 11 elements by line
                for(const data of array_sd_datas) {
                    if(data.length != 11) {
                        deleteFileUploaded('./files_uploaded/datas.csv'); // Delete the uploaded file
                        return res.status(400).json({ message: 'File malformed', code: -3 }); // Return the error
                    }

                    // Check if the informations are number or not
                    for(i = 1 ; i < data.length ; i++) {
                        if(isNaN(data[i])) {
                            deleteFileUploaded('./files_uploaded/datas.csv'); // Delete the uploaded file
                            return res.status(400).json({ message: 'File malformed', code: -3 }); // Return the error
                        }
                    }
                }


                // Get the datas present in database ordered by date
                Datas.find({ addrMicrocontroller: array_sd_datas[0][0]}).sort({ dateAdded: 1})
                .then(db_datas => {
                    var values_to_add_in_db = []; // Define an array to put the values in the database


                    // No entries in database => Return a specific error according to the datas in DB
                    if(db_datas.length == 0){
                        Microcontroller.findOne({ addr: array_sd_datas[0][0] })
                        .then(microcontroller => {
                            
                            // If the microcontrolleur doesn't exist => Return a specific error
                            if(microcontroller == null){
                                res.status(400).json({ message: "The microcontroller doesn't exist", code: -1 }); // Message to return
                                deleteFileUploaded('./files_uploaded/datas.csv'); // Delete the uploaded file
                                return;
                            
                            } else {

                                // Return an non-initializated error
                                res.status(400).json({ 
                                    message: "Need to initalize microcontroller DB. Re-launch the microcontroller", 
                                    code: -2 
                                });
                                return;
                            }
                        })
                        .catch(error => res.status(400).json(error))
                    
                    
                    // Entries in database so make the aggregation of all the datas
                    } else {

                        // Remove the message with the -1 as id
                        array_sd_datas = removeMinus1Message(array_sd_datas);     

                        
                        // Check if the recording of the SD are already stored in DB
                        for(const data of db_datas){
                            array_sd_datas = updateSDArray(array_sd_datas, data);
                        }


                        var pass_in_while = false; // To know if we will put datas in the database
                        var max_while_without_add = 100; // To put the max number of iterations without adding datas
                        var current_while = 0; // Current while to compare with max_while_without_add


                        // While all the datas in array_sd_datas are not all 1
                        while(!checkAllSDDataTo1(array_sd_datas) && current_while < max_while_without_add){
                            current_while++; // Addin current while
                            pass_in_while = true; // We will insert datas

                            // For each element in the array_sd_datas
                            for([index, data] of array_sd_datas.entries()){
                                var has_made_up_aggregation = false; // To know if we have made an up aggregation => Not push 2 times the same value
                                
                                // Check if the value is -1 (two possibilities : -1 or 1)
                                if(data[1] == '-1') {

                                    // Check if it is not the last element in the array => Oblige to do down aggregation
                                    if(index + 1 != array_sd_datas.length){ // Up aggregation (Characterize with +)

                                        // Up aggregation => Use of the next element to put the "good" datas
                                        if(array_sd_datas[index + 1][1] == 1) {
                                            has_made_up_aggregation = true; // Have made up agg. => So no down aggregation
                                            
    
                                            // Get the next record from the db
                                            var record = getNextRecordFromDB(array_sd_datas[index], db_datas);

                                            // If the record is null with database => Check in the values to add in the database
                                            if(record == null) record = getNextRecordFromDB(array_sd_datas[index], values_to_add_in_db);

                                            
                                            // If the record is not null => Continue the processing
                                            if(record != null) {
                                                // Get the time thanks to the time to sleep of the actual index
                                                var time = new Date(new Date(record.dateAdded).getTime() - (data[9] * 1000) - simulateRandomTime()); // x1000 because it is in milliseconds
                                                

                                                // Add the value in an array
                                                values_to_add_in_db.push(new Datas({
                                                    addrMicrocontroller: data[0],
                                                    ph: data[2],
                                                    humidity: data[3],
                                                    temperature: data[4],
                                                    conductivity: data[5],
                                                    nitrogen: data[6],
                                                    phosphorus: data[7],
                                                    potassium: data[8],
                                                    dateAdded: time,
                                                    dateModification: time,
                                                    idMsg: index
                                                }));

                                                current_while = 0; // Re-initialize current while
        

                                                data[1] = 1; // Datas are stored with the good values in the array => Can use this value for aggregation
                                            }
                                        }
                                    }
                                    

                                    // Check if the index is not 0 => Oblige to do up aggregation
                                    // Plus check if we have not done up aggregation on the same value before => Avoid add of 2 datas that are the same
                                    if(index != 0 && !has_made_up_aggregation){ // Down aggregation (Characterize with -)

                                        // Down aggregation => Use the last element to put the "good" datas
                                        if(array_sd_datas[index - 1][1] == 1) {

                                            // Get the last record from the db
                                            var record = getLastRecordFromDB(array_sd_datas[index], db_datas);
                                            
                                            // If the record is null with database => Check in the values to add in the database
                                            if(record == null) record = getLastRecordFromDB(array_sd_datas[index], values_to_add_in_db);
                                            

                                            // If the record is not null => Continue the processing
                                            if(record != null){
                                                // Get the time thanks to the time to sleep of the last index
                                                var time = new Date(new Date(record.dateAdded).getTime() + (array_sd_datas[index - 1][9] * 1000) + simulateRandomTime()); // x1000 because it is in milliseconds
                                                                                                
                                                                                            
                                                // Add the value in an array
                                                values_to_add_in_db.push(new Datas({
                                                    addrMicrocontroller: data[0],
                                                    ph: data[2],
                                                    humidity: data[3],
                                                    temperature: data[4],
                                                    conductivity: data[5],
                                                    nitrogen: data[6],
                                                    phosphorus: data[7],
                                                    potassium: data[8],
                                                    dateAdded: time,
                                                    dateModification: time,
                                                    idMsg: index
                                                }));

                                                current_while = 0; // Re-initialize current while

                                                data[1] = 1; // Datas are stored with the good values in the array => Can use this value for aggregation
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Test if we have exceeded the number of iterations
                        if (current_while >= max_while_without_add){
                            res.status(400).json({message: "Exceed the number of iterations, CSV problems", code: -3}); // Return error
                            deleteFileUploaded('./files_uploaded/datas.csv'); // Delete the uploaded file
                            return;

                        } else {
                            // Add the values in the database if the array length is more than 0
                            if(values_to_add_in_db.length != 0) Datas.insertMany(values_to_add_in_db); // Insert the datas into the database

                            // Return the response from the server
                            if(!pass_in_while) res.status(200).json({message: "Cannot upload datas => They're already in the db", code: 2});
                            else res.status(200).json({ message: "Upload of the aggregation is OK", code: 1 });
                            
                            deleteFileUploaded('./files_uploaded/datas.csv'); // Delete the uploaded file
                        }
                    }
                })
            }); 
        }

    } catch (err) {
        res.status(500).json({ err });
    }
}


/**
 * Function used to get the next record to be able to make the aggregation
 * @param {*} sd_data => Data of the SD card to compare with the DB ones
 * @param {*} db_datas => Datas from database to compare with the SD one
 * @returns the next record if we have found one ; null otherwise
 */
function getNextRecordFromDB(sd_data, db_datas){
    for(const data of db_datas) {
        if(Number(sd_data[10]) + 1 == data.idMsg) return data
    }

    return null
}


/**
 * Function used to get the last record to be able to make the aggregation
 * @param {*} sd_data => Data of the SD card to compare with the DB ones
 * @param {*} db_datas => Datas from database to compare with the SD one
 * @returns the last record if we have found one ; null otherwise
 */
function getLastRecordFromDB(sd_data, db_datas){
    for(const data of db_datas) {
        if(Number(sd_data[10]) - 1 == data.idMsg) return data
    }

    return null
}


/**
 * Function used to retrieve a value from the array with the ph, humidity, temperature, conductivity, nitrogen, phosphorus and potassium
 * @param {*} array_of_values => Contains all the values from the recording
 * @param {*} ph => PH we want
 * @param {*} humidity => Humidity we want
 * @param {*} temperature => Temperature we want
 * @param {*} conductivity => Conductivity we want
 * @param {*} nitrogen => Nitrogen we want
 * @param {*} phosphorus => Phosphorus we want
 * @param {*} potassium => Potassium we want
 * @returns null if no recoding ; the recording otherwise
 */
function findFrom(array_of_values, ph, humidity, temperature, conductivity, nitrogen, phosphorus, potassium) {
    for(const val of array_of_values){
        if(
            ph == val.ph && humidity == val.humidity && temperature == val.temperature &&
            conductivity == val.conductivity && nitrogen == val.nitrogen && phosphorus == val.phosphorus && potassium == val.potassium
        ) return val;
    }

    return null;
}


/**
 * Function used to check if the datas of the SD card are in the database
 * @param {*} sd_array => Array present in the SD card
 * @returns true if all the values are in the DB ; false otherwise
 */
function checkAllSDDataTo1(sd_array){
    for(const data of sd_array){
        if(data[1] == -1) return false;
    }

    return true;
}


/**
 * Function used to return the array contained in the SD card without -1 idMsg
 * @param {*} sd_array 
 * @returns the sd_array without -1 idMsg
 */
function removeMinus1Message(sd_array) {
    var array_to_return = [];

    // Just get the values with the idMsg != of -1
    for(const sd_data of sd_array) {
        if(sd_data[10] != -1) {
            array_to_return.push(sd_data);
        }
    }

    return array_to_return;
}


/**
 * Function used to update the SD array to know what data is already in the database
 * @param {*} sd_array => Array that contains all the SD recording
 * @param {*} data => Data to compare the idMsg
 * @returns the sd_array modified if a value is already in the db ; not modified otherwise
 */
function updateSDArray(sd_array, data){
    for(sd_data of sd_array){
        if(sd_data[10] == data.idMsg) {
            sd_data[1] = 1; // Put the value to 1 to say that the record is already stored in DB
            break;
        }
    }

    return sd_array;
}


/**
 * Function used to delete the file when the treatments on it are finished
 * @param {*} path => Path to access to the file
 */
function deleteFileUploaded(path){
    fs.unlink(path, function(error, result) {
        if(error) console.log(error);
    });
}


/**
 * Function used to generate a random between 500 and 1500 to simulate the average time taken for the end part of the cycle
 * It cand be just the OK message or a reconfiguration
 * @returns a random number between 500 and 1500
 */
function simulateRandomTime(){
    return Math.floor(Math.random() * 1500) + 500;
}