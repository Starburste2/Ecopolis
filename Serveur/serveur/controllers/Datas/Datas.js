const Datas = require("../../models/Datas"); // Import the model
const DatasValidation = require("./DatasValidation"); // Import the validation functions
const DatasHelper = require("./DatasHelper"); // Import the helper functions
const Microcontroller = require("../../models/Microcontroller"); // Import the microcontroller module to know if we need to do reconfiguration
const PoolAddress = require("../../models/PoolAddress"); // Import the pool address model
const Group = require("../../models/Group"); // Import the group model


/**
 * Function used to add the data sent by the microcontroller
 * And we send the reconfiguration of the microcontroller if we have one
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
exports.addData = function (req, res) {
    // Check the microcontroller address
    resultCheckAddr = DatasValidation.checkMicrocontrollerAddress(req.body.AM);
    if(!resultCheckAddr[0]){
        res.status(400).send({error: resultCheckAddr[1]});
        return;
    }


    // Check the soil ph captured by the microcontroller
    resultCheckPH = DatasValidation.checkPH(req.body.ph);
    if(!resultCheckPH[0]){
        res.status(400).send({error: resultCheckPH[1]});
        return;
    }
    

    // Check the soil temperature captured by the microcontroller
    resultCheckTemperature = DatasValidation.checkTemperatureCelsius(req.body.T);
    if(!resultCheckTemperature[0]){
        res.status(400).send({error: resultCheckTemperature[1]});
        return;
    }


    // Check the soil humidity captured by the microcontroller
    resultCheckHumidity = DatasValidation.checkHumidity(req.body.H);
    if(!resultCheckHumidity[0]){
        res.status(400).send({error: resultCheckHumidity[1]});
        return;
    }


    // Check the soil conductivity captured by the microcontroller
    resultCheckConductivity = DatasValidation.checkConductivity(req.body.C);
    if(!resultCheckConductivity[0]){
        res.status(400).send({error: resultCheckConductivity[1]});
        return;
    }


    // Check the soil nitrogen captured by the microcontroller
    resultCheckNitrogen = DatasValidation.checkNitrogen(req.body.N);
    if(!resultCheckNitrogen[0]){
        res.status(400).send({error: resultCheckNitrogen[1]});
        return;
    }


    // Check the soil phosphorus captured by the microcontroller
    resultCheckPhosphorus = DatasValidation.checkPhosphorus(req.body.P);
    if(!resultCheckPhosphorus[0]){
        res.status(400).send({error: resultCheckPhosphorus[1]});
        return;
    }


    // Check the soil potassium captured by the microcontroller
    resultCheckPotassium = DatasValidation.checkPotassium(req.body.K);
    if(!resultCheckPotassium[0]){
        res.status(400).send({error: resultCheckPotassium[1]});
        return;
    }


    // Check the idMsg of the microcontroller
    resultCheckIdMsg = DatasValidation.checkIdMsg(req.body.id);
    if(!resultCheckIdMsg[0]){
        res.status(400).send({error: resultCheckIdMsg[1]});
        return;
    }


    actual_date = new Date();

    
    // Find the microcontroller by addr
    Microcontroller.findOne({ addr: req.body.AM })
    .then(microcontroller => {
        if(!microcontroller) return res.status(400).json({ message: "The microcontroller doesn't exist" }); // Check if the microcontroller exists``

    
        // Create a microcontroller with the parameters for the possbile future saving
        const data = new Datas({
            addrMicrocontroller: req.body.AM,
            ph: req.body.ph,
            humidity: req.body.H,
            temperature: req.body.T,
            conductivity: req.body.C,
            nitrogen: req.body.N,
            phosphorus: req.body.P,
            potassium: req.body.K,
            dateAdded: actual_date,
            dateModification: actual_date,
            idMsg: req.body.id
        });


        // Save the data received from the microcontroller if it exists
        data.save()
        .then(() => {

            // Update the last sending value of the microcontroller
            Microcontroller.updateOne({ addr: microcontroller.addr }, { $set: { lastSending: new Date() } })
            .then(() => {

                // If it is the first time the microcontroller is on or we have a reconfiguration
                if(microcontroller.hasConfigChanged == true || req.body.FS !== undefined) {
                    var sleep_time_values = DatasHelper.getAllSleepTimeValues(microcontroller); // Get all we need for sending


                    // If the microcontroller has a group
                    if(microcontroller.groups != "") {

                        // If not manual reconfiguration => Possibly more than 24 values
                        if(!microcontroller.manualReconfiguration) {
                            
                            // Return a creation status with the next sleep time value with ENA
                            res.status(201).json({
                                HCC: true, // hasConfigChanged
                                V: sleep_time_values[1][0], // Sleep time
                                NBV: sleep_time_values[1].length, // Nb sleep time
                                NST: sleep_time_values[0], // Next sleep time
                                ENA: Number.parseInt(microcontroller.addr, 16) // Send the ENA in int 
                            }) 


                        // Manual reconfiguration => Less than 24 values
                        } else {
                            
                            // Return a creation status with the next sleep time value with ENA
                            res.status(201).json({
                                HCC: true, // hasConfigChanged
                                ST: sleep_time_values[1], // Sleep time
                                NST: sleep_time_values[0], // Next sleep time
                                ENA: Number.parseInt(microcontroller.addr, 16) // Send the ENA in int 
                            })
                        }
                
                    
                    // The microcontroller has no group
                    } else {

                        // If not manual reconfiguration => Possibly more than 24 values
                        if(!microcontroller.manualReconfiguration) {
                            
                            // Return a creation status with the next sleep time value with ENA
                            res.status(201).json({
                                HCC: true, // hasConfigChanged
                                V: sleep_time_values[1][0], // Sleep time
                                NBV: sleep_time_values[1].length, // Nb sleep time
                                NST: sleep_time_values[0] // Next sleep time
                            })


                        // Manual reconfiguration => Less than 24 values
                        } else {
                            
                            // Return a creation status according to the all sleep time values
                            res.status(201).json({
                                HCC: true, // hasConfigChanged
                                ST: sleep_time_values[1], // Sleep time
                                NST: sleep_time_values[0] // Next sleep time
                            })
                        }
                    }

                
                // No reconfiguration
                } else {
                    var next_sleep_time = DatasHelper.getTimeBetweenTwoSending(microcontroller); // Get the next sleep time value


                    // If the microcontroller has a group
                    if (microcontroller.groups != "") {
                        
                        // Return a creation status with the next sleep time value with the microcontroller addr
                        res.status(201).json({
                            HCC: microcontroller.hasConfigChanged, // hasConfigChanged
                            NST: next_sleep_time, // Next sleep time
                            ENA: Number.parseInt(microcontroller.addr, 16) // ENA in int
                        })

                    
                    // The microcontroller has no group
                    } else {

                        // Return a creation status with the next sleep time value without the microcontroller addr
                        res.status(201).json({
                            HCC: microcontroller.hasConfigChanged, // hasConfigChanged
                            NST: next_sleep_time // Next sleep time
                        })
                    }
                } 
            })
            .catch(error => res.status(400).json(error))
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
};


/**
 * Function used to return all the datas of a micrcontroller ordered by the added date
 * @param {*} req 
 * @param {*} res 
 * @returns if we have an error when we check the parameters
 */
exports.getDatasFromAddr = function (req, res) {
    var addr = req.params.addr;

    // Check the microcontroller address
    resultCheckAddr = DatasValidation.checkMicrocontrollerAddress(addr);
    if(!resultCheckAddr[0]){
        res.status(400).send({error: 'Addr invalid'});
        return;
    }


    // Check if the addr is defined in our pool
    PoolAddress.findOne({ addr: addr })
    .then(address => {
            
        // Test if the address is defined
        if(address == null) return res.status(400).json({ message: "The microcontroller doesn't exist" });
            

        // Get all the datas order by the added date 
        Datas.find({ addrMicrocontroller: addr }, { _id: 0, addrMicrocontroller: 0, __v: 0, dateModification: 0 }).sort({ dateAdded: 1 })
        .then(datas => {
            res.status(200).json(datas);
        })
        .catch(error => res.status(400).json({ error }))
    })
    .catch(error => res.status(400).json({ error }))
}


/**
 * Function used to return all the datas between an interval order by added date of one microcontroller given by its addr
 * @param {*} req 
 * @param {*} res 
 * @returns if we have an error when we check the parameters
 */
exports.getDatasFromInterval = function (req, res) {
    var addr = req.params.addr;

    // Check the microcontroller address
    resultCheckAddr = DatasValidation.checkMicrocontrollerAddress(addr);
    if(!resultCheckAddr[0]) return res.status(400).send({error: resultCheckAddr[1]});

    
    // Check firstDate and lastDate passed in parameter
    resultCheckDates = DatasValidation.checkDate(req.query.firstDate, req.query.lastDate);
    if(!resultCheckDates[0]) return res.status(400).json({ error: resultCheckDates[1] });


    // Verify if the address of the microcontroller is defined in the pool of addresses
    PoolAddress.findOne({ addr: addr })
    .then(address => {
        if(address == null) return res.status(400).json({ message: "The microcontroller doesn't exist" }); // No microcontroller with this address
        

        // Formatting the dates
        var firstDate = new Date(req.query.firstDate); // Get the first date
        firstDate.setUTCHours(0, 0, 0, 0); // Set the first date hours minutes seconds and milliseconds to 0

        var lastDate = new Date(req.query.lastDate); // Get the last date
        lastDate.setUTCHours(23, 59, 0, 0); // Set the last date hours to 23, minutes to 59, seconds and milliseconds to 0


        // Get all the datas
        Datas.find({ addrMicrocontroller: addr, dateAdded: { $gte: firstDate, $lt: lastDate } }, { _id: 0, addrMicrocontroller: 0, __v: 0, dateModification: 0 }).sort({ dateAdded: 1 })
        .then(datas => {
            res.status(200).json(datas); // Send the datas
        })
        .catch(error => res.status(400).json(error))
    })
    .catch(error => res.status(400).json(error))
}


/**
 * Function used to get the 50 last datas added by the microcontroller
 * @param {*} req 
 * @param {*} res 
 */
exports.getLastDatas = function (req, res) {
    var addr = req.query.addr;

    // Check the addr passed in query
    resultCheckAddr = DatasValidation.checkMicrocontrollerAddress(addr)
    if(!resultCheckAddr[0]) return res.status(400).json({ message: 'addr incorrect' });


    // Get the last 50 datas from the DB for one microcontroller
    Datas.find({ addrMicrocontroller: addr }, { _id: 0, __v: 0, dateModification: 0, addrMicrocontroller: 0 }).sort({ dateAdded: -1 }).limit(50)
    .then(datas => {
        res.status(200).json(datas.reverse())
    })
    .catch(error => res.status(400).json(error))
}