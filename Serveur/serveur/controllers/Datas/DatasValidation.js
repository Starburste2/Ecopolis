var check = require('check-types'); // Import the check-types package to check some variables
const moment = require('moment'); // Import the moment package to check if the date is correct


/**
 * Function to check if the address of the microcontroller passed in parameter
 * is good or not 
 * @param {*} addr => Address of a microcontroller
 * @returns an array => [false, error_message] or [true]
 */
function checkMicrocontrollerAddress(addr){
    if(addr == undefined) return [false, "Address is not defined"]; // Check undefined
    if(check.not.string(addr)) return [false, "Adress is not a string"]; // Check string
    if(addr.length > 2) return [false, "Address is too long"]; // Check the length

    return [true];
}


/**
 * Function to check if the temperature captured by the microcontroller is good not not
 * Here this function is used to check the temperature in CELSIUS
 * @param {*} temperature => Temperature got by the microcontroller
 * @returns an array => [false, error_message] or true
 */
function checkTemperatureCelsius(temperature){
    if(temperature == undefined) return [false, "Temperature is not defined"]; // Check undefined
    if(check.not.number(temperature)) return [false, "Temperature is a number"]; // Check if it is a number
    if(temperature < -50 || temperature > 70) return [false, "Temperature capture problem (temperature too low or too high)"]; // Check capture problem

    return [true];
}


/**
 * Function to check if the humidity percentage captured by the microcontroller is good or not
 * Here this function is used to check the humidity PERCENTAGE
 * @param {*} humidity => Percentage of humidity got by the microcontroller
 * @returns an array => [false, error_message] or true
 */
function checkHumidity(humidity){
    if(humidity == undefined) return [false, "Humidity is not defined"]; // Check undefined
    if(check.not.number(humidity)) return [false, "Humidity is a number"]; // Check if it is a number
    if(humidity < 0 || humidity > 100) return [false, "Humidity capture problem (humidity is not between 0 and 100 percents)"]; // Check capture problem

    return [true];
}


/**
 * Function to check if the dates passed in parameter are correct or not
 * @param {*} firstDate => Beginning date of the datas we need to retur
 * @param {*} lastDate => End date of the datas we need to return
 * @returns an array => [false, error_message] or true according to the tests on the dates
 */
function checkDate(firstDate, lastDate) {
    if(firstDate === undefined) return [false, "First date is undefined"]; // Check undefined first date
    if(!moment(firstDate, 'YYYY-MM-DD').isValid()) return [false, "First date is not valid"]; // Check if first date is valid

    if(lastDate === undefined) return [false, "Last date is undefined"]; // Check undefined last date
    if(!moment(lastDate, 'YYYY-MM-DD').isValid()) return [false, "Last date is not valid"]; // Check if last date is valid
    
    if(new Date(firstDate).getTime() > new Date(lastDate).getTime()) return [false, "First date after last date"]; // Check if the first date is after the last one

    return [true]
}


/**
 * Function used to check if the PH value passed in parameter is good or not
 * @param {*} ph => PH value captured by the microcontroller
 * @returns an array => [false, error_message] or true according to the tests on the datas
 */
function checkPH(ph) {
    if(ph === undefined) return [false, "PH is undefined"]; // Check undefined ph
    if(check.not.number(ph)) return [false, "PH is a number"]; // Check if ph is a number
    if(ph < 0 || ph > 14) return [false, "PH value is incorrect"]; // Check the PH value

    return [true];
}


/**
 * Function used to check if the conductivity value passed in parameter is good or not
 * @param {*} conductivity => Conductivity value captured by the microcontroller
 * @returns an array => [false, error_message] or true according to the tests on the datas
 */
function checkConductivity(conductivity) {
    if(conductivity === undefined) return [false, "Conductivity is undefined"]; // Check undefined conductivity
    if(check.not.number(conductivity)) return [false, "Conductivity is a number"]; // Check if conductivity is a number
    if(conductivity < 0) return [false, "Conductivity value is incorrect"]; // Check the conductivity value

    return [true];
}


/**
 * Function used to check if the nitrogen value passed in parameter is good or not
 * @param {*} nitrogen => Nitrogen value captured by the microcontroller
 * @returns an array => [false, error_message] or true according to the tests on the datas
 */
function checkNitrogen(nitrogen) {
    if(nitrogen === undefined) return [false, "Nitrogen is undefined"]; // Check undefined nitrogen
    if(check.not.number(nitrogen)) return [false, "Nitrogen is a number"]; // Check if nitrogen is a number
    if(nitrogen < 0) return [false, "Nitrogen value is incorrect"]; // Check the nitrogen value

    return [true];
}


/**
 * Function used to check if the phosphorus value passed in parameter is good or not
 * @param {*} phosphorus => Phosphorus value captured by the microcontroller
 * @returns an array => [false, error_message] or true according to the tests on the datas
 */
function checkPhosphorus(phosphorus) {
    if(phosphorus === undefined) return[false, "Phosphorus is undefined"]; // Check undefined phosphorus
    if(check.not.number(phosphorus)) return [false, "Phosphorus is a number"]; // Check if phosphorus is a number
    if(phosphorus < 0) return [false, "Phosphorus value is incorrect"]; // Check the phosphorus value

    return [true];
}


/**
 * Function used to check if the potassium value passed in parameter is good or not
 * @param {*} potassium => Potassium value captured by the microcontroller
 * @returns an array => [false, error_message] or true according to the tests on the datas
 */
function checkPotassium(potassium) {
    if(potassium === undefined) return [false, "Potassium is undefined"]; // Check undefined potassium
    if(check.not.number(potassium)) return [false, "Potassium is a number"]; // Check if potassium is a number
    if(potassium < 0) return [false, "Ptassium value is incorrect"]; // Check the potassium value

    return [true]
}


/**
 * Function used to check if the potassium value passed in parameter is good or not
 * @param {*} idMsg 
 * @returns an array => [false, error_message] or true according to the tests on the datas
 */
function checkIdMsg(idMsg) {
    if(idMsg === undefined) return [false, "ID msg is undefined"]; // Check undefined idMsg
    if(check.not.number(idMsg)) return [false, "ID msg is a number"]; // Check if idMsg is a number

    return [true];
}


// Exports the functions
exports.checkMicrocontrollerAddress = checkMicrocontrollerAddress
exports.checkTemperatureCelsius = checkTemperatureCelsius
exports.checkHumidity = checkHumidity
exports.checkDate = checkDate
exports.checkPH = checkPH
exports.checkConductivity = checkConductivity
exports.checkNitrogen = checkNitrogen
exports.checkPhosphorus = checkPhosphorus
exports.checkPotassium = checkPotassium
exports.checkIdMsg = checkIdMsg