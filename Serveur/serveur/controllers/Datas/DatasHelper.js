const SHIFT = 1.1 / 100; // Shift of 1.1 %

/**
 * Function to retrieve the next time to sleep stored in the database
 * @param {*} microcontroller => Microcontroller in the database
 * @returns an absolute integer of the difference between now and the next closer sending
 */
function getTimeBetweenTwoSending(microcontroller) {
    var today = new Date(); // Get today's date

    // Formate date as we want for next sending
    var tab_date = [];
    for (const sleepTime of microcontroller.sleepTime) {
        var values_sleep_time = sleepTime.split(":"); // Split to get hours and minutes
        var date = new Date(); // Date of today
        date.setHours(values_sleep_time[0], values_sleep_time[1], values_sleep_time[2], 0); // Set h, m, s and ms with the good values

        // Add one day if the date is passed
        if(date.getTime() < today.getTime()) {
            date.setDate(date.getDate() + 1);
        }

        // Push the date into the array
        tab_date.push(new Date(date));
    }


    // Get the next sending of the microcontroller
    var next_sending = -1; // Index in the array of the next sending
    var last_time_stamp = Infinity; // Last time stamp we get => Set to infinity at the beginning
    for (const [index, val] of tab_date.entries()) {
        var actual_difference = val.getTime() - today.getTime(); // Difference between the val into the array and now

        // Next sending => Smaller difference
        if (actual_difference < last_time_stamp) {
            next_sending = index; 
            last_time_stamp = actual_difference;
        }
    }
    
    var time_to_next_sending_in_s = (tab_date[next_sending].getTime() - today.getTime()) / 1000; // Calculate the time to the next sending in seconds
    time_to_next_sending_in_s += (Number.parseInt(Math.abs(time_to_next_sending_in_s)) * SHIFT); // Add the mean of the shift time done by the microcontroller in deep sleep

    return Number.parseInt(Math.abs(time_to_next_sending_in_s)); // Return the value in seconds
}


/**
 * Function to get the array of all sleep time but also we return the next sleep time
 * @param {*} microcontroller => Microcontroller in the database
 * @returns an array
 *  => sleep_time_to_next_sending => Time to sleep for the next sending to agree with the sleep time array in the database
 *  => tab_sleep_time => Array of sleep time, in seconds, from the first to the end value plus last to first
 */
function getAllSleepTimeValues(microcontroller) {
    var today = new Date(); // Get today's date

    // Formate date as we want for next sending
    var tab_date = [];
    for (const sleepTime of microcontroller.sleepTime) {
        var values_sleep_time = sleepTime.split(":"); // Split to get hours and minutes
        var date = new Date(); // Date of today
        date.setHours(values_sleep_time[0], values_sleep_time[1], values_sleep_time[2], 0); // Set h, m, s and ms with the good values

        // Add one day if the date is passed
        if(date.getTime() < today.getTime()) {
            date.setDate(date.getDate() + 1);
        }

        // Push the date into the array
        tab_date.push(new Date(date));
    }


    // Sort the array by date asc
    tab_date = tab_date.sort((a, b) => b.getTime() - a.getTime()).reverse()


    // Calculate the sleep time array we need
    var tab_sleep_time = [];
    var our_date = new Date();
    for (const [index, val] of tab_date.entries()) {
        var actual_difference = val.getTime() - our_date.getTime(); // Difference between the val into the array and now
        tab_sleep_time.push( Number.parseInt(Math.abs(actual_difference / 1000)) ); // Push the difference in seconds in the array for the microcontrollers
        our_date = val; // To make the difference between the two next date, etc …
    }


    // According to the number of values in the array
    if(tab_sleep_time.length === 1) { // One data
        var sleep_time_to_next_sending = tab_sleep_time.shift(); // Delete the only value int the array and return it at the end
        var last_to_first_sending = 86400; // 24 hours * 3600 seconds => 86400 seconds
        tab_sleep_time.push(last_to_first_sending); // Push the last sending into the array

    } else { // Multiple datas
        var sleep_time_to_next_sending = tab_sleep_time.shift(); // Delete the first value of the array and return it at the end
        var last_to_first_sending = 86400 -tab_sleep_time.reduce(sum); // 24 hours * 3600 seconds => 86400 seconds
        tab_sleep_time.push(last_to_first_sending); // Push the last sending into the array
    }


    // Add the shift value to all the element of the array
    tab_sleep_time.forEach(function(part, index, theArray) {
        if(Number.parseInt(theArray[index] * SHIFT) < 1) { // If the shift is less than 1
            theArray[index] += 5; // Put a shift of 5 seconds
        
        } else {
            theArray[index] += Number.parseInt(theArray[index] * SHIFT); // Add the normal shift
        }
    })


    sleep_time_to_next_sending += Number.parseInt(sleep_time_to_next_sending * SHIFT); // Add the shift to the value the microcontroller will use


    return [sleep_time_to_next_sending, tab_sleep_time];
}


/**
 * Function used to make the sum of an array by using : my_array.reduce(sum)
 * @param {*} total => Sum total of the array
 * @param {*} value => Value of the array
 * @param {*} index => Index in the array
 * @param {*} array => The array itself
 * @returns the sum of the array
 */
function sum(total, value, index, array) {
    return total + value;
}


// Export the functions
exports.getTimeBetweenTwoSending = getTimeBetweenTwoSending
exports.getAllSleepTimeValues = getAllSleepTimeValues
exports.sum = sum