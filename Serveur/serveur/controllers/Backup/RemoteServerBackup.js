// https://stackoverflow.com/questions/6558080/scp-secure-copy-to-ec2-instance-without-password => scp commands from stackoverflow
// https://queirozf.com/entries/scp-secure-copy-reference-examples => scp examples
// https://dropshare.zendesk.com/hc/en-us/articles/201139452-How-to-create-a-new-SSH-key-pair-for-SCP-over-SSH-upload- => Generate key/pair to be able to use scp
// https://www.techrepublic.com/article/how-to-use-secure-copy-with-ssh-key-authentication/ => Why not
// https://alvinalexander.com/linux-unix/how-use-scp-without-password-backups-copy/ => Better one (all explanations we need)
/**
 * Use this tuto : https://alvinalexander.com/linux-unix/how-use-scp-without-password-backups-copy/
 * Keys stored in ~/.ssh (enter no file, no passphrase) 
 * Install openssh-server (on the personnal remote server)
 * Create a backups folder in the home of the user
 * chmod 777 sur backups folder of the server 
*/

const child_process = require('child_process'); // Import the child_process module to be able to make commands in term
const ServerInfos = require('../../config/server_backup.json'); // Import the informations for the server backup
const fs = require('fs'); // Import file system module


/**
 * Function used to be able to copy the database backup on the remote server
 * @param {*} name_tgz_file => Name of the generated tgz backup file
 * @returns nothing if we have an error
 */
function remoteServerBackupSCP(name_tgz_file, name_file) {
    // Exec the command to send the backup to the remote server
    child_process.exec('scp ' + name_tgz_file + ' ' + ServerInfos.user + '@' + ServerInfos.server + ':./backups', (err, stdout, stderr) => {
        
        // If we have an error => Unlink files and return
        if(err) {
            console.log(`exec error: ${err}`);
            unlinkBackupFiles(name_file, name_tgz_file);
            return;
        }

        // Send backup to the server is successful => Unlink files and log
        console.log('Backup sent to the remote server successful');
        unlinkBackupFiles(name_file, name_tgz_file);
    });
}


/**
 * Function used to be able to delete the generated backup files
 * @param {*} name_file => Name of the generated gz file
 * @param {*} name_tgz_file => Name of the generatedtgz backup file
 */
function unlinkBackupFiles(name_file, name_tgz_file) {
    fs.unlinkSync('./database_backups/' + name_file); // Delete the gz backup file
    fs.unlinkSync(name_tgz_file); // Delete the tgz backup file
}


// Export the function
exports.remoteServerBackupSCP = remoteServerBackupSCP