const APIKeys = require('../../models/APIKey'); // Import APIKeys model to get the key to encrypt the tar gz
const { google } = require('googleapis'); // Import the google API package to be able to make a request to save the database backup
const credentials = require('../../config/credentials_service_account.json'); // Get the credentials for the service account
const fs = require('fs'); // Import the file system module to delete the files when they are uploaded
const child_process = require('child_process'); // Import the child process package to be able to execute commands in the terminal
const GGDriveFolderID = require('../../config/GGDriveFolderID.json'); // Import the ID of the folder in the Google Drive


/**
 * Function used to upload the database backups on Google Drive
 *   via the Google Drive API and a service account
 */
function googleDriveSaving() {
    // Get the today informations to rename the archive
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();

    var name_file = 'archive_' + day + '_' + month + '_' + year + '.gz'; // Rename the file

    // Rename the backup file
    fs.renameSync('./database_backups/archive.gz', './database_backups/' + name_file); // Rename the archive


    // Find the key to encrypt the the database backup gz file
    APIKeys.findOne({ applicationName: "encryptDB" })
    .then(key => {
      var key_to_encrypt = key.key; // Get the key for encryption
      var name_tgz_file = './database_backups/encrypted_backup' + day + '_' + month + '_' + year + '.tar.gz'; // Set the name of the encrypted file
      
      
      // Crypt the gz archive
      child_process.exec('tar -czf - ./database_backups/' + name_file + ' | openssl enc -e -aes256 -k ' + key_to_encrypt + ' -out ' + name_tgz_file, (err, stdout, stderr) => {
        
        // If we have an error => Delete the generated gz backup file
        if(err) {
          console.log(`exec error: ${err}`);
          fs.unlinkSync('./database_backups/' + name_file);
          return;
        }

        // Declaration of some variables to upload the backup file on Google Drive
        const scopes = [ 'https://www.googleapis.com/auth/drive' ]; // Declare the API we want to use
        const auth = new google.auth.JWT(credentials.client_email, null, credentials.private_key, scopes); // Creation of the JSON Web Token for the authentication
        const drive = google.drive({ version: "v3", auth }); // Authentication on the Google Drive to store the backup
        const fileMetadata = { 'name': name_tgz_file.split('/')[2], parents: [GGDriveFolderID.id] }; // Meta data concerning the file we want to upload => Name of the file and parent folder
        const media = { mimeType: 'application/gzip', body: fs.createReadStream(name_tgz_file) }; // Media we want to upload


        // Upload the datas on the Google Drive
        drive.files.create({ resource: fileMetadata, media: media, fields: 'id' }, (err, file) => {
          if (err) {
            console.error(err); // Handle the error
          
          } else {
            console.log('Upload of ' + name_tgz_file.split('/')[2] + ' is done'); // Console log that the upload is OK

            
            // Call the function to copy the database backup on a remote server + Delete the database backups from here
            require('./RemoteServerBackup').remoteServerBackupSCP(name_tgz_file, name_file);
          }
        });
      });
    })
    .catch(error => console.log(error))
}


// Export the function
exports.googleDriveSaving = googleDriveSaving