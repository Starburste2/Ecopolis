const dropboxV2Api = require('dropbox-v2-api'); // Import the package
const APIKeys = require('../../models/APIKey'); // Import the API Key model
const child_process = require('child_process'); // Import the child_process package to be able to make commands in terminal
const fs = require('fs'); // Import the file system package to delete the file when we have uploaded them
const dropboxToken = require('../../config/DropboxToken.json'); // Import the token to use to be able to upload files on dropbox


/**
 * Function to be able to store the database encrypted backup with Dropbox
 */
function dropboxSaving() {
    // Create the connection with the token contained in the JSON file
    const dropbox = dropboxV2Api.authenticate({ token: dropboxToken.token });


    // Get the date of today and formate the the name of the archive to save on dropbox
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();
    var archive_name = 'archive_' + day + '_' + month + '_' + year + '.gz';


    // Rename the backup file
    fs.renameSync('./database_backups/archive.gz', './database_backups/' + archive_name); // Rename the archive


    APIKeys.findOne({ applicationName: "encryptDB" })
    .then(key => {
        var key_to_encrypt = key.key; // Get the key for encryption
        var name_tgz_file = './database_backups/encrypted_backup' + day + '_' + month + '_' + year + '.tar.gz'; // Set the name of the encrypted file
        
        
        // Crypt the gz archive
        child_process.exec('tar -czf - ./database_backups/' + archive_name + ' | openssl enc -e -aes256 -k ' + key_to_encrypt + ' -out ' + name_tgz_file, (err, stdout, stderr) => {
            
            // If we have an error => Delete the generated gz backup file
            if(err) {
                console.log(`exec error: ${err}`);
                fs.unlinkSync('./database_backups/' + archive_name);
                return;
            }

            // Upload the database backup
            dropbox({ resource: 'files/upload', parameters: { path: '/database_backup/' + name_tgz_file.split('/')[2] }, readStream: fs.createReadStream(name_tgz_file) }, (err, result, response) => {
                if (err) return console.log(err); // Console log the error
                console.log('Upload of ' + name_tgz_file + ' is done') // Console lof if we have no error

                
                // Call the function to copy the database backup on a remote server + Delete the database backups from here
                require('./RemoteServerBackup').remoteServerBackupSCP(name_tgz_file, archive_name);
            });
        });
    })
    .catch(error => console.log(error))
}


// Export the function
exports.dropboxSaving = dropboxSaving