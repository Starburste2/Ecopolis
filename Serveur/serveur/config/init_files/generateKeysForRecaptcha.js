// If the third argument is equal to -h or -H
if(process.argv[2]) {
    if(process.argv[2] == '-h' || process.argv[2] == '-H') {
        console.log('Usage : node generateKeysForRecaptcha.js site_key secret_key');
        process.exit(0);
    }
}

// If we don't have enough arguments => Quit the program
if(process.argv.length != 4) {
    console.log('Too few arguments');
    console.log('For more informations : node generateKeysForRecaptcha.js -h')
    process.exit(-1);
}


// Define the constants for the packages
const fs = require('fs'); // Import the file system package
const siteKey = '{\n    "siteKey": "' + process.argv[2] + '"\n}'; // Site key string to push into the VueJS JSON


var template_file = fs.readFileSync('../../templates_source_file/template_Recaptcha.js'); // Read the template file
template_file = template_file.toString().split('SITE_KEY').join(process.argv[2]); // Split the file via the SITE_KEY and reconstruct it with the good one
template_file = template_file.split('SECRET_KEY').join(process.argv[3]); // Split the file via SECRET_KEY and reconstruct it

fs.writeFileSync('../../keys/getReCaptchaKeys.js', template_file); // Write the datas into the file
fs.writeFileSync('../../../../VueJS_Client/src/config/siteKey.json', siteKey); // Write the site_key in the JSON of Vue


console.log("The file has well been updated"); // OK message
process.exit(1); // Quit the program