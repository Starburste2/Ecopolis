const fs = require('fs'); // Import the file system module


if(process.argv.length !== 4) return console.log("Usage : node generateIPRangeFile.js min_ip max_ip"); // Return the usage error


// Get the arguments of the program
var minIP = process.argv[2];
var maxIP = process.argv[3];


// Split the IPs by their dot (.)
var minIPTest = minIP.split(".");
var maxIPTest = maxIP.split(".");


if(minIPTest.length !== 4 || maxIPTest.length !== 4) return console.log('IPs contains 4 number blocks => Example : 0.0.0.0'); // Return the IP number of blocks error


// FOR to check the format of the number in the IPs passed in parameter
for(var i = 0 ; i < minIPTest.length ; i++) {
    if(isNaN(minIPTest[i]) || minIPTest[i] < 0 || minIPTest[i] > 255) return console.log('IP format is incorrect'); // Return the IP format error
    if(isNaN(maxIPTest[i]) || maxIPTest[i] < 0 || maxIPTest[i] > 255) return console.log('IP format is incorrect'); // Return the IP format error
}


if(minIPTest >= maxIPTest) return console.log('IP range is incorrect'); // Return the IP range error


var template_IPRange = fs.readFileSync('../../templates_source_file/template_IPRange.js').toString(); // Read the template file and transform it in String
template_IPRange = template_IPRange.split("MINIP").join("'" + minIP + "'"); // Change the MINIP identifier by the real value passed in parameter
template_IPRange = template_IPRange.split("MAXIP").join("'" + maxIP + "'"); // Change the MAXIP identifier by the real value passed in parameter

fs.writeFileSync('../IPRanges.js', template_IPRange); // Write the updates into the file

console.log("The IP range file has been updated"); // Log that it is done to the user