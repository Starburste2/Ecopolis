const bcrypt = require('bcrypt'); // Import the bcrypt package to hash passwords
const fs = require('fs'); // Import the file system module


/**
 * Function used to display a message before quitting the function
 * @param {*} message => Message to display in the console
 */
function quit(message){
    console.log(message);
    process.exit(1);
}


// Generation of the salt to store
const saltRounds = 10; // Salt rounds
const salt = bcrypt.genSaltSync(saltRounds); // Get the generated salt 


// Formate the file as we want
const template_file = fs.readFileSync('../../templates_source_file/template_salt.js'); // Read the template file
const split_file = template_file.toString().split('SALT_TO_PUSH'); // Split the file
const my_file = split_file.join('\'' + salt + '\''); // File with the good salt


// Write the file in the one on the VueJS_Client
fs.writeFileSync('../../keys/getSalt.js', my_file);


quit("Generation of hash is finished"); // Quit the application