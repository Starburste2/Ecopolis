const fs = require('fs'); // Import the file system module to be able to read/write in files


// Test if we have the good number of arguments => 4
if(process.argv.length != 4) {
    return console.log('usage : node createServerBackupInformations.js user server'); // Return the error
}


// Write the user and server in the file
fs.writeFileSync('../server_backup.json', '{\n    "user": "' + String(process.argv[2]) + '",\n    "server": "' + String(process.argv[3]) + '"\n}');


// Return the log to say to the user that it is done
return console.log('Generate of the server backup informations file is done.');