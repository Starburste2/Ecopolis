const validator = require('validator'); // Import the validator package to validate the email address
const fs = require('fs'); // Import the file system module


// Error number of arguments
if(process.argv.length != 5) {
    return console.log('Usage : node generateCredentialsEmail.js email password smtp_address');
}


// Validate the email address
if(!validator.isEmail(process.argv[2])) {
    return console.log('It is not an address email');
}


// Write the credentials into the JSON file
fs.writeFileSync('../email_credentials.json', '{\n    "email": "' + process.argv[2] + '",\n    "password": "' + process.argv[3] + '",\n    "smtp_address": "' + process.argv[4] + '"\n}');


// OK message and quit the program
console.log('The credentials have been updated');
process.exit(1);