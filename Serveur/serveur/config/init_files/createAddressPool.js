var mongoClient = require("mongodb").MongoClient; 
var url = 'mongodb://localhost/ecopolis'; // Url for db
const prompt = require('prompt-sync')();


/**
 * Function used to insert datas into the pooladdresses collection
 */
const client = new mongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect().then( (client) => {

    var database = client.db('ecopolis'); // Precise the database to use
    var array_of_addresses = []; // Array to store all the possible values

    // To create our addresses in HEX format
    for (i = 0 ; i < 256 ; i++) {
        if (i < 16) { // To add a 0 before because we have just one character
            array_of_addresses.push({
                addr: '0' + i.toString(16),
                isAvailable: true
            });  
        
        } else {
            array_of_addresses.push({
                addr: i.toString(16),
                isAvailable: true
            });
        }
    }

    // Choice of the user to re-initialize the pool of addresses
    const choice = prompt('Do you want to drop the last pool of addresses. BE CAREFUL THIS CAN CAUSE PROBLEMS IF DATAS ARE ALREADY IN ! y/n');
    if(choice != "y"){
        quit("You choose to not delete the actual datas");
    }


    // Drop the acutal collection of addresses
    console.log("Re-initialization of the previous pool of addresses");
    database.collection('pooladdresses').drop().catch( (error) => {}); // Don't display the warning message (POSSIBLE THAT THE COLLECTION DOESN'T EXIST WHEN WE WANT TO DELETE IT)

    
    // Insert into the database
    database.collection('pooladdresses').insertMany(array_of_addresses, function(err, result){
        if (err) {
            quit("Insert not OK")
        } else {
            quit("Insert OK")
        }
    });
});


/**
 * Function used to display a message before quitting the function
 * @param {*} message => Message to display in the console
 */
function quit(message){
    console.log(message);
    process.exit(1);
}