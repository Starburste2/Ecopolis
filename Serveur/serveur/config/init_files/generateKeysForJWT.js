const keypair = require('keypair'); // Import the module to generate the pairs of public/private keys
const fs = require('fs'); // Import the fs module
const pair = keypair(); // Create the pair

fs.writeFileSync('../../keys/JWTPrivateKey.key', pair.private); // Write the private key
fs.writeFileSync('../../keys/JWTPublicKey.key', pair.public); // Write the public key

console.log("Keys are well created"); // Print to say that it is the end