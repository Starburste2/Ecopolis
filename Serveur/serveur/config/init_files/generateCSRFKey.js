const UUID = require('uuid'); // Import the UUID package to generate the unique identifier
const bcrypt = require('bcrypt'); // Import the bcrypt package
const fs = require('fs'); // Import the file system package


/**
 * Function used to display a message before quitting the function
 * @param {*} message => Message to display in the console
 */
 function quit(message){
    console.log(message);
    process.exit(1);
}


// Generate UUID, salt and encrypt the UUID
const saltRounds = 10; // Salt rounds for bcrypt
const salt = bcrypt.genSaltSync(saltRounds); // Generate the salt to encrypt the UUID
const uuid = UUID.v4(); // Generate a random UUID
const hash = bcrypt.hashSync(uuid, salt); // Hash the UUID


// Formate the file as we want
const template_file = fs.readFileSync('../../templates_source_file/template_CSRF.js'); // Read the template file
const split_file = template_file.toString().split('CSRF_TOKEN'); // Split the file
const my_file = split_file.join('\'' + hash + '\''); // File with the good salt


// Write in the file
fs.writeFileSync('../../keys/getCSRFKey.js', my_file);


quit("The CSRF key has been generated"); // Quit the program