const fs = require('fs'); // Import the file system module to write in files

// Check if we have the good number of parameters
if(process.argv.length != 3) return console.log('Usage : node generateIDFolderGGDrive.js id_of_folder');

fs.writeFileSync('../GGDriveFolderID.json', '{\n    "id": "' + String(process.argv[2]) + '"\n}'); // Write the id of the folder in the JSON file

return console.log("Update of the GGDriveFolderID is done"); // Quit the function by printing a message