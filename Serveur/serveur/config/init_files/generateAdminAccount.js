var mongoClient = require("mongodb").MongoClient; // Import the Mongo Client for connection to DB
var url = 'mongodb://localhost/ecopolis'; // Url for db
const bcrypt = require('bcrypt'); // Import the bcrypt package to encrypt admin password
const salt = require('../../keys/getSalt'); // Import the function to get the salt to encrypt passwords


/**
 * Function used to insert a new admin in the database
 */
const client = new mongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect().then( (client) => {

    var database = client.db('ecopolis'); // Precise the database to use

    
    // Search if an admin already exists or not
    database.collection('users').findOne({ role: "admin" }, function (err, result) {
        if(err) {
            quit('Cannot search for an admin'); // Error during the request => Quit the program
        
        } else { // No error => Continue the normal execution
            
            if(result == null) { // No one in the database is an admin => Create one
                var admin_to_push = {
                    email: "admin@admin.com", // Email of the admin
                    password: bcrypt.hashSync("admin", salt.getSalt()), // Password of the admin
                    role: "admin" // Role of the admin
                }

                database.collection('users').insertOne(admin_to_push, function (err, result) {
                    if(err) {
                        quit('An error has occured during the adding of the new admin'); // Error during the request => Quit the program
                    
                    } else {
                        console.log('Your new login  : ' + admin_to_push.email); // Print the login
                        console.log('Your new password : admin'); // Print the password

                        quit('Creation of the new admin is finished.\nADVISE : CHANGE YOUR EMAIL AND YOUR PASSWORD WITH THE PASSWORD YOU WANT TO FIX FOR THE ADMIN'); // Quit the program
                    }
                })
                
            
            } else {
                quit('An admin already exists in the database'); // Already an admin => Quit the program
            }
        }
    })
});


/**
 * Function used to display a message before quitting the function
 * @param {*} message => Message to display in the console
 */
function quit(message){
    console.log(message);
    process.exit(1);
}