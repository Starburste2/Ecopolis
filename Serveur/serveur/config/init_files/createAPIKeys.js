var mongoClient = require("mongodb").MongoClient; // Import he MongoClient connection
const uuidAPIKey = require('uuid-apikey'); // Import the package to create the API Keys
var url = 'mongodb://localhost/ecopolis'; // Url for db
const APIKey = require('../../models/APIKey'); // Import the API Key model
const prompt = require('prompt-sync')(); // Import the package to be able to ask the user
const fs = require('fs'); // Import the file system package
const bcrypt = require('bcrypt'); // Import bcrypt to be able to encrypt the key to use to encrypt the database backup


/**
 * Function used to insert API Keys in the database
 */
 const client = new mongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });
 client.connect().then( (client) => { 
    var database = client.db('ecopolis'); // Precise the database to use
    var APIKeys = []; // Array to store the APIKey objects
    var appName = ['VueAppMain', 'VueAppShowcase', 'Microcontrollers', 'encryptDB']; // Name of the applications that will use API keys


    // Choice of the user to re-initialize the pool of addresses
    const choice = prompt('Do you want to drop the last API Keys. BE CAREFUL THIS CAN CAUSE PROBLEMS IF DATAS ARE ALREADY IN ! y/n');
    if(choice != "y"){
        quit("You choose to not delete the actual datas");
    }
    

    // Create the API Keys for Vue app and for the microcontrollers
    for (i = 0 ; i < appName.length ; i++) {
        var api_key = uuidAPIKey.create(); // Create the couple UUID/Api Key
        

        // Push this into an array we will append to the DB if we are on api keys => Generate couple of keys
        if(i != appName.length - 1) {
            APIKeys.push( new APIKey({ UUID: api_key.uuid, key: api_key.apiKey, applicationName: appName[i] }) );
        
        // If we are on the key to encrypt the DB => Generate the key
        } else {
            var salt = bcrypt.genSaltSync(10); // Generate the salt to encrypt the key
            var key = bcrypt.hashSync(api_key.apiKey, salt); // Encrypt the key
            
            // Print the only copy for the user
            console.log("WARNING : THIS IS THE ONLY COPY OF THEY DECRYPTION KEY YOU WILL HAVE. MAKE SURE TO STORE IT AT A PLACE YOU WILL FIND IT.");
            console.log("WARNING : PLACE IT AT A PLACE THAT RISKS NOTHING.");
            console.log("Your key is : " + key);

            APIKeys.push( new APIKey({ UUID: '', key: key, applicationName: appName[i] })); // Push the values into the array
        }


        // Test if it is the main Vue application => Write the informations in the JS file of the main Vue app
        if(appName[i] == 'VueAppMain') {
            const template = fs.readFileSync("../../templates_source_file/template_APIKey.js"); // Read the template
            const split_template = template.toString().split("APIKEY"); // Split to include the API key
            const complete_template = split_template.join('\'' + api_key.apiKey + '\''); // File that contains all the informations

            fs.writeFileSync('../../../../VueJS_Client/src/config/getAPIKey.js', complete_template); // Write the informations in the associate file
        }

        // Test if it is the showcase Vue application => Write the informations in the JS file of the showcase Vue app
        if(appName[i] == 'VueAppShowcase') {
            const template = fs.readFileSync("../../templates_source_file/template_APIKey.js"); // Read the template
            const split_template = template.toString().split("APIKEY"); // Split to include the API key
            const complete_template = split_template.join('\'' + api_key.apiKey + '\''); // File that contains all the informations

            fs.writeFileSync('../../../../VueJS_showcase/src/config/getAPIKey.js', complete_template); // Write the informations in the associate file
        }
    }

 
    // Drop the acutal collection of addresses
   console.log("Re-initialization of the previous API Keys");
    database.collection('apikeys').drop().catch( (error) => {}); // Don't display the warning message (POSSIBLE THAT THE COLLECTION DOESN'T EXIST WHEN WE WANT TO DELETE IT)

    
    // Insert into the database
    database.collection('apikeys').insertMany(APIKeys, function(err, result){
        if (err) {
            quit("Insert not OK")
        } else {
            quit("Insert OK")
        }
    });
 });
 
 
/**
 * Function used to display a message before quitting the function
 * @param {*} message => Message to display in the console
*/
function quit(message){
    console.log(message);
    process.exit(1);
}