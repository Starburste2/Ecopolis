const fs = require('fs'); // Import the file system to be able to write datas in file

// Console log the usage error
if(process.argv.length != 3) return console.log('Usage : node generateDropboxTokenFile.js dropbox_access_token');

fs.writeFileSync('../DropboxToken.json', '{\n    "token": "' + String(process.argv[2]) + '"\n}'); // Write the token in the file

return console.log('DropboxToken.json has been updated'); // Quit the function by printing a message