/**
 * @returns the available IP range for IP filter
 */
 exports.getIPRanges = function () {
    return [['10.11.11.0', '10.11.11.70']];
}