# Server part 
It's a NodeJS server using Express for the routing. This server will be used to store datas that are coming from the end nodes. The server is linked at the main node with the HTTP or HTTPS protocol to be accessible. The main node will communicate with the end nodes with LoRa.

This server is also used to exchange datas with the VueJS client for the authentication, to list all the microcontrollers, to see their datas, to add, modify or delete microcontrollers, ….

To store our datas, we will use a MongoDB database and we will use Mongoose for the models.

All the logs will be be stored in debug.log and error.log.

The datas need to be validated before added to the database. This is to be sure that all the datas respect our storage conditions.

The API needs some token to be able to access routes. We have multiple tokens like authentication token (token provided by the server when you authenticate yourself on the API => via email/password or via authentication token), session ID (ID provided by the server to access to the different routes => 1 hour of validity) and CSRF tokens (token provided by the server to avoid possible CSRF attacks on important URLs => Same validity than the session IDs). We have also the API keys to identify the main node which makes requests on the API + a key for the VueJS application for the non login routes like (login, creation of an account, forgotten password, …).

All the users of the application have a specific role. The admin can access to all the routes of the server while the clients can just access to specific routes. The routes which can be accessible by the clients are the routes that don't have any impacts on the system like see the microcontrollers, see the datas or download the datas for example.

All the days, at the same hour, a backup of the database is done. We encrypt this backup and we send it in the cloud (Google Drive and/or Dropbox).

<br/>

# Packages used
## HTTP 
This package is used to be able to create an HTTP server using the HTTP protocol.

The package is already installed with NodeJS but you can see the documentation here : [https://nodejs.org/api/http.html](https://nodejs.org/api/http.html)

## HTTPS
This package is used to be able to create an HTTPS server using the HTTPS protocol

This package is already installed with NodeJS but you can see the documentation here : [https://nodejs.org/api/https.html](https://nodejs.org/api/https.html)

## Body-parser@1.19.0
This package is used to be able to parse the datas that are on a packet that a client sent us. 

To install or see the documentation of this package : [https://www.npmjs.com/package/body-parser](https://www.npmjs.com/package/body-parser) :
```bash
npm install body-parser --save
```

## Check-types@11.1.2
This package is used to do some tests on the datas sent by the client.

To install or see the documentation of this package : [https://www.npmjs.com/package/check-types](https://www.npmjs.com/package/check-types) :
```bash
npm install check-types --save
```

## Express@4.17.1
This package is used for the routing. With this package, we can defined our routes to construct our API.

To install or see the documentation of this package : [https://www.npmjs.com/package/express](https://www.npmjs.com/package/express) :
```bash
npm install express --save
```

## Express-fileupload@1.2.1
This package is used by the server to be able to upload files on it.

To install or see the documentation of this package : [https://www.npmjs.com/package/express-fileupload](https://www.npmjs.com/package/express-fileupload) : 
```bash
npm install express-fileupload --save
```

## Mongoose@5.12.2
This package is used to communicate with the database. It will help us to store, retrieve, modify or delete datas from the database.

To install or see the documentation of this package : [https://www.npmjs.com/package/mongoose](https://www.npmjs.com/package/mongoose) :
```bash
npm install mongoose --save
```

## Prompt-sync@4.2.0
This package is used when you want to create the pool of addresses. It leaves the choice to the user to re-initialize the pool or not.

To install or see the documentation of this package : [https://www.npmjs.com/package/prompt-sync](https://www.npmjs.com/package/prompt-sync) :
```bash
npm install prompt-sync-history --save
```

## FS
This package is used to manipulate files that are present on the system of the server. It will help us to manipulate files when the user wants to download microcontrollers code for example.

To install or see the documentation of this package : [https://www.npmjs.com/package/file-system](https://www.npmjs.com/package/file-system) :
```bash
npm install file-system --save
```

## bcrypt@5.0.1
This package is used to generate some hashes for the server. It can be salt to encrypt passwords but it is also used to directly generate the CSRF key for example (CSRF key is used to generate the CSRF token). The salt is used to encrypt passwords we will get on the server. ***WARNING : IF YOU CHANGE THE SALT, THE HASHED PASSWORD WILL NOT BE THE SAME AS BEFORE DESPITE THE NOT HASHED PASSWORD IS GOOD.***

To install or see the documentation of this package : [https://www.npmjs.com/package/bcrypt](https://www.npmjs.com/package/bcrypt) : 
```bash
npm install bcrypt --save
```

## UUID@8.3.2
This package is used to create an universally unique identifier for the session IDs we will generate when a user will login on the VueJS application. Each session IDs have a delimited time (here we have put 1 hour of time validity). After this time validity, this ID will be expired. We have a little process on the server that is executed every 10 minutes to delete the expired session IDs. It is also used to create the API and CSRF keys.

To install or see the documentation of this package : [https://www.npmjs.com/package/uuid](https://www.npmjs.com/package/uuid) :
```bash
npm install uuid --save
```

## jsonwebtoken@8.5.1
This package is used to generate a json web token to store in the browser of the user. It will contain informations about the user like the email and his IP address. Each authentication tokens have a delimited time (here we have put 7 days). After this time, this token will be expired. We have a little process on the server that is executed every hours to delete expired authentication tokens.

To install or see the documentation of this package : [https://www.npmjs.com/package/jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) :
```bash
npm install jsonwebtoken --save
```

## csrf@3.1.0
This package is used to generate and verify CSRF tokens. To generate CSRF tokens, we are using a key generated by generateCSRFKey.js. To verify a CSRF if a CSRF token can be valid, we are using this same key.

To install or see the documentation of this package : [https://www.npmjs.com/package/csrf](https://www.npmjs.com/package/csrf) :
```bash
npm install csrf --save
```

## request-ip@2.1.3
This package is used to get the IP of the client that sends the request. It will help us to know if the authentication token sent by the client is on the good machine or not (if it is not => the cookie may have been copied so the user is rejected, he needs to login him again)

To install or see the documentation of this package : [https://www.npmjs.com/package/request-ip](https://www.npmjs.com/package/request-ip) :
```bash
npm install request-ip --save
```

## uuid-apikey@1.5.2
This package is used to generate some API keys for the applications that will make requests on the server. The API key is mandatory for the VueJS (for login and accountCreation part for example) main application, VueJs showcase application and Microcontroller applications. This package is also used to verify if an API key can be a valid one.

To install or see the documentation of this package : [https://www.npmjs.com/package/uuid-apikey](https://www.npmjs.com/package/uuid-apikey) :
```bash
npm install uuid-apikey --save
```

## keypair@1.0.3
This package is used to generate the couple of private/public keys to encrypt/decrypt the JSON web tokens. We will use the private key to encrypt the JSON web tokens we will provide to the client. And we will use the public key to decrypt the JSON web tokens the client will send to the API.

To install or see the documentations of this package : [https://www.npmjs.com/package/keypair](https://www.npmjs.com/package/keypair) :
```bash
npm install keypair --save
```

## password-validator@5.1.1
This package is used to be able to validate the password sent by the user. This password needs to follow our security policy. This check is the same on the VueJS client before sending the informations to the server. We will check that the password is formed like that : it contains between 7 and 30 characters, it contains at least one uppercase and one lowercase, it contains at least 1 digit and it not contains space. If all these steps are respected, the password is considered as OK.

To install or see the documentation of this package : [https://www.npmjs.com/package/password-validator](https://www.npmjs.com/package/password-validator) : 
```bash
npm install password-validator --save
```

## express-recaptcha@5.0.2
This package is used to validate the token generated by the recaptcha in the VueJS application. This package needs also a secret_key and a site_key to be functional. These keys are given by Google when we create the recaptcha keys for the application. For more informations, see here : [https://github.com/Starburste/Ecopolis/tree/main/VueJS_Client#recaptcha](https://github.com/Starburste/Ecopolis/tree/main/VueJS_Client#recaptcha)

To install or see the documentation of this package : [https://www.npmjs.com/package/express-recaptcha](https://www.npmjs.com/package/express-recaptcha) :
```bash
npm install express-recaptcha --save
```

## moment@2.29.1
This package is used to validate some dates the user will pass to the server by the VueJS application. We will test them and if the dates given are valid, the user can retrieve the datas. Otherwise, he will get an error.

> - Installation : [https://momentjs.com](https://momentjs.com)
> - Documentation : [https://momentjs.com/docs/](https://momentjs.com/docs/)
```bash
npm install moment --save
```

## winston@3.3.3 and express-winston@4.1.0
These packages are used to save the logs that are done on the server in different files (debug.log for all requests that are not errors, error.log if we have errors, …).

To install these packages and see their documentation : 
> - winston <br/>
> [https://www.npmjs.com/package/winston](https://www.npmjs.com/package/winston) <br/>
> - express-winston <br/>
> [https://www.npmjs.com/package/express-winston](https://www.npmjs.com/package/express-winston)

```bash
npm install winston express-winston --save
```

## helmet@4.6.0
This package is used to secure the NodeJS/Express server application by seeting various HTTP headers.

To install or see the documentation of this package : [https://www.npmjs.com/package/helmet](https://www.npmjs.com/package/helmet)
```bash
npm install helmet --save
```

## express-ipfilter@1.2.0
This package is used to filter the requests on the server by allowed IPs. This package is only used on the routes that are used by the ESP32 to make requests (route to add datas or to reconfigure the state of an ESP32). By filtering, we allowed some IPs the ESP32 can use. The others are rejected.

To install or see the documentation of this package : [https://www.npmjs.com/package/express-ipfilter](https://www.npmjs.com/package/express-ipfilter)
```bash
npm install express-ipfilter --save
```

## nocache@3.0.0
This package is an express middleware to use to disable cache on the client side. So this package sets multiple HTTP headers to avoid that. By doing this, we ensure that the client has up-to-date resources.

To install or see the documentation of this package : [https://www.npmjs.com/package/nocache](https://www.npmjs.com/package/nocache)
```bash
npm install nocache --save
```

## validator@13.6.0
This package is used to be able to validate some elements like email address, UUID v4, etc …

To install or see the documentation of this package : [https://www.npmjs.com/package/validator](https://www.npmjs.com/package/validator)
```bash
npm install validator --save
```

## nodemailer@6.6.1
This package is used to send emails to the user to verify its identity when he creates an account or to reinitialize his password when he has forgotten his password.

To install or see the documentation of this package : [https://www.npmjs.com/package/nodemailer](https://www.npmjs.com/package/nodemailer)
```bash
npm install nodemailer --save
```

## node-cron@3.0.0
This package is used to schedule some tasks like the database backups or the verification of the validity of the tokens present in the database.

To install or see the documentation of this package : [https://www.npmjs.com/package/node-cron](https://www.npmjs.com/package/node-cron)
```bash
npm install node-cron --save
```

## dropbox-v2-api@2.4.39
This package is used to be able to upload files on our Dropbox to store backups for example.

To install or see the documentation of this package : [https://www.npmjs.com/package/dropbox-v2-api](https://www.npmjs.com/package/dropbox-v2-api)
```bash
npm install dropbox-v2-api --save
```

## googleapis@76.0.0
This packge is used to be able to upload files on our Google Drive to store backups for example.

To install or see the documentation of this package : [https://www.npmjs.com/package/googleapis](https://www.npmjs.com/package/googleapis)
```bash
npm install googleapis --save
```

<br/>

# Configuration of the server
The server is not configured by default. So you need to use arguments to pass the IP address of the computer on the network and the port number to use. Here is all the possible configurations you can use to launch the server : 
> - node serveur -h => Returns the helper
> - node serveur -H => Returns the helper
> - node serveur IP_address Port_to_listen => Launch the server

The server uses the HTTP and HTTPS protocols. The server using the HTTP protocol will use the port number you precise. The server using the HTTPS protocol will use the port number you precise plus one. But these two protocols are using the same IP address.

> - For example if you run :  
```bash
node serveur 1.1.1.1 3000
```
> You will have this result : 
>> HTTP server is running on http://1.1.1.1:3000 <br/>
>> HTTPS server is running on https://1.1.1.1:3001

To make the server functionnal you need to have MongoDB installed on your PC and running. If you don't have install MongoDB, follow this : [https://docs.mongodb.com/manual/administration/install-community/](https://docs.mongodb.com/manual/administration/install-community/). You also need to have NodeJS and NPM installed. Follow this link to install NodeJS and NPM : [https://nodejs.org/en/](https://nodejs.org/en/)

Finally, launch the MongoDB database and then go inside you folder named "serveur" and tap this command in a terminal : 
```bash
node serveur IP_ADDRESS PORT_TO_LISTEN
``` 

This command will launch the server on the IP address and on the port you precise. You now will be able to access the API by making some requests (via the browser or postman for example).

<br/>

# Creation of the email address and modification of parameters to send email
For now, the program uses Gmail to send email to validate the user's account. So you need to create a Google account or you can use an existing ones. When you have created your account, you need to modify one parameter to be able to send email via the server. To do this, go on this [url](https://myaccount.google.com) and connect you. Then on the left, click on "Sécurité" and then in the tab "Accès moins sécurisée des applications", click on "Activer l'accès (déconseillé)" (see the picture below).

![alt text](./README_images/app_less_secure.png "App less secure page")

After that, click on the slider to put in blue. When it is in blue, the server can send validation email to the user (see the picture below).

![alt text](./README_images/active_less_secure_parameter.png "Activate the less secure parameter to be able to send validation email")


# Database backups
## Google Drive backups
When you have your Google account, we can start to configure it a little to be able to use the database backups functionality. Go at this [address](http://console.cloud.google.com/) to be able to activate the Google Drive API and create the service account. Connect you the application with your Google account and then click on the menu at the left of the screen and then move your pointer to "API et services" and then click on "Tableau de bord" as shown on the picture below.

![alt text](./README_images/GoogleDrivePart1.png "Access to the dashboard of the active APIs")

When you are on the dashboard, click on "Activer les API et services" as shown below :

![alt text](./README_images/GoogleDrivePart2.png "Access to the APIs and services to activate")

Then search "Google Drive API" in the input given and select it. After that, you can activate the API by clicking on the button "Activer". When you have activated your API, select "Identifiants" in the left menu and then click on "Créer des identifiants" and then on "Compte de service" as shown on the picture below :

![alt text](./README_images/GoogleDrivePart3.png "Creation of the identifiers")

Then you can enter the name of the service account and then you can click on "Créer et continuer". And finally, you can click on "OK" to finish the creation of the service account. To get the keys of the service account, you need to create them. To do this, click on service account you have created and then go on the section "Clés", click on "Ajouter une clé" and select "Créer une clé" as shown below :

![alt text](./README_images/GoogleDrivePart4.png "Creation of the keys to be able to make requests from the server")

When you have done that, you will see a popup. Select JSON and click on "Créer". Wait for the creation and then download the keys from the site (either by having a message to download the datas or the download is launched directly). **WARNING : DON'T LOSE THE FILE, WE NEED IT TO BE ABLE TO COMMUNICATE WITH GOOGLE DRIVE.**

When you have download the file, you can make this command in the folder of the downloaded file :
```bash
mv name_file_downloaded path/to/Serveur/serveur/config/credentials_service_account.json
```
By doing this command, you will move the file into the configuration part of the server and you will rename it to credentials_service_account.json (it is necessary to rename it with this name).

When you have done that, go on [Google Drive](https://drive.google.com/drive/my-drive). When you are connected and you have access to Google Drive, create a file named as you want (right click => New folder (clic droit => Nouveau dossier ; en français)). After its creation, go in that folder. Then select the URL and copy the end of it as shown on the picture below :

![alt text](./README_images/GoogleDrivePart5.png "Get the folder ID")

This part is the ID of the folder you have created. You can execute the file generateIDFolderGGDrive.js in /config/init_files. The command to make is : 
```bash
node generateIDFolderGGDrive.js id_of_folder_copy_from_drive
```
With that, you will generate a JSON file that containing the ID of the folder for the future execution.

To finish, click on the folder you have created and then click on "Partager" and enter the email address of the service account you have created before. By doing this, you will allow the service account to upload datas on the Google Drive.

## Dropbox backups
To be able to use the dropbox backups, you need to create a dropbox account or to have one. When you have created your account, go to this [address](https://www.dropbox.com/developers/apps/) and click on the button "Create app". Then select the options as shown below : 

![alt text](./README_images/dropboxPart1.png "Creation of the application to be able to store the backups on dropbox")

Then click on the button "Create app". When you are in the dashboard of the application, make sure that the access token expiration is set to "No expiration". Then just above you have a part "Generated access token" and click on "Generate". This will generate a new access token to access the application. **WARNING : THIS PART WILL BE DEPRECATED IN THE FUTURE.** When you have generated the token, copy it and then use this command in /config/init_files :
```bash
node generateDropboxTokenFile.js access_token
```

This command will generate the JSON file that will contain the access token to be able to upload the backup database on Dropbox. When you have done that, go on the section "Permissions" and select the same permissions as the picture below :

![alt text](./README_images/dropboxPart2.png "Check the same permissions")

When all of the steps before are done, you need to create a folder named "database_backup" in the root of your dropbox. All of the backups will go inside this folder.


## More informations about the backups
All the database backups are gzip archive. This archive is generated when the command below (the command makes a backup of all the ecopolis database and then compress all the datas in gzip) is executed :
```bash
mongodump --db=ecopolis --archive=./database_backups/ --gzip
```

When we have generated this archive, we will compress it again and we will encrypt it by doing this command :
```bash
tar -czf - ./database_backups/archive_name | openssl enc -e -aes256 -k key -out name_tgz_file
```

When this encrypted archive is generated, it is transfered to the selected cloud (Google Drive and/or Dropbox).

## Decryption and restore of the database
To decrypt the archive, we need to make this command :
```bash
openssl enc -d -aes256 -k key -in tgz_archive_name | tar xz -C ./
```

With this command, the archive is decompressed and decrypted at the same time. Then, to restore the database, you can do this command :
```bash
mongorestore --gzip --archive=./archive_name --db=ecopolis
```

This command will restore all the informations that are present in the database when the backup has been done.

***IMPORTANT : THE KEY TO ENCRYPT AND DECRYPT IS THE SAME. WE USE SYMETRICAL ENCRYPTION. THAT'S WHY THE KEY IS GIVEN ONLY ONE TIME TO THE ADMIN IN /config/init_files/createAPIKeys.js. SO, THE KEY IS ONLY STORED INTO THE DATABASE AND THE ADMIN HAS JUST ONE COPY OF IT.***


# Routes available
All the routes used by the VueJS client needs a session ID provided by the server to be able to access to these routes. Some routes, that can modify important datas, are requiring a CSRF token to be accessible. The CSRF token is also provided by the server when a user login via email/password or via his authentication token. Some of the routes used by the VueJS main application will need an API key. It is the case of the routes to create an account and to login on the application. The route to create an account needs a recaptcha token to be accessible. The Microcontroller application will always need an API key to be able to make requests as well as the VueJS showcase application.

Some of the routes will need some sufficient rights to be able to make some important requests that will modify the database, so the functioning of the application, or to get important datas like source code of a microcontroller for example. That's why the roles are present, they will separate the administrators and the clients in their possible requests they will make on the server. So, if a client makes a request on a route that need administrator role, the client will be rejected. The administrator, him, has access to all of the routes.

## Microcontrollers
> - GET : /api/microcontroller => Return all the microcontrollers stored into the database in JSON <br/>
> **NEED TO USE A sessionID TO BE ABLE TO GET ALL MICROCONTROLLERS** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL MICROCONTROLLERS** <br/><br/>
> - GET : /api/microcontroller/:id => Return one microcontroller identified by ID in JSON <br/>
> **NEED TO USE A sessionID TO BE ABLE TO GET ONE MICROCONTROLLER** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO GET ONE MICROCONTROLLER** <br/>
> **NEED TO HAVE A VALID MONGOOSE OBJECT ID TO GET ONE MICROCONTROLLER** <br/><br/>
> - GET : /api/lessInfos/forMain => Return all the end nodes microcontrollers to display on the VueJS main application <br/>
> **NEED TO USE THE VUEJS MAIN API KEY TO GET ALL THE MICROCONTROLLERS FOR MAIN** <br/><br/>
> - GET : /api/lessInfos/forShowcase => Return all the end nodes microcontrollers to display on the VueJS showcase application <br/>
> **NEED TO USE THE VUEJS SHOWCASE API KEY TO GET ALL THE MICROCONTROLLERS FOR SHOWCASE** <br/><br/>
> - POST : /api/microcontroller => Add a microcontroller into the database <br/>
> **NEED TO USE A sessionID TO BE ABLE TO ADD A MICROCONTROLLER** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO ADD A MICROCONTROLLER** <br/>
> **NEED TO USE A CSRFToken TO BE ABLE TO ADD A MICROCONTROLLER** <br/>
> **NEED THE ADMINISTRATOR ROLE TO BE ABLE TO ADD A MICROCONTROLLER** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***addr***, ***sleepTime***, ***isEndNode***, ***latitude*** and ***longitude*** <br/><br/>
> - PUT : /api/microcontroller/:id => Update a microcontroller with its ID <BR/>
> **NEED TO USE A sessionID TO BE ABLE TO MODIFY A MICROCONTROLLER** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO MODIFY A MICROCONTROLLER** <br/>
> **NEED TO USE A CSRFToken TO BE ABLE TO ADD A MICROCONTROLLER** <br/>
> **NEED THE ADMINISTRATOR ROLE TO BE ABLE TO MODIFY A MICROCONTROLLER** <br/>
> **NEED A VALID MONGOOSE OBJECT ID TO BE ABLE TO MODIFY A MICROCONTROLLER** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***sleepTime***, ***latitude*** and ***longitude*** <br/><br/>
> - PUT : /api/microcontroller/ => Update the configuration state of a microcontroller with the addr passed into parameter <br/>
> **NEED TO USE THE API KEY OF THE MICROCONTROLLERS TO BE ABLE TO MODIFY THE STATE OF A MICROCONTROLLER** <br/>
> **NEED TO HAVE AN IP ADDRESS IN THE AVAILABLE RANGE TO BE ABLE TO MODIFY THE STATE OF A MICROCONTROLLER** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***A (for addr)*** <br/><br/>
> - DELETE : /api/microcontroller/:id/:addr => Delete a microcontroller by its ID and its address <br/>
> **NEED TO USE A sessionID TO BE ABLE TO DELETE A MICROCONTROLLER** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO DELETE A MICROCONTROLLER** <br/>
> **NEED TO USE A CSRFToken TO BE ABLE TO ADD A MICROCONTROLLER** <br/>
> **NEED THE ADMINISTRATOR ROLE TO BE ABLE TO DELETE A MICROCONTROLLER** <br/>
> **NEED TO HAVE A VALID MONGOOSE OBJECT TO BE ABLE TO DELETE A MICROCONTROLLER** <br/> <br/>
> - GET : /api/microcontroller/datas/InLastDay => Return the microcontrollers that have not sent data since the last day <br/>
> **NEED TO USE A sessionID TO BE ABLE TO GET THE MICROCONTROLLERS THAT HAVE NO SENT DATA SINCE 1 DAY** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO GET THE MICROCONTROLLERS THAT HAVE NO SENT DATA SINCE 1 DAY** <br/>
> **NEED THE ADMINISTRATOR ROLE TO BE ABLE TO GET THE MICROCONTROLLERS THAT HAVE NO SENT DATA SINCE 1 DAY** <br/>


A microcontroller can be added via an address and an array of sleep time. The address needs to be present in the pool of addresses defined to agree with the code which are present on the microcontrollers. Indeed, the addresses of the microcontrollers are defined and send in hexadecimal (just integers between 0 and 255 included) so we decided to use a pool to store all the possible addresses for the microcontrollers. A microcontroller can be defined as an end or main node. You can have multiple end nodes but only one main node is allowed. The array of sleep time represents just the hours of the day a microcontroller needs to wake up (for example : 08:00, 15:00 and 20:00). The server will calculate the sleep time to send to the microcontroller when he will receive a message.

When you add a microcontroller, you will modify a variable in the pool to say that this address is not available and used by a microcontroller. And when you delete a microcontroller, you will modify this variable to say that the address is available to be used on a new microcontroller. 

***WARNING : WHEN YOU DELETE A MICROCONTROLLER, THE ADDRESS WILL BE AVAILABLE FOR ANOTHER ONE, AND ALL THE DATAS OF THE MICROCONTROLLER, YOU HAVE DELETED, WILL BE DELETED.***

With that, we have one little problem : the addresses are not explicits for the user. But these addresses are necessary to optimize the data transmissions between the end nodes and the main one (with the pool of addresses we have defined, the address of the microcontroller is stored on only 1 byte). For our application, it will be sufficient to store the addresses on 1 byte because we can have 256 microcontrollers in our system. We can increase this number by using an array of bytes and here you will power the 256 by the number of bytes you will use. ***If the user can define himself the addresses, they will be more explicits but the problem is that the addresses will take more space and more time to be sent between the end and the main nodes. It is done for the optimization of the programs.***

Only the administrator user can add, update or delete a microcontroller for security reasons.


## Datas
> - GET : /api/datas/byAddr/:addr => Return all the datas present in the database according to the addr of the microcontroller (the datas are ordered by the added date in the database) <br/>
> **NEED TO USE A sessionID TO BE ABLE TO RETRIEVE DATAS BY ADDR** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO RETRIEVE DATAS BY ADDR** <br/><br/>
> - GET : /datasFromInterval/:addr?firstDate&lastDate => Return all the datas between two dates <br/>
> **NEED TO USE A sessionID TO BE ABLE TO RETRIEVE DATAS BY DATES** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO RETRIEVE DATAS BY DATES** <br/>
> **FORMAT** : Query in URL <br/>
> **MANDATORY VALUES** : ***firstDate*** and ***lastDate*** <br/><br/>
> - POST : /api/datas => Add datas in the database <br/>
> **NEED TO USE THE API KEY OF THE MICROCONTROLLERS TO BE ABLE TO ADD DATAS INTO THE DATABASE** <br/>
> **NEED TO HAVE AN IP ADDRESS IN THE AVAILABLE RANGE TO BE ABLE TO ADD DATAS INTO THE DATABASE** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***AM (for addrMicrocontroller)***, ***ph***, ***H (for humidity)***, ***T (for temperature)***, ***C (for conductivity)***, ***N (for nitrogen)***, ***P (for phosphorus)***, ***K (for potassium)***, ***id (for idMsg)*** and ***FS (for firstStart if it is the first time we launch the microcontroller)*** <br/><br/>
> - GET : /api/datas/lastForMain => Return the last fifty datas recorded by a microcontroller to display them on the VueJS main application <br/>
> **NEED TO USE THE VUEJS MAIN API KEY TO BE ABLE TO GET THE LAST DATAS FROM THE DATABASE** <br/>
> **FORMAT** : QUERY <br/>
> **MANDATORY VALUE** : ***addr*** <br/><br/>
> - GET : /api/datas/lastForShowCase => Return the last fifty datas recorded by a microcontroller to display them on the VueJS showcase application <br/>
> **NEED TO USE THE VUEJS SHOWCASE API KEY TO BE ABLE TO GET THE LAST DATAS FROM THE DATABASE** <br/>
> **FORMAT** : QUERY <br/>
> **MANDATORY VALUE** : ***addr***
 
This part is used to display datas directly on the web server (ask directly on the address and port of the server), to store datas sent by the microcontrollers or to return this datas to the VueJS client.


## Download
> - GET : /api/download/microcontrollerFile/:addr/ => Download the INO file associated to the addr of the microcontroller passed in parameter. <br/>
> **NEED TO USE A sessionID TO ABLE TO DOWNLOAD MICROCONTROLLER'S CODE** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO ABLE TO DOWNLOAD MICROCONTROLLER'S CODE** <br/>
> **NEED TO USE A CSRFToken TO ABLE TO DOWNLOAD MICROCONTROLLER'S CODE** <br/>
> **NEED THE ADMINISTRATOR ROLE TO BE ABLE TO DOWNLOAD MICROCONTROLLER'S CODE** <br/><br/>
> - GET : /api/download/microcontrollerDatas/:addr/ => Download the CSV file associated to the datas added from the addr passed in parameter <br/>
> **NEED TO USE A sessionID TO ABLE TO DOWNLOAD MICROCONTROLLER'S DATAS** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO ABLE TO DOWNLOAD MICROCONTROLLER'S DATAS** <br/>
> **NEED TO USE A CSRFToken TO ABLE TO DOWNLOAD MICROCONTROLLER'S DATAS** <br/>
> **FORMAT (not mandatory)** : QUERY <br/>
> **POSSIBLE VALUES** : ***withoutDuplicates***, ***firstDate*** and ***lastDate*** <br/><br/>
> - GET : /api/download/serverCertificate/ => Download the server's certificate to put it on the SD card of the main node to be able to make HTTPS requests <br/>
> **NEED TO USE A sessionID TO ABLE TO DOWNLOAD THE SERVER'S CERTIFICATE** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO ABLE TO DOWNLOAD THE SERVER'S CERTIFICATE** <br/>
> **NEED TO USE A CSRFToken TO ABLE TO DOWNLOAD THE SERVER'S CERTIFICATE** <br/>
> **NEED TO BE ADMIN TO ABLE TO DOWNLOAD THE SERVER'S CERTIFICATE** <br/><br/>
> - GET : /api/download/allMicrocontrollersDatas => Download a CSV file that will contain all the data recorded by all the microcontrollers <br/>
> **NEED TO USE A sessionID TO ABLE TO DOWNLOAD ALL THE DATAS OF THE DB** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO ABLE TO DOWNLOAD ALL THE DATAS OF THE DB** <br/>
> **NEED TO USE A CSRFToken TO ABLE TO DOWNLOAD ALL THE DATAS OF THE DB** <br/>

This part is used to download the code of the microcontrollers we have created and all of the possible datas the microcontroller has sent during its life cycle.

Only the administrator can download microcontroller's file or the server's certificate for security reasons. As the client can see datas via graphic or via the pagination, he can download the datas of the microcontrollers without restrictions.

We can download the datas in two ways :
> - Download all the datas without processing
> - Download the datas with some processing (no duplicates and/or interval of time for example)

## Upload
> - POST : /api/uploadDatas => Upload the datas from a SD card of a microcontroller <br/>
> **NEED TO USE A sessionID TO BE ABLE TO UPLOAD DATAS ON THE SERVER** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO UPLOAD DATAS ON THE SERVER** <br/>
> **NEED TO USE A CSRFToken TO BE ABLE TO UPLOAD DATAS ON THE SERVER** <br/>
> **NEED THE ADMINISTRATOR ROLE TO BE ABLE TO UPLOAD DATAS ON THE SERVER** <br/>
> **FORMAT** : KEY => VALUE (**CSV FILE** for datas) <br/>
> **MANDATORY VALUES** : ***datas (file in CSV format)***

This part is used to upload the records a microcontroller has on the SD card. So, we aggregate the datas from the SD card with the datas stored in the database.

Only the administrator can upload datas into the database for security reasons.

## PoolAddress
> - GET : /api/getAvailableAddresses => Return all the available addresses for the microcontrollers <br/>
> **NEED TO USE A sessionID TO BE ABLE TO GET ALL THE AVAILABLE ADDRESSES** <br/>
> **NEED TO HAVE AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE AVAILABLE ADDRESSES** <br/>
> **NEED THE ADMININSTRATOR ROLE TO BE ABLE TO GET ALL THE AVAILABLE ADDRESSES** <br/>

This part is only used by the VueJS client application when we want to create a microcontroller. <br/>


## User
> - POST : /api/user/ => Generate a new session ID, a new authentication token and a new CSRF token <br/>
> **NEED THE API KEY OF THE VUEJS MAIN APPLICATION TO BE ABLE TO LOGIN VIA EMAIL/PASSWORD** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***email***, ***password*** and ***g-recaptcha-response***<br/><br/>
> - POST : /api/user/add => Add a new user to the database with the email and the password + Send an email to validate the account <br/>
> **NEED THE API KEY OF THE VUEJS MAIN APPLICATION TO BE ABLE TO ADD A NEW USER** <br/>
> **NEED THE RECAPTCHA TOKEN** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***email***, ***password*** and ***g-recaptcha-response*** <br/><br/>
> - POST : /api/user/login => Generate a new session ID, a new authentication token and a new CSRF token <br/>
> **NEED THE API KEY OF THE VUEJS MAIN APPLICATION TO BE ABLE TO LOGIN VIA AUTHENTICATION TOKEN** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***token*** <br/><br/>
> - POST : /api/user/logout => Logout a user from the application (reset of the authenticationToken, sessionID and CSRF token) <br/>
> **NEED THE API KEY OF THE VUEJS MAIN APPLICATION TO BE ABLE TO LOGOUT** <br/><br/>
> - PUT : /api/user/modify => Modify the identifiers of a user and re-generate all the connection values like authentication token, session ID and CSRF token <br/>
> **NEED A sessionID TO BE ABLE TO MODIFY A USER** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO MODIFY A USER** <br/>
> **NEED A CSRFToken TO BE ABLE TO MODIFY A USER** <br/>
> **NEED THE authenticationToken TO BE ABLE TO MODIFY A USER** <br/>
> **NEED A Recaptcha token TO BE ABLE TO MODIFY A USER** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***email***, ***oldPassword***, ***password***, ***last_email*** <br/> <br/>
> - GET : /api/user/getAllUsers => Return all the users present into the database (id, email and role only) <br/>
> **NEED A sessionID TO BE ABLE TO GET ALL THE USERS** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE USERS** <br/>
> **NEED A CSRFToken TO BE ABLE TO GET ALL THE USERS** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO GET ALL THE USERS** <br/><br/>
> - POST : /api/user/addByAdmin => Addition a new user to the database by the administrator <br/>
> **NEED A sessionID TO BE ABLE TO ADD A USER WHEN WE ARE ADMIN** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO ADD A USER WHEN WE ARE ADMIN** <br/>
> **NEED A CSRFToken TO BE ABLE TO ADD A USER WHEN WE ARE ADMIN** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO ADD A USER WHEN WE ARE ADMIN** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***email***, ***password***, ***role*** and ***isAccountActivated*** <br/><br/>
> - PUT : /api/user/modifyByAdmin => Modify the password and/or the role of the user selected <br/>
> **NEED A sessionID TO BE ABLE TO MODIFY THE PASSWORD OR/AND ROLE OF THE USER OR/AND THE STATE OF THE ACCOUNT** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO MODIFY THE PASSWORD OR/AND ROLE OF THE USER OR/AND THE STATE OF THE ACCOUNT** <br/>
> **NEED A CSRFToken TO BE ABLE TO MODIFY THE PASSWORD OR/AND ROLE OF THE USER OR/AND THE STATE OF THE ACCOUNT** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO MODIFY THE PASSWORD OR/AND ROLE OF THE USER OR/AND THE STATE OF THE ACCOUNT** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***email***, ***password***, ***modifyPassword***, ***role***, ***modifyRole***, ***accountState***, ***modifyState*** <br/><br/>
> - DELETE : /api/user/deleteByAdmin/:id => Delete a user when we are admin <br/>
> **NEED A sessionID TO BE ABLE TO DELETE A USER** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO DELETE A USER** <br/>
> **NEED A CSRFToken TO BE ABLE TO DELETE A USER** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO DELETE A USER** <br/>
> **NEED A GOOD MONGOOSE ID TO BE ABLE TO DELETE A USER** <br/><br/>
> - POST : /api/user/activate-account => Activate the account of a user <br/>
> **NEED THE VUEJS MAIN API KEY BE ABLE TO ACTIVATE AN ACCOUNT** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***activationToken*** <br/><br/>
> - POST : /api/user/email-reinit-passwd => First of the three route to reinitialize a forgotten password (Search if the account exists and then send an email) <br/>
> **NEED THE VUEJS MAIN APIKEY TO BE ABLE TO GET AN EMAIL TO REINIT THE PASSWORD** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***email***<br/><br/>
> - GET : /api/user/access-modify-passwd/:token => Second of the three route to reinitialize a forgotten password (Check if the token exists) <br/>
> **NEED THE VUEJS MAIN APIKEY TO BE ABLE TO ACCESS THE FORM TO MODIFY THE PASSWORD** <br/><br/>
> - PUT : /api/user/modify-forgotten-passwd => Last of the three route to reinitialize a forgotten password (Reinitialization of the password) <br/>
> **NEED HTE VUEJS MAIN APIKEY TO BE ABLE TO MODIFY THE FORGOTTEN PASSWORD** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***email***, ***password***, ***token*** <br/><br/>

This part is used to identify or add a client into the database and to manage all the clients. This part is used by the VueJS client application. It is used to send the sessionID, the CSRF token, the email of the user, his role and the authentication token (contains the email, the role and the IP of the client => To know if the client that makes the authentication request via the good token and if this token is in the good machine. It is also to know if the cookie has just not be copied => if the cookie isn't on the good machine, the user is rejected and he needs to login). When a user logs out, we reset all the informations in the database to disable the possibility of requests from the user with the old informations about his last session.

When a user wants to modify his identifiers, he will sent all the possible values to verify that it is him that makes the request. To do that, we send in the header of the request the session ID and the CSRF token and, in the body, we put the other values like the authentication token, the recaptcha token, the email (the new one the user wants to use), the last email (old email the user doesn't want to use anymore), the old password (the old password the user doesn't want to use anymore) and the password (the new one he wants to use now). If the user cannot change his identifiers due to some problems (connection probem => server unreachable, a user has already this email, …), the server will return a problem. If not, the user can now use his new pair of email/password and all his application identifiers (authentication token, CSRF token and session ID) are updated with the new values he has entered.

When a user wants to create an account, his role will be client by default. This user cannot modify by himself his role. When he has created his account, the user needs to validate the creation of the account by clicking on the button "Activer mon compte" that he will receive by email.

When a user has lost his password, he can reinitialize it. To do this, first, he will give his account's email address. With that, we will check if a user corresponds to the email. If a user corresponds, an email will be sent to him with a link to follow. Secondly, when he will arrive on the page to change his password, the first request the VueJS application will do is verify if the token present in the URL corresponds to someone into the database. If someone has this token, the server will send a new token for the submition of the form (the last token is deleted), the form will appear and then the user will be able to give his email and new password. When he will validate the form, we will send to the server the token the server gives to us, the email and the new password. The server will check if the email and the token correspond to someone. If it is the case, the user will be updated.

***WARNING : ALL THE TOKENS TO REINIT A PASSWORD ARE UNIQUE. WHEN A TOKEN IS USED, IT CANNOT BE REUSED. SO, IF YOU HAVE MADE A MISTAKE, YOU WILL NEED TO RE-ASK FOR A PASSWORD REINITIALIZATION.***
 
***WARNING : ALL THE TOKENS USED TO REINIT A PASSWORD HAVE A LIFETIME OF AROUND 30 MINUTES. ***

On the VueJS application, the admin can access to a dashboard to see all the users (email and role) and manage them. The admin can modify the password and/or the role of the users. If the admin wants, he can also delete a user.

<br/>

## Logs
> - GET : /api/logs/getLogs => Get all the logs which are in agreement with the user's queries <br/>
> **NEED THE sessionID OF THE USER TO RETRIEVE THE LOGS** <br/>
> **NEED AN ACTIVATED ACCOUNT TO RETRIEVE THE LOGS** <br/>
> **NEED THE csrfToken OF THE USER TO RETRIEVE THE LOGS** <br/>
> **NEED THE ADMIN ROLE TO RETRIEVE THE LOGS** <br/>
> **FORMAT** : QUERY <br/>
> **MANDATORY VALUES** :  ***method***, ***code***, ***url***, ***firstDate*** and ***lastDate***<br/>

This part is used by the admin. The administrators can research what requests are done on the server by the name of the URL, the HTTP code, the method (GET, POST, PUT or DELETE) and between the first date and the last one.

<br/>

## Posts
> - GET : /api/posts/getAllPosts => Get all posts when we are admin <br/>
> **NEED A sessionID TO BE ABLE TO GET ALL THE POSTS** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE POSTS** <br/>
> **NEED A CSRFToken TO BE ABLE TO GET ALL THE POSTS** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO GET ALL THE POSTS** <br/><br/>
> - GET : /api/posts/fromMain => Get all the posts for the VueJS main application <br/>
> **NEED THE VUEJS MAIN APIKEY TO BE ABLE TO GET ALL THE POSTS FOR MAIN APPLICATION** <br/><br/>
> - GET : /api/posts/fromShowcase => Get all the posts for the VueJS showcase application <br/>
> **NEED THE VUEJS SHOWCASE APIKEY TO BE ABLE TO GET ALL THE POSTS FOR SHOWCASE APPLICATION** <br/><br/>
> - POST : /api/posts/addPost => Add a post into the database <br/>
> **NEED A sessionID TO BE ABLE TO ADD A POST** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO ADD A POST** <br/>
> **NEED A csrfToken TO BE ABLE TO ADD A POST** <br/>
> **NEED TO BE AN ADMIN TO BE ABLE TO ADD A POST** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***title***, ***text*** and ***addedBy*** <br/><br/>
> - PUT : /api/posts/modifyPost/:id => Modify a post via the ID in the database <br/>
> **NEED A VALID ID PASSED IN PARAMETER TO BE ABLE TO MODIFY A POST** <br/>
> **NEED A sessionID TO BE ABLE TO MODIFY A POST** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO MODIFY A POST** <br/>
> **NEED A csrfToken TO BE ABLE TO MODIFY A POST** <br/>
> **NEED TO BE AN ADMIN TO BE ABLE TO MODIFY A POST** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***title*** and ***text*** <br/><br/>
> - DELETE : /api/posts/deletePost/:id => Delete a post via the ID in the database <br/>
> **NEED A VALID ID PASSED IN PARAMETER TO BE ABLE TO DELETE A POST** <br/>
> **NEED A sessionID TO BE ABLE TO DELETE A POST** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO DELETE A POST** <br/>
> **NEED A csrfToken TO BE ABLE TO DELETE A POST** <br/>
> **NEED TO BE AN ADMIN TO BE ABLE TO DELETE A POST** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***titlePost*** <br/>

This part is used by the admin to be able to make some posts for all the users. The posts can contain some links, images, text, etc….

<br/>

## Groups
> - GET : /api/groups/ => Get all the groups <br/>
> **NEED A sessionID TO BE ABLE TO GET ALL THE GROUPS** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE GROUPS** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO GET ALL THE GROUPS** <br/><br/>
> - GET : /api/groups/withoutAllInfo => Get all the groups without all the informations of the group <br/>
> **NEED A sessionID TO BE ABLE TO GET ALL THE GROUPS WITHOUT ALL THE INFOS** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE GROUPS WITHOUT ALL THE INFOS** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO GET ALL THE GROUPS WITHOUT ALL THE INFOS** <br/><br/>
> - GET : /api/groups/getForReferences => Get all the available addresses that can be used as address for the relay <br/>
> **NEED A sessionID TO BE ABLE TO GET ALL THE AVAILABLE ADDRESSES TO CREATE A GROUP AND ITS RELAY** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE AVAILABLE ADDRESSES TO CREATE A GROUP AND ITS RELAY** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO GET ALL THE AVAILABLE ADDRESSES TO CREATE A GROUP AND ITS RELAY** <br/><br/>
> - GET : /api/groups/getMicrocontrollersGroup => Get all the microcontroller of one group by groupName <br/>
> **NEED A sessionID TO BE ABLE TO GET ALL THE MICROCONTROLLERS OF A GROUP** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE MICROCONTROLLERS OF A GROUP** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO GET ALL THE MICROCONTROLLERS OF A GROUP** <br/>
> **FORMAT** : QUERY <br/>
> **MANDATORY VALUES** :  ***groupName*** <br/><br/>
> - GET : /api/groups/getMicrocontrollersWithoutGroups => Get all the microcontroller that have no groups <br/>
> **NEED A sessionID TO BE ABLE TO GET ALL THE MICROCONTROLLERS THAT HAVE NO GROUPS** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO GET ALL THE MICROCONTROLLERS THAT HAVE NO GROUPS** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO GET ALL THE MICROCONTROLLERS THAT HAVE NO GROUPS** <br/><br/>
> - GET : /api/groups/applyConfigToAllUc => Update the configuration of the microcontrollers that have a group <br/>
> **NEED A sessionID TO BE ABLE TO APPLY THE CONFIGURATION OF A GROUP TO ITS MICROCONTROLLERS** <br/>
> **NEED AN ACTIVATED ACCOUNT TO APPLY THE CONFIGURATION OF A GROUP TO ITS MICROCONTROLLERS** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO APPLY THE CONFIGURATION OF A GROUP TO ITS MICROCONTROLLERS** <br/>
> **FORMAT** : QUERY <br/>
> **MANDATORY VALUES** :  ***referingNode*** <br/><br/>
> - GET : /api/groups/applyConfigToAllUcOfMainNodeGroup => Update the configuration of the microcontrollers that don't have a group <br/>
> **NEED A sessionID TO BE ABLE TO APPLY THE CONFIGURATION TO MICROCONTROLLERS WITHOUT GROUP** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO APPLY THE CONFIGURATION TO MICROCONTROLLERS WITHOUT GROUP** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO APPLY THE CONFIGURATION TO MICROCONTROLLERS WITHOUT GROUP** <br/>
> **FORMAT** : QUERY <br/>
> **MANDATORY VALUES** :  ***referingBeginningTime***, ***selectTime*** and ***delayUc*** <br/><br/>
> - POST : /api/groups/ => Used to be able to add a group in the database and create the relay associated to the group <br/>
> **NEED A sessionID TO BE ABLE TO ADD A GROUP AND ITS RELAY** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO ADD A GROUP AND ITS RELAY** <br/>
> **NEED THE CSRF TOKEN TO BE ABLE TO ADD A GROUP AND ITS RELAY** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO ADD A GROUP AND ITS RELAY** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***name***, ***referingNode***, ***beginningHour***, ***delayBetween2uCInS*** and ***delayBetweenHours*** <br/><br/>
> - POST : /api/groups/addMicrocontrollersToGroup => Used to be able to add microcontrollers to a group <br/>
> **NEED A sessionID TO BE ABLE TO ADD A LIST OF MICROCONTROLLERS IN A GROUP** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO ADD A LIST OF MICROCONTROLLERS IN A GROUP** <br/>
> **NEED THE CSRF TOKEN TO BE ABLE TO ADD A LIST OF MICROCONTROLLERS IN A GROUP** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO ADD A LIST OF MICROCONTROLLERS IN A GROUP** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** :  ***referingNode*** and ***microcontrollersList*** <br/><br/>
> - PUT : /api/groups/modify/:id => Used to be able to modify a group <br/>
> **NEED A sessionID TO BE ABLE TO MODIFY A GROUP** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO MODIFY A GROUP** <br/>
> **NEED THE CSRF TOKEN TO BE ABLE TO MODIFY A GROUP** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO MODIFY A GROUP** <br/>
> **NEED A VALID MONGOOSE ID OBJECT TO BE ABLE TO MODIFY A GROUP** <br/>
> **FORMAT** : JSON <br/>
> **MANDATORY VALUES** : ***name***, ***beginningHour***, ***delayBetween2uCInS*** and ***delayBetweenHours*** <br/><br/>
> - DELETE : /api/groups/delete/:id => Used to be able to delete a group and the relay too <br/>
> **NEED A sessionID TO BE ABLE TO DELETE A GROUP AND THE RELAY** <br/>
> **NEED AN ACTIVATED ACCOUNT TO BE ABLE TO DELETE A GROUP AND THE RELAY** <br/>
> **NEED THE CSRF TOKEN TO BE ABLE TO DELETE A GROUP AND THE RELAY** <br/>
> **NEED THE ADMIN ROLE TO BE ABLE TO DELETE A GROUP AND THE RELAY** <br/>
> **NEED A VALID MONGOOSE ID OBJECT TO BE ABLE TO DELETE A GROUP AND THE RELAY** <br/>

<br/>

# Download
## Microcontroller's file
***You need to be administrator to be able to download the microcontrollers' file. If you try without an administrator account, you will be rejected.***

According to the datas present in the database, we can know what type of file we need to download (if it is a sender or a receiver file). With that, we uses the templates to replace all the indicated parts with the microcontroller datas. Thanks to that, we can have a fully and operational microcontroller code to upload directly on one ESP32. This operation will be beneficial for people that don't know IT or to gain time for the others.

The codes will be temporary stored in the temp directory in the receiver or sender ones. These directories will store the custom files during their modifications and will be deleted after the user has downloaded the code.

To upload the code :
> - Get the code of a microcontroller on the server
> - Verify the file
>> - Sender
>>> For this file, it is a bit more difficult to check if the file is good. First, you need to check that the address of the microcontroller is good (See the yellow part on the pictures below). ***You need to take into account that the 0x before the address of your microcontroller is perfectly normal***. Secondly, you need to check the address of the main node you have put in place. Thirdly, you need to check if the first deep sleep is defined and the number of sleep time you have put for the microcontroller and finally, the values of these sleep times.
>>> ![alt text](./README_images/sender_verification_addr_1.png "Verification of the first place of the address") <br/><br>
>>> ![alt text](./README_images/sender_verification_addr_2.png "Verification of the first place of the address") <br/><br/>
>>> ![alt text](./README_images/sender_verification_addr_main_node.png "Verification of the main node's address") <br/><br/>
>>> ![alt text](./README_images/sender_verification_first_deep_sleep.png "Verification of the first deep sleep") <br/><br/>
>>> ![alt text](./README_images/sender_verification_number_sleepTime.png "Verification of the number of sleep times") <br/><br/>
>>> ![alt text](./README_images/sender_verification_values_sleepTime.png "Verification of the values of sleep times") <br/><br/>

>> -  HTTP receiver
>>> For this file, it is pretty simple to verify if  the file is good. First of all, you need to check that the address of the microcontroller is good (See the yellow part in the pictures below). ***You need to take into account that the 0x before the address of your microcontroller is perfectly normal***. And finally, you need to check if the API key has a good format (4 groups of seven characters separated by dashes (-)).<br/>
>>> ![alt text](./README_images/receiver_verfication_addr_part1.png "First verfication to do on the receiver file") <br/><br>
>>> ![alt text](./README_images/receiver_verfication_addr_part2.png "Second verification to do on the receiver file") <br/><br/>
>>> ![alt text](./README_images/receiver_verification_APIKey.png "Last verification to do on the receiver file") <br/><br/>

>> - HTTPS receiver
>>> For this file, it is a little more complicated to verify than the HTTP one. You need to verify all the informations as HTTP receiver plus the certificate value. To do that, first of all, make the verification of HTTP. Now, we can pass to the verification of the certificate. You need to verify that the certificate has this format : <br/>
>>> - a backslash then a line break, the begin certificate line with a \n then a backslash, a line break then a line of characters with a \n and a backslash, etc …. The last line needs to be the end certificate line with just a \n and a ; as you can see on the picture below: <br/>
>>> ![alt text](./README_images/certificate_verification.png "Verification of the certificate value") <br/><br/>

>> -  Relay
>>> For this file, it is pretty simple to verify if  the file is good. First of all, you need to check that the address of the microcontroller is good (See the yellow part in the pictures below). ***You need to take into account that the 0x before the address of your microcontroller is perfectly normal***. And finally, the address of the receiver.<br/>
>>> ![alt text](./README_images/relay_destination_and_local_addresses.png "First verfication to do on the relay file") <br/><br>
>>> ![alt text](./README_images/relay_local_address.png "Second verification to do on the relay file") <br/><br/>
>>> ![alt text](./README_images/relay_destination_address.png "Last verification to do on the relay file") <br/><br/>

> - Upload the code on the microcontroller
>> Check the card options (Here it is for our TTGO but it can work for LOLIN D32/D32 PRO) : <br/>
>>> ![alt text](./README_images/card_options.png "Verify the card optoions before the uploading") <br/><br/>
>> You can upload the file on the ESP32 : <br/>
>>> ![alt text](./README_images/upload.png "Upload code") <br/><br/>
>> When you see that, the code is well uploaded on the microcontroller : 
>>> ![alt text](./README_images/upload_OK.png "The upload of the code went well") <br/><br/>


## Microcontroller's datas
***All the users can download the microcontrollers' datas because all of them can see the datas via graphic or pagination.***

You can export all the datas that are present in the database too. With that, you can get a CSV file that will contain all the possible datas from the microcontroller formated in the CSV format. The format of the datas is the following : 
> microcontroller_addr ; ph ; humidity ; temperature ; conductivity ; nitrogen ; phosphorus ; potassium ; added_date

With this file, the user can read all the datas in plain text. Another usage, why not, would be using machine learning to predict the next datas that will come for this microcontroller.

# Upload of datas on the server
You can, if you have encoutered problems to send messages to the server, upload the datas directly on it. ***This feature should remain a last-aid solution to store the datas on the server because the microcontrollers are normally able to send their datas on it.***

The datas stored in the SD card are in the CSV format with the following structure :
> - Microcontroller address ; Server's transaction state ; ph ; humidity ; temperature ; conductivity ; nitrogen ; phosphorus ; potassium ; time to sleep ; idMsg

The microcontroller address will be one of our pool of addresses. The server's transaction state can just have 2 values : 1 and -1. The 1 indicates that the transaction with the server is good and -1 indicates that the server didn't respond to the request of the microcontroller. The ph, the humidity, the temperature, the conductivity, the nitrogen, the phosphorus and the potassium will be the values captured by the JXBS-3001-TR sensor. Finally, the time to sleep will be the time that the microcontroller will be in deep sleep before re-sending a new message.

**THE FILE NEEDS TO BE SENT WITH A KEY NAMED datas. THE UPLOADED FILE WILL BE DELETED OF THE SERVER AFTER ALL THE TREATMENTS.**

You have multiple possibilities to add datas : 
> - The server has all the datas into the database that are stored on the SD card
> - The server has some datas into the database but no the wholeness that are stored on the SD card

## The server has all the datas
The server will retrieve all the datas that are present in the database (datas corresponding to the address of the microcontroller). After that, we will compare all the informations stored into the database and the ones that are on the SD card. To know the datas that are already stored, we will mark them by putting a 1 in the server's transaction state. Indeed, this value represents the state of the transaction with the server but it is also to know if the server has stored the datas into the database. So putting a 1 can help us to know what is the datas that are already present or not. So if all the server's transaction state are put to 1, all the datas are already stored into the server, so nothing to store.

## The server has some datas but not all
The server will retrieve the datas that are present in the database (datas corresponding to the address of the microcontroller). After that, we will check if we have already put some datas inside (these are the same steps as before). But here, all the server's transaction state are not put to 1 so we need to store some of them. To do that, we will make an aggregation of all the datas.

![alt text](./README_images/aggregation_for_upload.png "Aggregation of the datas during the uploading of the file to insert datas in the database") <br/>

First, we need to know that the SD datas don't have time reference. That's why the aggregation is here. It is to put to the datas a time reference to be able to retrieve the datas with globally the correct added date to the database. To put a time reference, here, we will use the datas that are already stored in and we will do what we name "up" and "down" aggregation. On the diagram you can see that up aggregation is that when you use the next data to put a time reference on the actual and that down aggregation is that when you use the last data to put a time reference on the actual.

Indeed, we use the added date with the sleep time to estimate the date the message was sent plus, we added a little offset which is on 0.5 to 1.5 seconds to simulate the treatments an ESP32 can do after the sending.

With this technique, we can reconstruct the whole database with time references. So, we can add the red lines to the database to have the whole on the server.


## Possible code
We have multiple codes the alogrithm can return to show a specific message on the VueJS client. Here is the possible codes we have defined : 
> - 1
>> The code 1 is used to say to the application that all the datas are well inserted into the database. <br/><br/>
> - 2
>> The code 2 us used to say to the application that all the datas are already present into the database. <br/><br/>
> - -1
>> The code -1 is used to say to the application that we want to upload datas recorded by an undefined microcontroller. <br/><br/>
> - -2
>> The code -2 is used to say to the application that the microcontroller has not sent datas already. So we need to start it to be able to make aggregation. <br/><br/>
> - -3
>> The code -3 is used to say to the application that the CSV file is poorly formed and that it contains errors.