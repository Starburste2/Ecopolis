/*
  relay.ino file using Arduino_LoRa library

  Containing all the code to receive messages from LoRa and re-send it
  Can send and receive all types of messages from all types of nodes

  LoRa codes :
    - DATAS => 1 (To identify a message with datas inside)
    - OK => 2 (The main node and the server has well received the data + No reconfiguration)
    - OK_AND_CONF => 3 (The main node and the server has well received the data + With reconfiguration)
    - RECONF_OK => 4 (Reconfiguration on the end node is good => Tell the server that he can update its database)

  @version : 0.7
*/

// Librairies included
#include <SPI.h>
#include <LoRa.h>
#include <Wire.h>
#include <Arduino_JSON.h>


// Define the pins used by the transceiver module
#define NSS 5
#define RST -1 // If used => 14 ; If not used => -1
#define DIO0 4


// Define the messages ID for the acknowledgment messages
#define DATAS 1
#define OK 2
#define OK_AND_CONF 3
#define RECONF_OK 4


// Define the variables to put the microcontroller in deep sleep mode
#define uS_TO_S_FACTOR 1000000ULL // To convert microseconds in seconds and use formatter unsigned long long to have the correct value when we put the timer
RTC_DATA_ATTR int next_sleep_time = 0; // Next time to sleep
RTC_DATA_ATTR int time_to_wait_before_sleep = 0; // Time to wait before sleep
RTC_DATA_ATTR int nb_child = 0; // Number of child
RTC_DATA_ATTR int nb_messages_of_child = 0; // Number of messages got from child


// Addresses for sending
const byte local_address = LOCAL_ADDRESS; // Device's address (00 => FF)
const byte destination = DESTINATION; // Destination of the message


/**
   Set up of the variables and the modules
*/
void setup() {
  Serial.begin(115200); // Serial initialization
  while (!Serial);


  // Set the pins to use for chip select, reset and irq
  LoRa.setPins(NSS, RST, DIO0);


  // Antenna used => 433 MHz => 433E6
  if (!LoRa.begin(433E6)) {
    Serial.println("LoRa init failed. Check your connections.");
    while (true); // If problems do nothing
  }
  LoRa.setSpreadingFactor(10); // Set the spreading factor to 10
  Serial.println("LoRa init succeeded.");
}


/**
   Main program
*/
void loop() {
  // Parse for a packet, and call onReceive when we have receive a packet
  onReceive(LoRa.parsePacket());
}


/**
   Function used to be able to get the messages and to transmit them to the end node or to the main one
     according to the packets headers (LoRa.write) and body (LoRa.print)
   @param packet_size => Size of the packet (when we receive a message, it will be greater than 0)
*/
void onReceive(int packet_size) {
  if (packet_size == 0) return; // If there is not packet, quit the function


  // Read the first packet header bytes to know the number of headers bytes to read (3 more for end node ; 4 for main node)
  byte recipient = LoRa.read(); // Recipient address
  byte sender = LoRa.read(); // Sender address


  // If the recipient isn't this device or broadcast
  if (recipient != local_address && recipient != LOCAL_ADDRESS) {
    Serial.println("This message is not for me.");
    return; // Quit the function
  }


  // If the sender is the main node
  if (sender == destination && sender == DESTINATION) {
    Serial.println("Message received from the main node");

    // Receive the last information from the main node
    byte incoming_msg_id = LoRa.read(); // Message id
    byte incoming_length = LoRa.read(); // Message length
    byte cycle_identifier_sent = LoRa.read(); // Cycle identifier
    byte is_payload_ok = LoRa.read(); // Get the informations about the transaction with the server


    // Read the payload sent to forward to the end node
    String payload = "";
    while (LoRa.available()) {
      payload += (char)LoRa.read();
    }

    // Print the information for debugging
    Serial.println("Received from: 0x" + String(sender, HEX));
    Serial.println("Sent to: 0x" + String(recipient, HEX));
    Serial.println("Message ID: " + String(incoming_msg_id));
    Serial.println("Message length: " + String(incoming_length));
    Serial.println("Message: " + payload);
    Serial.println("Cycle identifier: " + String(cycle_identifier_sent));
    Serial.println("RSSI: " + String(LoRa.packetRssi()));
    Serial.println("Snr: " + String(LoRa.packetSnr()));
    Serial.println();


    // Deserialize the payload to retrieve the end node address
    JSONVar response_infos = JSON.parse(payload);


    // Check if the decoding is good or not => Not print message and quit the function
    if (JSON.typeof(response_infos) == "undefined") {
      Serial.println("Parse of the main node message failed");
      return; // Quit the function
    }


    byte end_node_address = int(response_infos["ENA"]); // Get the end node address


    // Print debugging
    Serial.print("Sending : ");
    Serial.print(payload);
    Serial.print(" ; to : ");
    Serial.print(String(end_node_address, HEX));
    Serial.print(" ; from : ");
    Serial.println(String(local_address, HEX));
    sendMessageToEndNode(payload, end_node_address, local_address, incoming_msg_id, incoming_length, cycle_identifier_sent, is_payload_ok); // Send the message to the end node


    // If the sender is an end node
  } else {
    Serial.println("Message received from an end node");

    // Receive the last information from the end node
    byte incoming_msg_id = LoRa.read(); // Message id
    byte incoming_length = LoRa.read(); // Message length
    byte cycle_identifier = LoRa.read(); // Cycle_identifer of end node


    // Read the message to forward to the main node
    String incoming = "";
    while (LoRa.available()) {
      incoming += (char)LoRa.read();
    }


    // Print the information for debugging
    Serial.println("Received from: 0x" + String(sender, HEX));
    Serial.println("Sent to: 0x" + String(recipient, HEX));
    Serial.println("Message length: " + String(incoming_length));
    Serial.println("Message ID: " + String(incoming_msg_id));
    Serial.println("Cycle identifier: " + String(cycle_identifier));
    Serial.println("Payload: " + incoming);
    Serial.println("RSSI: " + String(LoRa.packetRssi()));
    Serial.println("Snr: " + String(LoRa.packetSnr()));
    Serial.println();


    // Print debugging
    Serial.print("Sending : ");
    Serial.print(incoming);
    Serial.print(" ; to : ");
    Serial.print(String(destination, HEX));
    Serial.print(" ; from : ");
    Serial.println(String(local_address, HEX));
    sendMessageToMainNode(incoming, destination, local_address, incoming_msg_id, incoming_length, cycle_identifier); // Send the message to the main node
  }
}


/**
   Function used to forward the message coming from an end node to the main one
   @param message => Message, in JSON, for the server (contained all the data)
   @param destination => Destination of the message
   @param local_address => Address of the current device for the response
   @param messageID => ID of the message for the future processing on the main node
   @param message_length => Length of the original message
   @param cycle_identifier => Identifier of the cycle of the node
*/
void sendMessageToMainNode(String message, byte destination, byte local_address, int messageID, int message_length, byte cycle_identifier) {
  LoRa.beginPacket(); // Create the packet to send
  LoRa.write(destination); // Add destination address
  LoRa.write(local_address); // Add sender address
  LoRa.write(messageID); // Add message ID
  LoRa.write(message_length); // Add the length of the message
  LoRa.write(cycle_identifier); // Add the cycle identifier
  LoRa.print(message); // Add the message to send
  LoRa.endPacket(); // End and send the packet
}


/**
   Function used to forward the message coming from the main node to the end ones
   @param message => Message, in JSON, for the end node (contained sleepTime, reconfiguration, …)
   @param sender => Destination address
   @param recipient => Sender address
   @param id_message => ID of the message for future processing
   @param message_length => Length of the original message
   @param cycle_identifier => Identifier of the cycle of the node
   @param is_payload_ok => Server's transaction state (2 values : 1 (transaction is good) ; 2 otherwise)
*/
void sendMessageToEndNode(String message, byte sender, byte recipient, int id_message, int message_length, byte cycle_identifier, int is_payload_ok) {
  LoRa.beginPacket(); // Create the packet to send
  LoRa.write(sender);  // Add destination address
  LoRa.write(recipient); // Add sender address
  LoRa.write(id_message); // Add message ID
  LoRa.write(message_length); // Add the original length of the message
  LoRa.write(cycle_identifier); // Add the cycle_identifier of the end node
  LoRa.write(is_payload_ok); // Add the server's transaction state
  LoRa.print(message); // Add the message to send
  LoRa.endPacket(); // End and send the packet
}


/**
   Function used to put the modules and the microcontroller in deep sleep mode
*/
void goSleep() {
  LoRa.sleep(); // Sleep the LoRa module

  esp_sleep_enable_timer_wakeup(next_sleep_time * uS_TO_S_FACTOR); // Use the sleep time value from the server
  esp_deep_sleep_start(); // Can't put code after that because it will not be executed
}
