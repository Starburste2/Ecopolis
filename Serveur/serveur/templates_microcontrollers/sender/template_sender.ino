/*
  sender.ino file using Arduino_LoRa library

  Containing all the code to send datas picked up by the sensors and send them to the main node via LoRa
  Even if the datas are stored on the server, we store them in a microSD card which is on the ESP32
  When the microcontroller doesn't send or receive something, it will be put in deep sleep mode
  When the microcontroller can make transaction with the server, we take the sleep time returned by the server
    => because it will be more precise than those on the microcontrollers
  If we cannot, we will use the sleep time stored on the microcontrollers

  Can send : datas and ok_for_reconfiguration
  Can receive : ok and ok_with_configuration

  LoRa codes :
    - DATAS => 1 (To identify a message with datas inside)
    - OK => 2 (The main node and the server has well received the data + No reconfiguration)
    - OK_AND_CONF => 3 (The main node and the server has well received the data + With reconfiguration)
    - RECONF_OK => 4 (Reconfiguration on the end node is good => Tell the server that he can update its database)

  Payload codes :
    - 1 => The transaction with the server went well
    - 2 => The transaction with the server went wrong (cannot reach the server)

  Informations are sent by LoRa by using two elements:
    - "packet header" => sender address, recipient address, id of the message (sending datas or ok_reconf), message length and cycle identifier
    - body => message itself formatted into the JSON format for the server (contains datas got by the sensor + if it is the first launch + idMsg for the aggreagation)

  Infomations are received by LoRa using two elements:
    - "packet header" => sender address, recipient address, id of the message (ok or ok_with_reconf), message length, cycle identifier and if the payload is ok or not (server transaction)
    - body => message itself in JSON format to retrieve the datas (contains if we have a reconfiguration and the next time to sleep)

  SD codes :
    - true => The datas have well been added to the the datas.csv file on the microSD Card
    - false => Problem during the adding of the datas on the datas.csv file

  @version : 0.7
*/

// Librairies included
#include <SPI.h>
#include <LoRa.h>
#include <Wire.h>
#include <Arduino_JSON.h>
#include <SD_MMC.h> // SD Card uses some of the HSPI pins and the pin number 2
#include <FS.h>
#include <ModbusMaster.h> // To be able to retrieve the datas


// Define the pins used by the transceiver module
#define NSS 5
#define RST -1 // If used => 14 ; If not used => -1
#define DIO0 4


// Pins defined for software serial to get the informations from the JXBS-3001-TR sensor (via RS485)
#define RXD2 25 // RX microcontroller => TX sensor
#define TXD2 26 // TX microcontroller => RX sensor


// Define the messages ID for the acknowledgment messages
#define DATAS 1
#define OK 2
#define OK_AND_CONF 3
#define RECONF_OK 4


// Definition of paths for the microSD card
#define PATH_TO_FILE "/datas.csv" // Path to the CSV file that contains datas
#define PATH_TO_IDMSG "/idMsg.txt" // Path to the count idMsg
#define SDCARD "/sdcard" // Path to go on the sd card


// Define the variables to put the microcontroller in deep sleep mode
#define uS_TO_S_FACTOR 1000000ULL // To convert microseconds in seconds and use formatter unsigned long long to have the correct value when we put the timer
RTC_DATA_ATTR int sleep_time_length; // Size of the time_to_sleep array
RTC_DATA_ATTR int actual_sleep_time; // Actual index of time to sleep in the time_to_sleep array
RTC_DATA_ATTR int sleep_time[96]; // Max 96 sleep available => 1 by 1/4 hour
RTC_DATA_ATTR bool is_sleep_time_initialized = false; // To know if we have initialized the sleep time array
int next_sleep_time = 0; // If we have a transaction with the server => We use this value IN PRIORITY for sleeping


// Modbus object to be able to retrieve the informations from the sensor
ModbusMaster node;


// Addresses for sending
const byte local_address = ADDRESS_TO_MODIFY; // Device's address (00 => FF)
const byte destination = DESTINATION_TO_MODIFY; // Destination of the message


// Time for sending messages
long last_send_time = 0; // Last time we have sent a message
int interval = 0; // Interval between send of message => To avoid spam


// Informations for the first message sending
int nb_send_first_message = 0; // Number of first message (with the data we have sent)
bool response_first_message = false; // False if we don't have a response to the first message ; True otherwise


// Informations for the second message sending
bool response_second_message = false; // False if we don't have a response to the second message ; True otherwise
bool second_message_mandatory = false; // If we need to send an acknowledgment message
int nb_send_second_message = 0; // Number of second message sent


bool all_is_good = false; // If all_is_good == true => We can sleep
RTC_DATA_ATTR bool is_in_start_loop = true; // To know if it is the first message sent since first start of the board
int first_deep_sleep = ADD_FIRST_DEEP_SLEEP; // Deep sleep changing according to when you download the code => Variable use just when it is the first time you launch the microcontroller


// Variables for sensoring
float ph, humidity, temperature, conductivity, nitrogen, phosphorus, potassium;

// Actual idMsg
int actual_id_msg;


// Identifier of the cycle in progress to avoid messages of an old process
RTC_DATA_ATTR byte cycle_identifier = 0;


/**
   Set up of variables and modules
*/
void setup() {
  Serial.begin(115200); // Serial initialization
  while (!Serial);


  // Set the pins to use for chip select, reset and irq
  LoRa.setPins(NSS, RST, DIO0);


  // Antenna used => 433 MHz => 433E6
  if (!LoRa.begin(433E6)) {
    Serial.println("LoRa init failed. Check your connections.");
    while (true); // If problems do nothing
  }
  LoRa.setSpreadingFactor(10); // Set the spreading factor to 10
  Serial.println("LoRa init succeeded.");


  // Initialization of the transmission to get the informations from the JXBS-3001-TR
  pinMode(RXD2, INPUT);
  pinMode(TXD2, OUTPUT);

  // Init in receive mode
  digitalWrite(RXD2, 0);
  digitalWrite(TXD2, 0);

  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2); // Modbus communication runs at 9600 baud, Serial 8N1 (8 bit, no parity and 1 bit stop)
  node.begin(1, Serial2);// Modbus slave ID 1

  // Callbacks allow us to configure the RS485 transceiver correctly
  node.preTransmission(preTransmission);
  node.postTransmission(postTransmission);


  // Set up the sleep of the microcontroller if the array is not initialized
  if (!is_sleep_time_initialized) {
    Serial.println("Initialization of the sleep time tab");
    sleep_time_length = ADD_SLEEPTIME_LENGTH; // Set the length of the array to 1
    SLEEP_TIME_VALUES
    actual_sleep_time = 0; // Set the actual index in the sleep time tab to 0
    is_sleep_time_initialized = true; // Set the initialization to true

  } else { // If the sleep_time array has been set up

    // Not in start loop => Use the sleep time stored on the microcontroller
    // Because we have a first sleep time of X seconds to agree with the sleep time of the server
    if (is_in_start_loop) {
      is_in_start_loop = false; // It is not the first start anymore
    }

    actual_sleep_time++; // Increase the index of the actual_sleep_time
    actual_sleep_time = actual_sleep_time % sleep_time_length; // Make a cycle on the sleep_time array
  }


  printArray(sleep_time, sleep_time_length); // Print the array of sleep time

  // Get the actual idMsg to have in CSV or to pass to the server
  actual_id_msg = getIdMsgFromSD();
  Serial.println(actual_id_msg);
}


/**
   Main program
*/
void loop() {
  // FIRST MESSAGE WITH THE DATAS
  if (millis() - last_send_time > interval && response_first_message != true) { // Wait interval for sending a packet => To not spam the receiver

    // Check if we have sent 5 times the packet without return
    if (nb_send_first_message == 5) {
      String datas_to_store;

      // If we launch the ESP32 => Take the first deep sleep to agree with deep sleep on the server ; Else use the classical one
      if (is_in_start_loop) datas_to_store = ((String(local_address, HEX).length() == 1) ? ("0" + String(local_address, HEX)) : String(local_address, HEX)) + ";" + -1 + ";" + String(ph) + ";" + String(humidity) + ";" + String(temperature) + ";" + String(conductivity) + ";" + String(nitrogen) + ";" + String(phosphorus) + ";" + String(potassium) + ";" + first_deep_sleep + ";" + String(actual_id_msg);
      else datas_to_store = ((String(local_address, HEX).length() == 1) ? ("0" + String(local_address, HEX)) : String(local_address, HEX)) + ";" + -1 + ";" + String(ph) + ";" + String(humidity) + ";" + String(temperature) + ";" + String(conductivity) + ";" + String(nitrogen) + ";" + String(phosphorus) + ";" + String(potassium) + ";" + sleep_time[actual_sleep_time] + ";" + String(actual_id_msg);

      if (!addInSD(datas_to_store)) Serial.println("PROBLEM : Cannot add datas in SD card"); // Add the message to the SD Card

      goSleep(false); // Put the module in sleep mode => Too more sending => false because no server transaction
    }


    // Get sensor values
    ph = getPH(); // Get the PH value
    if (ph == 0 || ph == -1) ph = getPH(); // When the microcontroller is not sleeping anymore => The first request to the sensor gives us a time out exception => Re-get the PH value
    humidity = getHumidity(); // Get the humidity in percentage
    temperature = getTemperature(); // Get the temperature in celsius
    conductivity = getConductivity(); // Get the conductivity in us/cm
    nitrogen = getNitrogen(); // Get the nitrogen in mg/kg
    phosphorus = getPhosphorus(); // Get the phosphorus in mg/kg
    potassium = getPotassium(); // Get the potassium in mg/kg


    // Test the sensor values => If all the values equal to -1 => Problem
    if (ph == -1 && temperature == -1 && conductivity == -1 && nitrogen == -1 && phosphorus == -1 && potassium == -1) {
      Serial.println("Problem on the values we get => Go sleep");
      goSleep(false); // Put the module in sleep mode => Cannot get the datas
    }

    // DATAS message
    String message = toJSON(ph, humidity, temperature, conductivity, nitrogen, phosphorus, potassium, actual_id_msg, 1); // Formate the datas for the sending
    sendMessage(message, destination, local_address, DATAS, 1, cycle_identifier); // Send the message
    Serial.println("Sending " + message);


    last_send_time = millis(); // Get when we have sent the message
    interval = random(5000, 6001); // Change the interval of 5-6 seconds to be able to wait the entire communication with the relay
  }


  // SECOND MESSAGE FOR RECONFIGURATION
  if (second_message_mandatory && millis() - last_send_time > interval) { // Wait interval for sending a packet => To not spam the receiver

    // Check if we have sent 5 times the same packet without return
    if (nb_send_second_message == 5) {
      goSleep(true); // Put the module in sleep mode => Too more sending => true because we are on the second message (so first transaction get the next_sleep_time value)
    }


    // RECONF_OK message
    Serial.println("Send second message");
    String message = toJSON(0, 0, 0, 0, 0, 0, 0, -1, 2); // Formate the message to say that the reconf is OK
    sendMessage(message, destination, local_address, RECONF_OK, 2, cycle_identifier); // Send an acknowledgment message for the good reconfiguration


    last_send_time = millis(); // Get when we have sent the message
    interval = random(5000, 6001); // Change the interval of 5-6 seconds to be able to wait the entire communication with the relay
  }


  onReceive(LoRa.parsePacket()); // Parse for a packet, and call onReceive when we have received a packet
}



/**
   Function to send a message to a recipient
   @param message => Message to send (JSON format)
   @param destination => Destination of the message
   @param local_address => Address of the current device
   @param messageID => ID of the message for the future processing
   @param first_or_second_message => To know if it is the DATAS message of the ack message concerning the RECONF
   @param cycle_identifier => Identifier of the cycle of the node
*/
void sendMessage(String message, byte destination, byte local_address, int messageID, int first_or_second_message, byte cycle_identifier) {
  LoRa.beginPacket(); // Create the packet to send
  LoRa.write(destination); // Add destination address
  LoRa.write(local_address); // Add sender address
  LoRa.write(messageID); // Add message ID
  LoRa.write(message.length()); // Add the length of the message
  LoRa.write(cycle_identifier); // Add the cycle identifier
  LoRa.print(message); // Add the message to send
  LoRa.endPacket(); // End and send the packet


  // To know what type of message we have sent
  if (first_or_second_message == 1) {
    nb_send_first_message++; // Increment the number of first message sent => To stop sending when we have sent 5 messages without responses
  } else {
    nb_send_second_message++; // Increment the number of second message sent => To stop sending when we have sent 5 messages without responses
  }
}

/**
   Function used to get the id of the actual message from the sd card + We update the ID that is present in the file
   @return -1 if we cannot access to the SD card ; 0 if we have no file to read and the actual id if we can read it
*/
int getIdMsgFromSD() {
  // Check the connection to the SD card
  if (!SD_MMC.begin(SDCARD, true)) { // Uses the pins number : 15, 14 and 2
    Serial.println("Cannot connect to the SD card");
    return -1;
  }


  File file = SD_MMC.open(PATH_TO_IDMSG); // Open the file idMsg file in read mode

  // If the file doesn't exist
  if (!file) {
    Serial.println("Failed to open file => Creation of a new one with id 0");
    file.close(); // Close the file in read mode

    // Open it in write mode and put 0 as id
    file = SD_MMC.open(PATH_TO_IDMSG, FILE_WRITE); // Open the file in write mode
    file.println(1); // Write 1 for the next sending
    return 0; // Return the actual idMSg that is 0 when no file
  }


  // Variables to read the file
  char line[6]; // Array of char
  int i = 0; // Counter for the array of char
  String line_read = ""; // String to store the value

  // Read the file
  while (file.available() && i < 6) {
    char c = file.read(); // Read the next char
    line_read += String(c); // And put it in the string
  }

  file.close(); // Close the file that is in read mode

  int id_msg_to_return = line_read.toInt(); // Change the string value in int => Value to return
  int id_msg = id_msg_to_return + 1; // Add one to store this value in the file

  file = SD_MMC.open(PATH_TO_IDMSG, FILE_WRITE); // Open it in write mode
  file.println(id_msg); // Write the new idMsg for the next sending

  file.close(); // Close the file after updated the id
  SD_MMC.end(); // Close the library

  return id_msg_to_return; // Return the actual idMsg
}


/**
   Function used to formate the message to send into the JSON format
   It can formate message for sending datas, to say that the reconfiguration is good or to say that we have just been launched (mandatory to have the configuration)
   Normally, the formatting of the datas corresponds to what the server expects
   @param ph => PH picked by the sensor
   @param humidity => Humidity picked by the sensor
   @param temperature => Temperature picked by the sensor
   @param conductivity => Conductivity picked up by the sensor
   @param nitrogen => Nitrogen picked up by the sensor
   @param phosphorus => Phosphorus picked up by the sensor
   @param potassium => Potassium picked up by the sensor
   @param id_msg => ID of the message to send
   @param first_or_second_message => To identify the message to send
   @return => datas to send to the server OR the addr to say that the reconfiguration is good
*/
String toJSON(float ph, float humidity, float temperature, float conductivity, float nitrogen, float phosphorus, float potassium, int id_msg, int first_or_second_message) {
  if (first_or_second_message == 1) { // Message with datas

    // If it is the first time we launch the microcontroller => Get the configuration to be up to date ; Else => Basic message
    if (is_in_start_loop) return "{\"AM\":\"" + ((String(local_address, HEX).length() == 1) ? ("0" + String(local_address, HEX)) : String(local_address, HEX)) + "\",\"FS\":" + String(is_in_start_loop) + ",\"ph\":" + String(ph) + ",\"H\":" + String(humidity) + ",\"T\":" + String(temperature) + ",\"C\":" + String(conductivity) + ",\"N\":" + String(nitrogen) + ",\"P\":" + String(phosphorus) + ",\"K\":" + String(potassium) + ",\"id\":" + String(id_msg) + "}";
    else return "{\"AM\":\"" + ((String(local_address, HEX).length() == 1) ? ("0" + String(local_address, HEX)) : String(local_address, HEX)) + "\",\"ph\":" + String(ph) + ",\"H\":" + String(humidity) + ",\"T\":" + String(temperature) + ",\"C\":" + String(conductivity) + ",\"N\":" + String(nitrogen) + ",\"P\":" + String(phosphorus) + ",\"K\":" + String(potassium) + ",\"id\":" + String(id_msg) + "}";

  } else { // Message to say that the reconfiguration is ok
    return "{\"A\":\"" + ((String(local_address, HEX).length() == 1) ? ("0" + String(local_address, HEX)) : String(local_address, HEX)) + "\"}";
  }
}


/**
   Function called when we receive a packet on the node
   @param packet_size => To know if a packet is arrived or not (0 if no packet ; something else if a packet is arrived)
*/
void onReceive(int packet_size) {
  if (packet_size == 0) return; // If there's no packet, quit the function

  // Read packet header bytes
  int recipient = LoRa.read(); // Recipient address
  byte sender = LoRa.read(); // Sender address
  byte incoming_msg_id = LoRa.read(); // Message id
  byte incoming_length = LoRa.read(); // Message length
  byte cycle_identifier_sent = LoRa.read(); // Cycle identifier
  byte is_payload_ok = LoRa.read(); // Get the informations about the transaction with the server


  // If the message is not for me
  if (recipient != local_address && recipient != ADDRESS_TO_MODIFY) {
    Serial.println("This message is not for me.");
    return; // Quit the function
  }


  // If the cycle identifier is different of mine => Old process => Don't process it
  if (cycle_identifier_sent != cycle_identifier) {
    Serial.println("This message comes from an old process");
    return; // Quit the function
  }


  // If the message is for me and if the transaction with the server is good
  if (is_payload_ok == 1) {
    Serial.println("The transaction is good, we can receive the payload");


    // Read the payload sent
    String payload = "";
    while (LoRa.available()) {
      payload += (char)LoRa.read();
    }


    // Check the length of the payload
    if (incoming_length != payload.length()) {
      Serial.println("Error: payload length does not match length");
      return; // Quit the function if there are errors
    }


    // If message is for this device, print details of the message
    Serial.println("Received from: 0x" + String(sender, HEX));
    Serial.println("Sent to: 0x" + String(recipient, HEX));
    Serial.println("Message length: " + String(incoming_length));
    Serial.println("Message ID: " + String(incoming_msg_id));
    Serial.println("Cycle identifier: " + String(cycle_identifier_sent));
    Serial.println("Is payload OK : " + String(is_payload_ok));
    Serial.println("Payload: " + payload);
    Serial.println("RSSI: " + String(LoRa.packetRssi()));
    Serial.println("Snr: " + String(LoRa.packetSnr()));
    Serial.println();


    // Declaration and transform the payload into JSON to get the datas sent
    JSONVar response_infos = JSON.parse(payload);;


    // Check if the decoding is good or not => Not print message and quit the function
    if (JSON.typeof(response_infos) == "undefined") {
      Serial.println("Parse of the response of the server failed");
      return; // Quit the function
    }


    // Response to the first message => Store datas in the SD card
    if (!response_first_message) {
      char sleep_to_record [10]; // Buffer to store the sleep time we want to store into the SD card (Max: 86400 s => 5 digits)
      itoa (response_infos["NST"], sleep_to_record, 10); // Transform the number returned by the server into a char buffer
      String datas_to_store = ((String(local_address, HEX).length() == 1) ? ("0" + String(local_address, HEX)) : String(local_address, HEX)) + ";" + ((is_payload_ok == 1) ? 1 : -1) + ";" + String(ph) + ";" + String(humidity) + ";" + String(temperature) + ";" + String(conductivity) + ";" + String(nitrogen) + ";" + String(phosphorus) + ";" + String(potassium) + ";" + String(sleep_to_record) + ";" + String(actual_id_msg);

      if (!addInSD(datas_to_store)) Serial.println("PROBLEM : Cannot add datas in SD card"); // Add the datas in the SD card
    }


    // Have our first message response => Don't need to store the datas once again
    response_first_message = true;


    // Check the value of the message ID and run the appropriate structure
    switch (incoming_msg_id) {

      // For a classical acknowledgment message => Do nothing
      case OK:
        Serial.println("ID message : OK");
        all_is_good = true;

        // If we have the value nextSleepTime in the JSON sent => Put it in the next_sleep_time variable for next sleeping
        // Make a check because nextSleeptime is not always in the payload we can have
        if (response_infos.hasOwnProperty("NST")) {
          next_sleep_time = response_infos["NST"]; // Get the value sent by the server
          Serial.print("RESET SLEEP TIME TO : ");
          Serial.println(next_sleep_time);
        }

        break;


      // For an acknowledgment message with a reconfiguration of time to sleep
      case OK_AND_CONF:
        second_message_mandatory = true; // The ok for reconfiguration message is mandatory to be sent to the main node
        interval = 0; // Put the interval to 0 to send directly the ok reconfiguration message
        Serial.println("ID message : OK + CONFIG");
        Serial.print("Response of the server : ");
        Serial.println(response_infos);


        // Check if the configuration of the node has changed during the deep sleep
        if (response_infos["HCC"]) {
          Serial.println("Need to change the configuration");


          // Re-initialize the actual_sleep_time
          actual_sleep_time = -1;


          // If we have an automatic reconfiguration from the server
          if(response_infos.hasOwnProperty("NBV") && response_infos.hasOwnProperty("V")) {
            
            // Re-attribute the good values to the sleep_time values
            sleep_time_length = response_infos["NBV"];
  
  
            // Get the new values concerning the sleep_time of the microcontroller
            for (int i = 0; i < 96 ; i++) {
              if (i < sleep_time_length) {
                sleep_time[i] = response_infos["V"];
  
              } else {
                sleep_time[i] = 0;
              }
            }


          // If it's a manual one
          } else {
            
            // Re-attribute the good values to the sleep_time values
            sleep_time_length = response_infos["ST"].length();
  
  
            // Get the new values concerning the sleep_time of the microcontroller
            for (int i = 0; i < 96 ; i++) {
              if (i < sleep_time_length) {
                sleep_time[i] = response_infos["ST"][i];
  
              } else {
                sleep_time[i] = 0;
              }
            }
          }


          // Store the time to next sleep sent by the server
          next_sleep_time = response_infos["NST"];
          Serial.print("RESET SLEEP TIME TO : ");
          Serial.println(next_sleep_time);


          printArray(sleep_time, sleep_time_length); // Print the sleep_time array
        }

        break;

    }

  } else { // If the transaction is not good (value 2 => Negative values cannot work because just byte can be sent with LoRa.write => Cannot use -1)
    Serial.print("The transaction is not good, we retry to send datas : ");
    if (!second_message_mandatory) Serial.print(5 - nb_send_first_message); // Counter for first message
    else Serial.print(5 - nb_send_second_message); // Counter for second message
    Serial.println(" times");
  }


  // If all_is_good, we can sleep (all the tasks are done)
  if (all_is_good) {
    goSleep(true);
  }
}


/**
   Function to add datas on the SD Card
   Datas are stored like that : local_address ; is transaction with server OK ; ph ; humidity ; temperature ; conductivity ; nitrogen ; phosphorus ; potassium ; sleepTime (sent by the server or stored locally) ; idMsg
   @param informations_to_store => Informations to add to the CSV file on the microcontroller
   @return true => connection with the SD card is good and the file is added ; false otherwise (for connection or writing into the file)
*/
bool addInSD(String informations_to_store) {
  // SD card initialization
  if (!SD_MMC.begin(SDCARD, true)) { // Uses the pins number : 15, 14 and 2
    Serial.println("Cannot connect to the SD card");
    return false;
  }


  // Test if no problem during the appending of the datas
  if (!appendFile(SD_MMC, PATH_TO_FILE, informations_to_store.c_str())) {
    SD_MMC.end();
    return false;
  }

  SD_MMC.end(); // Close the library
  return true;
}


/**
   Function used to append datas to a file
   If the file is already created we append datas to it
   If not the file is created and we append datas to it
   @param fs => FileSystem => Here we put the path to the SD card
   @param path => path to the file
   @param message => Message to put into the file
   @return true => message has well been appended to the file ; false otherwise
*/
bool appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);
  File file = fs.open(path, FILE_APPEND); // Open the file if exists OR create it if not exists


  // File not open
  if (!file) {
    Serial.println("Failed to open file for appending");
    return false;
  }


  // Print data inside the file
  if (file.println(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }

  file.close(); // Close the file after added the message

  return true;
}


/**
   Function used to put the modules and the microcontroller in deep sleep mode
   Choose the deep sleep mode according to the response of the server and if we are in launching or not
   We also change the cycle identifier for the next cycle (increment and modulo)
   @param response_server => True if we have a response from the server ; false otherwise
*/
void goSleep(bool response_server) {
  LoRa.sleep(); // Sleep the LoRa module


  // Increment the cycle identifier % 256 => byte
  cycle_identifier = (cycle_identifier + 1) % 256;


  // Choose of the right deep sleep value
  if (response_server) { // We have a response of the server
    Serial.print("SLEEP TIME of (response server) : ");
    Serial.println(next_sleep_time);

    esp_sleep_enable_timer_wakeup(next_sleep_time * uS_TO_S_FACTOR); // Use the sleep time value from the server
    esp_deep_sleep_start(); // Can't put code after that because it will not be executed

  } else { // No response of the server

    // Check if we are in launching of the microcontroller
    if (is_in_start_loop) { // Use the deep sleep by default to agree with the server
      Serial.print("SLEEP TIME of (first sleep time local) : ");
      Serial.println(first_deep_sleep);

      esp_sleep_enable_timer_wakeup(first_deep_sleep * uS_TO_S_FACTOR); // Use the first sleep time value of the microcontroller
      esp_deep_sleep_start(); // Can't put code after that because it will not be executed

    } else { // Use the good deep sleep value stored on the microcontroller
      Serial.print("SLEEP TIME of (sleep time local with actual_sleep_time) : ");
      Serial.println(sleep_time[actual_sleep_time]);

      esp_sleep_enable_timer_wakeup(sleep_time[actual_sleep_time] * uS_TO_S_FACTOR); // Use the sleep time value stored in the array on the microcontroller
      esp_deep_sleep_start(); // Can't put code after that because it will not be executed
    }
  }
}


/**
   Put the pins ON for the transmission
*/
void preTransmission()
{
  digitalWrite(RXD2, 1);
  digitalWrite(TXD2, 1);
}


/**
   Put the pins OFF after the transmission
*/
void postTransmission()
{
  digitalWrite(RXD2, 0);
  digitalWrite(TXD2, 0);
}


/**
   Function used to get the PH value from the JXBS-3001-TR SENSOR with the RS485
   @returns the PH value of the soil or -1 if error
*/
float getPH() {
  uint8_t result;

  result = node.readHoldingRegisters(0x0006, 1); // Read the PH register
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0x00) / 100.0f; // Divide by 100 to obtain the good value

  } else {
    return -1;
  }
}


/**
   Function used to get the humidity percentage from the JXBS-3001-TR SENSOR with the RS485
   @returns the humidity percentage of the soil or -1 if error
*/
float getHumidity() {
  uint8_t result;

  result = node.readHoldingRegisters(0x0012, 1); // Read the humidity register
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0x00) / 10.0f; // Divide by 10 to obtain the good value

  } else {
    return -1;
  }
}


/**
   Function used to get the temperature in celsius degree from the JXBS-3001-TR SENSOR with the RS485
   @returns the celsius degree of the soil or -1 if error
*/
float getTemperature() {
  uint8_t result;

  result = node.readHoldingRegisters(0x0013, 1); // Read the temperature register
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0x00) / 10.0f; // Divide by 10 to obtain the good value

  } else {
    return -1;
  }
}


/**
   Function used to get the conductivity from the JXBS-3001-TR SENSOR with the RS485
   @returns the conductivity of the soil or -1 if error
*/
float getConductivity() {
  uint8_t result;

  result = node.readHoldingRegisters(0x0015, 1); // Read the conductivity register
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0x00); // No divison => Already the good value

  } else {
    return -1;
  }
}


/**
   Function used to get the nitrogen content from the JXBS-3001-TR SENSOR with the RS485
   @returns the nitrogen content of the soil or -1 if error
*/
float getNitrogen() {
  uint8_t result;

  result = node.readHoldingRegisters(0x001E, 1); // Read the nitrogen register
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0x00); // No divison => Already the good value

  } else {
    return -1;
  }
}


/**
   Function used to get the phosphorus content from the JXBS-3001-TR SENSOR with the RS485
   @returns the phosphorus content of the soil or -1 if error
*/
float getPhosphorus() {
  uint8_t result;

  result = node.readHoldingRegisters(0x001F, 1); // Read the phosphorus register
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0x00); // No divison => Already the good value

  } else {
    return -1;
  }
}


/**
   Function used to get the potassium content from the JXBS-3001-TR SENSOR with the RS485
   @returns the potassium content of the soil or -1 if error
*/
float getPotassium() {
  uint8_t result;

  result = node.readHoldingRegisters(0x0020, 1); // Read the potassium register
  if (result == node.ku8MBSuccess) {
    return node.getResponseBuffer(0x00); // No divison => Already the good value

  } else {
    return -1;
  }
}


/**
   Function used to print the array on the Serial to test if all is good or not
   @param my_array => Array to print
   @param my_array_length => Length of my_array
*/
void printArray(int my_array[], int my_array_length) {
  Serial.println("");
  for (int i = 0 ; i < my_array_length ; i++) {
    Serial.print(my_array[i]);
    Serial.print(", ");
  }
  Serial.println("");
}