/*
  receiver.ino file using Arduino_LoRa library

  Containing all the code to receive datas picked up by the sensor and send them to the server via HTTP requests
  Send data via LoRa to the end nodes (reconfiguration, ok messages)
  This microcontroller always listen for LoRa packet and LoRa module is restarted all the 5 minutes to avoid automatic idle

  Can send : ok and ok_with_configuration
  Can receive : datas and ok_for_reconfiguration
  Can send HTTP POST and PUT requests and receive the response from the server

  LoRa codes :
    - DATAS => 1 (To identify a message with datas inside)
    - OK => 2 (The main node and the server has well received the data + No reconfiguration)
    - OK_AND_CONF => 3 (The main node and the server has well received the data + With reconfiguration)
    - RECONF_OK => 4 (Reconfiguration on the end node is good => Tell the server that he can update its database)

  Payload codes :
    - 1 => The transaction with the server went well
    - 2 => The transaction with the server went wrong (cannot reach the server)

  Informations are sent by LoRa by using two elements:
    - "packet header" => sender address, recipient address, id of the message (ok or ok_with_reconf), message length, cycle identifier and if the payload is ok or not (server transaction)
    - body => message itself in JSON format to retrieve the datas

  Infomations are received by LoRa using two elements:
    - "packet header" => sender address, recipient address, id of the message (sending datas or ok_reconf), message length and cycle identifier
    - body => message itself formatted into the JSON format for the server

  @version : 0.7
*/

// Librairies included
#include <SPI.h>
#include <LoRa.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>


// Define the pins used by the transceiver module
#define NSS 5
#define RST -1 // If used => 14 ; If not used => -1
#define DIO0 4


// Define the messages ID for the acknowledgment messages
#define DATAS 1
#define OK 2
#define OK_AND_CONF 3
#define RECONFIG_OK 4


// WiFi identification
// #define SSID "AND"
// #define PASSWORD "@nd1sH3re" // Si pas H alors h

#define SSID "SFR-e068"
#define PASSWORD "DXGZZD3KIM1J"


// Server identification part (URL, PORT, complete addr)
#define URL "192.168.0.18"
#define PORT 3000
#define SERVER_ADDR "http://" + String(URL) + ":" + String(PORT)
#define ROUTE_MICROCONTROLLER "/api/microcontroller"
#define ROUTE_DATAS "/api/datas"
#define APIKEY APIKEY_TO_MODIFY


// Adress
const byte local_address = ADDRESS_TO_MODIFY; // Device


// Restart LoRa library => To avoid freeze problems (precisely automatic idle problems)
long last_update_time = 0; // Last time we restart the LoRa module
int interval = 300000; // Interval to restart the LoRa module (5 minutes)
bool is_in_conversation = false; // To know if we are in conversation or not


/**
   Set up the variables and the modules
*/
void setup() {
  Serial.begin(115200); // Serial initialization
  while (!Serial);


  // Set the pins to use for chip select, reset and irq
  LoRa.setPins(NSS, RST, DIO0);


  // Antenna used => 433 MHz => 433E6
  if (!LoRa.begin(433E6)) {
    Serial.println("LoRa init failed. Check your connections.");
    while (true); // If problems => Do nothing
  }

  LoRa.setSpreadingFactor(10); // Set the spreading factor to 10
  Serial.println("LoRa init succeeded.");


  // Connection to the WiFi with the credentials put above
  WiFi.begin(SSID, PASSWORD);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }


  // Connection Ok and get local ip address
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}


/**
   Main program
*/
void loop() {
  // Parse for a packet, and call onReceive when we have receive a packet
  onReceive(LoRa.parsePacket());


  // Restart the LoRa module to avoid automatic idle mode
  if (millis() - last_update_time > interval && is_in_conversation != true) {
    Serial.println();
    Serial.println("Restart the LoRa module");


    // LoRa connection reset
    LoRa.end(); // Stop the module
    if (!LoRa.begin(433E6)) { // Restart the module with the good antenna frequency
      Serial.println("LoRa init failed. Check your connections.");
      while (true); // If problems => Do nothing
    }

    LoRa.setSpreadingFactor(10); // Set the spreading factor to 10
    Serial.println("LoRa module restart is ok");
    Serial.println();


    // WiFi connection reset
    Serial.println("Restart the WiFi");
    WiFi.disconnect(); // Disconnect the WiFi 
    WiFi.begin(SSID, PASSWORD); // Re-connection to the WiFi
    Serial.println("Re-connection to the WiFi");
    while (WiFi.status() != WL_CONNECTED) { // Verify the status
      delay(500);
      Serial.print(".");
    }
    Serial.println("The WiFi has well restarted");
    Serial.println();

    last_update_time = millis(); // Reupdate the time
  }
}


/**
   Send a message to the sender
   @param message_to_send => Message to send to the sender (contains nothing if it's OK ; otherwise it contains the reconfiguration in seconds)
   @param sender => Destination address
   @param recipient => Sender address
   @param id_message => Message ID to send
   @param cycle_identifier => Identifier of the cycle of the end node
*/
void sendMessage(String message_to_send, byte sender, byte recipient, int id_message, byte cycle_identifier) {
  LoRa.beginPacket(); // Create the packet to send
  LoRa.write(sender);  // Add destination address
  LoRa.write(recipient); // Add sender address
  LoRa.write(id_message); // Add message ID
  LoRa.write(message_to_send.length()); // Add the length of the message
  LoRa.write(cycle_identifier); // Add the cycle_identifier of the end node


  if(message_to_send == "-1"){ // Problems with server 
    LoRa.write(2); // is_payload_ok fixed to 2 because there are problems with the server (-1 doesn't work because we can juste send bytes with LoRa.write)
    
  } else { // No problem with the server
    LoRa.write(1); // is_payload_ok fixed to 1
  }

  
  LoRa.print(message_to_send); // Add the message to send
  LoRa.endPacket(); // End and send the packet
}


/**
   Function called when we receive a packet
   And we analyse the message id to know what we need to do
   @param packet_size => To know if a packet is arrived or not (0 if no packet)
*/
void onReceive(int packet_size) {
  if (packet_size == 0) return; // If there's no packet, quit the function


  // Read packet header bytes
  int recipient = LoRa.read(); // Recipient address
  byte sender = LoRa.read(); // Sender address
  byte incoming_msg_id = LoRa.read(); // Message id
  byte incoming_length = LoRa.read(); // Message length
  byte cycle_identifier = LoRa.read(); // Cycle_identifer of end node


  // Read the message
  String incoming = "";
  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }


  // If the recipient isn't this device or broadcast
  if (recipient != local_address && recipient != ADDRESS_TO_MODIFY) {
    Serial.println("This message is not for me.");
    return; // Quit the function
  }


  // Check the length if there is errors or not
  if (incoming_length != incoming.length()) {
    Serial.println("error: message length does not match length");
    return; // Quit the function if there are errors
  }


  // Get RSSI, SNR and Frequency error
  int rssi = LoRa.packetRssi();
  float snr = LoRa.packetSnr();
  long frequency_error = LoRa.packetFrequencyError();


  // If message is for this device, print details of the message
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incoming_msg_id));
  Serial.println("Message length: " + String(incoming_length));
  Serial.println("Message: " + incoming);
  Serial.println("RSSI: " + String(rssi));
  Serial.println("Snr: " + String(snr));
  Serial.println("Freq err: " + String(frequency_error));
  Serial.println();


  is_in_conversation = true; // The microcontroller can receive messages => Don't restart the LoRa mmodule for now

  String payload; // Response of the server (in string format)
  JSONVar response_infos; // Response of the server formated in JSON
  
  
  // Identify the message sent
  switch (incoming_msg_id) {
    case DATAS:
      Serial.println("SEND DATA TO THE SERVER");
      
      payload = httpRequest(SERVER_ADDR, ROUTE_DATAS, incoming, "POST"); // Request to send datas to the server
      Serial.println(payload);
      
      response_infos = JSON.parse(payload); // Parse the payload to know if we have a reconfiguration or not


      // Test the parsing
      if (JSON.typeof(response_infos) == "undefined") {
        Serial.println("Problem during the parsing of the payload sent by the server"); // Problem

      } else {

        // No reconfiguration => Send OK message to the sender
        if((bool)response_infos["HCC"] == false) {
          Serial.println("Send OK message");
          sendMessage(payload, sender, recipient, OK, cycle_identifier); // Send an acknowledgment message to the sender
          is_in_conversation = false; // The microcontroller is not in a conversation => Possibility to restart the LoRa module


          // Reconfiguration
        } else {
          Serial.println("Send a reconfiguration message");
          sendMessage(payload, sender, recipient, OK_AND_CONF, cycle_identifier); // Send the reconfiguration message
        }
      }

      break;


    case RECONFIG_OK:
      Serial.println("Send server reconf is OK");
      
      payload = httpRequest(SERVER_ADDR, ROUTE_MICROCONTROLLER, incoming, "PUT"); // Request to say that the microcontroller has well received his configuration
      Serial.println(payload);
      
      sendMessage(payload, sender, recipient, OK, cycle_identifier); // Send an OK message to the sender
      is_in_conversation = false; // The microcontroller is not in a conversation => Possibility to restart the LoRa module
      
      break;
  }
}


/**
   Function used to send an HTTP request
   @param server => address of the server (Example : http://localhost:12345)
   @param route => Route to have access on the API (Example : /my_api/test)
   @param json => JSON which containing all the datas for the server
   @param type_of_request => Can be GET, POST or PUT
   @return => JSON if all is good ; -1 otherwise
*/
String httpRequest(String server, String route, String json, String type_of_request) {
  // Verify the WiFi connection to be able to well transmit the datas
  if(WiFi.status() != WL_CONNECTED) {
    WiFi.disconnect(); // Disconnect the WiFi 
    WiFi.begin(SSID, PASSWORD); // Reconnect the WiFi
  }

  
  HTTPClient http; // Object to make the HTTP request

  http.begin(server + route); // Begin the HTTP request with the address of the server given
  http.addHeader("Content-Type", "application/json"); // HTTP request with JSON
  http.addHeader("apikey", APIKEY); // Add the API key of the application
  
  int http_response_code; // Response code of the HTTP request


  // According to the type of request passed in parameter
  if (type_of_request.equals("GET")) {
    http_response_code = http.GET();

  } else if (type_of_request.equals("POST")) {
    http_response_code = http.POST(json);

  } else if (type_of_request.equals("PUT")) {
    http_response_code = http.PUT(json);

  } else {
    http_response_code = -1;
  }


  // If we have problems to send datas (Server unreachable) => Response code negative
  // Good response code => 200 (update of microcontroller) or 201 (add datas)
  if (http_response_code > 0 && (http_response_code == 201 || http_response_code == 200)) {
    Serial.print("HTTP response code : ");
    Serial.println(http_response_code);

    return http.getString();

  } else {
    // Print error code
    Serial.print("Error code : ");
    Serial.println(http_response_code);

    return "-1";
  }


  // Free resources
  http.end();
}