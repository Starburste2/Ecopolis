const User = require("../models/User"); // Import the User model


/**
 * Function used to be able to check if the account of the user is activated or not
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns an error if we have one ; nothing otherwise
 */
const requireAccountActivated = (req, res, next) => {
    var session_ID = req.headers.authorization; // Get directly the session ID without tests because they have been done before

    // Find the user associated to the session ID passed
    User.findOne({ sessionID: session_ID })
    .then(user => {
        // If the user is not activated => Cannot access to the app
        if(!user.isActivated) return res.status(403).json({ message: 'Account not activated' });


        // Someone is connected
        else {
            next(); // Go to the next function
        }
    })
    .catch(error => {
        res.status(400).json(error) // Return an error if we have one
    })
}

// Exports the function
exports.requireAccountActivated = requireAccountActivated