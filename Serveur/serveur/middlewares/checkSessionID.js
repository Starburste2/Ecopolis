const User = require("../models/User"); // Import the User model


/**
 * Function to validate the session ID sent by the client
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns the response if we have an error (to quit the function) ; pass to the next function otherwise
 */
const requireValidSessionID = (req, res, next) => {
    // Check if we have a session ID and if it is not null
    if(req.headers.authorization == undefined || req.headers.authorization == "") return res.status(403).json({ message: 'Not connected' })
    

    // Find the user associated to the session ID passed
    User.findOne({ sessionID: req.headers.authorization })
    .then(user => {
        // If the user is null => No one is connected
        if(user == null) return res.status(403).json({ message: 'Not connected' })


        // Someone is connected
        else {
            next(); // Go to the next function
        }
    })
    .catch(error => {
        res.status(400).json(error) // Return an error if we have one
    })
}

// Exports the function
exports.requireValidSessionID = requireValidSessionID