const APIKey = require("../models/APIKey"); // Import the APIKey model
const uuidAPIKey = require('uuid-apikey'); // Import the package to create the API Keys


/**
 * Function used to be able to verify if the API Key is the one used by the VueJS main application
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns an error if we have one ; nothing otherwise
 */
const requireValidAPIForVueJSMainApp = (req, res, next) => {
    // Check if we have the API Key in the header
    if(req.headers.apikey == undefined || req.headers.apikey == "" || !uuidAPIKey.isAPIKey(req.headers.apikey))
        return res.status(403).json({ message: 'APIKey incorrect' })
    

    // Find the user associated to the session ID passed
    APIKey.findOne({ key: req.headers.apikey, UUID: uuidAPIKey.toUUID(req.headers.apikey) })
    .then(key => {
        // If the key is null => API Key incorrect
        if(key == null) return res.status(403).json({ message: 'APIKey incorrect' })


        // Test if the API Key is linked to the Vue application
        else {
            if(key.applicationName == 'VueAppMain') next(); // Go to the next function
            else return res.status(403).json({ message: 'APIKey incorrect' })
        }
    })
    .catch(error => {
        res.status(400).json(error) // Return an error if we have one
    })
}


/**
 * Function used to be able to verify if the API Key is the one used by the VueJS showcase application
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns an error if we have one ; nothing otherwise
 */
const requireValidAPIForVueJSShowcaseApp = (req, res, next) => {
    // Check if we have the API Key in the header
    if(req.headers.apikey == undefined || req.headers.apikey == "" || !uuidAPIKey.isAPIKey(req.headers.apikey))
        return res.status(403).json({ message: 'APIKey incorrect' })
    

    // Find the user associated to the session ID passed
    APIKey.findOne({ key: req.headers.apikey, UUID: uuidAPIKey.toUUID(req.headers.apikey) })
    .then(key => {
        // If the key is null => API Key incorrect
        if(key == null) return res.status(403).json({ message: 'APIKey incorrect' })


        // Test if the API Key is linked to the Vue application
        else {
            if(key.applicationName == 'VueAppShowcase') next(); // Go to the next function
            else return res.status(403).json({ message: 'APIKey incorrect' })
        }
    })
    .catch(error => {
        res.status(400).json(error) // Return an error if we have one
    })
}


/**
 * Function used to be able to verify if the API Key is the one used by the microcontrollers application
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns an error if we have one ; nothing otherwise
 */
const requireValidAPIForMicrocontrollers = (req, res, next) => {
    // Check if we have the API Key in the header
    if(req.headers.apikey == undefined || req.headers.apikey == "" || !uuidAPIKey.isAPIKey(req.headers.apikey))
        return res.status(403).json({ message: 'APIKey incorrect' })
    

    // Find the user associated to the session ID passed
    APIKey.findOne({ key: req.headers.apikey, UUID: uuidAPIKey.toUUID(req.headers.apikey) })
    .then(key => {
        // If the key is null => API Key incorrect
        if(key == null) return res.status(403).json({ message: 'APIKey incorrect' })


        // Test if the API Key is linked to the microcontrollers application
        else {
            if(key.applicationName == 'Microcontrollers') next(); // Go to the next function
            else return res.status(403).json({ message: 'APIKey incorrect' })
        }
    })
    .catch(error => {
        res.status(400).json(error) // Return an error if we have one
    })
}

// Exports the function
exports.requireValidAPIForVueJSMainApp = requireValidAPIForVueJSMainApp
exports.requireValidAPIForMicrocontrollers = requireValidAPIForMicrocontrollers
exports.requireValidAPIForVueJSShowcaseApp = requireValidAPIForVueJSShowcaseApp