const Recaptcha = require('express-recaptcha').RecaptchaV2;  // Import the recaptcha package to verify the tokens
const RecaptchaKeys = require('../keys/getReCaptchaKeys'); // Import the functions to get the recaptcha keys

// Creation of the recaptcha object to check if the token is OK or not
const recaptcha = new Recaptcha(RecaptchaKeys.getSiteKey(), RecaptchaKeys.getSecretKey(), {callback:'cb'}); 


/**
 * Function used to verify the recaptcha token
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns if the recaptcha token is not good
 */
const requireValidRecaptcha = (req, res, next) => {
    recaptcha.verify(req, function (error, data) {
        if (!error) {
            next() // pass to the next function
        
        } else {
            return res.status(403).json(null) // Return a bad response => Null to fail the authentication on the client side
        }
    })
}


// Exports the function
exports.requireValidRecaptcha = requireValidRecaptcha