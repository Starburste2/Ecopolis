var mongoose = require('mongoose'); // Import to be able to validate the IDs or not


/**
 * Function to be able to check if the IDs passed in parameter in URL are ObjectId of Mongoose
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
const requireValidMongooseObjectID= (req, res, next) => {
    // If the id passed in parameter is a good Mongoose ObjectId
    if(mongoose.Types.ObjectId.isValid(req.params.id)) next();

    // Else return an error to the user
    else res.status(400).json({ message: "ID incorrect" });
}


// Exports the function
exports.requireValidMongooseObjectID = requireValidMongooseObjectID