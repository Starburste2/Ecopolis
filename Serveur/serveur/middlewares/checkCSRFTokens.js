const CSRF_Tokens = require('csrf'); // Import CSRF package to verify at first if the token passed is valid or not
const User = require("../models/User"); // Import the User model
const keys = require('../keys/getCSRFKey'); // Import the functions to get the keys for encryption


/**
 * Function used to validate the CSRF token sent by the client
 *  => Verification by the method of CSRF + Verficication in DB to know if the CSRF token and the session ID corresponds to someone
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns an eror if we have one ; nothing otherwise 
 */
const requireValidCSRFTokens = (req, res, next) => {
    var tokens = new CSRF_Tokens(); // Create the CSRF token object

    // Verify if the token can be valid via our tokens object
    if(!tokens.verify(keys.getCSRFKey(), req.headers.csrftoken)) return res.status(403).json({ message: 'Invalid token' })


    // Find a user with the corresponding session ID and CSRF token
    User.findOne({ sessionID: req.headers.authorization, csrfToken: req.headers.csrftoken })
    .then(user => {
        if(user == null) return res.status(403).json({ message: 'Invalid token' }) // No user found


        // We have found someone
        else {
            next(); // Go to the next function
        }
    })
    .catch(error => res.status(400).json(error))
}


// Exports the function
exports.requireValidCSRFTokens = requireValidCSRFTokens