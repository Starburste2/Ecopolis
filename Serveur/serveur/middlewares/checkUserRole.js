const User = require('../models/User'); // Import the User model


/**
 * Function to check the rights of the user on the given routes
 * This middleware shoudl not be used for all routes
 * It needs to be used on the sensititve URLs => Modification of the database
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
const requireAdminRole = (req, res, next) => {

    // No verification on the req.headers.authorization because we are already passed into checkSessionID
    // that checks the authorization given by the user is correct or not
    User.findOne({ sessionID: req.headers.authorization })
    .then(user => {
        // The user exists because checkSessionID has checked if he exists => Check the rights
        if(user.role != 'admin') return res.status(403).json({ message: "Not the rights" }); // Not the good rights
        
        // The user has the good rights => We can pass to the next function
        else {
            next(); // Continue to the next function
        }
    })
    .catch(error => res.status(400).json(error))
}

// Exports the function
exports.requireAdminRole = requireAdminRole