const express = require('express'); // Import express router module
const bodyParser = require('body-parser'); // Import body-parser
const mongoose = require('mongoose'); // Import mongoose for database
const fileUpload = require('express-fileupload'); // To be able to upload files
const User = require('./models/User'); // Import the User model to clean the old session ids
var expressWinston = require('express-winston'); // For Logging
var winston = require('winston'); // For transports.Console + Logging
const fs = require('fs'); // Import file system module
const helmet = require('helmet'); // Import the helmet package not show some headers
const nocache = require('nocache'); // Import this package to disable the cache on the client side
const cron = require('node-cron'); // Import the node-cron package to execute task every day at the hour given
const child_process = require('child_process'); // Import the child process package to be able to execute command in term
const Microcontroller = require('./models/Microcontroller'); // Import the Microcontroller model
const nodemailer = require('nodemailer'); // Import the nodemailer package to verify the identity of the user


//////////////////////////////////////////////// IMPORT ROUTES ////////////////////////////////////////////////
const routesMicrocontroller = require("./routes/Microcontroller"); // Import Microcontrollers routes
const routesDatas = require("./routes/Datas") // Import Datas routes
const routeDownload = require("./routes/Download"); // Import Download routes
const routeUpload = require("./routes/Upload"); // Import Upload routes
const routePoolAddresses = require("./routes/PoolAddresses"); // Import PoolAddress routes
const routeUser = require("./routes/User"); // Import User routes
const routeLogs = require("./routes/Logs"); // Import the Logs route
const routePosts = require("./routes/Post"); // Import the Posts route
const routeGroups = require("./routes/Group"); // Import the Groups route
//////////////////////////////////////////////// END IMPORT ROUTES ////////////////////////////////////////////////


// Connection to the mongodb database
mongoose.connect('mongodb://localhost/ecopolis',
  { 
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)
.then(() => console.log('Connexion à MongoDB réussie !'))
.catch(() => console.log('Connexion à MongoDB échouée !'));


const app = express(); // Create the express router
app.use(express.json()); // Uses JSON


// Method to allows for our API
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization, CSRFToken, apikey');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});


// Enable files upload +> Give access to req.files
app.use(fileUpload({
  createParentPath: true
}));


app.use(helmet()); // Use the helmet midlleware in the application
app.use(nocache()); // Disable cache on the client side

//////////////////////////////////////////////// CHECK SESSION IDS PART ////////////////////////////////////////////////
/**
 * Function to wait 10 minutes before calling reinitSessionIDs()
 */
function wait10min(){
  setTimeout(function(){
    reinitSessionIDs(wait10min);
  }, 600000);
}

/**
 * Function to update the session ids we have into the database (It updates also the CSRF tokens)
 * Validity time for a session id : 1 hour
 * Validity time for a CSRF token : 1 hour
 * @param {*} callback => Method to call
 */
function reinitSessionIDs(callback) {
  var today = new Date(); // Get the today's date
  today.setHours(today.getHours() - 1); // Set the today's hour minus 1 => 1 hour of validity for a session id/CSRF tokens

  // Delete the session id and the CSRF tokens if it has expired
  User.find({ dateSession: {$lt: today} }) // Date less than today
  .then(users => {

    // Reset all the users
    for (user of users) {
      user.sessionID = ""; // Set the session ID to nothing
      user.dateSession = null; // Set the dateSession to null
      user.csrfToken = ""; // Set the CSRF token to nothing
      user.dateCsrf = null; // Set the dataCSRF to null
      user.save(); // Save the state of the user
    }

    callback(); // Call the method passed in parameter
  })
}

reinitSessionIDs(wait10min); // Call the function to reinit the old session ids if it is necessary
//////////////////////////////////////////////// END CHECK SESSION IDS PART ////////////////////////////////////////////////


////////////////////////////////////////// CHECK TOKEN TO ACCESS TO FORM TO REINIT PASSWD PART //////////////////////////////////////////
/**
 * Function to wait 5 minutes before calling reinitTokenForgottenPasswordsToAccessToForm()
 */
 function wait5minToAccessToForm(){
  setTimeout(function(){
    reinitTokenForgottenPasswordsToAccessToForm(wait5minToAccessToForm);
  }, 300000);
}

/**
 * Function to update the forgotten passwords tokens to access to the form to update the password
 * Validity time for a forgotten password token to acces to the form is 30 minutes
 * @param {*} callback => Method to call
 */
function reinitTokenForgottenPasswordsToAccessToForm(callback) {
  var today = new Date(); // Get the today's date
  today.setMinutes(today.getMinutes() - 30); // Set the today's minutes minus 30 => 30 minutes of validity for a forgotten password token to access to form

  // Delete the forgotten password token to acces to the form and its date
  User.find({ dateTokenRouteForgottenPasswd: {$lt: today} }) // Date less than today
  .then(users => {

    // Reset all the users
    for (user of users) {
      user.tokenRouteForgottenPasswd = ""; // Set the token for password forgotten to nothing (Token to access to form)
      user.dateTokenRouteForgottenPasswd = null; // Set the dateTokenRouteForgottenPasswd to null
      user.save(); // Save the state of the user
    }

    callback(); // Call the method passed in parameter
  })
}

reinitTokenForgottenPasswordsToAccessToForm(wait5minToAccessToForm); // Call the function to access to the form to reinit the forgotten password tokens
////////////////////////////////////////// END CHECK TOKEN TO ACCESS TO FORM TO REINIT PASSWD PART //////////////////////////////////////////


//////////////////////////////////////////// CHECK TOKEN TO REINIT PASSWD PART ////////////////////////////////////////////
/**
 * Function to wait 5 minutes before calling reinitTokenForgottenPasswords()
 */
 function wait5min(){
  setTimeout(function(){
    reinitTokenForgottenPasswords(wait5min);
  }, 300000);
}

/**
 * Function to update the forgotten passwords tokens to update the password
 * Validity time for a forgotten password token is 30 minutes
 * @param {*} callback => Method to call
 */
function reinitTokenForgottenPasswords(callback) {
  var today = new Date(); // Get the today's date
  today.setMinutes(today.getMinutes() - 30); // Set the today's minutes minus 30 => 30 minutes of validity for a forgotten password token

  // Delete the forgotten password token and its date
  User.find({ dateTokenForgottenPasswd: {$lt: today} }) // Date less than today
  .then(users => {

    // Reset all the users
    for (user of users) {
      user.tokenForgottenPasswd = ""; // Set the token for password forgotten to nothing
      user.dateTokenForgottenPasswd = null; // Set the dateTokenForgottenPasswd to null
      user.save(); // Save the state of the user
    }

    callback(); // Call the method passed in parameter
  })
}

reinitTokenForgottenPasswords(wait5min); // Call the function to reinit the forgotten password tokens
//////////////////////////////////////////// END CHECK TOKEN TO REINIT PASSWD PART ////////////////////////////////////////////


//////////////////////////////////////////////// CHECK AUTHENTICATION TOKEN PART ////////////////////////////////////////////////
/**
 * Function to wait 1 hour before calling reinitAuthenticationTokens()
 */
 function wait1hour(){
  setTimeout(function(){
    reinitAuthenticationTokens(wait1hour);
  }, 3600000);
}

/**
 * Function to update the authentication tokens into the database
 * Validity time for an authentication token : 7 days
 * @param {*} callback => Method to call
 */
function reinitAuthenticationTokens(callback) {
  var today = new Date(); // Get the today's date
  today.setDate(today.getDate() - 7); // Set the today's date minus 7 => 7 days of validity for an authentication token

  // Delete the session id if it has expired
  User.find({ dateAuthentication: {$lt: today} }) // Date less than today
  .then(users => {

    // Reset all the users
    for (user of users) {
      user.authenticationToken = ""; // Set the authentication token to nothing
      user.dateAuthentication = null; // Set the dateAuthentication to null
      user.save(); // Save the state of the user
    }

    callback(); // Call the method passed in parameter
  })
}

reinitAuthenticationTokens(wait1hour); // Call the function to reinit the old authentication tokens if it is necessary
//////////////////////////////////////////////// END CHECK AUTHENTICATION TOKEN PART ////////////////////////////////////////////////


//////////////////////////////////////////////// CHECK LAST SENDING MICROCONTROLLER PART ////////////////////////////////////////////////
/**
 * Function to wait 6 hours before calling checkLastSending()
 */
 function wait6hours(){
  setTimeout(function(){
    checkLastSending(wait6hours);
  }, 21600000);
}


/**
 * Function used to be able to check the microcontrollers that have not sent their records since 1 day
 * And send an email to the admins to tell them that we have a problem on the microcontrollers
 * @param {*} callback => Method to call
 */
function checkLastSending(callback) {
  var sending_before = new Date(); // Get the date of today
  sending_before.setDate(sending_before.getDate() - 1); // Retrieve one day to the today date


  // Find the microcontroller that have not sent their records since 1 day
  Microcontroller.find({ lastSending: { $lt: sending_before }, isEndNode: true })
  .then(microcontrollers => {

    // If we have microcontrollers
    if(microcontrollers.length != 0) {
      var array_microcontrollers = []; // Array of microcontrollers addr
    
      // Push each addr in the array
      for (const microcontroller of microcontrollers){
        array_microcontrollers.push(microcontroller.addr)
      }


      // Find all the admin to send to them an email
      User.find({ role: 'admin' })
      .then(users => {
        var mail_list = []; // Array of email of users
        
        // Push each user email in the array
        for (const user of users){
          mail_list.push(user.email)
        }


        // Load the JSON of credentials to use to authentication
        var credentials = require('./config/email_credentials.json');
                      
        // Create the transporter to be able to send mail
        const transporter = nodemailer.createTransport({
            port: 465,
            host: credentials.smtp_address,
            auth: {
                user: credentials.email,
                pass: credentials.password
            },
            secure: true
        });

        // Mail template part
        var template_mail = fs.readFileSync('./controllers/Microcontroller/template/templateMailMicrocontrollers.html', 'utf8'); // Read the mail template
        template_mail = template_mail.split('MICROCONTROLLERS').join(array_microcontrollers.join(', ')); // Replace token by the good one in the URL

        // Create the mail
        const mail = {
          from: credentials.email,
          to: mail_list,
          subject: 'Problème concernant les microcontrôleurs',
          html: template_mail
        };


        // Send the email
        transporter.sendMail(mail, function (err, info) {
            if (err) console.log(err);
        });
      })
      .catch(error => console.log(error))
    }

    callback(); // Call the method passed in parameter
  })
  .catch(error => console.log(error))
}

checkLastSending(wait6hours); // Call the function to check if the microcontrollers have well sent their recorded data
//////////////////////////////////////////////// END CHECK LAST SENDING MICROCONTROLLER PART ////////////////////////////////////////////////


//////////////////////////////////////////////// LOGGING ////////////////////////////////////////////////
// Express Winston for the Logging
expressWinston.requestWhitelist.push('protocol');

// Creation of a specific format to print in files
const myFormat = winston.format.printf( ({ level, message, timestamp, ...metadata}) => {
  let msg = `${timestamp} ; ${message} ; `; // First part of the message

  // If we have metadata
  if(metadata) {
	  msg += JSON.stringify(metadata); // Add the metadata JSON to the msg
  }

  return msg
});

// Creation of the logger
const logger = winston.createLogger({
  level: 'debug',
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.splat(),
    winston.format.timestamp(),
    myFormat
  ),
  transports: [
	  new winston.transports.File({ filename: './logs/debug.log', level: 'debug' }), // Push debug in files
	  new winston.transports.File({ filename: './logs/error.log', level: 'error' }) // Push errors in files
  ],
});

const logger2 = expressWinston.logger({
  winstonInstance: logger,
  meta: true,
  msg: '{{req.protocol}} {{req.method}} {{req.url}}',
  expressFormat: false,
  colorize: false
})

app.use(logger2); // Use the expressWinston logger + custom logger

//////////////////////////////////////////////// END LOGGING ////////////////////////////////////////////////


//////////////////////////////////////////////// BACKUP ////////////////////////////////////////////////
// Task to execute all days at the hour indicated (minutes, hours, day of month, month, day of week)
let task = cron.schedule('55 15 * * *', () => {
  let backup_process = child_process.spawn('mongodump', ['--db=ecopolis', '--archive=./database_backups/', '--gzip']); // Create the command to execute

  // Execute this at the end of the command
  backup_process.on('exit', (code, signal) => {
    if (code) console.log('Backup process with exit code ' + code); // Quit with an error code
    
    else if (signal) console.log('Backup process was killed with signal ' + signal); // Quit with the signal of the killed process
    
    // All is good => Execute the functions to store the backups
    else {
      require('./controllers/Backup/GoogleDriveBackup').googleDriveSaving(); // Make a save of the encrypted database on Google Drive
      // require('./controllers/Backup/DropBoxBackup').dropboxSaving(); // Make a save of the encrypted database on Dropbox
    }
  })
});
//////////////////////////////////////////////// END BACKUP ////////////////////////////////////////////////


//////////////////////////////////////////////// ROUTES ////////////////////////////////////////////////
const ipfilter = require('express-ipfilter'); // Import the express ip filter to filter ips for the microcontroller and datas routes

// Use the microcontroller routes and add an IP denied error for not authorized people
app.use('/api/microcontroller', routesMicrocontroller, (err, req, res, _next) => {
  // If we have an IpDeniedError
  if (err instanceof ipfilter.IpDeniedError) {
    return res.status(403).json({ message: 'You are not authorized to make request on the server' })
  }
});

// Use the datas routes and add an IP denied error for not authorized people
app.use('/api/datas', routesDatas, (err, req, res, _next) => {
  
  // If we have an IpDeniedError
  if (err instanceof ipfilter.IpDeniedError) {
    return res.status(403).json({ message: 'You are not authorized to make request on the server' })
  }
})

app.use('/api/download', routeDownload) // Use the download routes
app.use('/api/uploadDatas', routeUpload) // Use the upload routes
app.use('/api/getAvailableAddresses', routePoolAddresses) // Use the pool addresses routes
app.use('/api/user', routeUser) // Use the user routes
app.use('/api/logs', routeLogs) // Use the logs routes
app.use('/api/posts', routePosts) // Use the posts routes
app.use('/api/groups', routeGroups) // Use the groups routes
//////////////////////////////////////////////// END ROUTES ////////////////////////////////////////////////


// Export the app variable to be used in serveur.js
module.exports = app;