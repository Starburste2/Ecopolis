const http = require('http'); // Import the HTTP module
const app = require('./app'); // Get the app variable by importing the file
const https = require('https'); // Import the HTTPS module
const fs = require('fs'); // Import the FileSystem module


// Define the port and the hostname that will be used
// var defined_port = 3000;
// var hostname_perso = 'localhost';
// var hostname_perso = '10.11.11.43';
// var hostname_perso = '192.168.0.18';
var defined_port;
var hostname_perso;


// Check the arguments
if(process.argv.length != 3 && process.argv.length != 4){
  
  if(process.argv.length < 3) console.log("Too few arguments :");
  else console.log("Too much arguments");
  
  console.log("   - 3 arguments for help");
  console.log("   - 4 arguments to launch the server");
  process.exit(-1);
}


// Check for the 3 arguments
if(process.argv.length == 3){
  
  // Check if we want to see the helper
  if(process.argv[2] == '-h' || process.argv[2] == '-H'){
    console.log("Usage : node serveur IP_address port_to_listen");
    process.exit(0);
  
  } else { // Error in the arguments
    console.log("Use 3 arguments to see the helper with : node serveur -h");
    process.exit(-1);
  }
}


// Check the enter ip address
if (
  /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(process.argv[2])
  || process.argv[2] == 'localhost'
) {

  if(process.argv[2] == 'localhost') hostname_perso = "127.0.0.1";
  else hostname_perso = process.argv[2];

} else {
  console.log("IP address invalid");
  process.exit(-1);
}


// Check the enter port
if(isNaN(process.argv[3]) || !Number.isInteger(parseFloat(process.argv[3]))){
  console.log("The port is an integer");
  process.exit(-1);
}
if(process.argv[3] < 0 || process.argv[3] > 65534){
  console.log("A port is between 0 and 65534 (Normally it is 65535 but we will create the HTTP server on the port 65534 and the HTTPS on 65535). WARNING : WE ADVISE YOU TO USE THE PORT > 1000 TO NOT OVERRIDE AN OPEN ONE");
  process.exit(-1);
}

defined_port = process.argv[3]; // Assign the value to port


// Get and set a valid port
const normalizePort = val => {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
};
const port = normalizePort(process.env.PORT || defined_port);
app.set('port', port);


// Function to check if we have errors or not
const errorHandler = error => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const address = server.address();
  const bind = typeof address === 'string' ? 'pipe ' + address : 'port: ' + port;
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges.');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use.');
      process.exit(1);
      break;
    default:
      throw error;
  }
};


// Read the key and certificate files
const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};


// Creation of the servers
const http_server = http.createServer(app); // HTTP
const https_server = https.createServer(options, app); // HTTPS


// Check errors for the HTTP server
http_server.on('error', errorHandler);
http_server.on('listening', () => {
  const address = http_server.address();
  const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + port;
  console.log('Listening on ' + bind);
});


// Listen on the given port for the HTTP server
http_server.listen(port, hostname_perso, function(){
  console.log("HTTP server running on http://" + hostname_perso + ":" + port);
});


// Check errors for the HTTPS server
https_server.on('error', errorHandler);
https_server.on('listening', () => {
  const address = https_server.address();
  const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + (parseInt(port)+1);
  console.log('Listening on ' + bind);
});


// Listen on the given port for the HTTPS server
https_server.listen(parseInt(port)+1, hostname_perso, function(){
  console.log("HTTPS server running on https://" + hostname_perso + ":" + (parseInt(port)+1));
});