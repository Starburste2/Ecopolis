# Ecopolis

## Little presentation
Ecopolis is a project that aims to study the evolution of the soil pollution of an old industrial wasteland.

<br/>

## Equipment used 
> - ESP32 TTGO T8 V1.7.1 for sender, receiver and relay equiped with :
>> - RA-02 SX1278 433 MHz LoRa module
>> - SD Card to store captured datas locally for end nodes and to store the server certificate for the main one
>> - JXBS-3001-TR Soil multi-parameter sensor for the end nodes

> - A web server with an API
>> - Using of NodeJS and Express for the routing
>> - MongoDB to store the data

> - A main VueJS client
>> - Manage the microcontrollers (add, modify, delete)
>> - Manage the datas (see via graphics or directly in text via pagination)
>> - Authentication of the user
>> - Upload of datas
>> - Download of microcontrollers code (end or main node) and datas captured

> - A showcase VueJS client
>> - Present the project to everyone
>> - Display the posts and the last activities

## Functionnalities
### ESP32 TTGO ([Arduino_LoRa](https://github.com/Starburste/Ecopolis/tree/main/Arduino_LoRa) and [RadioLib](https://github.com/Starburste/Ecopolis/tree/main/RadioLib))
The ESP32 TTGO end nodes will send datas, captured by sensors, to the main node using LoRa and they will save the data on the SD to be able to do an aggregation with the datas on the server later. The main node will send the message to the server using the HTTP protocol. A relay node is present to be able to receive and forward the data from the end node to main one and vice-versa.

### [Web server](https://github.com/Starburste/Ecopolis/tree/main/Serveur/serveur#server-part)
The web server will help you to store the datas sent by the microcontrollers but also all the datas concerning the microcontrollers, all the possible addresses and you can also generate the INO files for your microcontrollers. With that, you can directly upload the code on your microcontrollers and that's all. As the microcontrollers have a SD card on it, you will be able to make an aggregation, to be able to store all the datas on the database of the server. You can also login and create accounts for the VueJS client.

### [VueJS main client](https://github.com/Starburste/Ecopolis/tree/main/VueJS_Client#ecopolis)
The VueJS main client will help you to manage the microcontrollers you have created by adding them into the database, modify them or delete them (by deleting one microcontroller, you will delete all the associated data too). You will be able to see the datas stored into the database in graphics or via text using a pagination. You also will be able to download the code of the microcontroller you want and you can upload datas from CSV file into the database to be sure that all the datas are present inside. You can also download all the datas from a microcontroller too.

### [VueJS showcase client](https://github.com/Starburste/Ecopolis/tree/main/VueJS_showcase#ecopolis)
The VueJS showcase client will be used to present the project to everyone. This client will be more simpler than the main one because we will not have the authentication, the management of microcontrollers, etc …. We will present the project on the home and we will have some links to access to routes to view the posts of the administrators and see the last datas sent by the microcontrollers

<br/>

## Installation
### With the link
You can install the code for the application by downloading from this link : [https://github.com/Starburste/Ecopolis/archive/refs/heads/main.zip](https://github.com/Starburste/Ecopolis/archive/refs/heads/main.zip).

### With GitHub
You can install the application by using : 
```
cd ~/Desktop
git clone https://github.com/Starburste/Ecopolis.git Ecopolis
```

<br/>

## Initialization
### Server and applications Vue (showcase and main)
First, you need to initialize your project. To do so, open a terminal in the project root and then launch these commands:
```bash
cd Serveur/serveur/config/init_files

node createAddressPool.js
node generateSalt.js
node generateCSRFKey.js
node generateKeysForJWT.js
node generateAdminAccount.js
node createAPIKeys.js
node generateKeysForRecaptcha.js site_key secret_key
node generateIPRangeFile.js min_ip max_ip
node generateCredentialsEmail.js email password smtp_server
node generateDropboxTokenFile.js access_token
node generateIDFolderGGDrive.js id_folder_google_drive
node createServerBackupInformations.js user server

mkdir Serveur/serveur/templates_microcontrollers/receiver/temp
mkdir Serveur/serveur/templates_microcontrollers/sender/temp
mkdir Serveur/serveur/files_uploaded
mkdir Serveur/serveur/files_downloaded
mkdir Serveur/serveur/database_backups
```

By executing all these commands, your project will be able to work. We have described all the commands we have done to initialize the project below :
> - node createAddressPool.js <br/>
> This command will create the pool of available addresses you will use on your microcontrollers. <br/><br/>
> - node generateSalt.js <br/>
> This command is used to generate a salt value to be able to hash your passwords when a user will create an account or to compare two paswords during a login too. To generate the file, we will use a template where we just replace the mark by the good value and then, we will copy this file in the Serveur/serveur/keys/getSalt.js file. <br/><br/>
> - node generateCSRFKey.js <br/>
> This command will generate a key to be able to create our CSRF tokens. We will generate an UUID and we will encrypt it via a salt we have generated randomly. After that, we will use the template to generate the entire file with all the good values and we will copy it into Serveur/serveur/keys/getCSRFKey.js file. <br/><br/>
> - node createAPIKeys.js <br/>
> This command is used to generate all the API keys that can be used on the application. We will have one API key for the VueJS application and one for the Microcontrollers application. <br/><br/>
> - node generateKeysForRecaptcha.js site_key secret_key <br/>
> This command is used to generate all the files with the good keys for the proper functioning of the application. But, first, you need to generate your site and secret key to be able to make the application working properly. You can see more informations on how to create the keys [here](https://github.com/Starburste/Ecopolis/tree/main/VueJS_Client#recaptcha). Then, when you have created your keys, just replace site_key and secret_key with the keys given by Google in the command above. <br/><br/>
> - node generateKeysForJWT.js <br/>
> This command will generate the public and private keys for the JSON Web Tokens. These files are stored on the server and they are generated and copied in Serveur/serveur/keys/JWTPrivateKey.key for the private key and Serveur/serveur/keys/JWTPublicKey.key for the public key. <br/><br/>
> - node generateAdminAccount.js
> This command is used to create an account with the admin role if anyone has the admin role. If someone has the admin role, the program will quit. <br/><br/>
> - node generateIPRangeFile.js min_ip max_ip <br/>
> This command is used to generate the IP ranges file with the two parameters that are min_ip and max_ip. min_ip is the minimum available IP in the range and max_ip is the maximum available IP in the range. Some tests are done on the IPs to verify that they are in the good format, they are good, etc …. These tests are done on IPv4 so it will not work if you use IPv6. Make sure your ESP32's IP will be in the given range because if it is not, you will not be able to access to the routes of the ESP32. <br/><br/>
> - node generateCredentialsEmail.js email password smtp_server <br/>
> This command is used to generate a JSON to store the credentials and the smtp server address to be able to send emails to the user when the user will create an account or when he wants to reinitialize his password because he has forgotten it. <br/><br/>
> - node generateDropboxTokenFile.js access_token <br/>
> This command is used to generate the JSON file that will contain the access token to be able to upload datas on dropbox. <br/><br/>
> - node generateIDFolderGGDrive.js id_folder_google_drive <br/>
> This command is used to generate the JSON file that will contain the ID of the folder that will contain the uploaded datas on Google Drive. <br/><br/>
> - node createServerBackupInformations.js user server <br/>
> This command is used to generate the JSON file that will contain username and the address of the server to copy database backups on. <br/><br/>
> - The mkdir commands <br/>
> The mkdir commands will generate some folder to avoid the maximum of problems when we will use the application. By not creating these folders, we expose ourselves to some errors that can happen.

Before launching the server, make sure you have read and make the little commands given in the [server's README](https://github.com/Starburste/Ecopolis/tree/main/Serveur/serveur#server-part), the [VueJS main's one](https://github.com/Starburste/Ecopolis/tree/main/VueJS_Client#ecopolis) and the [VueJS showcase's one](https://github.com/Starburste/Ecopolis/tree/main/VueJS_showcase#écopolis-showcase-application).

Now, you can launch your NodeJS server via (from the root of the project) : 
```bash
cd Serveur/serveur
node serveur IP_ADDRESS PORT
```

You need also to overwrite the IP address and port in the microntrollers' templates code and in the VueJS application to make the application working. To do that : 
> - Change in VueJS_Client/src/config/getURLNode : 
>> - getURL() function with the good IP address of the server
>> - getPort() function with the good port of the server
> - Change in VueJS_showcase/src/config/getURLNode : 
>> - getURL() function with the good IP address of the server
>> - getPort() function with the good port of the server
> - Change the lines in Serveur/serveur/templates_microcontrollers/receiver/template_receiver.ino :
>> - #define URL … with the good IP address of the server
>> - #define PORT … with the good port of the server

You can also modify the IP address and port of your VueJS application. You can modify them into VueJS_Client/config/index.js.

When we have all modify the values, you can launch the main VueJS application by doing (in the root of the project) : 
```bash
cd VueJS_Client
npm run start
```

Your VueJS application will compile and then you will have a green signal that is saying that your application is running.

You can do the same for the VueJS showcase application by doing (in the root of the project) :
```bash
cd VueJS_showcase
npm run start
```

When you see the green signal, that's good.

<br/>

### Microcontrollers
After making all the requests we asked to initialize the Server and VueJS applications, we can initialize the microcontrollers. 

First of all, you need to create the main node of our global application. This is the node that will be in charge of communicating with the server. After the creation of the microcontroller, you need to download its code and upload it on an ESP32 ([See here for more informations](https://github.com/Starburste/Ecopolis/tree/main/Serveur/serveur#microcontrollers-file)). <br/> **WARNING : YOU CAN CREATE ONLY ONE MAIN NODE**

Finally, you can create all your network composed of end nodes to communicate with the main one. When you have created microcontrollers, you can download codes and upload them on multiple ESP32.

When you have put in place your network, you will be able to manage microcontrollers (add, update, delete, download its code) and to manage datas (see datas via graphics or pagination and upload some datas that the server don't have) via the VueJS application.

When you have done your network, you can put SD cards on your microcontrollers. The main node will read the certificate on the SD card and the end nodes will write their datas into a CSV file. To initialize a SD card, you need to format it. So to do this (on MacOS) : 
> - Open the application named "Utilitaires de disques"
> - Right click on your SD card you want to use and click on "Effacer"
> - As name, enter "SDCARD" (mandatory name for the good functioning) and select the format "MS-DOS (FAT 32)"
> - Finally click on "Effacer" to initalize your SD card
> - And when it's finished, you can click on "Terminer"

When you will want to put files on the SD card, be sure to put them at the root of the SD card (not in a folder of the SD card).