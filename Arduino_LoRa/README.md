# Main code microcontroller V0.7 with Arduino_LoRa

## Card and modules 
We are using ESP32 TTGO T8 V1.7.1 for all the nodes (main, end and relay nodes). An end node will be a a node that will capture all the datas (ph, temperature, humidity, conductivity, nitrogen, phosphorus and potassium of the soil) to send to the main node which will be in charge to communicate with the server via HTTPS. The relay will be used to be able to increase the range of communication (it will receive and forward the packet).

Here is the configuration of our nodes :
> - Main node <br/>
> This is the most simplest one. It is composed of an ESP32 TTGO T8 V1.7.1 with a LoRa RA-02 SX1278 433 MHz plugged in VSPI (pins 5 => NSS, 18 => SCK, 19 => MISO, 23 => MOSI and 4 => DIO0) and the SD card module integrated to the board to be able to read the server's certificate (more better to read it on the SD than in the memory because we can change it in the SD without problems) to send datas via the HTTPS protocol. <br/>
> ![alt text](./../README_images/configuration_main_node.png "Assembly of a main node") <br/><br/>
> - End node <br/>
> The end node is a little more complex. We are using an ESP32 TTGO T8 V1.7.1 with a LoRa RA-02 SX1278 433 MHz plugged in VSPI (pins 5 => NSS, 18 => SCK, 19 => MISO, 23 => MOSI and 4 => DIO0), a SD card module integrated to the board to be able to store, upload or see the datas the microcontroller has taken and a JXBS-3001-TR sensor which is a soil multi-parameter sensor which are using ModBus protocol and the pins 25 for the RX (read) and 26 for the TX (transmit). To be able to use this protocol, we are using a RS485 card. <br/>
> ![alt text](./../README_images/configuration_end_node.png "Assembly of an end node") <br/><br/>
> - Relay node <br/>
> It is composed of an ESP32 TTGO T8 V1.7.1 with a LoRa RA-02 SX1278 433 MHz plugged in VSPI (pins 5 => NSS, 18 => SCK, 19 => MISO, 23 => MOSI and 4 => DIO0). It will be in charge to receive and forward the data from the end node to the main one and vice-versa <br/>
> ![alt text](./../README_images/configuration_main_node.png "Assembly of a relay node")

<br/>

## Files
### sender.ino
sender.ino will contain the code which allows the microcontroller to capture all the datas which are ph, temperature, humidity, conductivity, nitrogen, phosphorus and potassium and send them to the main node via LoRa (all these datas concerns the soil). We will also store these datas in the SD card if it is available. The end nodes have the ability to reconfigure themselves in terms of wake-up times. ***WE STORE ALSO THE MESSAGE ID TO SEND TO THE SERVER IN A FILE NAMED idMsg.txt ON THE SD CARD. DON'T DELETE OR WRITE THE FILE BECAUSE THAT CAN CAUSE DAMAGE TO THE DATABASE WHEN YOU WILL MAKE AN UPLOAD OF THE DATAS ON THE SERVER.***

### receiver.ino
receiver.ino will contain the code which allows the microcontroller to exchange datas with the server, via HTTP, and then send the response of the server to the end nodes.

### https_receiver.ino
https_receiver.ino will contain the code which allows the microcontroller to exchange datas with the server, via HTTPS, and then send the response of the server to the end nodes. To be able to make requests in HTTPS, we need to have the certificate's value of the server to put it into the microcontroller's code (just if we cannot read the one from the SD card) and into the SD card to plug on the ESP32 (in the certificate.txt file). The certificate read from the SD card will prevail on the hard-stored certificate in the ESP32 memory.

### relay.ino
relay.ino will contain the code which allows the microcontroller to receive and forward the packet to the right microcontroller.

<br/>

## Librairies used
### [SPI](https://github.com/PaulStoffregen/SPI)
```arduino
#include <SPI.h>
```
SPI.h will help us to manage the SPI protocols and to give access to the different SPI interfaces like HSPI or VSPI.


### [LoRa](https://github.com/sandeepmistry/arduino-LoRa)
```arduino
#include <LoRa.h>
```
LoRa.h will help us to exchange data between end nodes and main node. It is very interesting to use LoRa to send datas because it is low power consuming and the messages can go through multiple kilometers in good conditions (good antenna parameters, open environment vs urban/forest environment, etc …).


### [WiFi, WifiClientSecure and HTTPClient](https://github.com/espressif/arduino-esp32)
```arduino
#include <WiFi.h>
#include <HTTPClient.h>
#include <WiFiClientSecure.h>
```
These librairies are used by the main node to exchange and send datas to the server. It can be used to send datas or update the state of the end nodes. HTTPClient.h is a native library given by Espressif for the ESP32 which can be used with the Arduino IDE. To add datas or update the state of a microcontroller, we need an API key which is given by the server when we download the code of the main microcontroller. The WiFiClientSecure is only used in the HTTPS code. This library helps us to put the certificate's value in the request to be able to make a secure transction (an HTTPS request) between the end node ESP32 and the server.


### [Arduino_JSON](https://github.com/arduino-libraries/Arduino_JSON)
```arduino
#include <Arduino_JSON.h>
```
Arduino_JSON.h allows us to transform a string into the JSON format. It is very usefull to send to the server or read the datas which are coming from the server.


### [SD_MMC and FS](https://github.com/espressif/arduino-esp32)
```arduino
#include <SD_MMC.h>
#include <FS.h>
```
These librairies are used to write datas on the SD card. FS will help us to append the datas in the file and SD_MMC will be used to open a connection with the SD card. SD_MMC is also use to read datas from the SD card to retrieve the server's certificate to be able to make HTTPS requests for example.


### [ModbusMaster](https://github.com/craftmetrics/esp32-modbusmaster)
```arduino
#include <ModbusMaster.h>
```
This library is used to be able to retrieve the datas from the JXBS-3001-TR sensor which uses the ModBus protocol. This sensor is connected to a RS485 card which is connected to the ESP32 as you can see on the assembly pictures.

<br/>

## Life cycle of the end nodes
![alt text](../README_images/life_cycle.png "Life cycle of the end node")

<br/>

## Reconfiguration
The reconfiguration of an end node is for the sleeping time (really, this represents the waking hours of a microcontroller). Indeed, these times can be modified by the user to retrieve less or more datas. We store them into an array which will be sent when the end nodes will need to be reconfigured. The end nodes will update their array of sleep to agree with the one on the server.

In fact (with the v0.3), if we can communicate with the server, we will use the sleep time he sends to us. With that, we can have a more precise sleep time than just get a static value we have stored on the microcontrollers. If we cannot communicate with the server, here, we will use the sleep times stored on the microcontrolller (which are the same on the server). When it is the first time that we launch an ESP32, the server request will contain a value to say to the server that it is the first time the microcontroller is up so we need to reconfigure it.

The sleep times can be like this :
> - In case of a manual reconfiguration :
> { "HCC": true, "ST": [ 500, 600, 800 ], "NST": 400 }<br/><br/>
> - In case of an automatic reconfiguration (generated by the server) :
> { "HCC": true, "V": 909, "NBV": 96, "NST": 498 } <br/><br/>
> With : HCC for hasConfigChanged (boolean) ; V for value (integer in seconds) ; NBV for number of values (integer) ; NST for next sleep time (integer in seconds) ; ST for sleep time (array of integers in seconds)

<br/>

## Possible codes
### Treatment codes 
The treatment codes are integers exchanged between the end and the main nodes. These codes are common to all the microcontrollers and are the following : 
> - 1
>>> The message contains all the datas to store into the database of the server (end to main node)
> - 2 
>>> No actions available so end node can sleep (main to end node)
> - 3
>>> Datas are well sent to the server and a reconfiguration is available (main to end node)
> - 4
>>> Reconfiguration of the end node is good, we can store it on the server (end to main node)


### Error codes
We have also error codes available which are :
> - 1 
>>> Transaction with the server is good
> - 2
>>> Problem during the transaction with the server


### SD codes (for end nodes to put datas in CSV)
We have two codes available when we want to write datas in the SD card : 
> - true
>>> The datas are well stored into the SD card
> - false
>>> Problem with the SD card (connection problem, problem when we want to write in the file, …)

### SD codes (for end nodes to get the actual message ID)
We have three possible code when we want to get the actual message ID to send to the server to identify the data saving :
> - -1
>>> We can't access to the SD card
> - 0
>>> Cannot read the idMsg file => Creation of a new one and put a 0
> - actual_msg_id
>>> Get the actual message ID to send to the server

### SD codes (for HTTPS main node)
We have two codes available when we want to read datas from the SD card :
> - -1
>>> The certificate's value cannot be read from the SD card => Use of the hard-stored value in the memory of the microcontrollers
> - certificate's value
>>> The certificate's value has been read and returned from the SD card

### Cycle identifier
An end node make a cycle when it sends datas to the main one. So, we have put in place counters of cycle that can identify in which cycle is the end node and we treat only the messages that are received for the current cycle. The others are not taken into account.

<br/>

## Packets
Packets which are sent between main and end nodes can have multiple forms and we add treatment codes to it. It is important to notify that the "packet headers" use bytes. We have four types of messages (you need to keep in mind that in the first wake up of the microcontroller a variable will be set to say to the server that it is the first the microcontroller woke up => This variable just appears in the datas message) : 
> - Datas message
>>> This is the message that contains the datas captured by the end nodes. The "packet header" is composed of the recipient and source address, the treatment ID, the length of the message and the cycle identifier of the end node. Finally, the body contains all the datas we need to store on the server (normally, we don't need to touch to the JSON sent).

> - Reconfiguration OK message
>>> This message means that the reconfiguration of the end node is good. Only the body of the message will change because, it now contains the address of the end node which is just reconfigured.

> - OK message
>>> This message means that all is OK (server has stored the datas and we don't have reconfiguration) and the end node can sleep now. The "packet header" will be nearly the same as 'Datas message' or 'Reconfiguration OK message' because, we just add the transaction state with the server. Here, the body of the message will the variable that says that we don't have a reconfiguration.

> - OK + Reconfiguration message
>>> This message means that the server has stored the datas and that a reconfiguration is available for the end node. The "packet header" will be the same as the 'OK message'. The body will contain the datas sent by the server which will allow the end node to reconfigure itself and a variable that says that we have a reconfiguration.

<br/>

## LoRa configuration
The LoRa configuration is the following:
> - Bandwidth 125 KHz
> - Spreading Factor 10
> - Coding Rate 5

This configuration can achieve ~200m by the tests we have done. The objective is to increase the range without sacrifice the time in the air (sending/receiving).

<br/>

## Execution time
The execution time will be calculated on the end nodes. The execution time is calculated from the boot of the board to the reception of the 'OK message'.
The execution time can be very variable according to the transaction with the server, the time to send the messages, if we have a relay in the system, etc ….
For example, if we get the acknowledgment message on the first try, the execution time will be very fast. However, if we don't receive the acknowledgment message in time, we re-send a message every 3-5 seconds (to avoid as much as possible the spam of the main node). 

| Case  | Execution time (Restart to the sleep of the board) => High range |
| :---------------: |:---------------:|
| **Send data with reconfiguration with a relay** | Cycle of ~7-8 seconds |
| **Send data without reconfiguration with a relay** | Cycle of ~5 seconds |
| **Send data with reconfiguration without a relay** | Cycle of ~5 seconds |
| **Send data without reconfiguration without a relay** | Cycle of ~2 seconds  |
| **Maximum time the end node can be active before sleeping** | Between 25-30 seconds (trying to send 5 messages) |

<br/>

## Power consumption
During the visualization of the power consumption on an oscilloscope, we can say that the waveform of the power consumption is always the same (See the picture below).

![alt text](../README_images/global_waveform_cycle_RadioLib.png "Global waveform of the power consumption of an end node")

In fact, for now, we can send up to 5 messages without response of the server. So, if we get the response at the first try, we will just have one peak of consumption and one waiting time for the message (we are not obliged to wait all the time, when the packet that says that all is OK is received, we can continue our treatment). If we have the SD card, we will always have the peak associated to it. If we don't have a SD card, we will not have this peak. ***We can simplify this diagram but if we do that, we will miss informations because we need to know that the triangles are isoceles so to calculate their area, we need the base (the time to send a message => length of the purple rectangles) multiplied by the height (we can get easily).***

With that, we can deduce a complete formula :
<br/>
<br/>

![alt text](../README_images/consumption_equation.png "Equation of the power consumption of an end node for one cycle")

<br/>

But we can express a more simpler one : 
<br/>
<br/>

![alt text](../README_images/consumption_simpler_equation.png "Equation of the power consumption of an end node for one cycle")

<br/>

So the total consumption of the ESP32 during one cycle will be the area of all our waveform. But, thanks to the equation, we can estimate the power consumption for the future cycles. We also need to take into account the power consumption of the board when it wakes up from the deep sleep.

To estimate the battery life, we need to know globally the power consumption in multiple cases. These cases are given below :
> - Deep sleep mode
> - Main cycle without reconfiguration and server problems
> - Main cycle with reconfiguration and without server problems
> - Main cycle with server connection problems

<br/>

***THESE RESULTS ARE GIVEN BY THE OSCILLOSCOPE AND BY A VOLTMETER TO BE SURE***

<br/>

| / | AVERAGE CONSUMPTION |
| :---------------: |:---------------:|
| **Deep sleep mode (all the system without the sensor)** | 60 uA |
| **Deep sleep mode (all the system)** | 10.5-11.5 mA |
| **ESP32 in process of acquisition of the data** | 62.3 mA |
| **ESP32 in waiting** | 85 mA |
| **ESP32 by sending data with LoRa** | 100-110 mA |

<br/>

These results are generally reliable because they are calculated directly from the waveform given by the oscilloscope or by the result given by the voltmeter. These results are the average of the consumption, per second, during the cycle of the microcontroller. So the consumption may vary according to the time passed to try to send messages, the peak of power consumption to add datas to the SD card, if we send data to a relay, etc …. After multiple tests, we can assume that the board will consume 85 mA as average consumption during a cycle (with and without reconfiguration). ***So here, the time will be a key for the global consumption of the board (more the board is powered more will be the power consumption).*** 
> - Let's take an example : If a board has a power consumption of 70 mA for one cycle but it is powered 1 second and the other board has a power consumption of 60 mA but on two seconds for one cycle. The capacity of the battery is also a 3000 mAh. The equation to calculate the number of cycles will be : 
![alt text](../README_images/cycles_equation.png "Equation to calculate the possible number of cycles on a battery")
>> - 70 mA => bat_capacity / power_consumption * hTOs / time => 3000 / 70 * 3600 / 1 = 154285 cycles <br/>
>> - 60 mA => bat_capacity / power_consumption * hTOs / time => 3000 / 60 * 3600 / 2 = 90000 cycles <br/>
> The example shows you that it will be better to send quickly the datas and consume a little more power than send them slowly but consumes less power.

THESE RESULTS DON'T INCLUDE THE BOOT OR THE WAKE UP OF THE BOARD.

<br/>

| / | ESTIMATED LIFE TIME (Always do the / item on ***12000 mAh battery***) |
| :---------------: |:---------------:|
| **Deep sleep mode (all the system without the sensor)** | 200 000 hours => 8333 days => 22.8 years |
| **Deep sleep mode (all the system)** | 1090.9 hours => 45.4 days |
| **ESP32 in process of acquisition of the data** | 193 hours => 8 days |
| **ESP32 in waiting** | 141 hours => 5.8 days |
| **ESP32 by sending data with LoRa** | 11.4 hours |

<br/>

These results are calculated on ***12000 mAh battery***. And like it is a battery that we can use on the final version, the result is quite reliable. ***NOTE THAT WE CAN INCREASE THE BATTERY LIFE BY USING A LITTLE RELAY TO USE IN DEEP SLEEP MODE TO REDUCE THE POWER CONSUMPTION BY CUTTING THE ENERGY CONSUMPTION OF THE SENSOR.***

<br/>

### Deep Sleep wake up
![alt text](../README_images/deep_sleep_waveform_wakeUp.png "Waveform of a wakeup for an ESP32")

A wake up from a deep sleep mode can take around 1 second. We can see that we have one rectangle and one power consumption peak. Generally, it takes less than 0.5 second to be on and to be ready to do tasks. We have multiple parts :
> - A rectangle which is the base consumption during the set up of the variables and the modules
> - A peak which corresponds to the booting of all the elements on the ESP32

With the oscilloscope, we can deduce that the part after the peak corresponds to the initialization of the LoRa RA-02 SX1278 and of the BME280 modules. The first part with the peak corresponds to the normal initialization of the ESP32. We can deduce that by deleting the initialization of a module at each step.

| TIME TO WAKE UP | AVERAGE POWER CONSUMPTION DURING A WAKE UP |
| :-------------: | :----------------------------------------: |
| 0.3 second | ≈ 43.5 mA |
| 0.4 second | ≈ 42.625 mA |

<br/>

## My observations
This code developed from Arduino_LoRa is more simpler and more faster to read and write. Indeed, when we want to send a message, we put all the main elements in the "packet header" like the recipient and sender addresses, the ID of the message, the size of the message, the payload (transaction state with the server) for the main node and the cycle identifier of an end node. Then, we put in the body the datas to the server. This structure gives us a structured message and the reading of the message on the main or end nodes is better because we can get just the datas we want (no need to transform a string to a JSON to know to whom the message is, whom has sent this message, etc …). In fact, we will just transform the message into JSON when we are sure that the message is for us and that the message is correct (good size, server transaction good or not ?, etc …). This allows us to not execute instructions that we don't need to execute to save battery life for example. We can see that, for now, the power consumption is quite good because the number of cycles are large and the deep sleep mode gives us more than 1 year ***with a little battery of 3000 mAh***, which is litterally the battery capacity of a smartphone. The only little problem that we have faced is that on the main node, the LoRa module puts itself in idle mode so we re-initialize this module every 5 minutes.