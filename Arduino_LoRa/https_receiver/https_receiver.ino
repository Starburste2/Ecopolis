/*
  https_receiver.ino file using Arduino_LoRa library

  Containing all the code to receive datas picked up by the sensor and send them to the server via HTTPS requests
  Send data via LoRa to the end nodes (reconfiguration, ok messages)
  This microcontroller always listen for LoRa packet and LoRa module is restarted all the 5 minutes to avoid automatic idle

  Can send : ok and ok_with_configuration
  Can receive : datas and ok_for_reconfiguration
  Can send HTTPS POST and PUT requests and receive the response from the server

  LoRa codes :
    - DATAS => 1 (To identify a message with datas inside)
    - OK => 2 (The main node and the server has well received the data + No reconfiguration)
    - OK_AND_CONF => 3 (The main node and the server has well received the data + With reconfiguration)
    - RECONF_OK => 4 (Reconfiguration on the end node is good => Tell the server that he can update its database)

  Payload codes :
    - 1 => The transaction with the server went well
    - 2 => The transaction with the server went wrong (cannot reach the server)

  Informations are sent by LoRa by using two elements:
    - "packet header" => sender address, recipient address, id of the message (ok or ok_with_reconf), message length, cycle identifier and if the payload is ok or not (server transaction)
    - body => message itself in JSON format to retrieve the datas

  Infomations are received by LoRa using two elements:
    - "packet header" => sender address, recipient address, id of the message (sending datas or ok_reconf), message length and cycle identifier
    - body => message itself formatted into the JSON format for the server

  SD codes :
    - -1 => Code got when we cannot read the certificate present on the SD card
    - certificate => Value of the certificate when we have well read the certificate present on the SD card

  @version : 0.7
*/

// Librairies included
#include <SPI.h>
#include <LoRa.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <WiFiClientSecure.h>
#include "SD_MMC.h"


// Define the pins used by the transceiver module
#define NSS 5
#define RST -1 // If used => 14 ; If not used => -1
#define DIO0 4


// Define the messages ID for the acknowledgment messages
#define DATAS 1
#define OK 2
#define OK_AND_CONF 3
#define RECONFIG_OK 4


// WiFi identification
// #define SSID "AND"
// #define PASSWORD "@nd1sH3re" // Si pas H alors h

#define SSID "SFR-e068"
#define PASSWORD "DXGZZD3KIM1J"


// Server identification part (URL, PORT, complete addr)
#define URL "192.168.0.18"
#define PORT 3001
#define SERVER_ADDR "https://" + String(URL) + ":" + String(PORT)
#define ROUTE_MICROCONTROLLER "/api/microcontroller"
#define ROUTE_DATAS "/api/datas"
#define APIKEY "5D0BENX-E6BMWJ2-Q4A93T3-B16KW47"


// Definition of paths for the microSD card
#define PATH_TO_FILE "/certificate.txt" // Path to the certificate
#define SDCARD "/sdcard" // Path to go on the sd card


// Declaration of the main certificate if we cannot read the SD card
const char* main_certificate = \
                               "-----BEGIN CERTIFICATE-----\n" \
                               "MIICljCCAX4CCQCQC92mluy4OTANBgkqhkiG9w0BAQUFADANMQswCQYDVQQGEwJm\n" \
                               "cjAeFw0yMTA0MTYxMjE1MDBaFw00ODA4MzExMjE1MDBaMA0xCzAJBgNVBAYTAmZy\n" \
                               "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3NsAtNfCZzBCGKxvhXMn\n" \
                               "gvveL0C2kc+Q+htYUaAdufWudVk4qZ8wTn2tPHOCcg8RcN8z6TLs/CBNRGgAs3rN\n" \
                               "1won+c2rCzWb1yXIhwwHyjzx1pR/NzYZC/GumaiSPwOJbUIP8V4lhobOcN7EOa4x\n" \
                               "07OMeu+OLPwD3AnYuo8pHqfQZJ6djLNWDlnabjP2UTaiqCKcQK9XDV1cou3xe2lJ\n" \
                               "PZPMHXpCXOXphdrdaWyQ62ZOTuDBH3QYPHXMPZ7voJDdwBcfV62sl2XcW6FagJhl\n" \
                               "5koejdp2aSdWpbHRHi0iNns76oEn/81r32aYt57ZOKAjwa4mUQYjn2KcF4EHGJ83\n" \
                               "VwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQAjDVIu8n9ZoL0sh9pTNLFN753kIyIZ\n" \
                               "NavbaJGEBRHcvZhkhVLfcM2zQM74S4ruGT/cii/yrZm39sFcbTJwTOVPh2mPQl82\n" \
                               "SCwdS6XquAm4NaeYRpyeZ21NJz3qBmqBq+PD3LQXffhlsqX5Oh7d78jTqvGz986f\n" \
                               "euEjNY9fFnHQlL+hf1bWFJWlBlSFnuCcdUurt9ssl5oh4UOoTTMzsGFQe8BGJClK\n" \
                               "Gazyqjkrymaw2yDcO0rd5+Ask8hqJXV8P99VwLiIP3FKuNkaY1E81V6sCMfxxsRZ\n" \
                               "t9B/2EH7MN0ACHOMfJCI0ZVgOK/7Co6hec0rBA9+la+9p0F/gk308ceE\n" \
                               "-----END CERTIFICATE-----\n";


// Adress
const byte local_address = 0xF3; // Device


// Restart LoRa ans WiFi libraries => To avoid freeze problems (precisely automatic idle problems)
long last_update_time = 0; // Last time we restart the LoRa module
int interval = 300000; // Interval to restart the LoRa module (5 minutes)
bool is_in_conversation = false; // To know if we are in conversation or not


/**
   Set up the variables and the modules
*/
void setup() {
  Serial.begin(115200); // Serial initialization
  while (!Serial);


  // Set the pins to use for chip select, reset and irq
  LoRa.setPins(NSS, RST, DIO0);


  // Antenna used => 433 MHz => 433E6
  if (!LoRa.begin(433E6)) {
    Serial.println("LoRa init failed. Check your connections.");
    while (true); // If problems => Do nothing
  }

  LoRa.setSpreadingFactor(10); // Set the spreading factor to 10
  Serial.println("LoRa init succeeded.");


  // Connection to the WiFi with the credentials put above
  WiFi.begin(SSID, PASSWORD);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }


  // Connection Ok and get local ip address
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}


/**
   Main program
*/
void loop() {
  // Parse for a packet, and call onReceive when we have receive a packet
  onReceive(LoRa.parsePacket());


  // Restart the LoRa and WiFi module to avoid automatic idle mode
  if (millis() - last_update_time > interval && is_in_conversation != true) {
    Serial.println();

    
    // LoRa connection reset
    Serial.println("Restart the LoRa module");
    LoRa.end(); // Stop the module
    if (!LoRa.begin(433E6)) { // Restart the module with the good antenna frequency
      Serial.println("LoRa init failed. Check your connections.");
      while (true); // If problems => Do nothing
    }
    LoRa.setSpreadingFactor(10); // Set the spreading factor to 10
    Serial.println("LoRa module restart is ok");
    Serial.println();


    // WiFi connection reset
    Serial.println("Restart the WiFi");
    WiFi.disconnect(); // Disconnect the WiFi
    WiFi.begin(SSID, PASSWORD); // Re-connection to the WiFi
    Serial.println("Re-connection to the WiFi");
    while (WiFi.status() != WL_CONNECTED) { // Verify the status
      delay(500);
      Serial.print(".");
    }
    Serial.println("The WiFi has well restarted");
    Serial.println();

    last_update_time = millis(); // Reupdate the time
  }
}


/**
   Send a message to the sender
   @param message_to_send => Message to send to the sender (contains nothing if it's OK ; otherwise it contains the reconfiguration in seconds)
   @param sender => Destination address
   @param recipient => Sender address
   @param id_message => Message ID to send
   @param cycle_identifier => Identifier of the cycle of the end node
*/
void sendMessage(String message_to_send, byte sender, byte recipient, int id_message, byte cycle_identifier) {
  LoRa.beginPacket(); // Create the packet to send
  LoRa.write(sender);  // Add destination address
  LoRa.write(recipient); // Add sender address
  LoRa.write(id_message); // Add message ID
  LoRa.write(message_to_send.length()); // Add the length of the message
  LoRa.write(cycle_identifier); // Add the cycle_identifier of the end node


  if (message_to_send == "-1") { // Problems with server
    LoRa.write(2); // is_payload_ok fixed to 2 because there are problems with the server (-1 doesn't work because we can juste send bytes with LoRa.write)

  } else { // No problem with the server
    LoRa.write(1); // is_payload_ok fixed to 1
  }


  LoRa.print(message_to_send); // Add the message to send
  LoRa.endPacket(); // End and send the packet
}


/**
   Function called when we receive a packet
   And we analyse the message id to know what we need to do
   @param packet_size => To know if a packet is arrived or not (0 if no packet)
*/
void onReceive(int packet_size) {
  if (packet_size == 0) return; // If there's no packet, quit the function


  // Read packet header bytes
  int recipient = LoRa.read(); // Recipient address
  byte sender = LoRa.read(); // Sender address
  byte incoming_msg_id = LoRa.read(); // Message id
  byte incoming_length = LoRa.read(); // Message length
  byte cycle_identifier = LoRa.read(); // Cycle_identifer of end node


  // Read the message
  String incoming = "";
  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }


  // If the recipient isn't this device or broadcast
  if (recipient != local_address && recipient != 0xF3) {
    Serial.println("This message is not for me.");
    return; // Quit the function
  }


  // Check the length if there is errors or not
  if (incoming_length != incoming.length()) {
    Serial.println("error: message length does not match length");
    return; // Quit the function if there are errors
  }


  // Get RSSI, SNR and Frequency error
  int rssi = LoRa.packetRssi();
  float snr = LoRa.packetSnr();
  long frequency_error = LoRa.packetFrequencyError();


  // If message is for this device, print details of the message
  Serial.println("Received from: 0x" + String(sender, HEX));
  Serial.println("Sent to: 0x" + String(recipient, HEX));
  Serial.println("Message ID: " + String(incoming_msg_id));
  Serial.println("Message length: " + String(incoming_length));
  Serial.println("Message: " + incoming);
  Serial.println("RSSI: " + String(rssi));
  Serial.println("Snr: " + String(snr));
  Serial.println("Freq err: " + String(frequency_error));
  Serial.println();


  is_in_conversation = true; // The microcontroller can receive messages => Don't restart the LoRa mmodule for now

  String payload; // Response of the server (in string format)
  JSONVar response_infos; // Response of the server formated in JSON
  String certificate = readCertificate(); // Read the certificate from the SD card


  // Identify the message sent
  switch (incoming_msg_id) {
    case DATAS:
      Serial.println("SEND DATA TO THE SERVER");


      // If we cannot read the certificate => Use the main certificate ; use the reading one otherwise
      if (certificate.equals("-1")) {
        payload = httpsRequest(SERVER_ADDR, ROUTE_DATAS, incoming, "POST", main_certificate); // Request to send datas to the server via the certificate stored as a variable because we cannot read SD card

      } else {
        payload = httpsRequest(SERVER_ADDR, ROUTE_DATAS, incoming, "POST", certificate); // Request to send datas to the server via the certificate present on the SD card
      }

      Serial.println(payload);

      response_infos = JSON.parse(payload); // Parse the payload to know if we have a reconfiguration or not


      // Test the parsing
      if (JSON.typeof(response_infos) == "undefined") {
        Serial.println("Problem during the parsing of the payload sent by the server"); // Problem

      } else {

        // No reconfiguration => Send OK message to the sender
        if ((bool)response_infos["HCC"] == false) {
          Serial.println("Send OK message");
          sendMessage(payload, sender, recipient, OK, cycle_identifier); // Send an acknowledgment message to the sender
          is_in_conversation = false; // The microcontroller is not in a conversation => Possibility to restart the LoRa module


          // Reconfiguration
        } else {
          Serial.println("Send a reconfiguration message");
          sendMessage(payload, sender, recipient, OK_AND_CONF, cycle_identifier); // Send the reconfiguration message
        }
      }

      break;


    case RECONFIG_OK:
      Serial.println("Send server reconf is OK");

      // If we cannot read the certificate => Use the main certificate ; use the reading one otherwise
      if (certificate.equals("-1")) {
        payload = httpsRequest(SERVER_ADDR, ROUTE_MICROCONTROLLER, incoming, "PUT", main_certificate); // Request to say that the microcontroller has well received his configuration via the main certificate => Cannot read SD card

      } else {
        payload = httpsRequest(SERVER_ADDR, ROUTE_MICROCONTROLLER, incoming, "PUT", certificate); // Request to say that the microcontroller has well received his configuration via the readed certificate
      }

      Serial.println(payload);


      sendMessage(payload, sender, recipient, OK, cycle_identifier); // Send an OK message to the sender
      is_in_conversation = false; // The microcontroller is not in a conversation => Possibility to restart the LoRa module

      break;
  }
}


/**
   Function used to retrieve the certificate from the SD card
   @return the certificate if we can read it ; -1 otherwise
*/
String readCertificate() {
  // SD card initialization
  if (!SD_MMC.begin(SDCARD, true)) { // Uses the pins number : 15, 14 and 2
    Serial.println("Cannot connect to the SD card");
    return "-1";
  }


  // Open the file to read
  File certificate = SD_MMC.open(PATH_TO_FILE, FILE_READ);


  // Check if we have well opened the file
  if (!certificate) {
    Serial.println("Opening file to read failed");
    return "-1";
  }


  String certificate_to_return = ""; // Value of the certificate read from the SD card


  // Read the certificate char by char
  while (certificate.available()) {
    char value_readed = certificate.read(); // Read char by char because ascii code is equals to value when we use char
    certificate_to_return += String(value_readed); // Add char to the certificate
  }


  certificate.close(); // Close the file to read
  SD_MMC.end(); // Close the library


  return certificate_to_return;
}


/**
  Function used to send an HTTPS request to the server
  @param server => Address of the server (Example : https://localhost:12345)
  @param route => Route to have access to the API (/api/datas for example)
  @param json => JSON to send to the server
  @param type_of_request => Request type to do (GET, POST, PUT, …)
  @param certificate => Certificate of the server to communicate with
  @return json send by the server if all is good ; -1 otherwise
*/
String httpsRequest(String server, String route, String json, String type_of_request, String certificate) {
  // Verify the WiFi connection to be able to well transmit the datas
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.disconnect(); // Disconnect the WiFi
    WiFi.begin(SSID, PASSWORD); // Reconnect the WiFi
  }

  WiFiClientSecure *client = new WiFiClientSecure; // Client for the secure transaction

  // If the creation of the client is good
  if (client) {
    client -> setCACert(certificate.c_str()); // Set the certificate

    { // Add a scoping block for HTTPClient https to make sure it is destroyed before WiFiClientSecure *client is


      HTTPClient https; // HTTPS client
      https.begin(server + route); // Begin the HTTPS request with the address of the server given
      https.addHeader("Content-Type", "application/json"); // Allow to send json
      https.addHeader("apikey", APIKEY);


      int https_response_code; // Response code we will have when we will make the request


      // According to the type of request passed in parameter
      if (type_of_request.equals("GET")) { // GET method
        https_response_code = https.GET();

      } else if (type_of_request.equals("POST")) { // POST method
        https_response_code = https.POST(json);

      } else if (type_of_request.equals("PUT")) { // PUT method
        https_response_code = https.PUT(json);

      } else { // Others
        https_response_code = -1;
      }

      if (https_response_code > 0 && (https_response_code == 201 || https_response_code == 200)) { // no error =>
        Serial.print("HTTPS response code : ");
        Serial.println(https_response_code);

        return https.getString();

      } else { // Print error code
        Serial.print("Error code : ");
        Serial.println(https_response_code);
        Serial.println(https.getString());

        return "-1";
      }


      // Free the resources
      https.end();


    } // End extra scoping block


    delete client; // Destroy the client

  } else {
    Serial.println("Unable to create client");
    return "-1";
  }
}
